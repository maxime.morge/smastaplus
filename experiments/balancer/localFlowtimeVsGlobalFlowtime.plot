set terminal svg #pdfcairo
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
#set xrange [2:12]
set auto y
set grid
set key autotitle columnhead
unset key
set key font ",20"
set key left above
set ticslevel 0
set style data lines
set xlabel "Number of nodes" font ",16"
set ylabel "Mean flowtime"  font ",16"
set output 'figures/localFlowtimeVsGlobalFlowtime/meanFlowtime.svg'
plot  "data/localFlowtimeVsGlobalFlowtime.csv" using 1:16:18 with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:17 with lines dt 3 lc "dark-green" title 'Local flowtime and makespan',\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:46:48 with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:47 with lines dt 7 lc "dark-blue" title 'Global flowtime',\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:76:78 with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:77 with lines dt 5 lc "dark-red" title 'Hill climbing'
      #"data/localFlowtimeVsGlobalFlowtime.csv" using 1:($7):($9) with filledcurves fc "light-grey" fs transparent solid 0.3 border lc "grey" notitle,\
      #"data/localFlowtimeVsGlobalFlowtime.csv" using 1:($8) with lines dt 1 lc "dark-grey" title 'Initial allocation',\
unset ylabel
set ylabel "Rescheduling time (s)" font ",16"
set xlabel "Number of nodes" font ",16"
set yrange [*:*]
set output 'figures/localFlowtimeVsGlobalFlowtime/schedulingTime.svg'
plot  "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($19*1E-9):($21*1E-9) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($20*1E-9) with lines dt 3 lc "dark-green" title 'Local flowtime and makespan',\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($49*1E-9):($51*1E-9) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($50*1E-9) with lines dt 7 lc "dark-blue" title 'Global flowtime',\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($79*1E-9):($81*1E-9) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($80*1E-9) with lines dt 5 lc "dark-red" title 'Hill climbing'
unset ylabel
set key font ",12"
set ylabel "Local availability ratio (%)"  font ",16"
set yrange [0:100]
set xlabel "Number of nodes" font ",16"
set output 'figures/localFlowtimeVsGlobalFlowtime/localityRatio.svg'
plot  "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($4*100):($6*100) with filledcurves fc "light-grey" fs transparent solid 0.3 border lc "grey" notitle,\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($5*100) with lines dt 1 lc "dark-grey" title 'Initial allocation',\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($22*100):($24*100) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
     "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($23*100) with lines dt 3 lc "dark-green" title 'Local  flowtime and makespan',\
     "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($52*100):($54*100) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
     "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($53*100) with lines dt 7 lc "dark-blue" title 'Global flowtime',\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($82*100):($84*100) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/localFlowtimeVsGlobalFlowtime.csv" using 1:($83*100) with lines dt 5 lc "dark-red" title 'Hill climbing'

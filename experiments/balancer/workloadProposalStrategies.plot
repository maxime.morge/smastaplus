set terminal svg #pdfcairo
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
# line styles
set style line 8 lc rgb '#FFF5F0' # very light red
set style line 7 lc rgb '#FEE0D2' #
set style line 6 lc rgb '#FCBBA1' #
set style line 5 lc rgb '#FC9272' # light red
set style line 4 lc rgb '#FB6A4A' #
set style line 3 lc rgb '#EF3B2C' # medium red
set style line 2 lc rgb '#CB181D' #
set style line 1 lc rgb '#99000D' # dark red
set style line 9 lc rgb '#FFF5EB' # very light orange
set style line 10 lc rgb '#FEE6CE' #
set style line 11 lc rgb '#FDD0A2' #
set style line 12 lc rgb '#FDAE6B' # light orange
set style line 13 lc rgb '#FD8D3C' #
set style line 14 lc rgb '#F16913' # medium orange
set style line 15 lc rgb '#D94801' #
set style line 16 lc rgb '#8C2D04' # dark orange
set auto y
set key autotitle columnhead
unset key
set key font ",20"
set key left above
set ticslevel 0
set style data lines
set xlabel "Time (s)" font ",16"
set xrange [0:2]
#set ylabel "Workloads"  font ",16"
#set ytics nomirror
#set yrange [0:4000]
#set yrange [200:400]
#set ytics nomirror tc "dark-blue"
set ytics nomirror tc "black"
#set ylabel "Mean flowtime" font ",16" tc "dark-blue"
set ylabel "Mean flowtime" font ",16" tc "black"
set output 'figures/singleVsMultiDelegationFlowtime/workloadSingleProposalByTime.svg'
plot     "data/globalFlowtimeSingleProposalStrategyNoneCounterProposalStrategylog.csv" using ($1*1E-9):($2) with lines dt 1 lc "black" notitle
#title 'Flowtime'
#axes x1y2
#"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($4+$5+$6+$7) with lines ls 1 title 'n01' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($8+$9+$10+$11) with lines ls 2 title 'n02' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($12+$13+$14+$15) with lines ls 3 title 'n03' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($16+$17+$18+$19) with lines ls 4 title 'n04' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($20+$21+$22+$23) with lines ls 5 title 'n05' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($24+$25+$26+$27) with lines ls 6 title 'n06' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($28+$29+$30+$31) with lines ls 7 title 'n07' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($32+$33+$34+$35) with lines ls 9 title 'n08' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($36+$37+$38+$39) with lines ls 10 title 'n09' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($40+$41+$42+$43) with lines ls 11 title 'n10' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($44+$45+$46+$47) with lines ls 12 title 'n11' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($48+$49+$50+$51) with lines ls 13 title 'n12' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($52+$53+$54+$55) with lines ls 14 title 'n13' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($56+$57+$58+$59) with lines ls 15 title 'n14' axes x1y1,\
  #"data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($60+$61+$62+$63) with lines ls 16 title 'n15' axes x1y1,\
  # "data/globalFlowtimeSingleProposalStrategylog.csv" using ($1*1E-9):($64+$65+$66+$67) with lines ls 16 title 'n16' axes x1y1,\

set output 'figures/singleVsMultiDelegationFlowtime/workloadMultiProposalByTime.svg'
plot "data/globalFlowtimeMultiProposalStrategyNoneCounterProposalStrategylog.csv" using ($1*1E-9):($2) with lines dt 1 lc "black" notitle
#title 'Flowtime'
#axes x1y2
# "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($4+$5+$6+$7) with lines ls 1 title 'n01',\
#      "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($8+$9+$10+$11) with lines ls 2 title 'n02',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($12+$13+$14+$15) with lines ls 3 title 'n03',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($16+$17+$18+$19) with lines ls 4 title 'n04',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($20+$21+$22+$23) with lines ls 5 title 'n05',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($24+$25+$26+$27) with lines ls 6 title 'n06',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($28+$29+$30+$31) with lines ls 7 title 'n07',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($32+$33+$34+$35) with lines ls 9 title 'n08',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($36+$37+$38+$39) with lines ls 10 title 'n09',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($40+$41+$42+$43) with lines ls 11 title 'n10',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($44+$45+$46+$47) with lines ls 12 title 'n11',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($48+$49+$50+$51) with lines ls 13 title 'n12',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($52+$53+$54+$55) with lines ls 14 title 'n13',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($56+$57+$58+$59) with lines ls 15 title 'n14',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($60+$61+$62+$63) with lines ls 16 title 'n15',\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using ($1*1E-9):($64+$65+$66+$67) with lines ls 16 title 'n16',\

unset xlabel
set xlabel "Number of delegations" font ",16"
set xrange [0:100]
set output 'figures/singleVsMultiDelegationFlowtime/workloadSingleProposalByDelegation.svg'
plot "data/globalFlowtimeSingleProposalStrategyNoneCounterProposalStrategylog.csv" using 0:($2) with lines dt 1 lc "black" notitle
#title 'Flowtime'
#axes x1y2
# "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($4+$5+$6+$7) with lines ls 1 title 'n01' axes x1y1,\
#      "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($8+$9+$10+$11) with lines ls 2 title 'n02' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($12+$13+$14+$15) with lines ls 3 title 'n03' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($16+$17+$18+$19) with lines ls 4 title 'n04' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($20+$21+$22+$23) with lines ls 5 title 'n05' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($24+$25+$26+$27) with lines ls 6 title 'n06' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($28+$29+$30+$31) with lines ls 7 title 'n07' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($32+$33+$34+$35) with lines ls 9 title 'n08' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($36+$37+$38+$39) with lines ls 10 title 'n09' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($40+$41+$42+$43) with lines ls 11 title 'n10' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($44+$45+$46+$47) with lines ls 12 title 'n11' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($48+$49+$50+$51) with lines ls 13 title 'n12' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($52+$53+$54+$55) with lines ls 14 title 'n13' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($56+$57+$58+$59) with lines ls 15 title 'n14' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($60+$61+$62+$63) with lines ls 16 title 'n15' axes x1y1,\
#     "data/globalFlowtimeSingleProposalStrategylog.csv" using 0:($64+$65+$66+$67) with lines ls 16 title 'n16' axes x1y1,\

set output 'figures/singleVsMultiDelegationFlowtime/workloadMultiProposalByDelegation.svg'
plot "data/globalFlowtimeMultiProposalStrategyNoneCounterProposalStrategylog.csv" using 0:($2) with lines dt 1 lc "black" notitle
#title 'Flowtime'
#axes x1y2
# "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($4+$5+$6+$7) with lines ls 1 title 'n01' axes x1y1,\
#      "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($8+$9+$10+$11) with lines ls 2 title 'n02' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($12+$13+$14+$15) with lines ls 3 title 'n03' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($16+$17+$18+$19) with lines ls 4 title 'n04' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($20+$21+$22+$23) with lines ls 5 title 'n05' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($24+$25+$26+$27) with lines ls 6 title 'n06' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($28+$29+$30+$31) with lines ls 7 title 'n07' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($32+$33+$34+$35) with lines ls 9 title 'n08' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($36+$37+$38+$39) with lines ls 10 title 'n09' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($40+$41+$42+$43) with lines ls 11 title 'n10' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($44+$45+$46+$47) with lines ls 12 title 'n11' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($48+$49+$50+$51) with lines ls 13 title 'n12' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($52+$53+$54+$55) with lines ls 14 title 'n13' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($56+$57+$58+$59) with lines ls 15 title 'n14' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($60+$61+$62+$63) with lines ls 16 title 'n15' axes x1y1,\
#     "data/globalFlowtimeMultiProposalStrategylog.csv" using 0:($64+$65+$66+$67) with lines ls 16 title 'n16' axes x1y1,\



set terminal svg #pdfcairo
set datafile separator ","
set style fill solid border rgb "green"
set style fill transparent solid 0.1 noborder
#set xrange [2:12]
set auto y
set grid
set key autotitle columnhead
unset key
set key font ",20"
set key left above
set ticslevel 0
set style data lines
set xlabel "Number of nodes" font ",16"
set ylabel "Mean flowtime"  font ",16"
set output 'figures/SingleVsMultiDelegationFlowtime/meanFlowtime.svg'
plot  "data/SingleVsMultiDelegationFlowtime.csv" using 1:16:18 with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/SingleVsMultiDelegationFlowtime.csv" using 1:17 with lines dt 1 lc "dark-red" title 'Unary delegation',\
      "data/SingleVsMultiDelegationFlowtime.csv" using 1:46:48 with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/SingleVsMultiDelegationFlowtime.csv" using 1:47 with lines dt 7 lc "dark-green" title 'N-ary delegation'
      #"data/SingleVsMultiDelegationFlowtime.csv" using 1:($7):($9) with filledcurves lc "light-grey" notitle,\
      #"data/SingleVsMultiDelegationFlowtime.csv" using 1:($8) with lines dt 1 lc "dark-grey" title 'Initial allocation',\
unset ylabel
set ylabel "Rescheduling time (s)" font ",16"
set xlabel "Number of nodes" font ",16"
set yrange [*:*]
set output 'figures/SingleVsMultiDelegationFlowtime/schedulingTime.svg'
plot  "data/SingleVsMultiDelegationFlowtime.csv" using 1:($19*1E-9):($21*1E-9) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/SingleVsMultiDelegationFlowtime.csv" using 1:($20*1E-9) with lines dt 1 lc "dark-red" title 'Unary delegation',\
      "data/SingleVsMultiDelegationFlowtime.csv" using 1:($49*1E-9):($51*1E-9) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/SingleVsMultiDelegationFlowtime.csv" using 1:($50*1E-9) with lines dt 7 lc "dark-green" title 'N-ary delegation'

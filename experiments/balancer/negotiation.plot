set terminal svg #pdfcairo
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
#set xrange [2:12]
set auto y
set grid
set key autotitle columnhead
unset key
set key font ",14"
set key out vert
set key center top
set key maxrows 4
set key horiz
set ticslevel 0
set style data lines
set xlabel "Number of nodes" font ",16"
set ylabel "Mean flowtime"  font ",16"
set output 'figures/negotiation/meanFlowtime.svg'
plot  "data/negotiation.csv" using 1:16:18 with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/negotiation.csv" using 1:17 with lines dt 3 lc "dark-green" title 'Bargaining',\
      "data/negotiation.csv" using 1:46:48 with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
      "data/negotiation.csv" using 1:47 with lines dt 7 lc "dark-blue" title 'ASSI',\
      "data/negotiation.csv" using 1:76:78 with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/negotiation.csv" using 1:77 with lines dt 5 lc "dark-red" title 'RSSI',\
      "data/negotiation.csv" using 1:106:108 with filledcurves fc "light-magenta" fs transparent solid 0.3 border lc "red" notitle,\
      "data/negotiation.csv" using 1:107 with lines dt 5 lc "dark-magenta" title 'Hill-climbing',\
      "data/negotiation.csv" using 1:($7):($9) with filledcurves fc "light-grey" fs transparent solid 0.3 border lc "grey" notitle,\
      "data/negotiation.csv" using 1:($8) with lines dt 1 lc "dark-grey" title 'Initial allocation'
unset ylabel
set ylabel "Scheduling time (s in log scale)" font ",16"
set logscale y
set xlabel "Number of nodes" font ",16"
set yrange [*:*]
set output 'figures/negotiation/schedulingTime.svg'
plot  "data/negotiation.csv" using 1:($19*1E-9):($21*1E-9) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/negotiation.csv" using 1:($20*1E-9) with lines dt 3 lc "dark-green" title 'Bargaining',\
      "data/negotiation.csv" using 1:($49*1E-9):($51*1E-9) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
      "data/negotiation.csv" using 1:($50*1E-9) with lines dt 7 lc "dark-blue" title 'ASSI',\
      "data/negotiation.csv" using 1:($79*1E-9):($81*1E-9) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/negotiation.csv" using 1:($80*1E-9) with lines dt 5 lc "dark-red" title 'RSSI',\
      "data/negotiation.csv" using 1:($109*1E-9):($111*1E-9) with filledcurves fc "light-magenta" fs transparent solid 0.3 border lc "red" notitle,\
      "data/negotiation.csv" using 1:($110*1E-9) with lines dt 5 lc "dark-magenta" title 'Hill-climbing'
unset ylabel
set ylabel "Local availability ratio (%)"  font ",16"
set yrange [0:100]
unset logscale y
set xlabel "Number of nodes" font ",16"
set output 'figures/negotiation/localityRatio.svg'
plot  "data/negotiation.csv" using 1:($4*100):($6*100) with filledcurves fc "light-grey" fs transparent solid 0.3 border lc "grey" notitle,\
      "data/negotiation.csv" using 1:($5*100) with lines dt 1 lc "dark-grey" title 'Initial allocation',\
      "data/negotiation.csv" using 1:($22*100):($24*100) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
     "data/negotiation.csv" using 1:($23*100) with lines dt 3 lc "dark-green" title 'Bargaining',\
     "data/negotiation.csv" using 1:($52*100):($54*100) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
     "data/negotiation.csv" using 1:($53*100) with lines dt 7 lc "dark-blue" title 'ASSI',\
      "data/negotiation.csv" using 1:($82*100):($84*100) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/negotiation.csv" using 1:($83*100) with lines dt 5 lc "dark-red" title 'RSSI',\
      "data/negotiation.csv" using 1:($112*100):($114*100) with filledcurves fc "light-magenta" fs transparent solid 0.3 border lc "red" notitle,\
      "data/negotiation.csv" using 1:($113*100) with lines dt 5 lc "dark-magenta" title 'Hill-climbing'

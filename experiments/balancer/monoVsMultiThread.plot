set terminal svg #pdfcairo
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
#set xrange [2:12]
set auto y
set grid
set key autotitle columnhead
unset key
set key font ",20"
set key left above
set ticslevel 0
set style data lines
set xlabel "Number of nodes" font ",16"
set ylabel "Rescheduling time (s)"  font ",16"
set output 'figures/monoVsMultiThread/schedulingTime.svg'
plot  "data/monoVsMultiThread.csv" using 1:($19*1E-9):($21*1E-9) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
      "data/monoVsMultiThread.csv" using 1:($20*1E-9) with lines dt 3 lc "dark-blue" title 'Mono-thread',\
      "data/monoVsMultiThread.csv" using 1:($49*1E-9):($51*1E-9) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/monoVsMultiThread.csv" using 1:($50*1E-9) with lines dt 7 lc "dark-green" title 'Multi-thread',\
      "data/monoVsMultiThread.csv" using 1:($79*1E-9):($81*1E-9) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/monoVsMultiThread.csv" using 1:($80*1E-9) with lines dt 1 lc "dark-red" title 'Hill climbing'
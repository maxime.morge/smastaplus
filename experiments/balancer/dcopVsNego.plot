set terminal svg #pdfcairo
set datafile separator ","
set style fill solid 0.25 border -1
set style boxplot outliers pointtype 7
set style data boxplot
#set boxwidth  0.3
#set pointsize 0.3
unset key
set xtics ("Negotiation" 1,  "MGM2 (2s)" 2, "MGM2 (5s)" 3, "MGM2 (10s)" 4) scale 0.0
set xtics nomirror
set ytics nomirror
set ylabel "Mean flowtime" font ",16"
set yrange [0:1000]
set output 'figures/dcopVsNego/dcopVsNego.svg'
plot 'data/dcopVsNego.csv' using (1):5 lc "dark-blue",\
    '' using (2):9 lc "dark-green",\
    '' using (3):13 lc "dark-green",\
    '' using (4):17 lc "dark-green"

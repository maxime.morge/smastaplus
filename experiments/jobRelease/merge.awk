# Copyright (C) Maxime MORGE, 2023
# gawk -f merge.awk data/noNegotiation*.csv > data/quartilesNoNegotiationLog.csv
# gawk -f merge.awk data/negotiation*.csv > data/quartilesNegotiationLog.csv
# gawk -f merge.awk data/assi*.csv > data/quartilesAssiLog.csv
# gawk -f merge.awk data/rssi*.csv > data/quartilesRssiLog.csv
BEGIN{
    FS=",";
    OFS=",";
}
(FNR!=1){
    nbDelegatedTasks[$2][nbLignes[$2]]=$3;
    nbConsumedTasks[$2][nbLignes[$2]]=$4;
    ongoingMeanGlobalFlowtime[$2][nbLignes[$2]]=$5;
    nbLignes[$2]+=1;
}
END {
    PROCINFO["sorted_in"] = "@ind_num_asc";
    print "logTimestamp,Q1nbDelegatedTasks,Q2nbDelegatedTasks,Q3nbDelegatedTasks,Q1nbConsumedTasks,Q2nbConsumedTasks,Q3nbConsumedTasks,Q1ongoingMeanGlobalFlowtime,Q2ongoingMeanGlobalFlowtime,Q3ongoingMeanGlobalFlowtime";
    for (key in nbLignes) {
        #print "key",key;
        #print "nbLignes",nbLignes[key];
        n1NbDelegatedTasks = asort(nbDelegatedTasks[key], sortedNbDelegatedTasks);
        #print "n1NbDelegatedTasks",n1NbDelegatedTasks ;
        #for (k in sortedNbDelegatedTasks){
        #    print sortedNbDelegatedTasks[k];
        #}
        q1NbDelegatedTasks = sortedNbDelegatedTasks[int(n1NbDelegatedTasks/4)] ;
        q2NbDelegatedTasks= sortedNbDelegatedTasks[int(n1NbDelegatedTasks/2)] ;
        q3NbDelegatedTasks = sortedNbDelegatedTasks[int(n1NbDelegatedTasks*3/4)] ;

        n1NbConsumedTasks = asort(nbConsumedTasks[key], sortedNbConsumedTasks);
        q1NbConsumedTasks = sortedNbConsumedTasks[int(n1NbConsumedTasks/4)] ;
        q2NbConsumedTasks = sortedNbConsumedTasks[int(n1NbConsumedTasks/2)] ;
        q3NbConsumedTasks = sortedNbConsumedTasks[int(n1NbConsumedTasks*3/4)] ;

        n1OngoingMeanGlobalFlowtime = asort(ongoingMeanGlobalFlowtime[key], sortedOngoingMeanGlobalFlowtime);
        q1OngoingMeanGlobalFlowtime = sortedOngoingMeanGlobalFlowtime[int(n1OngoingMeanGlobalFlowtime/4)] ;
        q2OngoingMeanGlobalFlowtime = sortedOngoingMeanGlobalFlowtime[int(n1OngoingMeanGlobalFlowtime/2)] ;
        q3OngoingMeanGlobalFlowtime = sortedOngoingMeanGlobalFlowtime[int(n1OngoingMeanGlobalFlowtime*3/4)] ;

        print key, q1NbDelegatedTasks, q2NbDelegatedTasks, q3NbDelegatedTasks, q1NbConsumedTasks, q2NbConsumedTasks, q3NbConsumedTasks, q1OngoingMeanGlobalFlowtime, q2OngoingMeanGlobalFlowtime, q3OngoingMeanGlobalFlowtime ;
    }
}
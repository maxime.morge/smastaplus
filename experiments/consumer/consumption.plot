set terminal svg
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
unset key
set key font ",14"
set key out vert
set key center top
set key width -8 vertical maxrows 4
set grid
set style data lines

set xrange [00:360]
set xlabel "Number of tasks" font ",16"

set yrange [0:3.5]
set ytics nomirror
set ylabel "Flowtime (mins)" font ",16"

set output 'figures/consumption4jobs8nodes5staps5allocations040-320tasks.svg'
plot "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($14/1000/60):($16/1000/60) with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle,\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($15/1000/60) with lines dt 1 lc "#32a68c" title 'C^R(Ae) for bargaining',\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($92/1000/60):($94/1000/60) with filledcurves fc "#2d0505" fs transparent solid 0.3 border lc "#000000" notitle,\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($93/1000/60) with lines dt 1 lc "#000000" title 'C^R(Ae) for ASSI',\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($53/1000/60):($55/1000/60) with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle,\
       "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($54/1000/60) with lines dt 1 lc "#5862ed" title 'C^R(Ae) for RSSI',\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($131/1000/60):($133/1000/60) with filledcurves fc "#ff6941" fs transparent solid 0.3 border lc "#fc535c" notitle,\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($132/1000/60) with lines dt 1 lc "#fc535c" title 'C^R(A0)',\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($17/1000/60):($19/1000/60) with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle,\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($18/1000/60) with lines dt 2 lc "#32a68c" title 'C^S_E(Ae) for bargaining',\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($95/1000/60):($97/1000/60) with filledcurves fc "#2d0505" fs transparent solid 0.3 border lc "#000000" notitle,\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($96/1000/60) with lines dt 2 lc "#000000" title 'C^S_E(Ae) for ASSI',\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($56/1000/60):($58/1000/60) with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle,\
      "data/consumption4jobs8nodes5staps5allocations040-320tasks.csv" using 3:($57/1000/60) with lines dt 2 lc "#5862ed" title 'C^S_E(Ae) for RSSI'

set output 'figures/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.svg'
plot "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($14/1000/60):($16/1000/60) with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($15/1000/60) with lines dt 1 lc "#32a68c" title 'C^R(Ae) for bargaining',\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($92/1000/60):($94/1000/60) with filledcurves fc "#2d0505" fs transparent solid 0.3 border lc "#000000" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($93/1000/60) with lines dt 1 lc "#000000" title 'C^R(Ae) for ASSI',\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($53/1000/60):($55/1000/60) with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($54/1000/60) with lines dt 1 lc "#5862ed" title 'C^R(Ae) for RSSI',\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($131/1000/60):($133/1000/60) with filledcurves fc "#ff6941" fs transparent solid 0.3 border lc "#fc535c" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($132/1000/60) with lines dt 1 lc "#fc535c" title 'C^R(A0)',\
       "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($17/1000/60):($19/1000/60) with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle,\
       "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($18/1000/60) with lines dt 2 lc "#32a68c" title 'C^S_E(Ae) for bargaining',\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($95/1000/60):($97/1000/60) with filledcurves fc "#2d0505" fs transparent solid 0.3 border lc "#000000" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($96/1000/60) with lines dt 2 lc "#000000" title 'C^S_E(Ae) for ASSI',\
#      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($56/1000/60):($58/1000/60) with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle,\
#      "data/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv" using 3:($57/1000/60) with lines dt 2 lc "#5862ed" title 'C^S_E(Ae) for RSSI'

set yrange [0:7]
set output 'figures/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.svg'
plot "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($14/1000/60):($16/1000/60) with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($15/1000/60) with lines dt 1 lc "#32a68c" title 'C^R(Ae) for bargaining',\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($92/1000/60):($94/1000/60) with filledcurves fc "#2d0505" fs transparent solid 0.3 border lc "#000000" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($93/1000/60) with lines dt 1 lc "#000000" title 'C^R(Ae) for ASSI',\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($53/1000/60):($55/1000/60) with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($54/1000/60) with lines dt 1 lc "#5862ed" title 'C^R(Ae) for RSSI',\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($131/1000/60):($133/1000/60) with filledcurves fc "#ff6941" fs transparent solid 0.3 border lc "#fc535c" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($132/1000/60) with lines dt 1 lc "#fc535c" title 'C^R(A0)',\
       "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($17/1000/60):($19/1000/60) with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle,\
       "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($18/1000/60) with lines dt 2 lc "#32a68c" title 'C^S_H(Ae) for bargaining',\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($95/1000/60):($97/1000/60) with filledcurves fc "#2d0505" fs transparent solid 0.3 border lc "#000000" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($96/1000/60) with lines dt 2 lc "#000000" title 'C^S_H(Ae) for ASSI',\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($56/1000/60):($58/1000/60) with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle,\
      "data/consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv" using 3:($57/1000/60) with lines dt 2 lc "#5862ed" title 'C^S_H(Ae) for RSSI'


#set yrange [0:1.5]
#set xrange [8:16]
#set y2tics
#set output 'figures/consumption4jobs5staps5allocationsByNodes.svg'
#plot  "data/consumption4jobs5staps5allocationsByNodes.csv" using 1:($14/1000/60):($16/1000/60) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
#      "data/consumption4jobs5staps5allocationsByNodes.csv" using 1:($15/1000/60) with lines dt 3 lc "dark-green" title 'C^R(Ae)',\
#      "data/consumption4jobs5staps5allocationsByNodes.csv" using 1:($53/1000/60):($55/1000/60) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "blue" notitle,\
#      "data/consumption4jobs5staps5allocationsByNodes.csv" using 1:($54/1000/60) with lines dt 7 lc "dark-red" title 'C^R(A0)',\
#      "data/consumption4jobs5staps5allocationsByNodes.csv" using 1:($17/1000/60):($19/1000/60) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "grey" notitle,\
#      "data/consumption4jobs5staps5allocationsByNodes.csv" using 1:($18/1000/60) with lines dt 1 lc "dark-blue" title 'C^S_E(Ae)''


# Copyright (C) Ellie BEAUPREZ, Maxime MORGE, 2021, 2022
# Python code describing the DCOP problem for
# Finding the minimal configuration for a STAP problem with n tasks, m computing nodes and l jobs 
import sys

maxCost = sys.maxsize

def all_different(*positions):
    """
    0 if all the positions are different
    maxCost otherwise
    """
    if len(set(positions)) == len(positions):
        return 0
    else:
        return maxCost


class Job:
    """
    A job is a set of tasks
    """

    def __init__(self, the_id, the_tasks):
        self.id = the_id
        self.tasks = the_tasks

    def get_id(self):
        return self.id

    def describe(self):
        """
        String description of the job
        """
        res = "J" + str(self.id) + " :"
        for t in self.tasks:
            res += "t" + str(t) + " "
        return res

    def last(self, node):
        """
        The last task of the job on a particular node 
        0 if none task of the job is over the node
        """
        max_pos = -1
        last_task = 0
        bundle = node.get_bundle()
        for task, position in bundle.items():
            if task in self.tasks:
                if max_pos < position:
                    max_pos = position
                    last_task = task
        return last_task

    def completion_time(self, nodes):
        """
        Completion time of the job
        """
        max_completion_time = 0
        for node in nodes:
            last_task = self.last(node)
            if last_task > 0:
                completion_time_last_task = node.completion_time(last_task)
                if completion_time_last_task > max_completion_time:
                    max_completion_time = completion_time_last_task
        return max_completion_time


class Node:
    """
    Computing node with its bundle
    """

    def __init__(self, the_id, the_costs, the_bundle):
        self.id = the_id
        self.costs = the_costs
        self.bundle = the_bundle

    def describe(self):
        """
        String description of the node
        """
        return str(self.id) + ": " + str(self.bundle)

    def get_bundle(self):
        return self.bundle

    def get_id(self):
        return self.id

    def workload(self):
        """
        The workload of the node, eventually 0
        """
        load = 0
        for task in self.bundle.keys():
            load += self.costs[task - 1]
        return load

    def completion_time(self, task_id):
        """
        The completion time of a task on the node,
        eventually 0 if the task is not in its bundle
        """
        if task_id not in self.bundle:
            return 0
        time = 0
        task_position = self.bundle[task_id]
        for task, position in self.bundle.items():
            if task == task_id or position < task_position:
                time += self.costs[task - 1]
        return time


def parse_problem_file(file_name):
    """
    The STAP instance pared from a text file with:
    n, l, m, 
    jobs, 
    costs
    """
    fp = open(file_name, 'r')
    lines = [line.rstrip('\n') for line in fp]
    fp.close()
    parameters = lines[0].split(", ")
    the_n, the_l, the_m = int(parameters[0]), int(parameters[1]), int(parameters[2])
    the_jobs = []
    index = 1
    for i in range(0, the_l):
        tasks = list(map(int, lines[index + i].split(", ")))
        job = Job(i + 1, tasks)
        the_jobs.append(job)
    the_costs = dict()
    index = the_l + 1
    for i in range(0, the_m):
        costs_of_node = list(map(float, lines[index + i].split(", ")))
        the_costs[i + 1] = costs_of_node
    return the_n, the_l, the_m, the_jobs, the_costs


# Import the stap problem from the text file
n, l, m, jobs, costs = parse_problem_file("experiments/edcop/scaler/stap.txt")


def get_position(task_id, *positions):
    """
    The position of the task with task_id
    """
    return positions[task_id - 1]


def assignment2position(position):
    """
    The pair (node_id, local_position) from a specific position
    """
    node_id = (position // n) + 1
    local_position = position % n + 1
    return node_id, local_position


def assignments2positions(positions):
    """
    The pairs (node_id, local_position) from the positions
    """
    assignments = dict()
    print(positions)
    for i in range(n):
        node_id, position = assignment2position(positions[i])
        assignments[i + 1] = (node_id, position)
    return assignments


def build_bundles(assignments):
    """
    The bundles 
    {node_id1 : { task_id1 : local_position1, task_id2 : local_position2}, 
    node_id : { task_id3 : local_position3, ...},
    ..}
    """
    bundles = dict()
    for task, assignment in assignments.items():
        node_id, position = assignment
        if node_id not in bundles.keys():
            bundles[node_id] = {task: position}
        else:
            bundles[node_id][task] = position
    for node_id in range(1, m+1):
        if node_id not in bundles.keys():
           bundles[node_id] = {} 
    return bundles


def build_nodes(bundles):
    """
    The nodes with their bundles
    """
    nodes = []
    for node_id, bundle in bundles.items():
        #print(node_id, bundle)
        node = Node(node_id, costs[node_id], bundle)
        nodes.append(node)
    return nodes


def build_jobs(tasks_per_jobs):
    """
    The jobs
    """
    the_jobs = []
    for jobId, tasks in tasks_per_jobs.items():
        job = Job(jobId, tasks)
        jobs.append(job)
    return the_jobs


def flowtime(*positions):
    """
    The flowtime of the current allocation
    """
    assignments = assignments2positions(positions)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    cost = 0
    for job in jobs:
        cost += job.completion_time(nodes)
    return cost


def min_flowtime(min, *positions):
    """
    0 if the mean flowtime is greater than min
    infinity otherwise
    """
    assignments = assignments2positions(positions)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    flowtime = 0
    for job in jobs:
        flowtime += job.completion_time(nodes)
    if min <= flowtime/m:
        return 0
    else:
        return maxCost


def max_flowtime(max, *positions):
    """
    0 if the mean flowtime is smaller than max
    maxCost otherwise
    """
    assignments = assignments2positions(positions)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    flowtime = 0
    for job in jobs:
        flowtime += job.completion_time(nodes)
    if max >= flowtime/m:
        return 0
    else:
        return maxCost


def makespan(*positions):
    """
    The makespan of the current allocation
    """
    assignments = assignments2positions(positions)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    worst_workload = 0
    for node in nodes:
        workload = node.workload()
        if worst_workload < workload:
            worst_workload = workload
    return worst_workload


def min_makespan(min, *positions):
    """
    0 if the makespan is greater than min
    maxCost otherwise
    """
    assignments = assignments2positions(positions)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    makespan = 0
    for node in nodes:
        workload = node.workload()
        if makespan < workload:
            makespan = workload
    if min <= makespan:
        return 0
    else:
        return maxCost


def max_makespan(max, *positions):
    """
    0 if the makespan is smaller than max
    maxCost otherwise
    """
    assignments = assignments2positions(positions)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    makespan = 0
    for node in nodes:
        workload = node.workload()
        if makespan < workload:
            makespan = workload
    if max >= makespan:
        return 0
    else:
        return maxCost

def active(index, activity, *positions):
    """
    0 if activity of the node at index is correct
    maxCost otherwise
    """
    assignments = assignments2positions(positions)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    #print(nodes)
    node = nodes[index]
    loaded = node.get_bundle()
    if (not loaded and activity == 0) or (loaded and activity == 1):
        return 0
    else:
        return maxCost

name := "smastaplus"

version := "2.2"

scalacOptions += "-deprecation" //Emit warning and location for usages of deprecated APIs
scalacOptions += "-feature" // Emit warning and location for usages of features that should be imported explicitly
scalacOptions += "-unchecked" // Enable additional warnings where generated code depends on assumptions.
scalacOptions += "-Xlint" // Enable recommended warnings

run / javaOptions += "-Xms400G"//400G on a blade
run / javaOptions += "-Xmx400G"//400G on a blade
run / javaOptions += "-Dfile.encoding=UTF-8"

/**
  * For experiments
  */
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.LocalVsGlobalCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.PSSIVsNegotiationCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.SSIVsNegotiationCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.NegotiationCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.MonoVsMultiThread")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.SingleVsMultiDelegationFlowtime")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.CompareMultiProposalStrategies")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.CompetitionBalancer")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.DelegationVsSwapCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.SwapCounterProposalStrategy")
//Compile / run / mainClass := Some("org.smastaplus.experiment.balancer.HillClimbingVsSimulatedAnnealingVsNegotiationCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.consumer.WithVsWithoutNegotiationCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.consumer.WithVsWithoutNegotiationByNodesCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.consumer.WithVsWithoutNegotiationSlowDownHalfNodeCampaign")
//Compile / run / mainClass := Some("org.smastaplus.experiment.consumer.WithVsWithoutNegotiationFromStableCampaign")
//Compile / run / mainClass := Some("org.smastaplus.utils.MergeRuns")

/**
  * For classical usage
  */
//Compile / run / mainClass := Some("org.smastaplus.utils.MASTAPlusBalancer")
//assembly / mainClass := Some("org.smastaplus.utils.MASTAPlusBalancer")
Compile / run / mainClass := Some("org.smastaplus.utils.MASTAPlusConsumer")
//Compile / run / mainClass := Some("org.smastaplus.utils.MASTAPlusJobRelease")
assembly / mainClass := Some("org.smastaplus.utils.MASTAPlusConsumer")
//assembly / mainClass := Some("org.smastaplus.utils.MASTAPlusJobRelease")
//Compile / run / mainClass := Some("org.smastaplus.utils.MASTAPlusScaler")
//assembly / mainClass := Some("org.smastaplus.utils.MASTAPlusScaler")

fork := true

cancelable in Global := true

Compile / unmanagedJars += file("lib/jacop-4.8.0.jar")
resolvers += ("Artima Maven Repository" at "http://repo.artima.com/releases").withAllowInsecureProtocol(true)
resolvers += ("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/").withAllowInsecureProtocol(true)

scalaVersion := "2.13.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.7.0",
  "com.typesafe.akka" %% "akka-remote" % "2.7.0",
  "org.scalactic" %% "scalactic" % "3.2.15",
  "org.scalatest" %% "scalatest" % "3.2.15" % "test",
  "io.github.cquiroz" %% "scala-java-time" % "2.5.0",
  "com.github.nscala-time" %% "nscala-time" % "2.32.0",
  "com.lihaoyi" %% "upickle" % "2.0.0"
)

Test / logBuffered := false
trapExit := false
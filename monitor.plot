set terminal pdfcairo
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
set auto y
set auto y2
set key autotitle columnhead
unset key
set key font ",10"
set key left above
set ticslevel 0
set style data lines
set xlabel "log(Temps) (ms)" font ",16"
set xtics nomirror
set key font ",14"
set key out vert
set key center top
set key maxrows 4
set key horiz
#set yrange [0:3]
#set y2range [0:128]
set ytics nomirror tc "black"
set y2tics nomirror tc "black"
set ylabel "Mean flowtime (min.)" font ",16" tc "black"
set y2label "Number of tasks" font ",16" tc "black"
set format x "10^%.0f"
set output 'monitor.pdf'
plot "negotiationLog.csv" using ($2):($3) smooth bezier dt 7 lc "dark-green" title 'Bargained tasks' axes x1y2,\
     "psiLog.csv" using ($2):($3) smooth bezier dt 7 lc "dark-magenta" title 'Auctioned tasks' axes x1y2,\
     "negotiationLog.csv" using ($2):($4) smooth bezier dt 8 lc "dark-green" title 'MASTA+ consumed tasks' axes x1y2,\
     "negotiationLog.csv" using ($2):($5) smooth bezier dt 1 lc "dark-green" title 'MASTA+' axes x1y1,\
     "noNegotiationLog.csv" using ($2):($4) smooth bezier dt 8 lc "dark-red" title 'Consumed tasks' axes x1y2,\
     "noNegotiationLog.csv" using ($2):($5) smooth bezier dt 1 lc "dark-red" title 'Consumption only' axes x1y1,\
     "assiLog.csv" using ($2):($4) smooth bezier dt 8 lc "dark-blue" title 'SSI consumed tasks' axes x1y2,\
     "assiLog.csv" using ($2):($5) smooth bezier dt 1 lc "dark-blue" title 'SSI' axes x1y1,\
     "rssiLog.csv" using ($2):($4) smooth bezier dt 8 lc "dark-magenta" title 'PSI consumed tasks' axes x1y2,\
     "rssiLog.csv" using ($2):($5) smooth bezier dt 1 lc "dark-magenta" title 'PSI' axes x1y1
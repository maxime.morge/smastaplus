# Worker Behaviour Specification
In this document, we specify
the [worker behaviour](../../src/main/scala/org/smastaplus/balancer/deal/mas/nodeAgent/worker/WorkerBehaviour.scala).
The [manager behaviour](./manager.md) is described here.

## Overview

In our approach, the worker executes (consumes) the tasks.  The worker agent behavior is specified by a
deterministic finite state automaton with the following states:

-   `Free` where a task is running;

-   `Busy` where no task is running.

The state transitions are such as:
```
 Event if conditions => actions.
```
They are triggered by an external event, e.g. the reception of a
message. The automaton is deterministic, i.e. the triggering
conditions of outgoing transitions for the same state are 
mutually exclusive. This behavioral design pattern allows 
handling multi-party dialogues.

![](figures/behaviourWorker.svg)


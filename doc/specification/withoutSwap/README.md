# Agent Behaviour Specification

<b>Warning. Please note this documentation is deprecated. Please read the [recent one](../README.md).</b>

SMASTA+ is a Scala implementation of the Extended Multi-agents Situated Task Allocation.
Its installation and usage are explained [here](../../../README.md). In this document, we specify
the [agent behaviour](../../../src/main/scala/org/smastaplus/balancer/deal/mas/NegotiationBehaviour.scala).

## Overview

In our approach, the task reallocation is the outcome of negotiations
between agents adopting the same behaviour: they
alternatively play the roles of proposer, responder and contractor.
The agents execute this behaviour according to their [knowledge and
beliefs](../../../src/main/scala/org/smastaplus/strategy/deal/MindWithPeerModelling.scala). 

The negotiation process is triggered by the [supervisor](../../../src/main/scala/org/smastaplus/balancer/deal/mas/Supervisor.scala) 
which initiates the bundle of tasks 
for each agent &nu;<sub>i</sub> using a message <tt>Give</tt>(B<sub>i</sub>) and waits for the 
sorted bundle after the reallocation through the message <tt>End</tt>(B<sub>i</sub>). The agent behavior is 
specified by a deterministic finite state automaton with the following states:

-   `Init` where the agent is triggerable by the
    `supervisor`;

-   `Waiting` where the agent is waiting to receive the job costs for each peer
    in order to initiate its peer modelling;

-   `Responder` where the agent replies to the proposals from its peers;

-   `Proposer` where the agent has sent a proposal for which it waits
    for an acceptance or a rejection before a deadline;

-   `Contractor` where the agent has accepted a proposal and its waits
    for a confirmation or withdrawal;

-   `Pause` where the offer strategy suggests none delegation, and the
    belief base is updated until a new opportunity (i.e. a potential delegation) 
    is found.

-   `Final` where the agent is ready to stop.

The state transitions are such as:
```
 Event if conditions => actions.
```
They are triggered by an external event, e.g. the reception of a
message. The automaton is deterministic, i. e. the triggering
conditions of outgoing transitions for the same state are mutually
exclusive. This behavioral design pattern allows handling multi-party
dialogues.

Beyond the knowledge/beliefs of the agent and its peer modelling, the
[agent mind](../../../src/main/scala/org/smastaplus/balancer/deal/NegotiatingMind.scala) 
contains two variables which
allow it to manage the negotiation process :

-   `informers`, the set of peers which have sent `Inform` messages in
    order to initiate its peer modelling;

-   `lastOffer`, the delegation suggested in the last proposal.


For clarity, we split the agent behaviour 
in two phases :
1. the initialisation
2. the negotiation

## Initialisation

In the figure below, the sending of messages is denoted `!`.
While `stash` enables an agent to temporarily stash away messages,
`unstashAll` prepends all messages in the stash to the mailbox. The
original sender is maintained if the agents `forward` messages.

![](figures/behaviourInit.svg)


In the state `Init`, the agent can receive the following messages:

-   an initialization <tt>Give</tt>(B<sub>i</sub>) from the `supervisor` of the
    task bundle,

    -   if the peer modelling is complete, the agent can sort its bundle,
        unstash the messages previously stashed and send
        itself a trigger to evaluate the offer strategy in the
        `Responder` state,

    -   if the peer modelling is incomplete, the agent waits for some `Inform`
        messages in the state `Waiting`;

    In any case, the agent informs its peers about the cost of
    each job in its bundle.

-   an information <tt>Inform</tt>(c(J,&nu;<sub>j</sub>))
    from the peer &nu;<sub>j</sub> indicating the cost of each job in its bundle
    which leads the agent to update <tt>informers</tt> and its belief base;

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is stashed 
    until the `Responder` state, then the agent stays in the state `Init`.

In the state `Waiting`, the agent can receive the following messages:

-   an information <tt>Inform</tt>(c(J,&nu;<sub>j</sub>))
    from the peer &nu;<sub>j</sub> indicating the job costs for it,

    -   if the peer modelling is complete, the agent can sort its bundle, 
        unstash the messages it had previously stashed and send
        to itself a trigger in order to evaluate the offer strategy in
        the `Responder` state,

    -   if the peer modelling is incomplete, the agent updates the `informers`,
        its belief base, and it stays in the state `Waiting`;

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is, as the state `Init`,
    stashed until the state `Responder`. Then, the agent stays in the state
    `Waiting`.

## Negotiation

![](figures/behaviour.svg)

In the state `Responder`, the agent can receive the following
messages:

-   a trigger `Next` from itself to initiate the offer strategy,

    -   if there is no triggerable delegation
        (O<sub>i</sub> = &theta;), it sends to itself a
        `Close` and goes to the state `Pause`,

    -   if there is a triggerable delegation
        (O<sub>i</sub> &ne; &theta;), it sends a proposal
         and goes to the state `Proposer`;

-   a reactivation message <tt>WakeUp</tt>(O<sub>i</sub>) sent by
    itself to indicate that a delegation has been selected by the offer
    strategy, then its sends a proposal and goes to the state `Proposer`;

-   a proposal <tt>Propose</tt>(&delta;(&tau;, &nu;<sub>j</sub>, &nu;<sub>i</sub>, A)) sent by a peer,

    -   if the delegation is new and
        acceptable, then the agent
        replies with an acceptance and goes to the state
        `Contractor`,

    -   if the proposal is neither new nor
        acceptable, then the agent
        replies with a rejection,
        sends to itself a trigger and stays in the state `Responder`.

    In any case, it is required to check if the offer is new
    because the agent may receive several times a proposal for the same task. This 
    verification prevents from evaluating an obsolete proposal for a task already added in its bundle;

-   an acceptance <tt>Accept</tt>(O<sub>i</sub>) sent by a peer. The agent
    replies to this obsolete message with a withdrawal
    and sends to itself a trigger;

-   an obsolete rejection <tt>Reject</tt>(O<sub>i</sub>) or an information
    (<tt>Inform</tt>(c(J,&nu;<sub>j</sub>))) sent by a
    peer. The agent sends to itself a trigger `Next` to reactivate its
    offer strategy.

In the state `Contractor`, the agent can receive the following
messages:

-   a confirmation <tt>Confirm</tt>(&delta;(&tau;, &nu;<sub>i</sub>, &nu;<sub>j</sub>, A))
    which leads the agent to add the task in its bundle, inform its peers of the job costs 
    for it, unstash the messages it had previously stashed,
    send to itself a trigger and go to the state `Responder`;

-   a withdrawal <tt>Withdraw</tt>(O<sub>j</sub>) to indicate that its
    previous acceptance was obsolete, leads the agent to unstash the messages
    previously stashed, send to itself a trigger
    and go to the state `Responder`;

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is stashed to
    be processed in the state `Responder`;

-   an acceptance <tt>Accept</tt>(O<sub>i</sub>) sent by a peer. The agent
    replies to this obsolete acceptance with a withdrawal 
    and stays in the same state;

-   an obsolete rejection <tt>Reject</tt>(O<sub>i</sub>) or an information
    (<tt>Inform</tt>(c(J, &nu;<sub>j</sub>)) sent by a
    peer, which required no processing;

-   a trigger `Next` which is ignored because this message will be sent
    once again before going to the state `Responder`;

-   an obsolete reactivation <tt>WakeUp</tt>(O<sub>i</sub>) which
    is ignored.

In the current state `Proposer`, the agents can receive the following
messages:

-   an acceptance <tt>Accept</tt>(O<sub>i</sub>),

    -   if this is the last offer of the agent,

        -   which is still valid,
            then the agent deletes the task from its bundle,
            informs its peers of the job costs for it and,
            replies with a confirmation,

        -   which is no longer valid since the task has been
            executed in the meantime, then the agent replies with a
            withdrawal,

        in any case, the agent unstashes the messages previously stashed, 
        it reactivates its offer strategy by sending to
        itself a trigger and, it goes to the state `Responder`,

    -   if this is not the last offer of the agent, it replies with a
        withdrawal and stays in the state `Proposer`;

-   a rejection <tt>Reject</tt>(O<sub>i</sub>),

    -   if this is the last offer of the agent, the agent unstashes the
        messages previously stashed, sends to
        itself a trigger and goes to the state `Responder`;

    -   if this is not the last offer of the agent, the agent stays in the state
        `Proposer` ;

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is stashed to
    be processed in the state `Responder` ;

-   a trigger `Next` which is ignored since this message will be sent
    once again when going to the state `Responder`;

-   an obsolete reactivation <tt>WakeUp</tt>(O<sub>i</sub>) or an
    <tt>Inform</tt>(c(J, &nu;<sub>j</sub>)) which is
    ignored.

In order to avoid deadlock, the state `Proposer` is associated with a
deadline (`Timeout`). The donor considers the absence of a reply to the
proposal as a rejection.

In the state `Pause`, the agent can receive the following
messages:

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is stashed to be processed in the state 
    `Responder;

-   an acceptance <tt>Accept</tt>(O<sub>i</sub>), a rejection
    <tt>Reject</tt>(O<sub>i</sub>) or an information
    <tt>Inform</tt>(c(J, &nu;<sub>j</sub>) is handled the
    same way as in the state `Responder`;

-   a trigger `Next` sent by itself to initiate its offer strategy,

    -   if there is a triggerable delegation (O<sub>i</sub> &ne; &theta;), 
        then it sends to
        itself a reactivation to
        send this offer in the state `Responder`,

    -   if there is no triggerable delegation (O<sub>i</sub> = &theta;),
        then the agent sends to itself a`Close` message and stays in the 
        state `Pause`;

-   a `Close` message indicating its mailbox is empty, then the agent sends a
    message <tt>End</tt>(B<sub>i</sub>) to the supervisor and move to
    the state `Final`.

If the agent receives a message in the state `Final`, it sends
a `Restart` message to the supervisor, and the received message is
stashed to be processed in the state `Pause`.

It is worth noticing the reception of the messages <tt>Propose</tt>, 
<tt>Accept</tt>, <tt>Reject</tt>, <tt>Confirm</tt> or <tt>Withdraw</tt> 
from a peer updates the belief base of the agent. In the same way, 
beyond the states `Init` and `Waiting`, the
messages `Inform` received in all the other states update the belief base. 
It is also worth noticing that if the agent receives a `Close` message
in another state than `Pause`, the message is ignored. For clarity,
these cases are not represented in the automaton.

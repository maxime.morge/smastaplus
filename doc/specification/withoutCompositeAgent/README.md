# Overview 

SMASTA+ is a Scala implementation of the Extended Multi-agents Situated Task Allocation.
Its installation and usage are explained [here](../../README.md). 

In this document, we give an overview of our framework.
The [negotiation behaviour](./negotiator.md) is described here.
The [supervisor behaviour](./supervisor.md) is described here.
The [manager behaviour](./manager.md) is described here.

## Big Picture

We consider two kinds of agents:
- the node agents, each of them representing a computing node by managing
its bundle. They are involved in task reallocation. They are composite agents
as described below. The  locality of the resources are depicted by grey circles.
- the supervisor which synchronize the negotiation stages.

![](bigPicture.jpg)

## Negotiation Protocol Specification

To achieve task reallocations, agents are involved in multiple bilateral negotiations.
Each negotiation is based on an alternating offer protocol, including four decision steps:
1. the proposer's offer strategy, which selects a delegation based on a delegation (a list of tasks in its bundle) and a responder
2. the counter-offer strategy that allows the responder to determine whether to decline the delegation, accepts it, or even makes a counter-offer
3. the possible confirmation or withdrawal of the endowment by the proposer depending on its consumption that has taken place concurrently
4. the possible confirmation or withdrawal of the counterpart by the responder depending on its consumption


![](protocol.svg)




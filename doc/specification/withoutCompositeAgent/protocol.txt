/**
Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
The command line to generate protocol.latex
java -jar ~/Logiciels/plantuml-1.2022.5.jar protocol.txt -tlatex
The command line to generate protocol.svg
java -jar ~/Logiciels/plantuml-1.2022.5.jar protocol.txt -tsvg
*/
@startuml
participant Proposer
participant Responder
autonumber
hide footbox
alt Proposer finds a socially rational delegation
Proposer -> Responder : Propose(delegation)
alt Responder expands on, with a socially rational swap
autonumber stop
autonumber 2 1
Responder -> Proposer : CounterPropose(swap)
         alt The endowment is up-to-date
         autonumber stop
         autonumber 3 2
         Proposer -> Responder : ConfirmSwap(swap)
                  alt The counterpart is up-to-date
                   autonumber stop
                   autonumber 4 3
                  Responder -> Proposer : ConfirmSwap(swap)
                  else The counterpart is deprecated
                   autonumber stop
                   autonumber 4 3
                  Responder -> Proposer : WithdrawSwap(swap)
                  end
         else The endowment is deprecated
         autonumber stop
         autonumber 3 2
                  Proposer -> Responder : Withdraw(delegation)
         end
else Responder finds no socially rational swap but the delegation is
autonumber stop
autonumber 2 1
Responder -> Proposer : Accept(delegation)
         alt The endowment is up-to-date
         autonumber stop
         autonumber 3 2
         Proposer -> Responder : Confirm(delegation)
         else The endowment is deprecated
         autonumber stop
         autonumber 3 2
         Proposer -> Responder : Withdraw(delegation)
         else
         end
else The delegation is not socially rational according to Responder
autonumber stop
autonumber 2 1
Responder -> Proposer : Reject(delegation)
end
end
@enduml

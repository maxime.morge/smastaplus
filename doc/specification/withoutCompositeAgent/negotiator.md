# Negotiation Behaviour Specification

In this document, we specify
the [negotiator behaviour](../../../src/main/scala/org/smastaplus/balancer/deal/mas/nodeAgent/negotiator/NegotiationBehaviour.scala).
The [supervisor behaviour](./supervisor.md) is described here.
The [manager behaviour](./manager.md) is described here.

## Overview

In our approach, the task reallocation is the outcome of negotiations
between agents adopting the same behaviour: they
alternatively play the roles of proposer, responder and contractor.
The agents execute this behaviour according to their [knowledge and
beliefs](../../../src/main/scala/org/smastaplus/balancer/deal/mas/nodeAgent/negotiator/NegotiatingMind.scala). 

The negotiation process is triggered by the
[manager](../../../src/main/scala/org/smastaplus/balancer/deal/mas/nodeAgent/manager/ManagerBehaviour.scala)
which commission the negotiator with a task bundle using a
message <tt>Commission</tt>(B<sub>i</sub>) and waits for the bundle after the
reallocation through the message <tt>End</tt>(B<sub>i</sub>). TODO The supervisor
also indicates to the agent whether we are in first or second stage. 
The negotiation behavior is specified by a
deterministic finite state automaton with the following states:

-   `Initial` where the agent is triggerable by the
    `supervisor`;

-   `Waiting` where the agent is waiting to receive the job 
costs for each peer in order to initiate its peer modelling;

-   `Responder` where the agent replies to the proposals from 
its peers;

-   `Proposer` where the agent has sent a proposal for which it 
waits for a counter-proposal, an acceptance or a rejection 
before a deadline;

- `Swapper` where the agent has confirmed its acceptance for a swap
and its waits for the double-confirmation from its peer;

-   `Contractor` where the agent has accepted a proposal or make
 a counter-proposal and its waits for a confirmation or withdrawal;

-   `Pause` where the offer strategy suggests none delegation,
 and the belief base is updated until a new opportunity (i.e. a 
 potential delegation) is found.

-   `EndStage` where the agent is ready to stop in the current 
stage;

-   `TransitionState` is where the agent is waiting for the next
stage to start.

The state transitions are such as:
```
 Event if conditions => actions.
```
They are triggered by an external event, e.g. the reception of a
message. The automaton is deterministic, i.e. the triggering
conditions of outgoing transitions for the same state are 
mutually exclusive. This behavioral design pattern allows 
handling multi-party dialogues.

Beyond the knowledge/beliefs of the agent and its peer 
modelling, the
[agent mind](../../src/main/scala/org/smastaplus/balancer/deal/decentralized/nodeAgent/negotiator/NegotiatingMind.scala) 
contains two variables to manage the negotiation process :

- `informers` is the set of peers which have sent `Inform` messages in order to
initiate its peer modelling;

- `lastOffer` is the delegation suggested in the last proposal;

- `isProactive` is true if the proactive behaviour of the agent as a proposer
  is activated;

- `isDesperated` is true if the triggering of the proactive behaviour fails
since a new offer cannot be proposed;

- `isFirstStage` is true if the agent is in the first stage, false otherwise.



For clarity, we split the negotiator behaviour 
in two part :
1. the initialisation
2. the negotiation

## Initialisation

In the figure below, the sending of messages is denoted `!`.
While `stash` enables an agent to temporarily stash away 
messages, `unstashAll` prepends all messages in the stash to the 
mailbox. The original sender is maintained if the agents 
`forward` messages.

![](figures/behaviourInit.svg)


In the state `Initial`, the negotiator can receive the following 
messages:

-   an enlightenment <tt>Light</tt>(directory) from the 
supervisor which informs the negotiator about its acquaintances;

-   an information about the task bundle 
<tt>GiveUpdatedBundle</tt>(B<sub>i</sub>) from the `manager`, which leads
the negotiator to update the belief base, then:

    -   if the peer modelling is complete, the negotiator can 
    unstash the messages previously stashed and send
    itself a trigger to evaluate the offer strategy in the
    `Responder` state,

    -   if the peer modelling is incomplete, the negotiator
     waits for some `Inform` messages in the state `Waiting`;


-   an information <tt>Inform</tt>(c(J,&nu;<sub>j</sub>))
    from the peer &nu;<sub>j</sub> indicating the cost of each
    job in its bundle which leads the negotiator to update 
    <tt>informers</tt> and its belief base;

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is stashed 
    until the `Responder` state, then the negotiator stays in the 
    state `Initial`.

In the state `Waiting`, the negotiator can receive the following messages:

-   an information <tt>Inform</tt>(c(J,&nu;<sub>j</sub>))
    from the peer &nu;<sub>j</sub> indicating the job costs for 
    it,

    -   if the peer modelling is complete, the negotiator can 
     unstash the messages it had previously stashed 
    and send to itself a trigger in order to evaluate the offer
    strategy in the `Responder` state,

    -   if the peer modelling is incomplete, the negotiator
    updates the `informers`, its belief base, and it stays in
    the state `Waiting`;

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is, as in 
the state `Initial`, stashed until the state `Responder`. Then,
 the negotiator stays in the state `Waiting`.

## Negotiation

![](figures/behaviourNegotiator.svg)

In the state `Responder`, the negotiator can receive the following
messages:

-   a trigger `Next` from itself to initiate the offer strategy,

    -   if there is no triggerable delegation
        (O<sub>i</sub> = &theta;), it sends to itself a
        `Close` and goes to the state `Pause`,

    -   if there is a triggerable delegation
        (O<sub>i</sub> &ne; &theta;), it sends a proposal, starts
        its timer and goes to the state `Proposer`;

-   a reactivation message <tt>WakeUp</tt>(O<sub>i</sub>) sent by
    itself to indicate that a delegation has been selected by
    the offer strategy, then its sends a proposal, starts its timer
     and goes to the state `Proposer`;

- an update <tt>GiveUpdatedBundle</tt> from the manager which leads the 
negotiator to update its belief base and to send itself a trigger 
<tt>Next</tt>;

-   a proposal <tt>Propose</tt>(&delta;(&tau;, &nu;<sub>j</sub>, &nu;<sub>i</sub>, A)) 
sent by a peer,

    - if the reallocation is new and this is the first stage of negotiation
    or if there is no triggerable swap, but the delegation is
    acceptable, then the negotiator replies with an acceptance and
    goes to the state `Contractor`,

    - if the reallocation is new, this the second stage of negotiation
    and there exists a triggerable swap with the proposer and
    the concerned task 
    (O<sub>i</sub>(&nu;<sub>j</sub>,&tau;)&ne; &theta;), 
    the negotiator sends a counter-proposal and goes to the state
    `Contractor`,

    - if the proposal is not new, or it is not acceptable and
    this is the first stage of negotiation or if there is no triggerable
    swap, then the negotiator replies with a rejection, sends to
    itself a trigger and stays in the state `Responder`.

    In any case, it is required to check if the offer is new
    because the negotiator may receive several times a proposal for
    the same task. This verification prevents from evaluating an
    obsolete proposal for a task already added in its bundle;

-   an acceptance <tt>Accept</tt>(O<sub>i</sub>) or a
counter-proposal 
<tt>CounterPropose</tt>(O<sub>i</sub>(&nu;<sub>j</sub>,&tau;))
sent by a peer. The negotiator replies to this obsolete message 
with a withdrawal and sends to itself a trigger;

-   an obsolete rejection <tt>Reject</tt>(O<sub>i</sub>) or an
information (<tt>Inform</tt>(c(J,&nu;<sub>j</sub>))) sent by a
peer. The negotiator sends to itself a trigger `Next` to 
reactivate its offer strategy.

In the state `Contractor`, the negotiator can receive the following
messages:

<!-- - an update <tt>GiveUpdatedBundle</tt> from the manager which leads the 
negotiator to update its belief base; -->

-  a confirmation <tt>Confirm</tt>(&delta;(&tau;, &tau;, &nu;<sub>j</sub>, &nu;<sub>i</sub>, A)) 
    of a delegation leading the negotiator to send an <tt>Add</tt> 
    message to the manager and to wait for the <tt>GiveUpdatedBundle</tt> 
    reply to update its view on the bundle. Then it unstashes the messages it had 
    previously stashed, sends to itself a trigger and goes to the 
    state `Responder`;

- a confirmation <tt>ConfirmSwap</tt>(&sigma;(&tau;, &tau;<sub>swap</sub>, &nu;<sub>j</sub>, &nu;<sub>i</sub>, A))
    of a swap which is stil valid leading the negotiator to send a <tt>Replace</tt> 
    message to the manager to ask to remove its own task by 
    the task from the peer in its bundle, and it waits for the 
    <tt>GiveUpdatedBundle</tt> reply to update its view on the bundle. 
    Then it answers with a second 
    <tt>Confirm</tt>(&sigma;(&tau;, &tau;<sub>swap</sub>, &nu;<sub>j</sub>, &nu;<sub>i</sub>, A)),
    it unstashes the messages it had previously stashed,
    sends to itself a trigger and goes to the state `Responder`;

- a confirmation <tt>ConfirmSwap</tt>(&sigma;(&tau;, &tau;<sub>swap</sub>, &nu;<sub>j</sub>, &nu;<sub>i</sub>, A))
    of a swap which is no longer valid (since &tau;<sub>swap</sub> 
    is no longer in the bundle) leading the negotiator to answer with 
    a withdrawal <tt>WithdrawSwap</tt>(&sigma;(&tau;, &tau;<sub>swap</sub>, &nu;<sub>j</sub>, &nu;<sub>i</sub>, A)).
    Then it unstashes the messages it had previously stashed, sends to 
    itself a trigger and goes to the state `Responder`;

-   a withdrawal <tt>Withdraw</tt>(O<sub>j</sub>) to indicate
that its previous acceptance was obsolete, leads the negotiator to
unstash the messages previously stashed, send to itself a 
trigger and go to the state `Responder`;

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is stashed
to be processed in the state `Responder`;

-   an acceptance <tt>Accept</tt>(O<sub>i</sub>) or a
counter-proposal <tt>CounterPropose</tt>(O<sub>i</sub>)) sent 
by a peer. The negotiator replies to this obsolete message with a
withdrawal and stays in the same state;

-   an obsolete rejection <tt>Reject</tt>(O<sub>i</sub>) or 
an information (<tt>Inform</tt>(c(J, &nu;<sub>j</sub>)) sent 
by a peer, which required no processing;

-   a trigger `Next` which is ignored because this message will
be sent once again before going to the state `Responder`;

-   an obsolete reactivation <tt>WakeUp</tt>(O<sub>i</sub>) which
    is ignored.

In the current state `Proposer`, the negotiator can receive the
following messages:

<!-- - an update <tt>GiveUpdatedBundle</tt> from the manager which leads the 
negotiator to update its belief base; -->

-   an acceptance <tt>Accept</tt>(O<sub>i</sub>),

    -   if this is the last offer of the negotiator,

        -   which is still valid, the negotiator replies to its 
            peer with a confirmation, then it asks the manager to 
            remove the task from the bundle with a <tt>Remove</tt>(&tau;) 
            message and waits for the <tt>GiveUpdatedBundle</tt> reply
            to update its view on the bundle,

        -   which is no longer valid since the task has been
            executed in the meantime, then the negotiator replies
            with a withdrawal,

        in any case, the agent resets its `lastOffer`, it unstashes the messages 
        previously stashed, it reactivates its offer strategy by sending to
        itself a trigger, it cancels its timer and, it goes to the state `Responder`,

    -   if this is not the last offer of the agent, it replies
    with a withdrawal and stays in the state `Proposer`;

- a counter-proposal <tt>CounterPropose</tt>(O<sub>i</sub>(&nu;<sub>j</sub>,&tau;)),
    -   if this swap is an expansion of the last offer of the
    agent which is still valid,
    
        - if the swap is acceptable then the negotiator asks the 
        manager to remove task in the bundle with a 
        <tt>Remove</tt> message and so it waits for the 
        <tt>GiveUpdatedBundle</tt> reply to update its view on the bundle.
         Then the negotiator replies to its peer with a confirmation 
         <tt>ConfirmSwap</tt>(O<sub>i</sub>(&nu;<sub>j</sub>,&tau;)), it
         cancels its timer and goes to the state `Swapper`,

        - if the swap is not acceptable, the negotiator replies 
        with a withdrawal, sends to itself a trigger, cancels its
         timer, and goes to the state `Responder`;
    
    - in all the other cases, the behaviour is the same as when
    receiving an acceptance message;
      
-   a rejection <tt>Reject</tt>(O<sub>i</sub>),

    -   if this is its last offer, the negotiator
    unstashes the messages previously stashed, sends to
    itself a trigger, cancels its timer and goes to the state `Responder`;

    -   if this is not its last offer, the negotiator
    stays in the state `Proposer` ;

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) which is randomly 
stashed to be processed in the state `Responder` ;

-   a trigger `Next` which is ignored since this message will 
be sent once again when going to the state `Responder`;

-   an obsolete reactivation <tt>WakeUp</tt>(O<sub>i</sub>) or 
an <tt>Inform</tt>(c(J, &nu;<sub>j</sub>)) which is ignored.

In order to avoid deadlock, the state `Proposer` is associated
with a deadline (`Timeout`). The donor considers the absence of
a reply to the proposal as a rejection.


In the state `Swapper`, the negotiator can receive the 
following messages:

- a confirmation <tt>ConfirmSwap</tt>(O<sub>i</sub>(&nu;<sub>j</sub>,&tau;)),
then the negotiator asks the manager to add the counterpart task 
from the peer with a message <tt>Add</tt>(&tau;<sub>swap</sub>) and to 
wait for the <tt>GiveUpdatedBundle</tt> reply to update its view on the bundle. 
Then it resets the last offer, it unstashes all the previously 
stashed messages and it sends itself a trigger <tt>Next</tt> 
before moving to the state `Responder`;

- a withdrawal <tt>WithdrawSwap</tt>(O<sub>i</sub>(&nu;<sub>j</sub>,&tau;)),
then the negotiator asks the manager to add the task &tau; it has 
previously removed in the bundle by sending a message 
<tt>Add</tt>(&tau;) (and it waits for the <tt>GiveUpdatedBundle</tt> reply
to update its view on the bundle), it resets the last offer, it unstashes all
 the previously stashed messages and it sends itself a trigger
  <tt>Next</tt> before moving to the state `Responder`;

- Other messages such as a <tt>GiveUpdatedBundle</tt> from the manager, a 
<tt>Propose</tt>(O<sub>j</sub>), an <tt>Inform</tt>(c(J, &nu;<sub>j</sub>)), 
or obsolete reactivation <tt>WakeUp</tt>(O<sub>i</sub>), rejection
<tt>Reject</tt>(O<sub>i</sub>) and <tt>CounterProposal</tt>(O<sub>i</sub>)
are handled the same way as in the `Proposer` state;


In the state `Pause`, the negotiator can receive the following
messages:

<!-- - an update <tt>GiveUpdatedBundle</tt> from the manager which leads the 
negotiator to update its belief base.  If the proactive behaviour 
of the agent as a proposer is not activated, then it is 
reactivated and the agent sends itself a trigger <tt>Next</tt>; -->

-   a proposal <tt>Propose</tt>(O<sub>j</sub>) leading the
negotiator to move to the state `Responder` to process it;

-   an acceptance <tt>Accept</tt>(O<sub>i</sub>) or a 
counter-proposal <tt>CounterProposal</tt>(O<sub>i</sub>) 
which are obsolete lead the agent to reply with a withdrawal. 
If the proactive behaviour of the agent as a proposer
is not activated, then it is reactivated and the agent sends itself a trigger;

- a rejection <tt>Reject</tt>(O<sub>i</sub>), or an 
    information <tt>Inform</tt>(c(J, &nu;<sub>j</sub>) do not 
    require a reply. If the proactive behaviour of the agent as 
    a proposer is not activated, then it is reactivated and the agent sends itself a trigger;

-   a trigger `Next` sent by itself to initiate its offer
strategy,

    -   if there is a triggerable delegation 
    (O<sub>i</sub> &ne; &theta;), then it resets its `isProactive`
    and `isDesperate` boolean, it sends to
        itself a reactivation <tt>Wake</tt>(O<sub>i</sub>) to 
        send this offer in the state `Responder`,

    -   if there is no triggerable delegation 
    (O<sub>i</sub> = &theta;), then the negotiator sends to itself 
    a `Close` message and stays in the state `Pause`;

-   a `Close` message indicating its mailbox is empty, then 
the negotiator informs the manager with a message 
<tt>FinishedStage</tt> and moves to the `TransitionState`;


In the state `EndStage`, the negotiator can receive the following
messages:

<!-- - an update <tt>GiveUpdatedBundle</tt> from the manager which leads the 
negotiator to update its belief base and to send itself a trigger
<tt>Next</tt>; -->

- a <tt>ReadyForNextStage</tt> from the manager which leads the 
negotiator to move to the `TransitionState`;

- an obsolete <tt>Reject</tt> is ignored;

If the negotiator receives another type message in the state
`EndStage`, it sends a `Restart` message to the manager, 
and the received message is stashed to be processed in the 
state `Pause`.


In the state `TransitionState`, the negotiator can receive the
following messages:

- a proposal <tt>Propose</tt>(O<sub>j</sub>) which is stashed 
until it returns to the `Responder` state;

- a message <tt>StartStage</tt>
initiates a new stage of the negotiation process. 
The agent then changes the `isFirstStage` variable, it
resets its metrics and its old proposals, it unstashes 
the previously stashed messages and it sends itself a 
trigger <tt>Next</tt> before moving to the `Responder` stage;

<!-- - an update <tt>GiveUpdatedBundle</tt> from the manager which leads the 
negotiator to update its belief base; -->

It is worth noticing the reception of the messages 
<tt>Propose</tt>, <tt>Accept</tt>, <tt>Reject</tt>, 
<tt>Confirm</tt> or <tt>Withdraw</tt> from a peer updates the
belief base of the negotiator. In the same way, beyond the states
`Initial` and `Waiting`, the messages `Inform` received in all the
other states update the belief base. 
It is also worth noticing that if the negotiator receives a `Close`
message in another state than `Pause`, the message is ignored.
For clarity, these cases are not represented in the automaton.

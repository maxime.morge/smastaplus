# Manager Behaviour Specification

In this document, we specify
the [manager behaviour](../../src/main/scala/org/smastaplus/balancer/deal/mas/nodeAgent/manager/ManagerBehaviour.scala).
The [negotiator behaviour](./negotiator.md) is described here. The [worker behaviour](./worker.md) is described here.

## Overview

In our approach, the manager is in charge of managing the bundle. It 
schedules the consumption process by giving the most prior task
to the worker and so it manages the transitions between the 
different negotiation phases by communicating with both
the negotiator and the supervisor.  
<!-- The agents execute this behaviour according to their [knowledge and
beliefs](../../src/main/scala/org/smastaplus/strategy/deal/MindWithPeerModelling.scala).  -->

The negotiation process is triggered by the
[supervisor](../../src/main/scala/org/smastaplus/balancer/deal/mas/supervisor/Supervisor.scala)
which initiates the bundle of tasks for each agent &nu;<sub>i</sub> using a
message <tt>Give</tt>(B<sub>i</sub>) and waits for the sorted bundle after the
reallocation through the message <tt>End</tt>(B<sub>i</sub>). The supervisor
also indicates to the agent whether we are in first or second stage. 
The manager agent behavior is specified by a
deterministic finite state automaton with the following states:

-   `Initial` where the agent is triggerable by the
    `supervisor`;

-   `Running` where the manager distribute tasks to the worker and 
    when the negotiations are in progress;

<!-- -   `Inactive` where the agent's bundle is empty but the negotiations
with other agents are not over; -->

-   `Pause` where there is still tasks in the bundle which are waiting
to be consumed but the negotiation process is over;

-   `EndStage` where the agent is ready to stop in the current 
stage;

-   `TransitionState` is where the agent is waiting for the next
stage to start.

The state transitions are such as:
```
 Event if conditions => actions.
```
They are triggered by an external event, e.g. the reception of a
message. The automaton is deterministic, i.e. the triggering
conditions of outgoing transitions for the same state are 
mutually exclusive. This behavioral design pattern allows 
handling multi-party dialogues.


![](figures/behaviourManager.svg)

In the state `Initial`, the manager can receive the following
 message:

-   an enlightenment <tt>Light</tt>(directory) from the 
supervisor which informs the manager about its acquaintances. 
The manager updates the directory and answers the supervisor 
with a <tt>Ready</tt>;

-   an initialization <tt>Give</tt>(B<sub>i</sub>) from the 
`supervisor` of the task bundle, then the manager can sort 
    its bundle, send an update <tt>GiveUpdatedBundle</tt>(B<sub>i</sub>)
    to the negotiator and move to the `Running` state.


In the state `Running`, the manager can receive the following
messages:

- a <tt>FinishedStage</tt> from the negotiator, then 
the manager sends an <tt>End</tt>(B<sub>i</sub>) to the 
supervisor and moves to the `EndStage` stage;

- an <tt>Add</tt>(&tau;), <tt>Remove</tt>(&tau;) or
<tt>Replace</tt>(&tau;<sub>swap</sub>, &tau;) from 
the negotiator, leading the manager to update the bundle 
by adding or removing the corresponding tasks, send the update 
<tt>GiveUpdatedBundle</tt>(B<sub>i</sub>) to the negotiator 
and send <tt>Inform</tt> to the peers about the new costs of
jobs.


<!-- In the state `Inactive`, the agent can receive the following
message:

- a <tt>FinishedStage</tt> stage from the negotiator, then 
the manager sends an <tt>End</tt>(B<sub>i</sub>) to the 
supervisor and moves to the `EndStage` stage; -->


In the state `EndStage`, the manager can receive the following
messages:

- a <tt>Query</tt> from the supervisor, then the manager
replies by sending it an <tt>Answer</tt>(metrics) containing 
its metrics;

- a <tt>Restart</tt> from the negotiator, then the manager
sends a <tt>Restart</tt> message to the supervisor before moving
to the `Running` state if the bundle is not empty or to the 
`Inactive` state otherwise;

- a message <tt>PreTrigger</tt>  which
initiates a new stage of the negotiation process. 
Then the manager sets its `isFirstStage` variable to True, it
resets its metrics and its old proposals, and sends a
<tt>readyForNextStage</tt> message to the supervisor before
moving to the `TransitionState`;

- a message <tt>PreTrigger</tt> in the first stage which
initiates a new second stage of the negotiation process. Then
the manager sends a <tt>readyForNextStage</tt> to the
supervisor and to the negotiator before moving to the 
`TransitionState`;

- <tt>Kill</tt> message from the supervisor which 
leads the manager to answer with a <tt>Finished</tt> message
before ending.

In the state `TransitionState`, the manager can receive the
following messages:

- a <tt>Done</tt> from the worker indicating it has finished its
 task which is stashed until the `Running` state;

- a <tt>Trigger</tt> from the supervisor informing the agent
that everyone is ready for the next stage. The manager sends 
a `StartStage` message to the negotiator. Then it unstashes 
previously stashed messages before moving to the `Running` state.
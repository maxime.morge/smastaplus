# Overview

SMASTA+ is a Scala implementation of the Extended Multi-agents Situated Task Allocation.
Its installation and usage are explained [here](../README.md).

In this document, we provide the instructions of the NodeJS monitoring interface.

## Installation

You need to install NodeJS. For instance with snap :

    sudo snap install node --classic

Copy the monitoring directory to nodejs. Then, complete the installation :

    cd monitoring
    npm init
    sudo npm install --save express
    sudo npm install express-generator --global
    sudo npm install --save nocache

## Usage

Start the server from nodejs/monitoring :

    npm run start

The server is now running at http://localhost:3000/

## Monitoring files

The initial allocation is in the file `public/alloc/allocation.json`. 
The files for the task bundles of the different computing nodes are in `public/alloc/trace`.

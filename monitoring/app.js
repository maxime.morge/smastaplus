const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
//const logger = require('morgan');
const nocache = require('nocache');

// routers
const indexRouter = require('./routes/index');
const monitorRouter = require('./routes/monitor');
const dataRouter = require('./routes/data');
const traceRouter = require('./routes/trace');
const errorRouter = require('./routes/error');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(nocache());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// routes for this application
app.use('/', indexRouter);
app.use('/monitor',monitorRouter);
app.use('/data',dataRouter);
app.use('/trace',traceRouter);

// else error
app.use(errorRouter)

module.exports = app;

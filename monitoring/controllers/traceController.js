const path = require('path');
const fs = require('fs');
const { json } = require('express');
const { emitKeypressEvents } = require('readline');
const dirTrace = path.join(__dirname + '/../public/alloc/trace/');

class TraceController{

  constructor(){
    this.jsonObject = undefined;
    this.load = this.load.bind(this);
  }
  
  /* send a json document with the tasks and the costs, from files in dirTrace (one file per node).
   * For instance :
   { "costs": [
      {"node":"cn1","t9":1,"t7":4,"t6":6},
      {"node":"cn2","t1":3,"t2":5},
      {"node":"cn3","t3":1,"t8":2,"t5":2,"t4":6}],
     "tasks" : ["t9","t7","t6","t1","t2","t3","t8","t5","t4"]
   }
   */
  load(req, res, next) {
    fs.readdir(dirTrace, (err, fileNameTab) => {
      if (err) {console.log('traceControllerError in load : '+err) ;
                return next(err);}

    /* Read the directory asynchronously and return the fileNameTab array
     * through a callback function. Then, for each file name in fileNameTab,
     * we create a Promise that reads the file. We store these promises in an array called promises.
     * */
      const promises = fileNameTab.map((fileName) => {
        if (fileName.search('json') >= 0) // to read only json files
          return new Promise((resolve, reject) => {
            fs.readFile(dirTrace + fileName, (err, data) => {
              if (err) return reject(err);
              resolve(data.toString());
            });
          });
        else return new Promise((resolve, reject) => {resolve("")});
      });

      /* Wait for all the promises to resolve, and then process
       * the resulting array of jsonStringCostsArray strings. 
       * Once we have the stringTabTasks and stringTabCosts arrays, 
       * we parse them into a JSON object and send it back as a response.
       * */
      Promise.all(promises)
        .then((jsonStringCostsArray) => {
          let stringTabCosts = "[";
          let stringTabTasks = "[";

          jsonStringCostsArray.forEach((jsonStringCosts) => {
            if (jsonStringCosts.length > 0) stringTabCosts += jsonStringCosts + ",";
            // extract task names from jsonStringCosts
            if (jsonStringCosts.includes(','))// there is at least one task
              jsonStringCosts.split(",").slice(1).forEach(function (s) {
                var beginTaskName = s.indexOf('"');
                var endTaskName = s.slice(beginTaskName + 1).indexOf('"');
                stringTabTasks += s.slice(beginTaskName - 1, beginTaskName + endTaskName + 2) + ","; // for instance t2
              });
          });
          
          // end the strings stringTabCosts and stringTabTasks
          if (stringTabCosts.length > 1)  // at least one task, so remove ',' and add ']'
            stringTabCosts = stringTabCosts.slice(0, stringTabCosts.length - 1) + "]";
          else stringTabCosts += ']' // no task, so empty array
          if (stringTabTasks.length > 1)
            stringTabTasks = stringTabTasks.slice(0, stringTabTasks.length - 1) + "]";
          else stringTabTasks += ']' 
          this.jsonObject = JSON.parse(
              '{ "costs": ' + stringTabCosts + ', "tasks": ' + stringTabTasks + "}"
          );
          res.send(this.jsonObject);
        })
        .catch((err) => 
          console.log('traceControllerError : '+err)
          );
    });
  }
}

module.exports = new TraceController();


const path = require('path');
const fs = require('fs');
const fileName = path.join(__dirname + '/../public/alloc/allocation.json');

class DataController{
  constructor(){
    this.jsonObject = undefined;
    this.load = this.load.bind(this);
  }

  // send a json document with the description of an allocation
  load(req, res, next) {
    const promise = new Promise((resolve, reject) => {
        fs.readFile(fileName, (err, data) => {
          if (err) return reject(err);
          resolve(data.toString());
        });
      });
  
    promise.then((jsonString) => {
      this.jsonObject = JSON.parse(jsonString) ;
      res.send(this.jsonObject)
    }).catch((err) => console.log('dataControllerError : '+err));
    
  }
  
}

module.exports = new DataController();


// Copyright (C) Anne-Cécile Caron, Maxime MORGE 2023

// Global variables are
let tasks ; // the names of tasks
let taskJobs = new Map(); // the map between the name of tasks and their job ids
let jobNames; // the names of jobs
let costs; // the cost for each task for each node, eventually 0.0 if the tasks is not allocated to the node
let oldStackMax = 0 // the vertical scale

// the list of colors for each jobs
const jobColors = ["#5862ed", "#32a68c", "#fc535c", "#7abaff", "#ff6941", "#000037", "#89e0b1","#ffb4d2","#ffd24b", "#002d2d", "#2d0505", "#000000" ];
// Indigo inclusif, Vert avenir, Rouge action, Bleu évasion, Orange chaleureux, Bleu horizon, Vert tendre, Rose accueillant,
// Jaune déterminé, Vert Nature, Marron durable, Noir

// Returns the color for a specific job name
function colorOfJob(name) {
    return jobColors[jobNames.indexOf(name)]
}

//Returns the job id for a task name
function jobOf(taskName) {
    return taskJobs.get(taskName)
}

// Creates the legend
// See https://gist.github.com/ChandrakantThakkarDigiCorp/2a9e6f11f2a27b6af0c28c4e5e524f10
function createChartLegend(mainDiv) {
    $(mainDiv).before("<div id='Legend' style='margin-top:0; margin-bottom:0;'></div>");
    jobNames.forEach(function (d) {
        $("#Legend").append(
            "<span style='display: inline-block; margin-right:10px;'>"
            + "<span style='background:"
            + colorOfJob(d) // colorCode
            + ";opacity: 0.6; width: 10px;height: 10px;display: inline-block;vertical-align: middle;'>&nbsp;</span>"
            + "<span style='padding-top: 0;font-family:Source Sans Pro, sans-serif;font-size: 13px;display: inline;'>"
            + d
            + "</span></span>");
    });
}


var mainDiv = "#charts";// Name of the div in HTML
var divName = "charts";

// Returns the flowtime of the bundle d in order to stack histograms
function type(d) {
    tasks.forEach(function (c) {
        d[c] = +d[c]; // d[c] is the cost of task c
    });
    return d;
}

// Scales
function stackMin(layers) {
    return d3.min(layers, function (d) {
        return d[0];
    });
}

// Scales constantly
function stackMax(layers) {
    let newMax = d3.max(layers, function (d) {return d[1];}) ;
    oldStackMax = Math.max(oldStackMax,  newMax) ;
    return oldStackMax;
}

// Gridlines in y axis function
function make_y_gridlines(y) {
    return d3.axisLeft(y)
        .ticks(5)
}

// Mai function
var createChart = function () {
    // d is the description of task costs for 1 node
    // d has the form { 'node': 'ν1', τ7:1.0, ..., τ9:0.0}
    // Tasks are ordered according to the consumption strategy
    costs.forEach(function (d) {
        d = type(d);
    });

    var layers = d3.stack()
        .keys(tasks)
        .offset(d3.stackOffsetDiverging)(costs);

    var svg = d3.select("svg"),
        margin = {
            top: 20,
            right: 30,
            bottom: 60,
            left: 60
        },
        width = +svg.attr("width"),
        height = +svg.attr("height");

    var x = d3.scaleBand()
        .rangeRound([margin.left, width - margin.right])
        .padding(0.1);

    x.domain(costs.map(function (d) {
        return d.node;
    }))

    var y = d3.scaleLinear()
        .rangeRound([height - margin.bottom, margin.top]);

    y.domain([d3.min(layers, stackMin), d3.max(layers, stackMax)])

    var maing = svg.append("g")
        .selectAll("g")
        .data(layers);

    var g = maing.enter().append("g")
        .attr("fill", function (d, i) {
            return jobColors[jobOf(tasks[i])-1]; // Retuts the color of the ith task
        });

    var rect = g.selectAll("rect")
        .data(function (d) {
            d.forEach(function (d1) {
                d1.key = d.key;
                return d1;
            });
            return d;
        })
        .enter()
        .append("rect")
        .attr("data", function (d) {
            var data = {};
            data["key"] = d.key;
            data["value"] = d.data[d.key];
            var total = 0;
            tasks.map(function (d1) {
                total = total + d.data[d1]
            });
            data["total"] = total;
            return JSON.stringify(data);
        })
        .attr("width", x.bandwidth)
        .attr("x", function (d) {
            return x(d.data.node);
        })
        .attr("y", function (d) {
            return y(d[1]);
        })
        .attr("height", function (d) {
            return y(d[0]) - y(d[1]);
        })
        .style('opacity', 0.6)
        .style("stroke", "black")
        .style("stroke-width", 1);

    var text = g.selectAll("text")
        .data(function (d) {
            d.forEach(function (d1) {
                d1.key = d.key;
                return d1;
            });
            return d;
        })
        .enter()
        .append("text")
        .attr("data", function (d) {
            var data = {};
            data["key"] = d.key;
            data["value"] = d.data[d.key];
            var total = 0;
            tasks.map(function (d1) {
                total = total + d.data[d1]
            });
            data["total"] = total;
            return JSON.stringify(data);
        })
        .attr("class", "text")
        .attr("x", function (d) {
            return x(d.data.node) + x.bandwidth() / 2;
        })
        .attr("y", function (d) {
            return y(d[1]);
        })
        .style("text-anchor", "middle")
        .style("font-size", "12px")
        .style("fill", "black")
        .text(function (d) {
            if (d.data[d.key] != 0) return d.key;
            return "";
        });

    svg.append("g")
        .attr("transform", "translate(0," + y(0) + ")")
        .call(d3.axisBottom(x))
        .append("text")
        .attr("x", width / 2)
        .attr("y", margin.bottom * 0.5)
        .attr("dx", "0.32em")
        .attr("fill", "#000")
        .attr("font-weight", "bold")
        .attr("text-anchor", "start")
        .text("Nodes");

    svg.append("g")
        .attr("transform", "translate(" + margin.left + ",0)")
        .call(d3.axisLeft(y))
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", 0 - (height / 2))
        .attr("y", 15 - (margin.left))
        .attr("dy", "0.32em")
        .attr("fill", "#000")
        .attr("font-weight", "bold")
        .attr("text-anchor", "middle")
        .text("Workload");

    svg.append("g")
        .attr("class", "grid")
        .attr('transform', "translate(" + margin.left + ",0)")
        .attr("stroke-width", .5)
        .attr("fill", "none")
        .call(make_y_gridlines(y)
            .tickSize(-width)
            .tickFormat("")
        )
        .lower();


    var helpers = {
        getDimensions: function (id) {
            var el = document.getElementById(id);
            var w = 0,
                h = 0;
            if (el) {
                var dimensions = el.getBBox();
                w = dimensions.width;
                h = dimensions.height;
            } else {
                console.log("error: getDimensions() " + id + " not found.");
            }
            return {
                w: w,
                h: h
            };
        }
    };

    d3.select('#dl').on('click', function () {
        var config = {
            filename: 'ex3swap',
        }
        d3_save_svg.save(d3.select('svg').node(), config);
    });

}

// To force fetch function to reload the file => don't use the cache
let myHeaders = new Headers();
myHeaders.append('pragma', 'no-cache');
myHeaders.append('cache-control', 'no-cache');

let myInit = {
  method: 'GET',
  headers: myHeaders
};

async function readFirstData (){
        var response = await fetch('http://localhost:3000/data', myInit);
        while (! response.ok)
            response = await fetch('http://localhost:3000/data', myInit);
        return await response.json();
}

async function readData (){
    try{
        const response = await fetch('http://localhost:3000/trace', myInit);
        if (! response.ok)
            throw new Error('pb with a file during monitoring');
        return await response.json();
    }
    catch(error) {                                 
        console.log(error.message);
        return {}
    }
}

const emptyChart = function() {
    document.getElementById("charts").innerHTML = '<svg width="800" height="500"/>';
}

const refreshChart = function() {
    // reload data from json file and rebuild the chart
    readData().then(function(jsonObject) {
        if (jsonObject.costs && jsonObject.tasks){
            tasks = jsonObject.tasks ;
            costs = jsonObject.costs ;
            emptyChart() ;
            createChart();
        }
        
    }) ; 
}

const refreshChartAll = function() {
    // reload data from json file and rebuild the chart
        readFirstData().then(function(jsonObject) {
        tasks = jsonObject.tasks ; // The list of tasks
        //the map between the name of tasks and their job ids
        for (let i = 0; i < jsonObject.tasks.length; i++) {
          const task = jsonObject.tasks[i];
          const job =  jsonObject.jobs[i];
          taskJobs.set(task, job);
        }
        // The names of jobs
        jobNames = jsonObject.jobNames.sort() ;
        // The costs
        costs = jsonObject.costs ;
         }
        ).catch((err) => console.log('dataviz error : '+err)) ;
}

// Loads the data
const init = function () {
    readFirstData().then(function(jsonObject) {
    tasks = jsonObject.tasks ; // The list of tasks
    //the map between the name of tasks and their job ids
    for (let i = 0; i < jsonObject.tasks.length; i++) {
      const task = jsonObject.tasks[i];
      const job =  jsonObject.jobs[i];
      taskJobs.set(task, job);
    }
    // The names of jobs
    jobNames = jsonObject.jobNames.sort() ;
    // The costs
    costs = jsonObject.costs ; 
    createChartLegend(mainDiv);
    createChart();
    setInterval("refreshChart()",100); // each 100ms the chart is refreshed
    setInterval("refreshChartAll()",1000); // each 1s the allocation is refreshed
     }
    ).catch((err) => console.log('dataviz error : '+err)) ; 
}
window.addEventListener("load", init);




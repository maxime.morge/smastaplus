const express = require('express');
const router = express.Router();

// this router manage the root route "/trace"
const traceController = require('../controllers/traceController.js');

router.get('/', traceController.load );

module.exports = router;


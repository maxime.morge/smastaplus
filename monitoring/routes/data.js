const express = require('express');
const router = express.Router();

// this router manage the root route "/data"
const dataController = require('../controllers/dataController.js');

router.get('/', dataController.load );

module.exports = router;


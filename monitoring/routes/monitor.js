const express = require('express');
const router = express.Router();

// this router manage the root route "/monitor"
const monitorController = require('../controllers/monitorController');

router.get('/', monitorController.home );

module.exports = router;
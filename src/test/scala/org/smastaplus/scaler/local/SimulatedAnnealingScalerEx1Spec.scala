// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.scaler.local

import org.smastaplus.provisioning._

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the simulated annealing scaler
  */
class SimulatedAnnealingScalerEx1Spec extends AnyFlatSpec {

  val debug = false

  behavior of "HillClimbingScaler"

  "Simulated annealing scaling of the minimal stap example" should " return a just-in-need configuration" in {
    import org.smastaplus.example.allocation.stapMinimal.exMinimalAllocation._
    import org.smastaplus.example.stap.exMinimal._
    val problem = ProvisioningProblem(stap, 10.0, QoSFlowtime)
    val initialConfiguration = new Configuration(problem, stap.ds.computingNodes, a)
    val autoScaler = new SimulatedAnnealingScaler(problem)
    val configuration = autoScaler.rescale(initialConfiguration)
    if (debug) println(s"A configuration found by hill climbing scaling of the first stap example")
    assert( configuration.get == initialConfiguration)
    assert( configuration.get.isJustInNeed )
  }

  "Hill climbing scaling of the first stap example" should " return a just-in-need configuration" in {
    import org.smastaplus.example.stap.ex1._
    val problem = ProvisioningProblem(stap, 10.0, QoSFlowtime)
    val autoScaler = new SimulatedAnnealingScaler(problem)
    val configuration = autoScaler.scale()
    if (debug) println(s"No just-in-need configuration found by hill climbing scaling of the first stap example")
    assert(configuration.isEmpty)
  }

}
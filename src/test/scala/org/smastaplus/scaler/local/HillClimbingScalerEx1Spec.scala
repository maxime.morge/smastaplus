// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.scaler.local

import org.smastaplus.provisioning._

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the hill climbing scaler
  */
class HillClimbingScalerEx1Spec extends AnyFlatSpec {

  val debug = false

  behavior of "HillClimbingScaler"

  "Hill climbing scaling of the minimal stap example" should " returns the initial just-in-need configuration" in {
    import org.smastaplus.example.stap.exMinimal._
    import org.smastaplus.example.allocation.stapMinimal.exMinimalAllocation._
    val problem = ProvisioningProblem(stap, 10.0, QoSFlowtime)
    val initialConfiguration = new Configuration(problem, stap.ds.computingNodes, a)
    val autoScaler = new HillClimbingScaler(problem)
    val configuration = autoScaler.rescale(initialConfiguration)
    if (debug) println(s"A configuration found by hill climbing scaling of the first stap example")
    assert( configuration.get == initialConfiguration && configuration.get.isJustInNeed )
  }

  "Hill climbing scaling of the first stap example" should " returns no just-in-need configuration" in {
    import org.smastaplus.example.stap.ex1._
    val problem = ProvisioningProblem(stap, 10.0, QoSFlowtime)
    val autoScaler = new HillClimbingScaler(problem)
    val configuration = autoScaler.scale()
    if (debug) println(s"No just-in-need configuration found by hill climbing scaling of the first stap example")
    assert(configuration.isEmpty)
  }

}
// Copyright (C) Maxime MORGE 2022
package org.smastaplus.scaler

import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.provisioning._

/**
  * Unit test for the random scaler
  */
class RandomScalerEx1Spec extends AnyFlatSpec {

  behavior of "ExhaustiveScaler"

  "Exhaustive scaling of the first stap example" should " return a just-in-need configuration" in {
    import org.smastaplus.example.stap.ex1._
    val problem = ProvisioningProblem(stap, 10.0, QoSFlowtime)
    val autoScaler = new RandomScaler(problem)
    val configuration = autoScaler.scale()
    assert( configuration.get.isJustInNeed )
  }
}
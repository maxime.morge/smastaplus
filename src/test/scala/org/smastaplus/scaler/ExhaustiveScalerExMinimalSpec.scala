// Copyright (C) Maxime MORGE 2022
package org.smastaplus.scaler

import org.smastaplus.provisioning._

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the exhaustive scaler
  */
class ExhaustiveScalerExMinimalSpec extends AnyFlatSpec {

  behavior of "ExhaustiveScaler"

  "Exhaustive scaling of the minimal stap example" should " return a just-in-need configuration" in {
    import org.smastaplus.example.stap.exMinimal._
    import org.smastaplus.example.allocation.stapMinimal.exMinimalAllocation._
    val problem = ProvisioningProblem(stap, 10.0, QoSFlowtime)
    val autoScaler = new ExhaustiveScaler(problem)
    val configuration = autoScaler.scale()
    assert( configuration.get.isJustInNeed )
    assert( configuration.get.allocation == a )
  }
}
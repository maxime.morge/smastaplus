// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.core

import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.utils.MathUtils._

/**
  * Unit test for the initial allocation of the second stap example
  */
class ExecutedAllocationEx2Spec extends AnyFlatSpec {

  import org.smastaplus.example.stap.ex2._
  import org.smastaplus.example.allocation.stap2.ex2Allocation._

  behavior of "Example 1"

  "The workload" should " be the sum of the costs of the task in the bundles" in {
    assert( a.workload(cn1) ~= 10.0 )
    assert( a.workload(cn2) ~= 7.0 )
    assert( a.workload(cn3) ~= 8.0 )
    assert( a.makespan ~= 10.0 )
  }

  "The completion times of the tasks" should " include the delay" in {
    assert( a.completionTime(t1) ~= 3.0 )
    assert( a.completionTime(t2) ~= 7.0 )
    assert( a.completionTime(t3) ~= 2.0 )
    assert( a.completionTime(t4) ~= 6.0 )
    assert( a.completionTime(t5) ~= 2.0 )
    assert( a.completionTime(t6) ~= 8.0 )
    assert( a.completionTime(t7) ~= 10.0 )
    assert( a.completionTime(t8) ~= 1.0 )
    assert( a.completionTime(t9) ~= 1.0 )
  }

  "The completion times of the jobs" should " include the delay" in {
    assert( a.completionTime(j1) ~= 7.0 )
    assert( a.completionTime(j2) ~= 8.0 )
    assert( a.completionTime(j3) ~= 10.0 )
    assert( a.meanGlobalFlowtime ~= 25.0/3.0 )
  }
}

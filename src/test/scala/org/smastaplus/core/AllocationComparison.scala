package org.smastaplus.core

import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.example.allocation.stap1.ex1Allocation

class AllocationComparison extends AnyFlatSpec {

  "Two identical maps" should "be equal" in {
    val m1: Map[String,Int] = Map[String,Int]("n1"->5,"n2"->3)
    val m2: Map[String,Int] = Map[String,Int]("n2"->3,"n1"->5)
    println(m1)
    println(m2)
    assert(m1.equals(m2))
    assert(m1==m2)
  }

  "Two identical allocations" should "be equal" in {
    val a1 : Allocation = ex1Allocation.a
    val a2 : Allocation = a1.copy()
    println(a1)
    println(a2)
    assert(a1.equals(a2))
    assert(a1==a2)
  }
}

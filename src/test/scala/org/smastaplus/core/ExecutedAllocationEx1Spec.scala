// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.core

import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.utils.MathUtils._

/**
  * Unit test for the initial allocation of the first stap example
  */
class ExecutedAllocationEx1Spec extends AnyFlatSpec {

  import org.smastaplus.example.stap.ex1._
  import org.smastaplus.example.allocation.stap1.ex1Allocation._

  behavior of "Example 1"

  "The workload" should " be the sum of the costs of the task in the bundles" in {
    println(a.stap)
    println(a.stap.costs(cn3))
    assert( a.workload(cn1) ~= 12.0 )
    assert( a.workload(cn2) ~= 10.0 )
    println(a.workload(cn3))
    assert( a.workload(cn3) ~= 9.0 )
    assert( a.makespan ~= 12.0 )
  }

  "The completion times of the tasks" should " include the delay" in {
    assert( a.completionTime(t1) ~= 12.0 )
    assert( a.completionTime(t2) ~= 10.0 )
    assert( a.completionTime(t3) ~= 1.0 )
    assert( a.completionTime(t4) ~= 6.0 )
    assert( a.completionTime(t5) ~= 5.0 )
    assert( a.completionTime(t6) ~= 2.0 )
    assert( a.completionTime(t7) ~= 1.0 )
    assert( a.completionTime(t8) ~= 1.0 )
    assert( a.completionTime(t9) ~= 9.0 )
  }

  "The completion times of the jobs" should " include the delay" in {
    assert( a.completionTime(j1) ~= 12.0 )
    assert( a.completionTime(j2) ~= 6.0 )
    assert( a.completionTime(j3) ~= 9.0 )
    assert( a.meanGlobalFlowtime ~= 9.0 )
  }
}

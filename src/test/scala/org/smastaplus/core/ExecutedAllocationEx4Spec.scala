// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.core

import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.utils.MathUtils._

/**
  * Unit test for the initial allocation of the fourth stap example
  */
class ExecutedAllocationEx4Spec extends AnyFlatSpec {

  import org.smastaplus.example.allocation.stap4.ex4Allocation._
  import org.smastaplus.example.stap.ex4._

  behavior of "Example 4"

  "The workload" should " be the sum of the costs of the task in the bundles" in {
    assert( a.workload(cn1) ~= 8.0 )
    assert( a.workload(cn2) ~= 12.0 )
    assert( a.workload(cn3) ~= 12.0 )
    assert( a.makespan ~= 12.0 )
  }

  "The completion times of the tasks" should " include the delay" in {
    assert( a.completionTime(t1) ~= 7.0 )
    assert( a.completionTime(t2) ~= 8.0 )
    assert( a.completionTime(t3) ~= 5.0 )
    assert( a.completionTime(t4) ~= 4.0 )
    assert( a.completionTime(t5) ~= 2.0 )
    assert( a.completionTime(t6) ~= 12.0 )
    assert( a.completionTime(t7) ~= 2.0 )
    assert( a.completionTime(t8) ~= 4.0 )
    assert( a.completionTime(t9) ~= 12.0 )
  }

  "The completion times of the jobs" should " include the delay" in {
    assert( a.completionTime(j1) ~= 8.0 )
    assert( a.completionTime(j2) ~= 12.0 )
    assert( a.completionTime(j3) ~= 12.0 )
    assert( a.meanGlobalFlowtime ~= 32.0/3.0 )
  }
}

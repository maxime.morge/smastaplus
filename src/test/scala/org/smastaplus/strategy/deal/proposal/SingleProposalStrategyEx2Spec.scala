// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.strategy.deal.proposal

import org.smastaplus.core.{ExecutedAllocation, GlobalFlowtime, LocalFlowtime, LocalFlowtimeAndMakespan, Makespan}
import org.smastaplus.process._
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.strategy.deal.proposal.single.ConcreteConservativeProposalStrategy

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the single-proposal strategy with the second stap example
  */
class SingleProposalStrategyEx2Spec extends AnyFlatSpec {

  import org.smastaplus.example.allocation.stap2.ex2lcjf._
  import org.smastaplus.example.stap.ex2._

  val heuristic: ConcreteConservativeProposalStrategy = ConcreteConservativeProposalStrategy()

  behavior of "Example 1 with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst"

  "1 - The delegation strategy by the donor #3 " should "selects the recipient #3 and the task t3 " in {
    val mind1 = MindWithPeerModelling(stap, cn1, a)
    var delegation = heuristic.selectOffer(mind1, LocalFlowtimeAndMakespan)
    assert(delegation.isEmpty)

    val mind2 = MindWithPeerModelling(stap, cn2, a)
    delegation = heuristic.selectOffer(mind2, LocalFlowtimeAndMakespan)
    assert(delegation.isEmpty)

    val mind3 = MindWithPeerModelling(stap, cn3, a)
    delegation = heuristic.selectOffer(mind3, LocalFlowtimeAndMakespan)

    assert(delegation.contains(new SingleDelegation(stap, cn3, cn2, t3)))
    assert(delegation.get.isSociallyRational(a,LocalFlowtime))
    assert(delegation.get.isSociallyRational(a,GlobalFlowtime))
    assert(heuristic.triggerable(delegation.get, mind3, LocalFlowtime))
    assert(heuristic.triggerable(delegation.get, mind3, GlobalFlowtime))
    assert(heuristic.triggerable(delegation.get, mind3, Makespan))
    assert(heuristic.acceptabilityCriteria(delegation.get, mind2, LocalFlowtime))
    assert(heuristic.acceptabilityCriteria(delegation.get, mind2, GlobalFlowtime))
    assert(heuristic.acceptabilityCriteria(delegation.get, mind2, Makespan))

    val a2 = delegation.get.execute(a)
    assert(a2.bundle(cn1) == List(t1, t4, t7))
    assert(a2.bundle(cn2) == List(t5, t8, t3, t2))
    assert(a2.bundle(cn3) == List(t9, t6))

  }

  "2 - No more delegation " should "be triggered " in {
    val a2 = new ExecutedAllocation(stap)
    a2.bundle += (cn1 -> List(t1, t4, t7))
    a2.bundle += (cn2 -> List(t5, t8, t3, t2))
    a2.bundle += (cn3 -> List(t9, t6))

    val mind1 = MindWithPeerModelling(stap, cn1, a2)
    var delegation = heuristic.selectOffer(mind1, LocalFlowtimeAndMakespan)
    assert(delegation.isEmpty)

    val mind2 = MindWithPeerModelling(stap, cn2, a2)
    delegation = heuristic.selectOffer(mind2, LocalFlowtimeAndMakespan)
    assert(delegation.isEmpty)

    val mind3 = MindWithPeerModelling(stap, cn3, a2)
    delegation = heuristic.selectOffer(mind3, LocalFlowtimeAndMakespan)
    assert(delegation.isEmpty)
  }
}
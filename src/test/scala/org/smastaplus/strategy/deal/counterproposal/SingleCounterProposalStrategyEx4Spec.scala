// Copyright (C) Maxime MORGE, Luc BIGAND 2021, 2022
package org.smastaplus.strategy.deal.counterproposal

import org.smastaplus.core.{GlobalFlowtime, LocalFlowtime, LocalFlowtimeAndGlobalFlowtime, LocalFlowtimeAndMakespan, Makespan}
import org.smastaplus.process.SingleSwap
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.strategy.deal.counterproposal.single.NoneSingleCounterProposalStrategy

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the single counter-proposal strategy with the fourth stap example
  */
class SingleCounterProposalStrategyEx4Spec extends AnyFlatSpec {

  import org.smastaplus.example.allocation.stap4.ex4Allocation._
  import org.smastaplus.example.stap.ex4._

  behavior of "Example 4 with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst"

  "The swap strategy by the initiator #2 " should "selects the responder #3 and the tasks t9 and t6 " in {
    val swap = new SingleSwap(stap, cn2, cn3, t9, t6)
    val mindInitiator = MindWithPeerModelling(stap, swap.initiator, a)
    val mindResponder = MindWithPeerModelling(stap, swap.responder, a)
    assert( swap.isSociallyRational(a, LocalFlowtime) )
    assert( swap.isSociallyRational(a, Makespan) )
    assert( swap.isSociallyRational(a, GlobalFlowtime) )
    val swapStrategy = new NoneSingleCounterProposalStrategy()
    assert( swapStrategy.acceptabilityCriteria(swap, mindResponder, Makespan) )
    assert( swapStrategy.acceptabilityCriteria(swap, mindInitiator, Makespan) )
    assert( swapStrategy.acceptabilityCriteria(swap, mindInitiator, LocalFlowtime) )
    assert( swapStrategy.acceptabilityCriteria(swap, mindResponder, LocalFlowtime) )
    assert( swapStrategy.acceptabilityCriteria(swap, mindInitiator, GlobalFlowtime) )
    assert( swapStrategy.acceptabilityCriteria(swap, mindResponder, GlobalFlowtime) )
    assert( swapStrategy.acceptabilityRule(swap, mindResponder, LocalFlowtimeAndMakespan) )
    assert( swapStrategy.acceptabilityRule(swap, mindResponder, LocalFlowtimeAndGlobalFlowtime) )
    assert( swapStrategy.acceptabilityRule(swap, mindInitiator, LocalFlowtimeAndMakespan) )
    assert( swapStrategy.acceptabilityRule(swap, mindInitiator, LocalFlowtimeAndGlobalFlowtime) )
  }
}

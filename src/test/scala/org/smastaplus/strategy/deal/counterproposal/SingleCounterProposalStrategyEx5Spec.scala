// Copyright (C) Ellie BEAUPREZ, Maxime MORGE, 2021, 2022
package org.smastaplus.strategy.deal.counterproposal

import org.smastaplus.core._
import org.smastaplus.strategy.deal.proposal.single.ConcreteConservativeProposalStrategy
import org.smastaplus.strategy.deal.proposal.single.ConcreteLiberalSingleProposalStrategy
import org.smastaplus.strategy.deal.counterproposal.single.ConcreteSingleCounterProposalStrategy

import akka.actor.ActorSystem
import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.consumer.deal.mas.AgentBasedConsumer

/**
  * Unit test for the single counter-proposal strategy with the fifth stap example
  */
class SingleCounterProposalStrategyEx5Spec extends AnyFlatSpec {

  import org.smastaplus.example.allocation.stap4.swap.ex5Swap._
  import org.smastaplus.example.allocation.stap4.swap.ex5SwapC
  import org.smastaplus.example.stap.ex4._

  behavior of "Example 4 with the decentralized balancer with " +
    "the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst, " +
    "the localFlowtime rule, " +
    "the ConcreteSingleProposalStrategy and " +
    "the ConcreteSingleCounterProposalStrategy"

  "Some delegations/swaps" should "modify the allocation" in {
    val r = scala.util.Random
    val system = ActorSystem("Test" + r.nextInt().toString)

    val balancer = new AgentBasedConsumer(stap, LocalFlowtimeAndMakespan,
      new ConcreteConservativeProposalStrategy, Some(ConcreteLiberalSingleProposalStrategy()), new ConcreteSingleCounterProposalStrategy,
      system, dispatcherId = "akka.actor.default-dispatcher" , monitor = false)
    balancer.trace = false
    val outcome = balancer.consume(a)
    system.terminate()
    assert(outcome != ex5SwapC.a)
  }
}
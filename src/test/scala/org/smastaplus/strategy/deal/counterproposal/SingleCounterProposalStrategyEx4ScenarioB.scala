// Copyright (C) Ellie BEAUPREZ 2021, 2022
package org.smastaplus.strategy.deal.counterproposal

import org.smastaplus.core._
import org.smastaplus.process._
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.strategy.deal.counterproposal.single.ConcreteSingleCounterProposalStrategy

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the single counter-proposal strategy with the fourth stap example
  */
class SingleCounterProposalStrategyEx4ScenarioB extends AnyFlatSpec {

  import org.smastaplus.example.allocation.stap4.swap.ex5Swap._
  import org.smastaplus.example.stap.ex4._

  behavior of "Example 4 "

  "δ(ν1,ν2,τ8)" should "be socially rational wrt LocalFlowtime/Makespan/GlobalFlowtime" in {
    val delegation = new SingleDelegation(stap, cn1, cn2, t8)
    assert(delegation.isSociallyRational(a, LocalFlowtime)  &&
      delegation.isSociallyRational(a, Makespan)
    )
  }

  "σ(ν3,ν2,τ7,τ3)" should "be socially rational wrt LocalFlowtime/Makespan but not wrt GlobalFlowtime" in {
    val swap = new SingleSwap(stap, cn3, cn2, t7, t3)
    val a2 = new ExecutedAllocation(stap)
    a2.bundle += ( cn1 -> List(t2, t5, t6) )
    a2.bundle += ( cn2 -> List(t3, t8, t4 ) )
    a2.bundle += ( cn3 -> List(t1, t7, t9) )
    assert(swap.isSociallyRational(a2, LocalFlowtime) &&
      swap.isSociallyRational(a2, Makespan) &&
      ! swap.isSociallyRational(a2, GlobalFlowtime)
    )
  }

  "δ(τ6,ν1,ν2)" should "be socially rational wrt LocalFlowtime/Makespan/GlobalFlowtime" in {
    val delegation = new SingleDelegation(stap, cn1, cn2, t6)
    val a3 = new ExecutedAllocation(stap)
    a3.bundle += ( cn1 -> List(t2, t5, t6) )
    a3.bundle += ( cn2 -> List(t4, t7, t8 ) )
    a3.bundle += ( cn3 -> List(t9, t3, t1) )
    assert(delegation.isSociallyRational(a3, LocalFlowtime) &&
      delegation.isSociallyRational(a, Makespan) &&
      delegation.isSociallyRational(a3, GlobalFlowtime)
    )
  }

  "σ(ν1,ν2,t6,t4)" should "be socially rational wrt LocalFlowtime/Makespan/GlobalFlowtime" in {
    val swap = new SingleSwap(stap, cn1, cn2, t6, t4)
    val a3 = new ExecutedAllocation(stap)
    a3.bundle += ( cn1 -> List(t2, t5, t6) )
    a3.bundle += ( cn2 -> List(t4, t7, t8 ) )
    a3.bundle += ( cn3 -> List(t9, t3, t1) )
    assert(swap.isSociallyRational(a3, LocalFlowtime) &&
      swap.isSociallyRational(a3, Makespan) &&
      swap.isSociallyRational(a3, GlobalFlowtime)
    )
  }

  "σ(ν1,ν2,t6,t4)" should  "is better than δ(τ6,ν1,ν2) even if it does not improve the globalFlowtime" in {
    val a3 = new ExecutedAllocation(stap)
    a3.bundle += ( cn1 -> List(t2, t5, t6) )
    a3.bundle += ( cn2 -> List(t4, t7, t8 ) )
    a3.bundle += ( cn3 -> List(t9, t3, t1) )
    val swap = new SingleSwap(stap, cn1, cn2, t6, t4)
    val delegation = new SingleDelegation(stap, cn1, cn2, t6)
    val a3swap = swap.execute(a3)
    val a3delegation = delegation.execute(a3)
    assert( ! (a3swap.globalFlowtime < a3delegation.globalFlowtime) )
    // In other words
    assert(delegation.isSociallyRational(a3, GlobalFlowtime))
    assert(swap.isSociallyRational(a3, GlobalFlowtime))
    assert(!new SingleDelegation(stap, cn2, cn1, t4).isSociallyRational(a3delegation, GlobalFlowtime))
    // But selected by the strategy
    val mind2 = MindWithPeerModelling(stap, cn2, a3)
    val swapStrategy = new ConcreteSingleCounterProposalStrategy
    assert(swapStrategy.betterLocalFlowtimeThanDelegation(swap, mind2))
  }
}
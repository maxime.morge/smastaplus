// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
package org.smastaplus.balancer.dual

import org.smastaplus.core._
import org.smastaplus.example.stap.ex3._
import org.smastaplus.utils.MathUtils._
import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the deputation balancer for the example 3 without the heuristic
  */
class DeputationEx3Spec extends AnyFlatSpec {
  val debug =  false
  val trace = false
  val fullTrace = false

  // similar than ex2 with the task #1 as the last task within the bundle of the node #2
  behavior of "Example 3"

  val a = new ExecutedAllocation(stap)
  a.bundle += ( cn1 -> List(t4, t7, t14, t17) )
  a.bundle += ( cn2 -> List(t8, t5, t2, t1, t18, t15, t12, t11) )
  a.bundle += ( cn3 -> List(t9, t3, t6, t10, t13, t16) )

  if (trace) {
    println(s"Initial allocation\n$a")
    println(s"Initial globalFlowtime: ${a.globalFlowtime}")
  }

  val balancer = new DeputationBalancer(stap, GlobalFlowtime, heuristic = false)
  if (fullTrace) balancer.debug = true
  val a2 : ExecutedAllocation = balancer.reallocate(a)

  if (trace){
    println(s"Nb of reassignments ${balancer.nbReassignments}")
    println(s"Final allocation\n$a2")
    println(s"Final globalFlowtime: ${a2.globalFlowtime}")
  }

  "The globalFlowtime" should " be 39.0" in {
    if (debug) println(s"globalFlowtime ${a2.globalFlowtime}")
    assert(a2.globalFlowtime~= 39.0)
  }

  "The bundle of the node #1" should "be [τ7, τ17, τ4, τ14, τ1]" in {
    if (debug) println("node #1: "+ a2.bundle(cn1).mkString("[", ", ", "]"))
    //node #1: [τ4, τ7, τ17, τ14, τ11]
    assert( a2.bundle(cn1) == List(t7, t17, t4, t14, t1) )
  }

  "The bundle of the node #2" should "be [τ8, τ5, τ2, τ18, τ15, τ12, τ11]" in {
    if (debug) println("node #2: "+ a2.bundle(cn2).mkString("[", ", ", "]"))
    assert( a2.bundle(cn2) == List(t8, t5, t2, t18, t15, t12, t11) )
  }

  "The bundle of the node #3" should "be [τ9, τ6, τ10, τ16, τ13, τ3]" in {
    if (debug) println("node #3: "+ a2.bundle(cn3).mkString("[", ", ", "]"))
    assert( a2.bundle(cn3) == List(t9, t6, t10, t16, t13, t3) )
  }

}

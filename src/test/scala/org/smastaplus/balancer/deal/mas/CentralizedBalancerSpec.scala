// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.balancer.deal.mas

import akka.actor.ActorSystem
import akka.dispatch.MessageDispatcher
import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.core._
import org.smastaplus.strategy.deal.counterproposal.single.NoneSingleCounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.multi.ConcreteMultiProposalStrategy

/**
  * Unit test for the centralized balancer
  */
class CentralizedBalancerSpec extends AnyFlatSpec {

  behavior of "Decentralized balancer"

  "Reallocation should be" should
    "be sound" in {
    val centralizedDispatcherId = "single-thread-dispatcher"
    val centralizedSystem : ActorSystem = ActorSystem("CentralizedBalancer" + AgentBasedBalancer.id)
    val l = 4 // with l jobs
    val m = 16 // with m nodes
    val n = 3  * l * m // with n tasks
    val o = 1 // with o resources per tasks
    val d = 3 // with d duplicated instances per resource
      val pb = STAP.randomProblem(l = l, m = m, n = n, o, d , Uncorrelated)
      val a = Allocation.randomAllocation(pb)
      val balancer = new AgentBasedBalancer(pb, GlobalFlowtime, ConcreteMultiProposalStrategy(0.0), None, new NoneSingleCounterProposalStrategy, centralizedSystem, name = "CentralizedBalancer", dispatcherId = centralizedDispatcherId, false)
      balancer.trace = false
      val outcome = balancer.reallocate(a)
    centralizedSystem.terminate()
    assert(outcome.isSound)
  }
}
// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.balancer.deal.mas

import org.smastaplus.core._
import org.smastaplus.strategy.deal.proposal.single.ConcreteConservativeProposalStrategy
import org.smastaplus.strategy.deal.counterproposal.single.NoneSingleCounterProposalStrategy
import akka.actor.ActorSystem
import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.example.stap.ex1.t8

/**
  * Unit test for the decentralized balancer for the first stap example
  */
class AgentBasedBalancerEx1Spec extends AnyFlatSpec {

  import org.smastaplus.example.stap.ex1._
  import org.smastaplus.example.allocation.stap1.ex1lcjf._

  behavior of "Example 1 with the centralized balancer with " +
    "the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst"

  "LocalFlowtime: the delegation by the donor #1 to the recipient #3 of the task t7" should
    "modify the allocation with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst" in {

    val r = scala.util.Random
    val system = ActorSystem("Test" + r.nextInt().toString)
    val balancer = new AgentBasedBalancer(stap, LocalFlowtimeAndMakespan, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy,
      system, dispatcherId = "akka.actor.default-dispatcher", monitor = false)
    balancer.trace = false
    balancer.debug = false
    val outcome = balancer.reallocate(a)
    system.terminate()
    assert(
      (outcome.bundle(cn1) == List(t7, t4, t1) &&
        outcome.bundle(cn2) == List(t8, t5, t2) &&
        outcome.bundle(cn3) == List(t3, t6, t9)
        )
    ||
    (outcome.bundle(cn1) == List(t4, t1) &&
      outcome.bundle(cn2) == List(t8, t5, t2) &&
      outcome.bundle(cn3) == List(t3, t6, t7, t9)
    )
    )
  }

  "GlobalFlowtime: the delegation strategy by the donor #3 " should
    "selects the recipient #2 and the task t3" in {
    val r = scala.util.Random
    val system = ActorSystem("Test" + r.nextInt().toString)
    val balancer = new AgentBasedBalancer(stap, GlobalFlowtime, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy,
      system, dispatcherId = "akka.actor.default-dispatcher" , monitor = false)
    balancer.trace = false
    balancer.debug = false
    val outcome = balancer.reallocate(a)
    system.terminate()
    assert(
      (outcome.bundle(cn1) == List(t7, t4, t1) &&
        outcome.bundle(cn2) == List(t8, t5, t2) &&
        outcome.bundle(cn3) == List(t3, t6, t9))
    ||
    (outcome.bundle(cn1) == List(t7, t4, t1) &&
      outcome.bundle(cn2) == List(t8, t5, t3, t2) &&
      outcome.bundle(cn3) == List(t6, t9))
    )
  }
}
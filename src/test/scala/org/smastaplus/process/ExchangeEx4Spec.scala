// Copyright (C) Maxime MORGE, Luc BIGAND 2021
package org.smastaplus.process

import org.smastaplus.core._
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.utils.MathUtils._

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the consumption in the example 4
  */
class ExchangeEx4Spec extends AnyFlatSpec {

  behavior of "Example 4"

  import org.smastaplus.example.stap.ex4._

  "The swap between the task t9 of the initiator #2 with task t6 of the responder #3" should
    "with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst is socially rational wrt the makespan and the localFlowtime" in {
    import org.smastaplus.example.allocation.stap4.ex4Allocation._
    val swap = new SingleSwap(stap, cn2, cn3, t9, t6)

    var mindInitiator = MindWithPeerModelling(a.stap, swap.initiator, a)
    mindInitiator = mindInitiator.removeTask(t9)
    mindInitiator = mindInitiator.addTask(t6)
    var mindResponder = MindWithPeerModelling(a.stap, swap.responder, a)
    mindResponder = mindResponder.removeTask(t6)
    mindResponder = mindResponder.addTask(t9)

    assert( mindInitiator.sortedTasks == List(t4, t6))
    assert( mindResponder.sortedTasks == List(t1, t7, t9))
    assert( a.makespan ~= 12.0 )
    assert( a.meanGlobalFlowtime ~= 32.0 / 3.0 )
    val a2 = swap.execute(a)
    assert( a2.bundle(cn1) == List(t5, t8, t3, t2))
    assert( a2.bundle(cn2) == List(t4, t6))
    assert( a2.bundle(cn3) == List(t1, t7, t9))
    assert( a2.makespan ~= 11.0 )
    assert( a2.meanGlobalFlowtime ~= 28.0 / 3.0 )
    assert( swap.isSociallyRational(a, LocalFlowtime) )
    assert( swap.isSociallyRational(a, GlobalFlowtime) )
    assert( swap.isSociallyRational(a, Makespan) )
  }
}

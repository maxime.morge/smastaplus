// Copyright (C) Maxime MORGE 2022
package org.smastaplus.process

import org.smastaplus.provisioning._

import org.scalatest.flatspec.AnyFlatSpec
import scala.collection.SortedSet

/**
  * Unit test for provisioning with the fourth stap example for elasticity
  */
class ActivationEx4Spec extends AnyFlatSpec {

  import org.smastaplus.example.allocation.stap4.elasticity._
  import org.smastaplus.example.stap.ex4Elasticity._

  behavior of "Example 4 with 6 nodes"

  val pb: ProvisioningProblem = ProvisioningProblem(stap, maxThreshold = 16.5, QoSFlowtime )

  "The configuration with six nodes where 5 nodes are loaded" should " activate one node" in {
    val configuration = new Configuration(pb, SortedSet(cn1,cn2, cn3, cn4, cn6), ex4AllocationWith5Nodes.a)
    val activation = new Activation(pb)
    val updatedConfiguration = activation.execute(configuration, cn5)
    assert( updatedConfiguration.isSound )
    assert( updatedConfiguration.nodes == SortedSet(cn1, cn2, cn3, cn4, cn5, cn6) )
    assert( updatedConfiguration.allocation.bundle(cn5).isEmpty)
  }

  "The configuration with six nodes where 5 nodes are loaded" should " deactivate one node" in {
    val configuration = new Configuration(pb, SortedSet(cn1,cn2, cn3, cn4, cn5, cn6), ex4AllocationWith5Nodes.a)
    val deactivation = new Deactivation(pb)
    val updatedConfiguration = deactivation.execute(configuration, cn5)
    assert( updatedConfiguration.isSound )
    assert( updatedConfiguration.nodes == SortedSet(cn1, cn2, cn3, cn4, cn6) )
  }

}

// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.process

import org.scalatest.flatspec.AnyFlatSpec

import org.smastaplus.core._
import org.smastaplus.example.allocation.stap2.ex2lcjf.a
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.utils.MathUtils._

/**
  * Unit test for the consumption with the second stap example
  */
class DelegationEx2Spec extends AnyFlatSpec {

  behavior of "Example 2"

  import org.smastaplus.example.stap.ex2._


  "The first delegation by the donor #1 to the recipient #3 of the task t7" should
    "modify the allocation with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst" in {
    import org.smastaplus.example.allocation.stap2.ex2lcjf._

    val delegation = new SingleDelegation(stap, cn1, cn2, t1)
    var mindDonor = MindWithPeerModelling(a.stap, delegation.donor, a)
    mindDonor = mindDonor.removeTask(t1)
    var mindRecipient = MindWithPeerModelling(a.stap, delegation.recipient, a)
    mindRecipient = mindRecipient.addTask(t1)
    assert(mindDonor.sortedTasks == List(t4, t7))
    assert(mindRecipient.sortedTasks == List(t5, t8, t1, t2))


    val a2 = delegation.execute(a)
    assert(a2.bundle(cn1) == List(t4, t7))
    assert(a2.bundle(cn2) == List(t5, t8, t1, t2))
    assert(a2.bundle(cn3) == List(t3, t9, t6))
    assert(a.workload(cn2) + a.stap.cost(t1, cn2) ~= 10.00)
    assert(a.workload(cn1) ~= 10.00)
    assert(!delegation.isSociallyRational(a, Makespan))
    assert(
      Math.max(a.completionTime(j1, delegation.donor), a.completionTime(j1, delegation.recipient)) +
        Math.max(a.completionTime(j2, delegation.donor), a.completionTime(j2, delegation.recipient)) +
        Math.max(a.completionTime(j3, delegation.donor), a.completionTime(j3, delegation.recipient))
        ~= 23.00
    )
    assert(
      Math.max(a2.completionTime(j1, delegation.donor), a2.completionTime(j1, delegation.recipient)) +
        Math.max(a2.completionTime(j2, delegation.donor), a2.completionTime(j2, delegation.recipient)) +
        Math.max(a2.completionTime(j3, delegation.donor), a2.completionTime(j3, delegation.recipient))
        ~= 20.00
    )
    assert(delegation.isSociallyRational(a, LocalFlowtime))
    assert(a.meanGlobalFlowtime ~= 25.00 / 3)
    assert(a2.meanGlobalFlowtime ~= 25.00 / 3)
    assert(!delegation.isSociallyRational(a, GlobalFlowtime))
  }

  "The second delegation by the donor #3 to the recipient #2 of the task t3" should
    "modify the allocation with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst" in {
    val a2 = new ExecutedAllocation(stap)
    a2.bundle += (cn1 -> List(t4, t7))
    a2.bundle += (cn2 -> List(t5, t8, t1, t2))
    a2.bundle += (cn3 -> List(t3, t9, t6))

    val delegation = new SingleDelegation(stap, cn3, cn2, t3)
    val a3 = delegation.execute(a2)
    assert(a3.bundle(cn1) == List(t4, t7))
    assert(a3.bundle(cn2) == List(t5, t8, t3, t1, t2))
    assert(a3.bundle(cn3) == List(t9, t6))
    assert(
      Math.max(a3.completionTime(j1, delegation.donor), a3.completionTime(j1, delegation.recipient)) +
        Math.max(a3.completionTime(j2, delegation.donor), a3.completionTime(j2, delegation.recipient)) +
        Math.max(a3.completionTime(j3, delegation.donor), a3.completionTime(j3, delegation.recipient))
        ~= 19.50
    )
    assert(
      Math.max(a2.completionTime(j1, delegation.donor), a2.completionTime(j1, delegation.recipient)) +
        Math.max(a2.completionTime(j2, delegation.donor), a2.completionTime(j2, delegation.recipient)) +
        Math.max(a2.completionTime(j3, delegation.donor), a2.completionTime(j3, delegation.recipient))
        ~= 20.00
    )
    assert(delegation.isSociallyRational(a2, LocalFlowtime))
    assert(a2.workload(cn2) + a.stap.cost(t3, cn2) ~= 10.50)
    assert(a2.workload(cn3) ~= 8.00)
    assert(!delegation.isSociallyRational(a2, Makespan))
    assert(a2.meanGlobalFlowtime ~= 25.0/3)
    assert(a3.meanGlobalFlowtime ~= 24.5/3)
    assert(delegation.isSociallyRational(a2, LocalFlowtime))
  }

  "The third delegation by the donor #2 to the recipient #3 of the task t5" should
    "modify the allocation with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst" in {
    val a3 = new ExecutedAllocation(stap)
    a3.bundle += (cn1 -> List(t4, t7))
    a3.bundle += (cn2 -> List(t5, t8, t3, t1, t2))
    a3.bundle += (cn3 -> List(t9, t6))

    val delegation = new SingleDelegation(stap, cn2, cn3, t5)
    val a4 = delegation.execute(a3)
    assert(a4.bundle(cn1) == List(t4, t7))
    assert(a4.bundle(cn2) == List(t8, t3, t1, t2))
    assert(a4.bundle(cn3) == List(t9, t5, t6))
    // Pb
    assert(
      Math.max(a3.completionTime(j1, delegation.donor), a3.completionTime(j1, delegation.recipient)) +
        Math.max(a3.completionTime(j2, delegation.donor), a3.completionTime(j2, delegation.recipient)) +
        Math.max(a3.completionTime(j3, delegation.donor), a3.completionTime(j3, delegation.recipient))
        ~= 19.50
    )
    assert(
      Math.max(a4.completionTime(j1, delegation.donor), a4.completionTime(j1, delegation.recipient)) +
        Math.max(a4.completionTime(j2, delegation.donor), a4.completionTime(j2, delegation.recipient)) +
        Math.max(a4.completionTime(j3, delegation.donor), a4.completionTime(j3, delegation.recipient))
        ~= 18.50
    )
    assert(delegation.isSociallyRational(a3, LocalFlowtime))
    assert(a3.workload(cn3) + a.stap.cost(t5, cn3) ~= 8.00)
    assert(a3.workload(cn2) ~= 10.50)
    assert(delegation.isSociallyRational(a3, Makespan))
    assert(a3.meanGlobalFlowtime ~= 24.5/3)
    assert(a4 .meanGlobalFlowtime ~= 24.5/3)
    assert(!delegation.isSociallyRational(a3, GlobalFlowtime))
  }
  "The fourth delegation by the donor #3 to the recipient #1 of the task t5" should
    "modify the allocation with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst" in {
    val a4 = new ExecutedAllocation(stap)
    a4.bundle += (cn1 -> List(t4, t7))
    a4.bundle += (cn2 -> List(t8, t3, t1, t2))
    a4.bundle += (cn3 -> List(t9, t5, t6))

    val delegation = new SingleDelegation(stap, cn3, cn1, t5)
    val a5 = delegation.execute(a4)
    assert(a5.bundle(cn1) == List(t7, t5, t4))
    assert(a5.bundle(cn2) == List(t8, t3, t1, t2))
    assert(a5.bundle(cn3) == List(t9, t6))
    assert(
      Math.max(a4.completionTime(j1, delegation.donor), a4.completionTime(j1, delegation.recipient)) +
        Math.max(a4.completionTime(j2, delegation.donor), a4.completionTime(j2, delegation.recipient)) +
        Math.max(a4.completionTime(j3, delegation.donor), a4.completionTime(j3, delegation.recipient))
        ~= 15.00
    )
    assert(
      Math.max(a5.completionTime(j1, delegation.donor), a5.completionTime(j1, delegation.recipient)) +
        Math.max(a5.completionTime(j2, delegation.donor), a5.completionTime(j2, delegation.recipient)) +
        Math.max(a5.completionTime(j3, delegation.donor), a5.completionTime(j3, delegation.recipient))
        ~= 13.00
    )
    assert(delegation.isSociallyRational(a4, LocalFlowtime))
    assert(a4.workload(cn3) + a.stap.cost(t5, cn3) ~= 9.00)
    assert(a4.workload(cn2) ~= 9.50)
    assert(!delegation.isSociallyRational(a4, Makespan))
    assert(a4.meanGlobalFlowtime ~= 24.5/3)
    assert(a5.meanGlobalFlowtime ~= 22.5/3)
    assert(delegation.isSociallyRational(a4, GlobalFlowtime))
  }

  "The fifth delegation by the donor #1 to the recipient #2 of the task t5" should
    "modify the allocation with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst" in {
    val a5 = new ExecutedAllocation(stap)
    a5.bundle += (cn1 -> List(t7, t5, t4))
    a5.bundle += (cn2 -> List(t8, t3, t1, t2))
    a5.bundle += (cn3 -> List(t9, t6))
    val delegation = new SingleDelegation(stap, cn1, cn2, t5)
    val a6 = delegation.execute(a5)
    assert(a6.bundle(cn1) == List(t4, t7))
    assert(a6.bundle(cn2) == List(t5, t8, t3, t1, t2))
    assert(a6.bundle(cn3) == List(t9, t6))
    assert(
      Math.max(a5.completionTime(j1, delegation.donor), a5.completionTime(j1, delegation.recipient)) +
        Math.max(a5.completionTime(j2, delegation.donor), a5.completionTime(j2, delegation.recipient)) +
        Math.max(a5.completionTime(j3, delegation.donor), a5.completionTime(j3, delegation.recipient))
        ~= 22.5
        )
    assert(
      Math.max(a6.completionTime(j1, delegation.donor), a6.completionTime(j1, delegation.recipient)) +
        Math.max(a6.completionTime(j2, delegation.donor), a6.completionTime(j2, delegation.recipient)) +
        Math.max(a6.completionTime(j3, delegation.donor), a6.completionTime(j3, delegation.recipient))
        ~= 20.5)
    assert(delegation.isSociallyRational(a5, LocalFlowtime))
    assert(a5.meanGlobalFlowtime ~= 22.5/3)
    assert(a6.meanGlobalFlowtime ~=  24.5/3)
    assert(!delegation.isSociallyRational(a5, GlobalFlowtime))
  }
}
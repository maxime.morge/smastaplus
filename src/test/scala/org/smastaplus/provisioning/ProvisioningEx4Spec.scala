// Copyright (C) Maxime MORGE 2022
package org.smastaplus.provisioning

import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.utils.MathUtils._

/**
  * Unit test for provisioning with the fourth stap example for elasticity
  */
class ProvisioningEx4Spec extends AnyFlatSpec {

  behavior of "Example 4 with 6 nodes"

  "The initial configuration " should " be just-in-need and minimal" in {
    import org.smastaplus.example.configuration.ex4ConfigurationWith3Nodes._
    assert( ex4ConfigurationWith3Nodes.isSound )
    assert( ex4ConfigurationWith3Nodes.allocation.meanGlobalFlowtime ~= 34.0/3)
    assert( ex4ConfigurationWith3Nodes.isJustInNeed )
    // assert( ex4ConfigurationWith3Nodes.isMinimal ) // OK but too long to compute
   }

  "The configuration with two nodes " should " be under-provisioning" in {
    import org.smastaplus.example.configuration.ex4ConfigurationWith2Nodes._
    assert( ex4ConfigurationWith2Nodes.isSound )
    assert( ex4ConfigurationWith2Nodes.allocation.meanGlobalFlowtime ~= 19.0)
    assert( ex4ConfigurationWith2Nodes.isUnderProvisioning )
  }

  "The configuration with four nodes" should " be just-in-need" in {
    import org.smastaplus.example.configuration.ex4ConfigurationWith4Nodes._
    assert( ex4ConfigurationWith4Nodes.isSound )
    assert( ex4ConfigurationWith4Nodes.allocation.meanGlobalFlowtime ~= 29.0/3 )
    assert( ex4ConfigurationWith4Nodes.isJustInNeed )
    // assert( !ex4ConfigurationWith4Nodes.isMinimal ) // KO too long and do not work
  }

  "The configuration with five nodes" should " be just-in-need" in {
    import org.smastaplus.example.configuration.ex4ConfigurationWith5Nodes._
    assert( ex4ConfigurationWith5Nodes.allocation.isSound )
    assert( ex4ConfigurationWith5Nodes.allocation.meanGlobalFlowtime ~= 20.0/3 )
    assert( ex4ConfigurationWith5Nodes.isJustInNeed )
  }

  "The configuration with six nodes" should " be over-provisioning" in {
    import org.smastaplus.example.configuration.ex4ConfigurationWith6Nodes._
    assert( ex4ConfigurationWith6Nodes.isSound )
    assert( ex4ConfigurationWith6Nodes.allocation.meanGlobalFlowtime ~= 5.0 )
    assert( ex4ConfigurationWith6Nodes.isOverProvisioning )
  }
}

// Copyright (C) Maxime MORGE 2021
int M = ...; // Number of agents
int N = ...; // Number of tasks
int L = ...; // Number of jobs
range A = 1..M; // The set of agents
range T = 1..N; // The set of tasks
range J = 1..L; // The set of jobs
float Y[T][J] = ... ; // the membership of the tasks to the jobs
float C[A][T] = ...; // The costs of the tasks for the agents

dvar float X[T][T][A] in 0..1; // The decision variables for the allocation

/* Preprocessing */
float startingTime;
execute{
	var before = new Date();
	startingTime = before.getTime();
}

/* Solving the model */
minimize
	sum(j in J) max(i in T) Y[i][j] * max(o in A) sum(k in T) X[i][k][o] * sum(i2 in T) sum(m in k..N)  X[i2][m][o]*C[o][i2];// the globalFlowtime
	subject to {
		forall(i in T)
			ct_taskAssignment:
				sum(o in A) sum(k in T) X[i][k][o] == 1.0;
		forall(k in T, o in A)
			ct_positionAssignment:
				sum(i in T) X[i][k][o] <= 1.0;
	}

/* Postprocessing */
execute{
	var endTime = new Date();
	var processingTime=endTime.getTime()-startingTime //ms
	var outputFile = new IloOplOutputFile("../../../experiments/opl/lpOutput.txt");
	outputFile.writeln(cplex.getObjValue());// the globalFlowtime
	outputFile.writeln(processingTime);//T in millisecond
	for(var i in thisOplModel.T){
		var found = false
		for(var o in thisOplModel.A){
			for(var k in thisOplModel.T){
				if (thisOplModel.X[i][k][o] != 0.0){
					outputFile.writeln(o);//"ag:"+o+" t:"+i+" k:"+k+" x:"+thisOplModel.X[i][k][o]
					found = true ;
					break;
				}
			}
			if (found) break;
		}
	}
	outputFile.close();
}

// Copyright (C) Maxime MORGE 2022
package org.smastaplus.scaler.dual

import org.smastaplus.scaler.ReScaler
import org.smastaplus.provisioning.ProvisioningProblem
import org.smastaplus.utils.MyTime

/* Abstract class for scaling a provisioning problem instance
* by translating the problem/outcome
* @param problem instance to be solved
* @param name of the scaler
  */
abstract class DualScaler(problem: ProvisioningProblem,
                          name: String = "DualScaler")
  extends ReScaler(problem, name) {
  var preScalingTime : Long = 0
  var postScalingTime : Long = 0

  override def toString: String =
    s"$name  (${MyTime.show(scalingTime)} with pre ${MyTime.show(preScalingTime)} and post ${MyTime.show(postScalingTime)})"
}

//Copyright (C) Maxime MORGE, 2022
package org.smastaplus.scaler.local

import org.smastaplus.provisioning.{Configuration, ProvisioningProblem}
import org.smastaplus.scaler.ReScaler
import org.smastaplus.scheduler._
import org.smastaplus.utils.MyTime

/**
  * Abstract class representing an auto-scaler based on local-search
  * @param problem is a stap instance to tackle
  * @param name of the balancer
  */
abstract class LocalScaler(problem: ProvisioningProblem,
                           name: String = "LocalAutoScaler")
  extends ReScaler(problem, name) {

  // Specific metrics
  var initializationTime : Long = 0
  var initialMeanFlowtime = 0.0
  var nbSuccessfulDelegations : Int = 0
  var nbDelegations : Int = 0
  var nbSuccessfulActivations : Int = 0
  var nbActivations : Int = 0
  var nbSuccessfulDeactivations : Int = 0
  var nbDeactivations : Int = 0

  override def toString: String =
    s"$name with (${MyTime.show(scalingTime)} with initialization ${MyTime.show(initializationTime)})"

  val scheduler = new Scheduler()

  /**
    * Returns eventually a configuration
    */
  override def scale(): Option[Configuration] = {
    var initializationTime: Long = System.nanoTime()
    val initialConfiguration = init()
    initialMeanFlowtime = initialConfiguration.allocation.meanGlobalFlowtime
    initializationTime = System.nanoTime() - initializationTime
    val finalAllocation = rescale(initialConfiguration)
    finalAllocation
  }

  /**
    * Returns an initial configuration which is random by default
    */
  def init(configuration: Configuration = Configuration.randomConfiguration(problem)): Configuration = {
    if (debug) println(s"$name: initial random configuration\n$configuration")
    configuration
  }

  /**
    * Returns eventually a just-in-need configuration
    */
  @throws(classOf[RuntimeException])
  def rescale(configuration: Configuration): Option[Configuration]
}


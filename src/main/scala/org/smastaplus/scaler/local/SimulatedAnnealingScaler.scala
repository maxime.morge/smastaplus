//Copyright (C) Maxime MORGE, 2022
package org.smastaplus.scaler.local

import org.smastaplus.process.{Activation, Deactivation, Operation, SingleDelegation}
import org.smastaplus.provisioning._
import org.smastaplus.utils.MathUtils.MathUtils

import scala.util.Random

/**
  * Class representing a simulated annealing method for auto-scaling
  * @param problem is a provisioning problem instance to tackle
  * @param name    of the scaler
  */
class SimulatedAnnealingScaler(problem: ProvisioningProblem, name: String = "HillClimbingScaler")
  extends LocalScaler(problem, name) {
  val initialTemperature: Double = 100.0 // e.g. 1, 10, 100
  val threshold: Double = 0.01 // e.g. 0.1, 0.01, 0.001
  val decayRate: Double = 0.99 // e.g. 0.9, 0.99, 0.999

  /**
    * Returns the acceptance probability that depends on the
    * @param currentEnergy energy of the current state
    * @param newEnergy     energy of the new state
    * @param temperature   a global time-varying parameter
    */
  def acceptanceProbability(currentEnergy: Double, newEnergy: Double, temperature: Double): Double = {
    if (newEnergy ~< currentEnergy) return 1
    math.exp(-(newEnergy - currentEnergy) / temperature)
  }

  /**
    * Returns a random just-in-need neighbor and the deal for it
    */
  def randomSuccessor(current: Configuration): Option[(Configuration, Operation)] = {
    var successors: List[(Configuration, Operation)] = List()
    current.nodes.foreach { initiator => // Foreach initiator
      current.allocation.bundle(initiator).foreach { task => // Foreach task in its bundle
        (current.nodes diff Set(initiator)).foreach { responder => // Foreach responder
          nbDelegations += 1
          val delegation = new SingleDelegation(problem.stap, initiator, responder, task)
          val nextAllocation = delegation.execute(current.allocation)
          val nextConfiguration = new Configuration(problem, current.nodes, nextAllocation)
          if (nextConfiguration.isJustInNeed)
            successors ::= ((nextConfiguration, delegation))
        }
      }
    }
    // Deactivation
    current.nodes.foreach { node => // Foreach activated node
      if (current.allocation.bundle(node).isEmpty) {
        val deactivation = new Deactivation(problem)
        val nextConfiguration = deactivation.execute(current, node)
        if (nextConfiguration.isJustInNeed)
          successors ::= ((nextConfiguration, deactivation))
      }
    }
    // Activation
    (problem.stap.ds.computingNodes diff current.nodes).foreach { node => // Foreach deactivated node
      val activation = new Activation(problem)
      val nextConfiguration = activation.execute(current, node)
      if (nextConfiguration.isJustInNeed)
        successors ::= ((nextConfiguration, activation))
    }
    if (successors.isEmpty) None
    else Some(successors(Random.nextInt(successors.length)))
  }

  /**
    * Returns a local minima
    */
  @throws(classOf[RuntimeException])
  def rescale(configuration: Configuration): Option[Configuration] = {
    initialMeanFlowtime = configuration.allocation.meanGlobalFlowtime
    val current = new Configuration(configuration.problem, configuration.nodes, scheduler.schedule(configuration.allocation))
    var temperature = initialTemperature
    while (temperature ~> threshold) {
      val successor = randomSuccessor(current)
      if (successor.isEmpty) {
        if (current.isJustInNeed) return Some(current)
        return None
      }
      else {
        val (neighbor, operation) = successor.get
        val (newValue, currentValue) = (neighbor.nodes.size, current.nodes.size)
        val p = acceptanceProbability(currentValue, newValue, temperature)
        if (p > Random.nextDouble()) {
          operation match {
            case _: SingleDelegation => nbSuccessfulDelegations += 1
            case _: Activation => nbSuccessfulActivations += 1
            case _: Deactivation => nbSuccessfulDeactivations += 1
            case operation: Operation => throw new RuntimeException(s"SimulatedAnnealingScaler: operation $operation was not expected")
          }
        }
        temperature *= decayRate
      }
    }
    // Update metrics
    nbDelegations = nbSuccessfulDelegations
    nbActivations = nbSuccessfulActivations
    nbDeactivations = nbSuccessfulDeactivations
    Some(current)
  }
}


// Copyright (C) Maxime MORGE 2022
package org.smastaplus.scaler

import org.smastaplus.provisioning.{Configuration, ProvisioningProblem}

/**
  * Abstract class representing a rescaler
  * @param problem is a stap instance to tackle
  * @param name of the rescaler
  */
abstract class ReScaler(problem: ProvisioningProblem,
                        name: String)
  extends Scaler(problem, name){

  /**
    * Returns eventually a configuration
    */
  protected def scale() : Option[Configuration]

  /**
    * Returns eventually a configuration
    */
  protected def rescale(configuration: Configuration) : Option[Configuration]

  /**
    * Returns eventually a configuration and update scaling time
    */
  def run(initialConfiguration: Configuration) : Option[Configuration] = {
    val startingTime = System.nanoTime()
    val configuration : Option[Configuration] = rescale(initialConfiguration)
    if (configuration.isDefined) {
      scalingTime = System.nanoTime() - startingTime
      if (configuration.get.isSound) return configuration
      else {
        println(s"WARNING Scaler $name: the outcome is not sound")
        return None
      }
    } else {
      if (debug) println(s"Scaler : balancer $name do not return any configuration for $problem")
    }
    configuration
  }
}

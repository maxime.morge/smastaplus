// Copyright (C) Maxime MORGE 2022
package org.smastaplus.scaler

import org.smastaplus.provisioning._

/**
  * Scaler testing all the configurations
  * @param problem instance to be solved
  * @param name of the scaler
  */
class ExhaustiveScaler(problem: ProvisioningProblem,
                       name: String = "ExhaustiveScaler")
  extends Scaler(problem, name) {
  debug = false

  /**
    * Returns any just-in-time configuration
    */
  @throws(classOf[RuntimeException])
  override def scale(): Option[Configuration] = {
    //cut in problem.allJustInNeedConfigurations.headOption
    problem.stap.ds.computingNodes.subsets().filter(_.nonEmpty).foreach{ nodes =>
      problem.stap.allAllocations(nodes).foreach{ allocation =>
        val configuration = new Configuration(problem, nodes, allocation)
        if (configuration.isJustInNeed) {
          return Some(configuration)
        }
      }
    }
    None
  }
}


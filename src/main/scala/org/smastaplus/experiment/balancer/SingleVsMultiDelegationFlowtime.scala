// Copyright (C) Maxime MORGE 2022
package org.smastaplus.experiment.balancer

/**
 * Object running a campaign which compares
 * Single vs Multi delegation
 */
object SingleVsMultiDelegationFlowtime extends App {
  val fileName = "singleVsMultiDelegationFlowtime.csv"
  val balancerTypes : List[String] = List(
    "DecentralizedBalancerDelegationGlobalFlowtime",
    "DecentralizedBalancerMultiDelegationGlobalFlowtime",
  )
  val maxNodes4HillClimbing = 16
  val campaign = new CampaignBalancer
  campaign.run(balancerTypes, maxNodes4HillClimbing, fileName)
}
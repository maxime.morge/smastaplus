// Copyright (C) Maxime MORGE 2021, 2022
package org.smastaplus.experiment.balancer

import org.smastaplus.core._
import org.smastaplus.experiment._
import org.smastaplus.balancer._
import org.smastaplus.balancer.deal._
import org.smastaplus.balancer.dual.DCOPBalancer
import org.smastaplus.balancer.local.HillClimbingBalancer

/**
  * A Balancer Configuration is the setting of an experiment
  * @param m is the number of computing nodes
  * @param l is the number of jobs per computing node
  * @param randomGenerationRule is the rule the random generation of the STAP
  */
class BalancerConfiguration(val m: Int,
                            val l : Int,
                            val randomGenerationRule : RandomGenerationRule) extends Configuration{
  override def description : String = "m,l,randomGenerationRule"
  override def value: String = s"$m,$l,$randomGenerationRule"
}

/**
  * Class representing the setting of an experiment for the reallocation
  * by the Balancer
  */
class BalancerSetting(val allocation : Allocation, val Balancer: Balancer) extends Setting{
  def localityRate : Double = allocation.localAvailabilityRatio
  def globalFlowtime : Double = allocation.meanGlobalFlowtime
  def makespan : Double = allocation.makespan
}

/**
  *  Class representing the outcome of an experiment for the Balancer, i.e.
  *  the mean localFlowtime, the scheduling time and the locality rate
  */
class BalancerOutcome(metrics: Map[String,Double] = Map(
  "00Makespan"-> 0.0,
  "01MeanGlobalFlowtime"-> 0.0,
  "02SolvingTime" -> 0.0,
  "03LocalRatio" -> 0.0,
  "04SuccessfulRate" -> 0.0,
  "05NbDelegations" -> 0.0,
  "06NbSwaps" -> 0.0,
  "07NbTimeouts"  -> 0.0,
  "08EndowmentSize" -> 0.0,
  "09NbDelegatedTasks" -> 0.0
))
  extends ExperimentOutcome(metrics)

/**
  *  Class representing the outcome of an experiment for the Balancer comparison, i.e.
  *  the mean localFlowtime, the scheduling time, the locality rate, and the reallocation
  */
class CompareBalancerOutcome(metrics: Map[String,Double] = Map(
  "MeanGlobalFlowtime"-> 0.0,
  "SolvingTime" -> 0.0,
  "LocalRatio" -> 0.0,
  "isReallocated" -> 0.0
))
  extends ExperimentOutcome(metrics)

/**
  * Class representing an experiment for the Balancer
  */
class BalancerExperiment(setting: BalancerSetting) extends Experiment[BalancerOutcome](setting) {
  val debug = false
  /**
    * Runs the experiment
    */
  def run() :BalancerOutcome = {
    val startingTime = System.nanoTime()
    val result : Allocation = setting.Balancer match {
      case s : DealBalancer  =>
        if (debug) s.trace = true
        s.reallocate(setting.allocation)
      case s : HillClimbingBalancer  =>
        s.reallocate(setting.allocation)
      case s : DCOPBalancer=>
        s.reallocate(setting.allocation)
      case s =>
        s.run() match {
          case Some(allocation) => allocation
          case None => setting.allocation
        }
    }
    val solvingTime = System.nanoTime() - startingTime
    //setting.Balancer.solvingTime does not works if we reallocate
    if (result.isSound) new BalancerOutcome(
      Map(
        "00Makespan"-> result.makespan,
        "01MeanGlobalFlowtime"-> result.meanGlobalFlowtime,
        "02SolvingTime" -> solvingTime.toDouble,
        "03LocalRatio" -> result.localAvailabilityRatio,
        "04NbDeals" -> setting.Balancer.nbDeals,
        "05NbDelegations" -> setting.Balancer.nbDelegations,
        "06NbSwaps" -> setting.Balancer.nbSwaps,
        "07NbTimeouts" ->  setting.Balancer.nbTimeouts,
        "08EndowmentSize" -> setting.Balancer.avgEndowmentSize,
        "09NbDelegatedTasks" -> setting.Balancer.nbDelegatedTasks
      )
      )
    else throw new RuntimeException(s"Balancer: the outcome\n $result\nis not sound for\n ${setting.allocation.stap}")
  }
}

/**
  * Class representing an experiment for the Balancer
  */
class CompareBalancerExperiment(setting: BalancerSetting) extends Experiment[CompareBalancerOutcome](setting) {
  val debug = false
  /**
    * Runs the experiment
    */
  def run() : CompareBalancerOutcome = {
    val startingTime = System.nanoTime()
    val result : Allocation = setting.Balancer match {
      case s : DealBalancer  =>
        if (debug) s.trace = true
        s.reallocate(setting.allocation)
      case s : HillClimbingBalancer  =>
        s.reallocate(setting.allocation)
      case s : DCOPBalancer=>
        s.reallocate(setting.allocation)
      case s =>
        s.run() match {
          case Some(allocation) => allocation
          case None => setting.allocation
        }
    }
    val solvingTime = System.nanoTime() - startingTime
    //setting.Balancer.solvingTime does not works if we reallocate
    if (setting.allocation != result)
      new CompareBalancerOutcome(
      Map("MeanGlobalFlowtime"-> result.meanGlobalFlowtime,
        "SolvingTime" -> solvingTime.toDouble,
        "LocalRatio" -> result.localAvailabilityRatio,
        "isReallocated" -> 1.0
      )
    )
    else new CompareBalancerOutcome(
      Map("MeanGlobalFlowtime"-> setting.allocation.meanGlobalFlowtime,
        "SolvingTime" -> solvingTime.toDouble,
        "LocalRatio" -> setting.allocation.localAvailabilityRatio,
        "isReallocated" -> 0.0
      )
    )
  }
}

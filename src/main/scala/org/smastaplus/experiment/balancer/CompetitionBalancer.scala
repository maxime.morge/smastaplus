// Copyright (C) Maxime MORGE 2021, 2022
package org.smastaplus.experiment.balancer

import org.smastaplus.core._
import org.smastaplus.experiment._
import org.smastaplus.balancer.deal.mas._
import org.smastaplus.balancer.dual.EDCOPBalancer
import org.smastaplus.strategy.deal.proposal.single.ConcreteConservativeProposalStrategy
import org.smastaplus.strategy.deal.counterproposal.single.NoneSingleCounterProposalStrategy

import java.io._
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.ActorSystem
import akka.dispatch.MessageDispatcher

/**
  * Object to compare the outcome of balancers with timeout with respect to a baseline
  */
object CompetitionBalancer extends Campaign{

  debug = true

  def subFolder : String = folder + "balancer/data/"
  def fileName : String = "dcopVsNego.csv"
  val writer = new PrintWriter(new File(subFolder +fileName))

  val decentralizedDispatcherId = "akka.actor.default-dispatcher"
  AgentBasedBalancer.id += 1
  val decentralizedSystem : ActorSystem = ActorSystem("DecentralizedBalancer" + AgentBasedBalancer.id)
  implicit val decentralizedContext: MessageDispatcher = decentralizedSystem.dispatchers.lookup(decentralizedDispatcherId)

  val BalancerTypes: Seq[String] = List("DecentralizedBalancer", "DCOPBalancer.2s", "DCOPBalancer.5s", "DCOPBalancer1s")

  var BalancerOutcome = new CompareBalancerOutcome()

  val l : Int = 4 // with l jobs
  val m : Int = 2 // with m nodes
  val n : Int = 3 * l * m // with l * m tasks not 3 * l * m
  val o : Int = 1 // with o resources per task
  val d : Int = 3 // with d duplicated instances per resource
  private val nbSTAPSs = 10
  val nbAllocations = 10
  val configuration = new BalancerConfiguration(m, l, Uncorrelated)

  // The header of the file
  private def header : String =
    s"${configuration.description}," +
      "InitialLocalAvailabilityRatio,"+
      BalancerTypes.flatMap(Balancer  =>
        BalancerOutcome.metrics.keys.map { metric =>
          s"$Balancer$metric"
        }
      ).mkString("",",","\n")



  /**
    * Runs the campaign
    */
  def main(args: Array[String]): Unit = {
    writer.write(header)
    // Repeated experiments
    var nbRuns = 0
    for (_ <- 1 to nbSTAPSs) { // foreach STAP
      val stap = STAP.randomProblem(l, m, n, o, d, configuration.randomGenerationRule)
      for (_ <- 1 to nbAllocations) { // foreach initial allocation
        var csv = ""
        nbRuns += 1
        if (debug) println(s"Run: $nbRuns")
        val allocation = Allocation.randomAllocation(stap)

        // Run baseline
        val baselineBalancer = new AgentBasedBalancer(stap, GlobalFlowtime, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy, decentralizedSystem, name = "DecentralizedBalancer", dispatcherId = decentralizedDispatcherId, false)
        val settingBaseline = new BalancerSetting(allocation, baselineBalancer)
        val baselineExperiment = new CompareBalancerExperiment(settingBaseline)
        if (debug) println(s"CompetitionBalancer: configuration=$configuration Balancer=${baselineBalancer.name} run=$nbRuns")
        var outcome = baselineExperiment.run()
        // Write baseline
        csv += s"${configuration.value},"
        csv += s"${settingBaseline.localityRate},"
        csv += s"${outcome.metrics("MeanGlobalFlowtime")}," +
          s"${outcome.metrics("SolvingTime")}," +
          s"${outcome.metrics("LocalRatio")}," +
          s"${outcome.metrics("isReallocated")}"

        // Run DCOP 2 seconds
        var Balancer = new EDCOPBalancer(stap, GlobalFlowtime, "DCOPBalancer1s", 2 second)
        var setting = new BalancerSetting(allocation, Balancer)
        var income = setting.localityRate
        var experiments = new CompareBalancerExperiment(setting)
        if (debug) println(s"CompetitionBalancer: configuration=$configuration Balancer=${Balancer.name} run=$nbRuns")
        outcome = experiments.run()
        // Write DCOP
        csv += s",${outcome.metrics("MeanGlobalFlowtime")}," +
          s"${outcome.metrics("SolvingTime")}," +
          s"${outcome.metrics("LocalRatio")}," +
          s"${outcome.metrics("isReallocated")}"

        // Run DCOP 5 seconds
        Balancer = new EDCOPBalancer(stap, GlobalFlowtime, "DCOPBalancer5s", 5 seconds) //timeout * factor nanoseconds
        setting = new BalancerSetting(allocation, Balancer)
        income = setting.localityRate
        experiments = new CompareBalancerExperiment(setting)
        if (debug) println(s"CompetitionBalancer: configuration=$configuration Balancer=${Balancer.name} run=$nbRuns")
        outcome = experiments.run()
        // Write DCOP
        csv += s",${outcome.metrics("MeanGlobalFlowtime")}," +
          s"${outcome.metrics("SolvingTime")}," +
          s"${outcome.metrics("LocalRatio")}," +
          s"${outcome.metrics("isReallocated")}"

        // Run DCOP 10 seconds
        Balancer = new EDCOPBalancer(stap, GlobalFlowtime, "DCOPBalancer10s", 10 seconds)
        setting = new BalancerSetting(allocation, Balancer)
        income = setting.localityRate
        experiments = new CompareBalancerExperiment(setting)
        if (debug) println(s"CompetitionBalancer: configuration=$configuration Balancer=${Balancer.name} run=$nbRuns")
        outcome = experiments.run()
        // Write DCOP
        csv += s",${outcome.metrics("MeanGlobalFlowtime")}," +
          s"${outcome.metrics("SolvingTime")}," +
          s"${outcome.metrics("LocalRatio")}," +
          s"${outcome.metrics("isReallocated")}\n"

        //Write the experiments
        writer.write(csv)
        writer.flush()
      }
    }
    writer.close()
    decentralizedSystem.terminate()
  }
}

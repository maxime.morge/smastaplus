// Copyright (C) Maxime MORGE 2022
package org.smastaplus.experiment.balancer

/**
 * Object running a campaign which compares
 * mono vs multi vs hill climbing
 */
object MonoVsMultiThread extends App {
  val fileName = "monoVsMultiThread.csv"
  val balancerTypes : List[String] = List(
    "CentralizedBalancerDelegationGlobalFlowtime",
    "DecentralizedBalancerDelegationGlobalFlowtime",
    "HillClimbingBalancerDelegationFlowtime"
  )
  val maxNodes4HillClimbing = 4
  val campaign = new CampaignBalancer
  campaign.run(balancerTypes, maxNodes4HillClimbing, fileName)
}
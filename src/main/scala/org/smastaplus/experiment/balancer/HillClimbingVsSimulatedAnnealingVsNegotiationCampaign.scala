// Copyright (C) Maxime MORGE 2022
package org.smastaplus.experiment.balancer

/**
 * Object running a campaign which compares
 * centralized vs decentralized vs hill climbing
 */
object HillClimbingVsSimulatedAnnealingVsNegotiationCampaign extends App {
  val fileName = "hillClimbingVsSimulatedAnnealingVsNegotiation.csv"
  val balancerTypes : List[String] = List(
    "DecentralizedBalancerDelegationGlobalFlowtime",
    "HillClimbingBalancerDelegationFlowtime",
    "SimulatedAnnealingDelegationFlowtime"
  )
  val maxNodes4HillClimbing = 9
  val campaign = new CampaignBalancer
  campaign.run(balancerTypes, maxNodes4HillClimbing, fileName)
}
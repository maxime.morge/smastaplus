// Copyright (C) Maxime MORGE 2023
package org.smastaplus.experiment.balancer

/**
 * Object running a campaign which compares
 * local flowtime and makespan vs. global flowtime vs hill climbing
 */
object ASSIVsNegotiationCampaign extends App {
  val fileName = "assiVsNegotiation.csv"
  val balancerTypes : List[String] = List(
    "DecentralizedBalancerDelegationGlobalFlowtime",
    "ASSIGlobalFlowtime",
    "HillClimbingBalancerDelegationFlowtime"
  )
  val maxNodes4Slow = 5
  val campaign = new CampaignBalancer
  campaign.run(balancerTypes, maxNodes4Slow, fileName)
}
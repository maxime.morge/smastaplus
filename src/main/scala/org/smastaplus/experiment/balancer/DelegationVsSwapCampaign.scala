// Copyright (C) Maxime MORGE 2022
package org.smastaplus.experiment.balancer

/**
 * Object running a campaign which compares
 * Delegation vs Swap
 */
object DelegationVsSwapCampaign extends App {
  val fileName = "delegationVsSwap.csv"
  val balancerTypes : List[String] = List(
  "DecentralizedBalancerDelegationGlobalFlowtime",
  "DecentralizedBalancerSwapGlobalFlowtime",
  "HillClimbingBalancerSwap",
  )
  val maxNodes4HillClimbing = 4
  val campaign = new CampaignBalancer
  campaign.run(balancerTypes, maxNodes4HillClimbing, fileName)
}

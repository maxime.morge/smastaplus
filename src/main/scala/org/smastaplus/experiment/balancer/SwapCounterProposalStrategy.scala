// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.experiment.balancer

import org.smastaplus.core.{Allocation, GlobalFlowtime, STAP, Uncorrelated}
import org.smastaplus.balancer.deal.mas.AgentBasedBalancer
import org.smastaplus.strategy.deal.counterproposal.single.ConcreteSingleCounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.single.{ConcreteLiberalSingleProposalStrategy, ConcreteConservativeProposalStrategy}

import akka.actor.ActorSystem

/**
 * Object for testing the counter-proposal strategy
 */
object SwapCounterProposalStrategy extends App {
  var id : Int = 1
  val r = scala.util.Random
  val system = ActorSystem("Test" + r.nextInt().toString)
  val m = 16 // with m nodes
  val l = 4 // with l=4 jobs
  val n = 3  * l * m // with n=3 * l * m tasks
  val o = 10  // with o resources per task
  val d = 3 // 3 instances per resource
  val stap = STAP.randomProblem(l = l, m = m, n = n, o = o, d = d, Uncorrelated)
  //println(pb.ds)
  //println(pb)
  val a = Allocation.randomAllocation(stap)
  //println(a)
  val balancer = new AgentBasedBalancer(stap, GlobalFlowtime,
    ConcreteConservativeProposalStrategy(), Some(ConcreteLiberalSingleProposalStrategy()), new ConcreteSingleCounterProposalStrategy,
    system, dispatcherId = "akka.actor.default-dispatcher" , monitor = true)
  println(s"Run $balancer")
  val outcome = balancer.reallocate(a)
  //println(outcome)
  println(s"nbDelegations nbSwaps nbTimeouts")
  println(s"${balancer.nbDelegations} ${balancer.nbSwaps} ${balancer.nbTimeouts}")
  if (!outcome.isSound)
    throw new RuntimeException("Balancer does not return back a sound allocation")
  balancer.close()
}

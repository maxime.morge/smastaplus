// Copyright (C) Maxime MORGE 2021, 2022
package org.smastaplus.experiment.balancer

import org.smastaplus.core._
import org.smastaplus.balancer.deal.mas.AgentBasedBalancer
import org.smastaplus.strategy.deal.counterproposal.single.NoneSingleCounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.multi.ConcreteMultiProposalStrategy
import org.smastaplus.strategy.deal.proposal.single.ConcreteConservativeProposalStrategy

import akka.actor.ActorSystem

/**
  * Compare the runtime of the strategies
  * ConcreteSingleProposalStrategy() and ConcreteMultiProposalStrategy(),
  */
object CompareMultiProposalStrategies extends App {
  var id : Int = 1
  val r = scala.util.Random
  private val system1 = ActorSystem("Test" + r.nextInt().toString)
  private val system2 = ActorSystem("Test" + r.nextInt().toString)
  val l = 4 // with l jobs
  val m = 16 // with m nodes
  val n = 3  * l * m // with 3 tasks per job/node
  val o = 10 // with o resources per task
  val d = 3 // with 3 duplicated instances per resource
  val stap = STAP.randomProblem(l = l, m = m, n = n, o = o, d = d, Uncorrelated)
  println(stap)
  val a = Allocation.randomAllocation(stap)
  println(a)
  var balancer = new AgentBasedBalancer(stap, GlobalFlowtime, ConcreteConservativeProposalStrategy(), None, new NoneSingleCounterProposalStrategy,
    system1, dispatcherId = "akka.actor.default-dispatcher" , monitor = true)
  balancer.trace = false
  var outcome = balancer.reallocate(a)
  if (!outcome.isSound)
    throw new RuntimeException("Balancer does not return back a sound allocation")
  println(outcome)
  balancer = new AgentBasedBalancer(stap, GlobalFlowtime, ConcreteMultiProposalStrategy(0.0), None, new NoneSingleCounterProposalStrategy,
    system2, dispatcherId = "akka.actor.default-dispatcher" , monitor = true)
  balancer.trace = false
  outcome = balancer.reallocate(a)
  println(outcome)
  system1.terminate()
  system2.terminate()
  if (!outcome.isSound)
    throw new RuntimeException("Balancer does not return back a sound allocation")
}

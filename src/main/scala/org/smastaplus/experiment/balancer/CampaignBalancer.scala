// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.experiment.balancer

import org.smastaplus.core._
import org.smastaplus.experiment._
import org.smastaplus.balancer._
import org.smastaplus.balancer.local._
import org.smastaplus.balancer.deal.DealBalancer
import org.smastaplus.balancer.deal.mas._
import org.smastaplus.strategy.deal.proposal.multi._
import org.smastaplus.strategy.deal.proposal.single._
import org.smastaplus.strategy.deal.counterproposal.single._

import java.io._
import scala.collection.immutable.ListMap
import akka.actor.ActorSystem
import akka.dispatch.MessageDispatcher
import org.smastaplus.balancer.local.HillClimbingBalancer

/**
  * Experimental campaign comparing our balancer with a DCOP Balancer
  */
class CampaignBalancer extends Campaign{
  debug = true
  val trace = false
  def subFolder : String = folder + "balancer/data/"
  val centralizedDispatcherId = "single-thread-dispatcher"
  val decentralizedDispatcherId = "akka.actor.default-dispatcher"
  var centralizedSystem : ActorSystem = ActorSystem("CentralizedBalancer" + AgentBasedBalancer.id)
  implicit val centralizedContext: MessageDispatcher = centralizedSystem.dispatchers.lookup(centralizedDispatcherId)
  AgentBasedBalancer.id += 1
  var decentralizedSystem : ActorSystem = ActorSystem("DecentralizedBalancer" + AgentBasedBalancer.id)
  implicit val decentralizedContext: MessageDispatcher = decentralizedSystem.dispatchers.lookup(decentralizedDispatcherId)
  AgentBasedBalancer.id += 1
  var decentralizedSystem2 : ActorSystem = ActorSystem("DecentralizedBalancer2" + AgentBasedBalancer.id)
  implicit val decentralizedContext2: MessageDispatcher = decentralizedSystem2.dispatchers.lookup(decentralizedDispatcherId)
  AgentBasedBalancer.id += 1

  val configurations : List[BalancerConfiguration] = { // The configurations
    val numbersOfNodes = List.range(6, 9, 1) // m in [2,17[
    val numbersOfJobs = List.range(4, 5, 1) // l in [4,5[
    val couples = for(j <- numbersOfJobs ; i <- numbersOfNodes) yield (i, j)
    couples.map{ case (i, j)  =>
      new BalancerConfiguration(i, j, Uncorrelated)
    }
  }

  private val nbSTAPs = 10
  val nbAllocations = 10
  var BalancerOutcome = new BalancerOutcome()

  // The header of the file
  private def header(BalancerTypes : List[String]) : String =
    s"${configurations.head.description}," +
      "InitialLocalAvailabilityRatioOpen,InitialLocalAvailabilityRatioMean,InitialLocalAvailabilityRatioClose,"+
      "InitialGlobalFlowtimeOpen,InitialGlobalFlowtimeMean,InitialGlobalFlowtimeClose,"+
      "InitialMakespanOpen,InitialMakespanMean,InitialMakespanClose,"+
  BalancerTypes.flatMap(Balancer  => // Sort keys in alphabetic order
      ListMap(BalancerOutcome.metrics.toSeq.sortBy(_._1):_*).keys.map { metric =>
        val columnName = s"$Balancer$metric"
        columnName + "Open," + columnName + "Mean," + columnName + "Close"
      }
    ).mkString("",",","")

  /**
    * Run a configuration for a list of Balancers
   * and returns the outcome as a string
    */
  def runConfiguration(configuration: BalancerConfiguration, BalancerTypes : List[String]) : String = {
    var csv = ""
    if (debug) println("configuration: "+configuration.value)
    // Repeated experiments
    var nbRuns = 0
    var outcomes = Map[String,List[BalancerOutcome]]()
    var incomesLocalityRatios = List[Double]()
    var incomesGlobalFlowtimes = List[Double]()
    var incomesMakespans = List[Double]()
    var Balancers = List[Balancer]()
    for(_ <- 1 to nbSTAPs) { // foreach MASTA+
      val l = configuration.l// with l jobs
      val m = configuration.m // with m nodes
      val n = 3 * l * m // with n tasks, 3 tasks per job/node
      val o = 10 // with o resources per task
      val d = 3 // d instances per resource
      val stap = STAP.randomProblem(l, m, n, o, d, configuration.randomGenerationRule)
      for(_ <- 1 to nbAllocations) { // foreach initial allocation
        centralizedSystem = ActorSystem("CentralizedBalancer" + AgentBasedBalancer.id)
        AgentBasedBalancer.id += 1
        decentralizedSystem = ActorSystem("DecentralizedBalancer" + AgentBasedBalancer.id)
        AgentBasedBalancer.id += 1
        decentralizedSystem2 = ActorSystem("DecentralizedBalancer2" + AgentBasedBalancer.id)
        AgentBasedBalancer.id += 1
        val allocation =Allocation.randomAllocation(stap)
        nbRuns += 1
        Balancers = List[Balancer]()
        BalancerTypes.foreach{ BalancerType => // foreach Balancer
          val Balancer = BalancerType match {
            case "CentralizedBalancerDelegation" =>
              new AgentBasedBalancer(stap, LocalFlowtimeAndMakespan, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy, centralizedSystem, name = BalancerType, dispatcherId = centralizedDispatcherId, false)
            case "DecentralizedBalancerDelegation" => //
              new AgentBasedBalancer(stap, LocalFlowtimeAndMakespan, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy, decentralizedSystem, name = BalancerType, dispatcherId = decentralizedDispatcherId, false)
            case "CentralizedBalancerDelegationGlobalFlowtime" =>
              new AgentBasedBalancer(stap, GlobalFlowtime, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy, centralizedSystem, name = BalancerType, dispatcherId = centralizedDispatcherId, false)
            case "DecentralizedBalancerDelegationGlobalFlowtime" => //
              new AgentBasedBalancer(stap, GlobalFlowtime, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy, decentralizedSystem, name = BalancerType, dispatcherId = decentralizedDispatcherId, false)
            case "DecentralizedBalancerMultiDelegationGlobalFlowtime" =>
              new AgentBasedBalancer(stap, GlobalFlowtime, new ConcreteMultiProposalStrategy, None, new NoneSingleCounterProposalStrategy, decentralizedSystem2, name = BalancerType, dispatcherId = decentralizedDispatcherId, false)
            case "DecentralizedBalancerSwapGlobalFlowtime" =>
              new AgentBasedBalancer(stap, GlobalFlowtime, new ConcreteConservativeProposalStrategy, Some(new ConcreteLiberalSingleProposalStrategy), new ConcreteSingleCounterProposalStrategy, decentralizedSystem2, name = BalancerType, dispatcherId = decentralizedDispatcherId, false)
            case "HillClimbingBalancerDelegationFlowtime" => //
              new HillClimbingBalancer(stap, GlobalFlowtime, BalancerType, false)
            case "HillClimbingBalancerSwap" =>
              new HillClimbingBalancer(stap, GlobalFlowtime, BalancerType, true)
            case "SimulatedAnnealingDelegationFlowtime" => //
              new SimulatedAnnealingBalancer(stap, GlobalFlowtime, BalancerType, false)
            case "SimulatedAnnealingBalancerSwap" =>
              new SimulatedAnnealingBalancer(stap, GlobalFlowtime, BalancerType, true)
            case "ASSIGlobalFlowtime" =>
              new ASSIBalancer(stap, GlobalFlowtime,BalancerType)
            case "RSSIGlobalFlowtime" =>
              new RSSIBalancer(stap, GlobalFlowtime, BalancerType)

            case s =>
              throw new RuntimeException(s"ERROR CampaignBalancer is not able to manage Balancer $s")
          }
          //Debug
          if (trace){ Balancer match {
            case s: DealBalancer => s.trace = true
            case _ =>
            }
          }
          Balancers ::= Balancer
          val setting = new BalancerSetting(allocation, Balancer)
          val experiments = new BalancerExperiment(setting)
          if (debug) println(s"CampaignBalancer: configuration=$configuration Balancer=${Balancer.name} run=$nbRuns")
          incomesLocalityRatios  ::= setting.localityRate
          incomesGlobalFlowtimes ::= setting.globalFlowtime
          incomesMakespans ::= setting.makespan
          val outcome = experiments.run()
          outcomes = outcomes.updatedWith(Balancer.name)({
            case Some(list) => Some(outcome :: list)
            case None => Some(List(outcome))
          })
        }
      }
    }
    Balancers = Balancers.reverse
    if (debug) println(s"nbRuns: $nbRuns")
    csv += s"${configuration.value}"
    // Collect income
    incomesLocalityRatios = incomesLocalityRatios.sortWith(_ < _)
    incomesGlobalFlowtimes = incomesGlobalFlowtimes.sortWith(_ < _)
    incomesMakespans = incomesMakespans.sortWith(_ < _)
    val (openLocalRatio,meanLocalRatio,closeLocalRatio) =
      (incomesLocalityRatios(incomesLocalityRatios.size/4),
        incomesLocalityRatios(incomesLocalityRatios.size/2),
        incomesLocalityRatios(incomesLocalityRatios.size*3/4))
    val (openGlobalFlowtime,meanGlobalFlowtime,closeGlobalFlowtimeRatio) =
      (incomesGlobalFlowtimes(incomesGlobalFlowtimes.size/4),
        incomesGlobalFlowtimes(incomesGlobalFlowtimes.size/2),
        incomesGlobalFlowtimes(incomesGlobalFlowtimes.size*3/4))
    val (openMakespan,meanMakespan,closeMakespan) =
      (incomesMakespans(incomesMakespans.size/4),
        incomesMakespans(incomesMakespans.size/2),
        incomesMakespans(incomesMakespans.size*3/4))
    csv += s",$openLocalRatio,$meanLocalRatio,$closeLocalRatio," +
      s"$openGlobalFlowtime,$meanGlobalFlowtime,$closeGlobalFlowtimeRatio," +
      s"$openMakespan,$meanMakespan,$closeMakespan"
    // Collect outcome
    Balancers.foreach { Balancer =>
      println(Balancer.name)
      val makespans : List[Double] = outcomes(Balancer.name).map( _.metrics("00Makespan").asInstanceOf[Double]).sortWith(_ < _)
      val meanGlobalFlowtimes : List[Double] = outcomes(Balancer.name).map( _.metrics("01MeanGlobalFlowtime").asInstanceOf[Double]).sortWith(_ < _)
      val solvingTimes : List[Double] = outcomes(Balancer.name).map( _.metrics("02SolvingTime").asInstanceOf[Double]).sortWith(_ < _)
      val localRatios : List[Double] = outcomes(Balancer.name).map( _.metrics("03LocalRatio").asInstanceOf[Double]).sortWith(_ < _)
      val nbDeals : List[Double] = outcomes(Balancer.name).map( _.metrics("04NbDeals").asInstanceOf[Double]).sortWith(_ < _)
      val nbDelegations : List[Double] = outcomes(Balancer.name).map( _.metrics("05NbDelegations").asInstanceOf[Double]).sortWith(_ < _)
      val nbSwaps : List[Double] = outcomes(Balancer.name).map( _.metrics("06NbSwaps").asInstanceOf[Double]).sortWith(_ < _)
      val nbTimeouts : List[Double] = outcomes(Balancer.name).map( _.metrics("07NbTimeouts").asInstanceOf[Double]).sortWith(_ < _)
      val avgEndowmentSize : List[Double] = outcomes(Balancer.name).map( _.metrics("08EndowmentSize").asInstanceOf[Double]).sortWith(_ < _)
      val nbDelegatedTasks : List[Double] = outcomes(Balancer.name).map( _.metrics("09NbDelegatedTasks").asInstanceOf[Double]).sortWith(_ < _)

      val open = new BalancerOutcome(Map(
        "00Makespan" -> makespans(nbRuns/4),
        "01MeanGlobalFlowtime" -> meanGlobalFlowtimes(nbRuns/4),
        "02SolvingTime" -> solvingTimes(nbRuns/4),
        "03LocalRatio" -> localRatios(nbRuns/4),
        "04NbDeals" -> nbDeals(nbRuns/4),
        "05NbDelegations" -> nbDelegations(nbRuns/4),
        "06NbSwaps" -> nbSwaps(nbRuns/4),
        "07NbTimeouts" -> nbTimeouts(nbRuns/4),
        "08EndowmentSize" ->  avgEndowmentSize(nbRuns/4),
        "09NbDelegatedTasks" -> nbDelegatedTasks(nbRuns/4)
      ))
      val mean = new BalancerOutcome(Map(
        "00Makespan" -> makespans(nbRuns/2),
        "01MeanGlobalFlowtime" -> meanGlobalFlowtimes(nbRuns/2),
        "02SolvingTime" -> solvingTimes(nbRuns/2),
        "03LocalRatio" -> localRatios(nbRuns/2),
        "04NbDeals" -> nbDeals(nbRuns/2),
        "05NbDelegations" -> nbDelegations(nbRuns/2),
        "06NbSwaps" -> nbSwaps(nbRuns/2),
        "07NbTimeouts" -> nbTimeouts(nbRuns/2),
        "08EndowmentSize" -> avgEndowmentSize(nbRuns/2),
        "09NbDelegatedTasks" -> nbDelegatedTasks(nbRuns/2)
      ))
      val close = new BalancerOutcome(Map(
        "00Makespan" -> makespans(nbRuns*3/4),
        "01MeanGlobalFlowtime" -> meanGlobalFlowtimes(nbRuns*3/4),
        "02SolvingTime" -> solvingTimes(nbRuns*3/4),
        "03LocalRatio" -> localRatios(nbRuns*3/4),
        "04NbDeals" -> nbDeals(nbRuns*3/4),
        "05NbDelegations" -> nbDelegations(nbRuns*3/4),
        "06NbSwaps" -> nbSwaps(nbRuns*3/4),
        "07NbTimeouts" -> nbTimeouts(nbRuns*3/4),
        "08EndowmentSize" -> avgEndowmentSize(nbRuns*3/4),
        "08EndowmentSize" -> avgEndowmentSize(nbRuns*3/4),
        "09NbDelegatedTasks" -> nbDelegatedTasks(nbRuns*3/4)
      ))
      csv += s"," +
        s"${open.metrics("00Makespan")},${mean.metrics("00Makespan")},${close.metrics("00Makespan")}," +
        s"${open.metrics("01MeanGlobalFlowtime")},${mean.metrics("01MeanGlobalFlowtime")},${close.metrics("01MeanGlobalFlowtime")}," +
        s"${open.metrics("02SolvingTime")},${mean.metrics("02SolvingTime")},${close.metrics("02SolvingTime")}," +
        s"${open.metrics("03LocalRatio")},${mean.metrics("03LocalRatio")},${close.metrics("03LocalRatio")}," +
        s"${open.metrics("04NbDeals")},${mean.metrics("04NbDeals")},${close.metrics("04NbDeals")}," +
        s"${open.metrics("05NbDelegations")},${mean.metrics("05NbDelegations")},${close.metrics("05NbDelegations")}," +
        s"${open.metrics("06NbSwaps")},${mean.metrics("06NbSwaps")},${close.metrics("06NbSwaps")}," +
        s"${open.metrics("07NbTimeouts")},${mean.metrics("07NbTimeouts")},${close.metrics("07NbTimeouts")}," +
        s"${open.metrics("08EndowmentSize")},${mean.metrics("08EndowmentSize")},${close.metrics("08EndowmentSize")}," +
        s"${open.metrics("09NbDelegatedTasks")},${mean.metrics("09NbDelegatedTasks")},${close.metrics("09NbDelegatedTasks")}"
    }
    csv // Returns the string representation of the open/mean/close outcomes
  }

  /**
    * Runs the campaign with the list of Balancer types and the maxNodes4HillClimbing
    */
  def run(BalancerTypes : List[String], maxNodes4Slow : Int, fileName : String): Unit = {
    val writer = new PrintWriter(new File(subFolder + fileName))
    writer.write(header(BalancerTypes)+"\n")
    writer.flush()
    for(configuration <- configurations){
      val outcome = if (configuration.m <= maxNodes4Slow) runConfiguration(configuration, BalancerTypes)
      else runConfiguration(configuration, BalancerTypes.dropRight(1))
      writer.write(s"$outcome\n")
      writer.flush()
    }
    writer.close()
    centralizedSystem.terminate()
    decentralizedSystem.terminate()
    decentralizedSystem2.terminate()
  }
}

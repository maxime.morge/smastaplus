// Copyright (C) Maxime MORGE, 2023
package org.smastaplus.experiment.consumer

import org.smastaplus.core.SimulatedCostE

/**
 * Object running a campaign which compares
 * consumption with and without negotiation
 * by nodes
 */
object WithVsWithoutNegotiationByNodesCampaign extends App {
  val fileName = "consumption4jobs5staps5allocationsByNodes08-16nodes"
  val balancerTypes : List[String] = List("DecentralizedConsumerMultiDelegationGlobalFlowtime")
  private val campaign = new ConsumptionCampaign(simulatedCost = new SimulatedCostE, byNodes = true)
  campaign.run(balancerTypes, fileName)
}

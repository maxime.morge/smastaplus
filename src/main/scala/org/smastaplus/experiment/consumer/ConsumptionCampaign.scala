// Copyright (C) Maxime MORGE, Ellie Beauprez 2020, 2021, 2022, 2023
package org.smastaplus.experiment.consumer

import org.smastaplus.core._
import org.smastaplus.consumer.{Consumer, SynchronousConsumer}
import org.smastaplus.balancer.deal.mas.AgentBasedBalancer
import org.smastaplus.consumer.deal.mas.AgentBasedConsumer
import org.smastaplus.consumer.nodeal.SynchronousDecentralizedConsumer
import org.smastaplus.strategy.deal.counterproposal.single._
import org.smastaplus.strategy.deal.proposal.multi._
import org.smastaplus.experiment._

import java.io._
import scala.collection.immutable.ListMap
import akka.actor.ActorSystem

/**
  * Experimental campaign comparing outcome without and with negotiation
  * @param simulatedCost which is applied
  */
class ConsumptionCampaign(simulatedCost: SimulatedCost, fromStable: Boolean = false, byNodes: Boolean) extends Campaign{
  debug = true
  val trace = false
  def subFolder : String = folder + "consumer/data/"
  val decentralizedDispatcherId = "akka.actor.default-dispatcher"

  val configurations : List[ConsumptionConfiguration] = { // The configurations
    val numbersOfJobs = List.range(4, 5, 1) // l in [4,5[
    val numbersOfTasks = List.range(40, 360, 40)
    if (byNodes)
      List.tabulate(9)(nbNodes => new ConsumptionConfiguration(8+ nbNodes, 4, 120, Uncorrelated))
    else {
      val couples = for(j <- numbersOfJobs ; i <- numbersOfTasks) yield (i, j)
      couples.map{ case (i, j)  =>
        new ConsumptionConfiguration(8, j, i, Uncorrelated)
      }
    }
  }
  private val nbSTAPS : Int = if (fromStable) {
    25
  } else {
    5
  }
  val nbAllocations: Int = if (fromStable) {
    1
  } else {
    5
  }
  private val outcomeWithNegotiation = new DealConsumptionOutcome()
  private var outcomeWithoutNegotiation = new DealConsumptionOutcome()

  var consumer : SynchronousDecentralizedConsumer = _

  /**
   * Returns the header of the main file
   */
  private def header(ConsumerTypes : List[String]) : String =
    s"${configurations.head.description}," +
      "InitialLocalAvailabilityRatioOpen,InitialLocalAvailabilityRatioMean,InitialLocalAvailabilityRatioClose,"+
      "InitialGlobalFlowtimeOpen,InitialGlobalFlowtimeMean,InitialGlobalFlowtimeClose," +
      "InitialSimulatedFlowtimeOpen,InitialSimulatedFlowtimeMean,InitialSimulatedFlowtimeClose"+
      ConsumerTypes.flatMap(consumer  => // Sort keys in alphabetic order
        ListMap(outcomeWithNegotiation.metrics.toSeq.sortBy(_._1):_*).keys.map { metric =>
          val columnName = s"$consumer$metric"
          columnName + "Open," + columnName + "Mean," + columnName + "Close"
      }).mkString("",",",",") +
      ListMap(outcomeWithoutNegotiation.metrics.toSeq.sortBy(_._1):_*).keys.map { metric =>
        val columnName = s"Consumer$metric"
        columnName + "Open," + columnName + "Mean," + columnName + "Close"
      }.mkString("",",",",")

  /**
   * Returns the header of the detailed file
   */
  private def headerDetailedRuns(ConsumerTypes : List[String]) : String =
    s"${configurations.head.description}," +
      "InitialLocalAvailabilityRatio,"+
      "InitialGlobalFlowtime,"+
      "InitialSimulatedFlowtime,"+
      ConsumerTypes.flatMap(consumer  => // Sort keys in alphabetic order
        ListMap(outcomeWithNegotiation.metrics.toSeq.sortBy(_._1):_*).keys.map { metric =>
          val columnName = s"$consumer$metric"
          columnName //+ "Open," + columnName + "Mean," + columnName + "Close"
      }).mkString("",",",",") +
      ListMap(outcomeWithoutNegotiation.metrics.toSeq.sortBy(_._1):_*).keys.map { metric =>
        val columnName = s"Consumer$metric"
        columnName //+ "Open," + columnName + "Mean," + columnName + "Close"
      }.mkString("",",",",")

  /**
    * Run a configuration for a list of consumers
   * and returns the outcome as a string
    */
  def runConfiguration(configuration: ConsumptionConfiguration, consumerTypes : List[String], fileName: String) : String = {
    var csv = ""

    val writerDetailed = new PrintWriter(new File(subFolder + fileName + "Detailed" + configuration.m + "-" + configuration.n + ".csv"))
    writerDetailed.write(headerDetailedRuns(consumerTypes) +"\n")
    writerDetailed.flush()

    if (debug) println("configuration: "+configuration.value)
    // Repeated experiments
    var nbRuns = 0
    var outcomes = Map[String,List[ConsumptionOutcome]]()
    var consumptionOutcomes = Map[String,List[ConsumptionOutcome]]()
    var incomesLocalityRatios = List[Double]()
    var incomesGlobalFlowtimes = List[Double]()
    var incomesSimulatedFlowtimes = List[Double]()

    var meanRealFlowtimes : List[Double] = List[Double]()
    var meanGlobalFlowtimes : List[Double] = List[Double]()
    var meanSimulatedFlowtimes : List[Double] = List[Double]()

    var consumers = List[Consumer]()
    for(_ <- 1 to nbSTAPS) { // foreach MASTA+
      val l = configuration.l// with l jobs
      val m = configuration.m // with m nodes
      val n = configuration.n // with n tasks, 3 tasks per job/node
      val o = 10 // with o resources per task
      val d = 3 // d instances per resource
      val stap = STAP.randomProblem(l, m, n, o, d, configuration.randomGenerationRule)
      for(_ <- 1 to nbAllocations) { // foreach initial allocation
        val decentralizedSystem = ActorSystem("DecentralizedSystem1")
        val decentralizedSystem2 = ActorSystem("DecentralizedSystem2")
        val decentralizedSystem3 = ActorSystem("DecentralizedSystem3")
        val decentralizedConsumerSystem = ActorSystem("DecentralizedConsumer")
        val decentralizedBalancerSystem = ActorSystem("DecentralizedBalancer")

        var allocation = ExecutedAllocation.randomAllocation(stap)
        if (fromStable){
          val balancer = new AgentBasedBalancer(stap, GlobalFlowtime,
            ConcreteMultiProposalStrategy(),
            None,
            new NoneSingleCounterProposalStrategy,
            decentralizedBalancerSystem, "AgentBasedBalancer",decentralizedDispatcherId, false)
            allocation = ExecutedAllocation(balancer.run().get)
        }
        var detailedDataForOneRun = s"${configuration.value},"
        var outcome = new ConsumptionOutcome()
        var initialLocalRatio = 0.0
        var initialGlobalFlowtime = 0.0
        var initialSimulatedFlowtime = 0.0
        nbRuns += 1
        consumers = List[SynchronousConsumer]()
        val setting = new ConsumptionSetting(allocation, consumer)
        initialLocalRatio = setting.localityRate
        initialGlobalFlowtime = setting.globalFlowtime
        initialSimulatedFlowtime = setting.simulatedFlowtime
        detailedDataForOneRun += s"$initialLocalRatio,$initialGlobalFlowtime,$initialSimulatedFlowtime,"
        consumerTypes.foreach{ ConsumerType => // foreach consumer type
          val consumer = ConsumerType match {
            case "DecentralizedConsumerMultiDelegationGlobalFlowtime" =>
               new AgentBasedConsumer(stap,
                 GlobalFlowtime,
                 ConcreteMultiProposalStrategy(1000.0),
                 None,
                 new NoneSingleCounterProposalStrategy,
                 decentralizedSystem, name = ConsumerType, dispatcherId = decentralizedDispatcherId,
                 monitor =false, simulatedCost)
            case "PSI" =>
              new SynchronousDecentralizedConsumer(stap,
                decentralizedSystem2,
                name = "PSI",
                dispatcherId = decentralizedDispatcherId,
                monitor = false,
                simulated = simulatedCost,
                assi = false,
                rssi = true)
            case "SSI" =>
              new SynchronousDecentralizedConsumer(stap,
                decentralizedSystem3,
                name = "SSI",
                dispatcherId = decentralizedDispatcherId,
                monitor = false,
                simulated = simulatedCost,
                assi = true,
                rssi = false)
            case s =>
              throw new RuntimeException(s"ERROR ConsumptionCampaign is not able to manage consumer $s")
          }
          //Debug
          if (trace){ consumer match {
            case s: AgentBasedConsumer => s.trace = false
            case _ =>
            }
          }
          consumers ::= consumer
          val setting = new ConsumptionSetting(allocation, consumer)
          initialLocalRatio = setting.localityRate
          initialGlobalFlowtime = setting.globalFlowtime
          initialSimulatedFlowtime = setting.simulatedFlowtime
          val experiments = new ConsumptionExperiment(setting)
          if (debug) println(s"ConsumptionCampaign: configuration=$configuration consumer=${consumer.name} run $nbRuns")
          incomesLocalityRatios  ::= setting.localityRate
          incomesGlobalFlowtimes ::= setting.globalFlowtime
          incomesSimulatedFlowtimes ::= setting.simulatedFlowtime
          outcome = experiments.run()
          if (debug) println(s"ConsumptionCampaign: configuration=$configuration consumer=${consumer.name} run $nbRuns ends")
          outcomes = outcomes.updatedWith(consumer.name)({
            case Some(list) => Some(outcome :: list)
            case None => Some(List(outcome))
          })
          detailedDataForOneRun += s"${outcome.metrics("00MeanRealFlowtime")},${outcome.metrics("01MeanGlobalFlowtime")},${outcome.metrics("02SolvingTime")}," +
            s"${outcome.metrics("03LocalRatio")},${outcome.metrics("04NbDeals")},${outcome.metrics("05NbDelegations")},${outcome.metrics("06NbSwaps")}," +
            s"${outcome.metrics("07NbTimeouts")},${outcome.metrics("08EndowmentSize")},${outcome.metrics("09NbDelegatedTasks")}," +
            s"${outcome.metrics("10NbFirstStages")},${outcome.metrics("11NbSecondStages")},${outcome.metrics("12MeanSimulatedFlowtime")},"
        }

        consumer = new SynchronousDecentralizedConsumer(stap,
          decentralizedConsumerSystem,
          name ="DecentralizedConsumer",
          decentralizedDispatcherId,
          monitor = false,
          simulatedCost,
          assi = false,
          rssi = false)

        val consumptionSetting = new ConsumptionSetting(allocation, consumer)
        val consumptionExperiments = new ConsumptionExperiment(consumptionSetting)
        if (debug) println(s"ConsumptionCampaign: configuration=$configuration consumer=${consumer.name} run $nbRuns")

        outcomeWithoutNegotiation = consumptionExperiments.run()
        if (debug) println(s"ConsumptionCampaign: configuration=$configuration consumer=${consumer.name} run $nbRuns ends")
        consumptionOutcomes = consumptionOutcomes.updatedWith(consumer.name)({
            case Some(list) => Some(outcomeWithoutNegotiation :: list)
            case None => Some(List(outcomeWithoutNegotiation))
          })

        detailedDataForOneRun += s"${outcomeWithoutNegotiation.metrics("00MeanRealFlowtime")},${outcomeWithoutNegotiation.metrics("01MeanGlobalFlowtime")},${outcomeWithoutNegotiation.metrics("02SolvingTime")}," +
          s"${outcomeWithoutNegotiation.metrics("03LocalRatio")},${outcomeWithoutNegotiation.metrics("04NbDeals")},${outcomeWithoutNegotiation.metrics("05NbDelegations")},${outcomeWithoutNegotiation.metrics("06NbSwaps")}," +
          s"${outcomeWithoutNegotiation.metrics("07NbTimeouts")},${outcomeWithoutNegotiation.metrics("08EndowmentSize")},${outcomeWithoutNegotiation.metrics("09NbDelegatedTasks")}," +
          s"${outcomeWithoutNegotiation.metrics("10NbFirstStages")},${outcomeWithoutNegotiation.metrics("11NbSecondStages")},${outcomeWithoutNegotiation.metrics("12MeanSimulatedFlowtime")},"

        writerDetailed.write(s"$detailedDataForOneRun\n")
        writerDetailed.flush()
        
        decentralizedSystem.terminate()
        decentralizedConsumerSystem.terminate()
      }
    }
    writerDetailed.close()

    consumers = consumers.reverse
    if (debug) println(s"nbRuns: $nbRuns")
    csv += s"${configuration.value}"
    // Collect income
    incomesLocalityRatios = incomesLocalityRatios.sortWith(_ < _)
    incomesGlobalFlowtimes = incomesGlobalFlowtimes.sortWith(_ < _)
    incomesSimulatedFlowtimes = incomesSimulatedFlowtimes.sortWith(_ < _)
    val (openLocalRatio,meanLocalRatio,closeLocalRatio) =
      (incomesLocalityRatios(incomesLocalityRatios.size/4),
        incomesLocalityRatios(incomesLocalityRatios.size/2),
        incomesLocalityRatios(incomesLocalityRatios.size*3/4))
    val (openGlobalFlowtime,meanGlobalFlowtime,closeGlobalFlowtime) =
      (incomesGlobalFlowtimes(incomesGlobalFlowtimes.size/4),
        incomesGlobalFlowtimes(incomesGlobalFlowtimes.size/2),
        incomesGlobalFlowtimes(incomesGlobalFlowtimes.size*3/4))
    val (openSimulatedFlowtime,meanSimulatedFlowtime,closeSimulatedFlowtime) =
      (incomesSimulatedFlowtimes(incomesSimulatedFlowtimes.size/4),
        incomesSimulatedFlowtimes(incomesSimulatedFlowtimes.size/2),
        incomesSimulatedFlowtimes(incomesSimulatedFlowtimes.size*3/4))
    csv += s",$openLocalRatio,$meanLocalRatio,$closeLocalRatio," +
      s"$openGlobalFlowtime,$meanGlobalFlowtime,$closeGlobalFlowtime,"+
      s"$openSimulatedFlowtime,$meanSimulatedFlowtime,$closeSimulatedFlowtime"

    // Collect outcome for consumers
    consumers.foreach { consumer =>
      println(consumer.name)
      meanRealFlowtimes = outcomes(consumer.name).map( _.metrics("00MeanRealFlowtime").asInstanceOf[Double]).sortWith(_ < _)
      meanGlobalFlowtimes = outcomes(consumer.name).map( _.metrics("01MeanGlobalFlowtime").asInstanceOf[Double]).sortWith(_ < _)
      val solvingTimes : List[Double] = outcomes(consumer.name).map( _.metrics("02SolvingTime").asInstanceOf[Double]).sortWith(_ < _)
      val localRatios : List[Double] = outcomes(consumer.name).map( _.metrics("03LocalRatio").asInstanceOf[Double]).sortWith(_ < _)
      val nbDeals : List[Double] = outcomes(consumer.name).map( _.metrics("04NbDeals").asInstanceOf[Double]).sortWith(_ < _)
      val nbDelegations : List[Double] = outcomes(consumer.name).map( _.metrics("05NbDelegations").asInstanceOf[Double]).sortWith(_ < _)
      val nbSwaps : List[Double] = outcomes(consumer.name).map( _.metrics("06NbSwaps").asInstanceOf[Double]).sortWith(_ < _)
      val nbTimeouts : List[Double] = outcomes(consumer.name).map( _.metrics("07NbTimeouts").asInstanceOf[Double]).sortWith(_ < _)
      val avgEndowmentSize : List[Double] = outcomes(consumer.name).map( _.metrics("08EndowmentSize").asInstanceOf[Double]).sortWith(_ < _)
      val nbDelegatedTasks : List[Double] = outcomes(consumer.name).map( _.metrics("09NbDelegatedTasks").asInstanceOf[Double]).sortWith(_ < _)
      val nbFirstStages: List[Double] = outcomes(consumer.name).map( _.metrics("10NbFirstStages").asInstanceOf[Double]).sortWith(_ < _)
      val nbSecondStages: List[Double] = outcomes(consumer.name).map( _.metrics("11NbSecondStages").asInstanceOf[Double]).sortWith(_ < _)
      meanSimulatedFlowtimes = outcomes(consumer.name).map( _.metrics("12MeanSimulatedFlowtime").asInstanceOf[Double]).sortWith(_ < _)

      val open = new DealConsumptionOutcome(Map(
        "00MeanRealFlowtime" -> meanRealFlowtimes(nbRuns/4),
        "01MeanGlobalFlowtime" -> meanGlobalFlowtimes(nbRuns/4),
        "02SolvingTime" -> solvingTimes(nbRuns/4),
        "03LocalRatio" -> localRatios(nbRuns/4),
        "04NbDeals" -> nbDeals(nbRuns/4),
        "05NbDelegations" -> nbDelegations(nbRuns/4),
        "06NbSwaps" -> nbSwaps(nbRuns/4),
        "07NbTimeouts" -> nbTimeouts(nbRuns/4),
        "08EndowmentSize" ->  avgEndowmentSize(nbRuns/4),
        "09NbDelegatedTasks" -> nbDelegatedTasks(nbRuns/4),
        "10NbFirstStages" -> nbFirstStages(nbRuns/4),
        "11NbSecondStages" -> nbSecondStages(nbRuns/4),
        "12MeanSimulatedFlowtime" -> meanSimulatedFlowtimes(nbRuns/4)
      ))
      val mean = new DealConsumptionOutcome(Map(
        "00MeanRealFlowtime" -> meanRealFlowtimes(nbRuns/2),
        "01MeanGlobalFlowtime" -> meanGlobalFlowtimes(nbRuns/2),
        "02SolvingTime" -> solvingTimes(nbRuns/2),
        "03LocalRatio" -> localRatios(nbRuns/2),
        "04NbDeals" -> nbDeals(nbRuns/2),
        "05NbDelegations" -> nbDelegations(nbRuns/2),
        "06NbSwaps" -> nbSwaps(nbRuns/2),
        "07NbTimeouts" -> nbTimeouts(nbRuns/2),
        "08EndowmentSize" -> avgEndowmentSize(nbRuns/2),
        "09NbDelegatedTasks" -> nbDelegatedTasks(nbRuns/2),
        "10NbFirstStages" -> nbFirstStages(nbRuns/2),
        "11NbSecondStages" -> nbSecondStages(nbRuns/2),
        "12MeanSimulatedFlowtime" -> meanSimulatedFlowtimes(nbRuns/2)
      ))
      val close = new DealConsumptionOutcome(Map(
        "00MeanRealFlowtime" -> meanRealFlowtimes(nbRuns*3/4),
        "01MeanGlobalFlowtime" -> meanGlobalFlowtimes(nbRuns*3/4),
        "02SolvingTime" -> solvingTimes(nbRuns*3/4),
        "03LocalRatio" -> localRatios(nbRuns*3/4),
        "04NbDeals" -> nbDeals(nbRuns*3/4),
        "05NbDelegations" -> nbDelegations(nbRuns*3/4),
        "06NbSwaps" -> nbSwaps(nbRuns*3/4),
        "07NbTimeouts" -> nbTimeouts(nbRuns*3/4),
        "08EndowmentSize" -> avgEndowmentSize(nbRuns*3/4),
        "09NbDelegatedTasks" -> nbDelegatedTasks(nbRuns*3/4),
        "10NbFirstStages" -> nbFirstStages(nbRuns*3/4),
        "11NbSecondStages" -> nbSecondStages(nbRuns*3/4),
        "12MeanSimulatedFlowtime" -> meanSimulatedFlowtimes(nbRuns*3/4),
      ))
      csv += s"," +
        s"${open.metrics("00MeanRealFlowtime")},${mean.metrics("00MeanRealFlowtime")},${close.metrics("00MeanRealFlowtime")}," +
        s"${open.metrics("01MeanGlobalFlowtime")},${mean.metrics("01MeanGlobalFlowtime")},${close.metrics("01MeanGlobalFlowtime")}," +
        s"${open.metrics("02SolvingTime")},${mean.metrics("02SolvingTime")},${close.metrics("02SolvingTime")}," +
        s"${open.metrics("03LocalRatio")},${mean.metrics("03LocalRatio")},${close.metrics("03LocalRatio")}," +
        s"${open.metrics("04NbDeals")},${mean.metrics("04NbDeals")},${close.metrics("04NbDeals")}," +
        s"${open.metrics("05NbDelegations")},${mean.metrics("05NbDelegations")},${close.metrics("05NbDelegations")}," +
        s"${open.metrics("06NbSwaps")},${mean.metrics("06NbSwaps")},${close.metrics("06NbSwaps")}," +
        s"${open.metrics("07NbTimeouts")},${mean.metrics("07NbTimeouts")},${close.metrics("07NbTimeouts")}," +
        s"${open.metrics("08EndowmentSize")},${mean.metrics("08EndowmentSize")},${close.metrics("08EndowmentSize")}," +
        s"${open.metrics("09NbDelegatedTasks")},${mean.metrics("09NbDelegatedTasks")},${close.metrics("09NbDelegatedTasks")}," +
        s"${open.metrics("10NbFirstStages")},${mean.metrics("10NbFirstStages")},${close.metrics("10NbFirstStages")},"+
        s"${open.metrics("11NbSecondStages")},${mean.metrics("11NbSecondStages")},${close.metrics("11NbSecondStages")},"+
        s"${open.metrics("12MeanSimulatedFlowtime")},${mean.metrics("12MeanSimulatedFlowtime")},${close.metrics("12MeanSimulatedFlowtime")}"
    }

    // Collect outcomes for consumption only
    println(consumer.name)
    val meanRealFlowtimesConsumption : List[Double] = consumptionOutcomes(consumer.name).map( _.metrics("00MeanRealFlowtime").asInstanceOf[Double]).sortWith(_ < _)
    val openConsumption = new DealConsumptionOutcome(Map(
        "00MeanRealFlowtime" -> meanRealFlowtimesConsumption(nbRuns/4)
      ))
    val meanConsumption = new DealConsumptionOutcome(Map(
        "00MeanRealFlowtime" -> meanRealFlowtimesConsumption(nbRuns/2)
      ))
    val closeConsumption = new DealConsumptionOutcome(Map(
        "00MeanRealFlowtime" -> meanRealFlowtimesConsumption(nbRuns*3/4)
      ))
    csv += s"," +
        s"${openConsumption.metrics("00MeanRealFlowtime")},${meanConsumption.metrics("00MeanRealFlowtime")},${closeConsumption.metrics("00MeanRealFlowtime")},"
    csv // Returns the string representation of the open/mean/close outcomes
  }

  /**
    * Runs the campaign with the list of consumer types
    */
  def run(consumerTypes : List[String], fileName : String): Unit = {
    val writer = new PrintWriter(new File(subFolder + fileName + ".csv"))
    writer.write(header(consumerTypes) +"\n")
    writer.flush()
    for(configuration <- configurations){
      val outcome = runConfiguration(configuration, consumerTypes, fileName)
      writer.write(s"$outcome\n")
      writer.flush()
    }
    writer.close()
    println("That's all folk !")
  }
}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2023
package org.smastaplus.experiment.consumer

import org.smastaplus.core.SimulatedCostE

/**
 * Object running a campaign which compares consumption process, eventually  with (re)allocation
 * from a stable allocation
 */
object WithVsWithoutNegotiationFromStableCampaign extends App {
  val fileName = "consumption4jobs8nodes5staps5allocationsFromStable040-320tasks"
  val balancerTypes : List[String] = List("DecentralizedConsumerMultiDelegationGlobalFlowtime", "ASSI", "RSSI")
  private val campaign = new ConsumptionCampaign(simulatedCost = new SimulatedCostE, fromStable = true, false)
  campaign.run(balancerTypes, fileName)
}

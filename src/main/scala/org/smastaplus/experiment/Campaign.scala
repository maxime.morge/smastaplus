// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.experiment

import java.time._
import java.time.format._

/**
  * Abstract class representing
 */
class Campaign {

  var debug = false

  protected val timestamp : DateTimeFormatter =
    format.DateTimeFormatter.ofPattern("YYYY-MM-dd-H-MM", java.util.Locale.FRANCE)

  val folder : String = "experiments/"

}

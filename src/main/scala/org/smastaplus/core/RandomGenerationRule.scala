// Copyright (C) Maxime MORGE 2020, 2021
package org.smastaplus.core

/**
  * Class representing a random generation rule
  */
class RandomGenerationRule
case object Uncorrelated extends RandomGenerationRule{
  override def toString : String = "Uncorrelated"
}
case object NodeCorrelated extends RandomGenerationRule{
  override def toString : String = "NodeCorrelated"
}
case object TaskCorrelated extends RandomGenerationRule{
  override def toString : String = "TaskCorrelated"
}
case object JobCorrelated extends RandomGenerationRule{
  override def toString : String = "JobCorrelated"
}
case object NodeTaskCorrelated extends RandomGenerationRule{
  override def toString : String = "NodeTaskCorrelated"
}
case object NodeJobCorrelated extends RandomGenerationRule{
  override def toString : String = "NodeJobCorrelated"
}
case object TaskJobCorrelated extends RandomGenerationRule{
  override def toString : String = "TaskJobCorrelated"
}
case object NodeTaskJobCorrelated extends RandomGenerationRule{
  override def toString : String = "NodeTaskJobCorrelated"
}

// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.core

/**
  * Class representing a computing node
  * @param name of the computing node
  */
final class ComputingNode(val name : String) extends Ordered[ComputingNode]{

  override def toString: String = name

  override def equals(that: Any): Boolean =
    that match {
      case that: ComputingNode => that.canEqual(this) && this.name == that.name
      case _ => false
    }
  def canEqual(a: Any) : Boolean = a.isInstanceOf[ComputingNode]

  /**
    * Returns 0 if this an that are the same, negative if this < that, and positive otherwise
    * Computing nodes are sorted with their name
    */
  def compare(that: ComputingNode) : Int = {
    if (this.name == that.name) return 0
    else if (this.name > that.name) return 1
    -1
  }
}

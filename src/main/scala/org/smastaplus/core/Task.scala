// Copyright (C) Maxime MORGE 2020
package org.smastaplus.core

/**
  * Class representing a task which requires resources to produce an outcome
  * @param name of the task
  * @param resources which needs to be processed
  */
class Task(val name : String,
           var resources: Set[Resource]) extends Ordered[Task]{

  override def toString: String = name

  /**
    * Returns a full description of the task
    */
  def describe: String = s"$name: "+resources.mkString(", ")

  override def equals(that: Any): Boolean =
    that match {
      case that: Task => that.canEqual(this) && this.name == that.name
      case _ => false
    }

  def canEqual(a: Any): Boolean = a.isInstanceOf[Task]

  /**
    * Returns 0 if this and that are the same, negative if this < that, and positive otherwise
    * Tasks are sorted with their name
    */
  def compare(that: Task) : Int = {
    if (this.name == that.name) return 0
    else if (this.name > that.name) return 1
    -1
  }
}

/**
  * The default task
  */
object NoTask extends Task("NoTask", Set[Resource]()){
  override def toString: String = "θ"
}


// Copyright (C) Maxime MORGE 2020, 2021
package org.smastaplus.core

/**
 * Class representing a social rule
 */
class SocialRule{
  @throws(classOf[RuntimeException])
  override def toString: String = this match{
    case Makespan => "makespan"
    case LocalFlowtime => "localFlowtime"
    case GlobalFlowtime => "globalFlowtime"
    case LocalFlowtimeAndMakespan => "localFlowtimeAndMakespan"
    case LocalFlowtimeAndGlobalFlowtime => "localFlowtimeAndGlobalFlowtime"
    case rule => throw new RuntimeException(s"ERROR : Social rule $rule is neither makespan nor localFlowtime nor globalFlowtime")
  }
}
case object LocalFlowtimeAndGlobalFlowtime extends SocialRule
case object LocalFlowtimeAndMakespan extends SocialRule
case object LocalFlowtime extends SocialRule
case object GlobalFlowtime extends SocialRule
case object Makespan extends SocialRule
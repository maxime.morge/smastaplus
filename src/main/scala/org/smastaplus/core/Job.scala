// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.core

import org.joda.time.LocalDateTime

/**
  * Class representing a job is a set of independent, non divisible and non preemptive tasks without precedence order.
  * @param name of the job
  * @param tasks set of tasks
  * @param releaseTime release time of the job, now by default
  */
class Job(val name: String,
          var tasks: Set[Task],
          var releaseTime: LocalDateTime = LocalDateTime.now()) extends Ordered[Job]{

  override def toString: String = name

  /**
    * Returns a full description of the job with the release time and the tasks
    * */
  def describe: String = s"$name: " + tasks.mkString(", ")+"\n"+
  s"releaseTime: $name $releaseTime"

  override def equals(that: Any): Boolean =
    that match {
      case that: Job => that.canEqual(this) && this.name == that.name
      case _ => false
    }
  def canEqual(a: Any): Boolean = a.isInstanceOf[Job]

  /**
    * Returns 0 if this and that are the same, negative if this < that, and positive otherwise
    * Jobs are sorted with their name
    */
  def compare(that: Job) : Int = {
    if (this.name == that.name) return 0
    else if (this.name > that.name) return 1
    -1
  }

  /**
    * Returns a job without the task
    */
  def remove(task : Task) : Job = new Job(name, tasks.filter(_ != task), releaseTime)

  /**
    * Reset the job's release time
    */
  def resetReleaseTime() : Unit = {
    releaseTime = LocalDateTime.now()
  }

  /**
   * Update the job's release time
   */
  def updateReleaseTime(time: LocalDateTime): Unit = {
    releaseTime = time
  }

}

// Copyright (C) Maxime MORGE, , Ellie BEAUPREZ 2020, 2023
package org.smastaplus.core

import org.smastaplus.utils.RandomUtils

import scala.collection.SortedSet
import scala.util.{Failure, Success, Try}
import org.joda.time.LocalDateTime

/**
  * Class representing a distributed system which is composed of
  * @param computingNodes a pool of computing nodes available within the infrastructure which can be activated in order to process the tasks
  * @param resourceNodes each task requires resources which are distributed across different resource nodes
  * @param acquaintance is a vicinity property which assesses if a computing node is local to a resource node
  * @param resources requires to compute the tasks. These resources are transferable and non consumable.
  * @param locations the location of resources, which are possibly replicated
  */
class DistributedSystem(val computingNodes: SortedSet[ComputingNode],
                        val resourceNodes: SortedSet[ResourceNode],
                        val acquaintance: Map[(ComputingNode, ResourceNode), Boolean],
                        val resources: SortedSet[Resource],
                        val locations: Map[Resource, Set[ResourceNode]]) {
  /**
    * Initial boot time
    */
  var t0: LocalDateTime = LocalDateTime.now()

  def m : Int = computingNodes.size
  def m2 : Int = resourceNodes.size
  def k : Int = resources.size

  /**
    * Returns a string describing the MASTAPlus problem
    */
  override def toString: String = {
    "m: " + m + "\n" +
      "m2: " + m + "\n" +
      "k: " + k + "\n" +
      "computingNodes: " + computingNodes.mkString(", ") + "\n" +
      "resourceNodes: " + computingNodes.mkString(", ") + "\n" +
      "resources: " + resources.map(_.describe).mkString(", ") + "\n" +
      resources.toList.map(resource =>
          s"$resource: " + locations(resource).mkString(", ")
      ).mkString("\n") + "\n" +
      "acquaintance: " + acquaintance.filter(a => a._2 ).keys.mkString("; ")
  }

  /**
   * Returns true if a resource is local to a computing node
   */
  def local(resource: Resource, computingNode: ComputingNode): Boolean =
    locations(resource).exists( resourceNode =>
      Try(acquaintance(computingNode, resourceNode)) match {
        case Success(_) => acquaintance(computingNode, resourceNode)
        case Failure(_) => false
      }
    )

  /**
    * Returns true if the all the acquaintances and locations are specified
    */
  def isFullySpecified: Boolean = locations.size == k

  /**
    * Returns a computing node
    * @param name of the computing node
    */
  def getComputingNode(name: String): ComputingNode = {
    computingNodes.find(a => a.name.equals(name)) match {
      case Some(s) => s
      case None => throw new RuntimeException("No computing node " + name + " has been found")
    }
  }

  /**
    * Returns a resource node
    * @param name of the resource node
    */
  def getResourceNode(name: String): ResourceNode = {
    resourceNodes.find(a => a.name.equals(name)) match {
      case Some(s) => s
      case None => throw new RuntimeException("No resource node " + name + " has been found")
    }
  }

  /**
    * Returns a resource
    * @param name of the resource
    */
  def getResource(name: String): Resource = {
    resources.find(a => a.name.equals(name)) match {
      case Some(s) => s
      case None => throw new RuntimeException("No node " + name + " has been found")
    }
  }

  /**
    * Returns the total size of resources
    */
  def sizeOfResources : Double = resources.foldLeft(0.0)((sum: Double, resource: Resource) => sum + resource.size)
}

/**
  * Factory for Distributed system
  */
object DistributedSystem{

  implicit val order  : Ordering[Double] = Ordering.Double.TotalOrdering
  // eventually Ordering.Double.IeeeOrdering

  /**
    * Returns a random distributed system
    * with m computing nodes and k resources
    * of size size
    */
  def randomProblem(m: Int, k: Int, rule: RandomGenerationRule, duplicate: Int = 3, size: Int): DistributedSystem = {
    // Nodes
    val computingNodes: SortedSet[ComputingNode] = collection.immutable.SortedSet[ComputingNode]() ++
      (for (i <- 1 to m) yield new ComputingNode(name = "cn%02d".format(i)))
    val resourceNodes: SortedSet[ResourceNode] = collection.immutable.SortedSet[ResourceNode]() ++
      (for (i <- 1 to m) yield new ResourceNode(name = "rn%02d".format(i)))

    // which are connected one-by-one
    var acquaintance = Map[(ComputingNode, ResourceNode), Boolean]()
    for(i <- 0 until m) {
      for (j <- 0 until m) {
        val nodeI = computingNodes.toList(i)
        val nodeJ = resourceNodes.toList(j)
        if (j == i) acquaintance += ((nodeI, nodeJ) -> true)
        else acquaintance += ((nodeI, nodeJ) -> false)
      }
    }

    // Resources
    val resources: SortedSet[Resource] = collection.immutable.SortedSet[Resource]() ++ (rule match {
      case JobCorrelated => for (i <- 1 to k) yield new Resource(name = s"r$i", size )
      case _ =>  for (i <- 1 to k) yield new Resource(name = s"r$i", RandomUtils.strictPositiveWeight()* size)
    })
    // which are distributed on duplicate (e.g. 3) different nodes
    var locations = Map[Resource, Set[ResourceNode]]()
    resources.foreach { r =>
      locations += (r -> RandomUtils.pick[ResourceNode](resourceNodes, duplicate))
    }
    new DistributedSystem(computingNodes, resourceNodes, acquaintance, resources, locations)
    }

  /**
    * Test Example 1
    */
  def main(args: Array[String]): Unit = {
    import org.smastaplus.example.stap.ex1._
    println(ds)
  }
}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.core

import org.smastaplus.utils.RandomUtils

import scala.collection.SortedSet
import org.joda.time.LocalDateTime
import scala.annotation.unused

//import org.joda.time.LocalDateTime

/**
  * Class representing a Situated Task Allocation Problem with concurrent jobs
  * @param ds is the underlying distributed system
  * @param jobs are the concurrent jobs
  * @param tasks are the tasks of all the jobs
  */
class STAP(val ds: DistributedSystem,
           val jobs: SortedSet[Job],
           val tasks : SortedSet[Task]) {

  /**
    * Returns a string describing the MASTAPlus problem
    */
  override def toString: String = {
    s"$ds\n" +
      s"n: ${tasks.size}\n" +
      s"l: ${jobs.size}\n" +
      "tasks: " + tasks.mkString(", ") + "\n" +
      "jobs: " + jobs.mkString(", ") + "\n" +
      tasks.map(_.describe).mkString("\n") + "\n" +
      jobs.map(_.describe).mkString("\n")
  }

  /**
    * Returns the number of computing nodes
    */
  def m: Int = ds.computingNodes.size

  /**
    * Returns the number of tasks
    */
  def n: Int = tasks.size

  /**
    * Returns the number of jobs
    */
  def l: Int = jobs.size


  def resetReleaseTime() : Unit = {
    ds.t0 = LocalDateTime.now()
    for (job <- jobs) job.resetReleaseTime()
  }
  

  /**
    * Returns the cost of a task for a node
    * We assume that the resources are accessible for any agent and
    * the download cost is equal to the processing cost
    */
  @throws(classOf[RuntimeException])
  def cost(task: Task, computingNode: ComputingNode) : Double = {
    if (! tasks.contains(task) || ! ds.computingNodes.contains(computingNode))
      throw new RuntimeException(s"No meaning to give the cost of the task $task for the computing node $computingNode")
    var cost = 0.0
    task.resources.foreach{ resource =>
      if (ds.local(resource, computingNode))
        cost += resource.size
      else cost +=  resource.size * 2 
    }
    cost
  }

  /**
    * Returns the cost for a computing node as a Map
    */
  def costs(node : ComputingNode) : Map[Task, Double] =  (tasks.toSet map { task : Task => (task ,cost(task, node)) }).toMap

  /**
    * Return the maximum workload of a computing node
    */
  @unused
  def maximumWorkload : Double = ds.computingNodes.map(n => costs(n).values.sum ).max

  /**
    * Return a string description of the corresponding transportation problem
    * in the Optimization Programming Language
    */
  def toTransportation: String = {
    var q= Seq[java.io.Serializable]()
    for (k <- 1 to m) {
      q :+= ds.computingNodes.toList.map(node =>
        tasks.toList.map(task =>
          (k*cost(task, node)).toString
        ).mkString("[", ", ", "]"))
    }
    "M = " + m + "; \n" +
      "N = " + n + "; \n" +
      "Q = " + q.mkString("[", ", ", "] ;\n").replace("List(", "").replace(")", "")
  }

  /**
    * Return a string description of the corresponding time minimizing
    * transportation problem with bundles
    * in the Optimization Programming Language
    */
  def toMinimizingTimeTransportation: String = {
    var q= Seq[java.io.Serializable]()
    for (k <- 1 to m) {
      q :+= ds.computingNodes.toList.map(node =>
        tasks.toList.map(task =>
          (k*cost(task, node)).toString
        ).mkString("[", ", ", "]"))
    }
    val y : Seq[java.io.Serializable] = tasks.toList.map(task =>
      jobs.toList.map(job =>
        if (job.tasks.contains(task)) "1.0" else "0.0"
      ).mkString("[", ", ", "]"))

    "M = " + m + "; \n" +
      "N = " + n + "; \n" +
      "L = " + l + "; \n" +
      "Q = " + q.mkString("[", ", ", "] ;\n").replace("List(", "").replace(")", "") +
      "Y = " + y.mkString("[", ", ", "] ;\n").replace("List(", "").replace(")", "")
  }


  /**
    * Return a string description of the corresponding non-linear program
    * in the Optimization Programming Language
    */
  def toNonLinear: String = {
    var c= Seq[java.io.Serializable]()
    c :+= ds.computingNodes.toList.map(node =>
      tasks.toList.map(task =>
        cost(task, node).toString
      ).mkString("[", ", ", "]"))
    val y : Seq[java.io.Serializable] = tasks.toList.map(task =>
      jobs.toList.map(job =>
        if (job.tasks.contains(task)) "1.0" else "0.0"
      ).mkString("[", ", ", "]"))

    "M = " + m + "; \n" +
      "N = " + n + "; \n" +
      "L = " + l + "; \n" +
      "C = " + c.mkString("[", ", ", "] ;\n").replace("List(", "").replace(")", "") +
      "Y = " + y.mkString("[", ", ", "] ;\n").replace("List(", "").replace(")", "")
  }

  /**
    * Returns true if the jobs contains all the tasks and only the tasks
    */
  def isFullySpecified: Boolean = {
    jobs.foldLeft(Set[Task]())((acc, j) => acc.union(j.tasks)) == tasks.toSet
  }

  /**
    * Returns the job of the task
    */
  @throws(classOf[RuntimeException])
  def jobOf(task : Task) : Job = {
    jobs.find( job => job.tasks.contains(task)) match {
      case Some(j) => j
      case None => throw new RuntimeException(s"No job containing $task has been found")
    }
  }

  /**
    * Returns the jobs of the tasks
    */
  def jobsOf(tasks: Set[Task]): Set[Job] = tasks.map(jobOf)

  /**
    * Returns the tasks of a job in a set of tasks
    */
  def tasksOf(tasks: Set[Task], job : Job): Set[Task] = tasks.filter(jobOf(_) == job)

  /**
   * Returns the tasks of the other jobs
   */
  private def tasksOfOtherJobs(job: Job): SortedSet[Task] = collection.immutable.SortedSet[Task]() ++ tasks.filter(jobOf(_) != job)

  /**
    * Returns a computing node
    * @param name of the computing node
    */
  @throws(classOf[RuntimeException])
  def getComputingNode(name: String): ComputingNode = {
    ds.computingNodes.find(a => a.name.equals(name)) match {
      case Some(s) => s
      case None => throw new RuntimeException("No computing node " + name + " has been found")
    }
  }

  /**
    * Returns a resource node
    * @param name of the resource node
    */
  @throws(classOf[RuntimeException])
  def getResourceNode(name: String): ResourceNode = {
    ds.resourceNodes.find(a => a.name.equals(name)) match {
      case Some(s) => s
      case None => throw new RuntimeException("No resource node " + name + " has been found")
    }
  }

  /**
    * Returns a job
    * @param name of the job
    */
  @throws(classOf[RuntimeException])
  @unused
  def getJob(name: String): Job = {
    jobs.find(j => j.name.equals(name)) match {
      case Some(s) => s
      case None => throw new RuntimeException("No job " + name + " has been found")
    }
  }

  /**
    * Return a JSON description of the STAP
    */
  @throws(classOf[RuntimeException])
  def toJSON: String = {
    ds.resourceNodes.map(node =>
      ds.resources.map { resource =>
        val job = jobs.find(j => j.tasks.exists(t => t.resources.contains(resource))) match {
          case Some(j) => j
          case None => throw new RuntimeException(s"STAP.toJSON: no job found for the resource $resource")
        }
        val size = if (ds.locations(resource).contains(node)) resource.size
        else 0.0
        s"""\t\t\t{\"node\": \"${node.name}\", \"resource\": \"${resource.name}\", \"job\": \"${job.name}\", \"size\": $size }"""
      }.mkString("", ",\n\t", "")
    ).mkString("\t\t[\n\t", ",\n\t","\n\t\t]")
  }

  /**
    * Returns all the potential allocations
    */
  def allAllocations(): Set[ExecutedAllocation] = allAllocations(ds.computingNodes, tasks)

  /**
    * Returns all the potential allocations for a subset of computing nodes
    */
  def allAllocations(computingNodes: SortedSet[ComputingNode]): Set[ExecutedAllocation] =
    allAllocations(computingNodes, tasks)

  /**
    * Returns all the potential allocations
    * @param nodes the nodes
    * @param tasks the tasks
    */
  def allAllocations(nodes: SortedSet[ComputingNode], tasks: SortedSet[Task]): Set[ExecutedAllocation] = {
    if (nodes.size == 1) {
      var allocations = Set[ExecutedAllocation]()
      tasks.toList.permutations.foreach { permutation =>
        var allocation = new ExecutedAllocation(this)
        allocation = allocation.update(nodes.head, permutation)
        allocations += allocation
      }
      return allocations // returns allocation where workers has no task
    }
    var allocations = Set[ExecutedAllocation]()
    val node = nodes.head // Select one node
    val otherNodes = nodes. filter(_ != node)
    tasks.subsets().foreach { bundle => // For each subset of tasks
      val complementary = tasks.diff(bundle)
      val subAllocations = allAllocations(otherNodes, complementary) // compute the sub-allocation of the complementary
      // and allocate the current bundle to the node
      subAllocations.foreach { a =>
        bundle.toList.permutations.foreach{ permutation =>
          val newAllocation = a.update(node, permutation)
          allocations += newAllocation
        }
      }
    }
    allocations
  }

  /**
   * Return the STAP without job
   */
  def removeJob(job: Job) : STAP = new STAP(ds, jobs - job , tasksOfOtherJobs(job))
}

/**
  * Factory for STAP
  */
object STAP{

  implicit val order  : Ordering[Double] = Ordering.Double.TotalOrdering
  // eventually Ordering.Double.IeeeOrdering

  private val MAX_COST = 500 // Maximum resource size
  private val VARIANCE = 100  // Variance of resource size

  /**
    * Returns a random STAP instance with
    * @param l jobs
    * @param m nodes
    * @param n tasks
    * @param o resources per task, 1 per default
    * @param d duplicated instances per resources, 3 per default
    * @param rule for random generation
    */
  def randomProblem(l: Int, m: Int, n: Int, o: Int = 1, d: Int = 3, rule: RandomGenerationRule): STAP = {
    if (n % l != 0 ) throw new RuntimeException(s"STAP.randomProblem n ($n) is not divisible by l ($l)")
    val ds = DistributedSystem.randomProblem(m, n * o , rule, d, MAX_COST)
    // Tasks
    var tasks: SortedSet[Task] = SortedSet[Task]()
    var j = 1
    var resources = Set[Resource]()
    ds.resources.foreach{r =>
      resources += r
      if ( j % o == 0){
        tasks = tasks.union(Set(new Task(name = s"t${j/o}", resources)))
        resources = Set[Resource]()
      }
      j += 1
    }

    // Jobs
    var tasks4jobs = tasks
    var jobs: SortedSet[Job] = SortedSet[Job]()
    for (i <- 1 to l) {
      val tasks4thisJob = RandomUtils.pick[Task](tasks4jobs, n/l)
      tasks4jobs = tasks4jobs.diff(tasks4thisJob)
      val job = new Job(name = s"J$i", tasks4thisJob)
      jobs = jobs.union(Set(job))
    }
    //println(s"tâches restantes : $tasks4jobs")

    // m variances for nodes
    val nodeVariance : Array[Double] =  Array.fill(m) {
      rule match {
        case NodeCorrelated =>
          RandomUtils.random(1, MAX_COST)
        case NodeJobCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case NodeTaskCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case NodeTaskJobCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case _ =>
          MAX_COST
      }
    }
    // l variances for jobs
    val jobVariance : Array[Double] =  Array.fill(l) {
      rule match {
        case JobCorrelated =>
          RandomUtils.random(1, MAX_COST)
        case TaskJobCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case NodeJobCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case NodeTaskJobCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case _ =>
          MAX_COST

      }
    }

    //  n variances for tasks
    val taskVariance : Array[Double] =  Array.fill(n) {
      rule match {
        case TaskCorrelated =>
          RandomUtils.random(1, MAX_COST)
        case TaskJobCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case NodeTaskCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case NodeTaskJobCorrelated =>
          RandomUtils.random(1, VARIANCE)
        case _ =>
          MAX_COST
      }
    }

    // Adjust the resource sizes
    jobs.foreach{ job =>
      job.tasks.foreach{ task =>
        task.resources.foreach{ resource =>
          resource.size =  rule match {
            case Uncorrelated =>
              RandomUtils.random(1, MAX_COST)

            case NodeCorrelated =>
              val locations : Set[ResourceNode] = ds.locations(resource)
              val groundNodes : Set[Double] = locations.map{ node =>
                val indexNode : Int = ds.computingNodes.toVector.indexOf(node)
                nodeVariance(indexNode)
              }
              groundNodes.sum/groundNodes.size + RandomUtils.random(1, VARIANCE)

            case TaskCorrelated =>
              val indexTask : Int = tasks.toVector.indexOf(task)
              val groundTask = taskVariance(indexTask)
              groundTask + RandomUtils.random(1, VARIANCE)

            case JobCorrelated =>
              val indexJob : Int = jobs.toVector.indexOf(job)
              val groundJob = jobVariance(indexJob)
              groundJob + RandomUtils.random(1, VARIANCE)

            case NodeTaskCorrelated =>
              val indexTask : Int = tasks.toVector.indexOf(task)
              val groundTask = taskVariance(indexTask)
              val locations : Set[ResourceNode] = ds.locations(resource)
              val groundNodes : Set[Double] = locations.map{ node =>
                val indexNode : Int = ds.computingNodes.toVector.indexOf(node)
                nodeVariance(indexNode)
              }
              groundNodes.sum/groundNodes.size * groundTask + RandomUtils.random(1, VARIANCE)

            case NodeJobCorrelated =>
              val indexJob : Int = jobs.toVector.indexOf(job)
              val groundJob = jobVariance(indexJob)
              val locations : Set[ResourceNode] = ds.locations(resource)
              val groundNodes : Set[Double] = locations.map{ node =>
                val indexNode : Int = ds.computingNodes.toVector.indexOf(node)
                nodeVariance(indexNode)
              }
              groundNodes.sum/groundNodes.size * groundJob +RandomUtils.random(1, VARIANCE)

            case TaskJobCorrelated =>
              val indexTask : Int = tasks.toVector.indexOf(task)
              val groundTask = taskVariance(indexTask)
              val indexJob : Int = jobs.toVector.indexOf(job)
              val groundJob = jobVariance(indexJob)
              groundJob * groundTask + RandomUtils.random(1, VARIANCE)

            case NodeTaskJobCorrelated =>
              val indexTask : Int = tasks.toVector.indexOf(task)
              val groundTask = taskVariance(indexTask)
              val indexJob : Int = jobs.toVector.indexOf(job)
              val groundJob = jobVariance(indexJob)
              val locations : Set[ResourceNode] = ds.locations(resource)
              val groundNodes : Set[Double] = locations.map{ node =>
                val indexNode : Int = ds.computingNodes.toVector.indexOf(node)
                nodeVariance(indexNode)
              }
              groundNodes.sum/groundNodes.size * groundTask * groundJob + RandomUtils.random(1, VARIANCE)

            case _ =>
              throw new RuntimeException(s"MASTAPlus.randomProblem : $rule not yet implemented")
          }
        }
      }
    }
    new STAP(ds, jobs, tasks)
  }
}

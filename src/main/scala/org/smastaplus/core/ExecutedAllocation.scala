// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022, 2023
package org.smastaplus.core

import org.smastaplus.strategy.consumption.Mind
import org.smastaplus.utils._

import org.joda.time.{Duration, LocalDateTime}
import scala.io.Source
import scala.collection.{SortedSet, mutable}
import scala.collection.immutable.ListMap
import scala.annotation.unused

/**
  * Class representing a time-extended allocation and an execution
  * @param stap instance
  * @param delay since the initial boot time of the distributed system [[DistributedSystem.t0]]
  */
class ExecutedAllocation(stap: STAP,
                         delay: Duration = Duration.ZERO) extends
Allocation(stap, delay){
  // Warning the simulated cost is hard coded
  var simulatedCost : SimulatedCost = new SimulatedCostH()
  // ExecutedAllocation inherits of a bundle: Map[ComputingNode, List[Task]] from Allocation plus
  // The bundle of tasks consumed by each node
  var consumedBundles: Map[ComputingNode, Map[Task, LocalDateTime]] = Map[ComputingNode, Map[Task, LocalDateTime]]()
  // Eventually the current task of the nodes
  var tasksInProgress: Map[ComputingNode, Option[Task]] = Map[ComputingNode, Option[Task]]()

  // Initialize the execution
  stap.ds.computingNodes.foreach{
    node => consumedBundles += node -> Map[Task, LocalDateTime]()
  }
  stap.ds.computingNodes.foreach{
    node => tasksInProgress += node -> None
  }

  /**
   * Returns a string representation of the time-extended allocation and execution
   */
  override def toString: String =
    stap.ds.computingNodes.toList.map(node => s"$node: " +
    bundle(node).mkString(", ")).mkString("\n")

  /**
   * Returns a string representation of the execution
   */
  def consumedTasksToString: String =  //for debug
    stap.ds.computingNodes.toList.map(node => s"$node: " +
      consumedBundles(node).keys.mkString(", ")).mkString("\n")

  /**
   * Equality test
   */
  override def equals(that: Any): Boolean =
    that match {
      case that: ExecutedAllocation => that.canEqual(this) &&
        this.bundle == that.bundle &&
        this.consumedBundles == that.consumedBundles &&
        this.tasksInProgress == that.tasksInProgress
      case _ => false
    }

  /**
   * Equality of type
   */
  override def canEqual(a: Any): Boolean = a.isInstanceOf[ExecutedAllocation]

  /**
    * Returns a copy
    */
  @throws(classOf[RuntimeException])
  override def copy(): ExecutedAllocation = {
    val allocation = new ExecutedAllocation(stap, Duration.ZERO)
    this.bundle.foreach {
      case (node: ComputingNode, task: List[Task]) =>
        allocation.bundle = allocation.bundle.updated(node, task)
      case _ => throw new RuntimeException("Not able to copy bundle")
    }
    for ((node, consumedTasks)  <- this.consumedBundles){
      allocation.consumedBundles += (node -> consumedTasks)
    }
    for ((node, task) <- this.tasksInProgress) {
      allocation.tasksInProgress += (node -> task)
    }
    allocation.simulatedCost = simulatedCost
    allocation
  }

  /**
   * Returns a copy with a new delay, for now by default
   */
  @throws(classOf[RuntimeException])
  def setDelay(time :  LocalDateTime = LocalDateTime.now()) : ExecutedAllocation = {
    val duration = new Duration(stap.ds.t0.toDateTime,time.toDateTime())
    val allocation = new ExecutedAllocation(stap, duration)
    this.bundle.foreach {
      case (node: ComputingNode, task: List[Task]) =>
        allocation.bundle = allocation.bundle.updated(node, task)
      case _ => throw new RuntimeException("Not able to copy bundle")
    }
    for ((node, consumedTasks) <- this.consumedBundles) {
      allocation.consumedBundles += (node -> consumedTasks)
    }
    for ((node, task) <- this.tasksInProgress) {
      allocation.tasksInProgress += (node -> task)
    }
    allocation.simulatedCost = simulatedCost
    allocation
  }

  /**
   * Change the simulated cost
   */
  def setSimulatedCost(simulated: SimulatedCost): Unit = {
    this.simulatedCost = simulated
  }

  /**
   * Updates an allocation by removing a task from a bundle for a computing node
   */
  def removeTask(node: ComputingNode, task : Task): ExecutedAllocation = {
    val allocation = this.copy()
    allocation.bundle = allocation.bundle.updated(node, allocation.bundle(node).filter(_ != task))
    allocation
  }

  /**
   * Updates an allocation by removing all the task of a job
   */
  def removeJob(job : Job): ExecutedAllocation = {
    val allocation = this.copy()
    stap.ds.computingNodes.foreach{ node =>
      allocation.bundle = allocation.bundle.updated(node, allocation.bundle(node).filter(!job.tasks.contains(_)))
    }
    allocation
  }

  /**
   * Updates an allocation with a new task list for a computing node
   */
  def update(additionalAllocation: ExecutedAllocation): ExecutedAllocation = {
    val allocation = this.copy()
    stap.ds.computingNodes.foreach { node =>
      allocation.bundle = allocation.bundle.updated(node, allocation.bundle(node) ++ additionalAllocation.bundle(node))
    }
    allocation.sort()
  }


  /**
   * Updates an allocation with a new task list for a computing node
   */
  override def update(node: ComputingNode, taskList: List[Task]): ExecutedAllocation = {
    val allocation = this.copy()
    allocation.bundle = allocation.bundle.updated(node, taskList)
    allocation
  }

  /**
   * Updates an allocation with a new task in progress for a computing node
   */
  def updateTaskInProgress(node: ComputingNode, task: Option[Task]): ExecutedAllocation = {
    var allocation = this.copy()
    allocation.tasksInProgress += (node -> task)
    if (task.isDefined) allocation = allocation.removeTask(node,task.get)
    allocation
  }

  /**
   * Updates an allocation and with a consumed tasks for a computing node
   */
  def updateCompletedTask(node: ComputingNode, task: Task, time : LocalDateTime = LocalDateTime.now()): ExecutedAllocation = {
    val allocation = this.copy()
    allocation.consumedBundles += (node -> (consumedBundles(node) + (task -> time)))
    allocation
  }

  /**
    * Updates an allocation and with the consumed tasks for a computing node
    */
  def updateCompletedTasks(node: ComputingNode, consumedTasks: Map[Task, LocalDateTime]) : ExecutedAllocation = {
    val allocation = this.copy()
    allocation.consumedBundles += (node -> consumedTasks)
    allocation
  }

  /**
    * Updates an allocation with a new bundle and with the consumed tasks for a computing node
    */
  def update(node: ComputingNode, bundle: List[Task],
             consumedTasks: Map[Task, LocalDateTime],
             taskInProgress: Option[Task]) : ExecutedAllocation = {
    val allocation = this.copy()
    allocation.bundle = allocation.bundle.updated(node, bundle)
    allocation.consumedBundles += (node -> consumedTasks)
    allocation.tasksInProgress += (node -> taskInProgress)
    allocation
  }


  /**
   * Returns the tasks of the allocation
   */
  override  def tasks: Set[Task] = taskList().values.foldLeft(Set[Task]())((acc, bundle) => acc ++ bundle.toSet)

  /**
   * Returns the jobs which are released
   */
  override def jobs: Set[Job] = stap.jobsOf(tasks)


  /**
    * Sorts the allocation according to the consumption strategy
    */
  override def sort(): ExecutedAllocation = {
    var sortedAllocation = copy()
    sortedAllocation.stap.ds.computingNodes.foreach{ peer =>
      val peerMind = Mind(sortedAllocation.stap, peer, this)
      sortedAllocation = sortedAllocation.update(peer, peerMind.sortedTasks, consumedBundles(peer), this.tasksInProgress(peer))
    }
    sortedAllocation
  }

  /**
   * Returns the bundles which contains the consumed tasks and the tasks in progress
   */
  private def taskList() : Map[ComputingNode, List[Task]]  = {
    // Create a mutable map to hold the merged data
    val taskList: mutable.Map[ComputingNode, List[Task]] = mutable.Map.empty
    // Merge bundle into the taskList
    taskList ++= bundle
    // Merge consumedBundles into the taskList
    for ((node, consumedTasks) <- consumedBundles) {
      val existingTasks = taskList.getOrElse(node, List.empty)
      val allTasks = existingTasks ++ consumedTasks.keys.toList
      taskList.update(node, allTasks)
    }
    // Merge tasksInProgress into the taskList
    for ((node, inProgressTask) <- tasksInProgress) {
      val existingTasks = taskList.getOrElse(node, List.empty)
      val allTask = inProgressTask.toList ++ existingTasks
      taskList.update(node, allTask)
    }
    // Convert the mutable map back to an immutable map
    taskList.toMap
    }

  /**
   * Returns the pending tasks
   */
  def pendingTasks(): List[Task] = bundle.values.flatten.toList


  /**
   * Returns true if the allocation is a partition, i.e. the intersection of any two distinct bundle is empty
   */
  override def isPartition: Boolean = taskList().values
    .forall(b1 => taskList().values.filter(_ != b1).forall(b2 => b2.toSet.intersect(b1.toSet).isEmpty))

  /**
   * Returns true if the allocation is complete, any task is performed by at most one task
   */
  override def isComplete: Boolean =
    stap.tasks == taskList().values.foldLeft(Set[Task]())((acc, bundle) => acc ++ bundle.toSet)

  /**
   * Returns true if all the tasks of the released jobs are assigned
   */
  def isReady: Boolean = jobs.foldLeft(Set[Task]())((acc, job) => job.tasks ++ acc) == tasks

  /**
    * Returns true if the allocation is sound, i.e a complete partition of the tasks
    */
  override def isSound: Boolean = isPartition && isComplete

  /**
    * Returns the computing node which has the task in its bundle
    */
  override def node(task : Task) : Option[ComputingNode] = {
    for( (node: ComputingNode, tasks: List[Task]) <- this.bundle){
      if (tasks.contains(task)) return Some(node)
    }
    // if the task is not in the remaining bundles
    // either it has been already consumed
    for( (node: ComputingNode, consumedTasks: Map[Task, LocalDateTime]) <- this.consumedBundles){
      if (consumedTasks.keySet.contains(task)) return Some(node)
    }
    // or it is a task-in-progress
    for( (node: ComputingNode, taskInProgress: Option[Task]) <- this.tasksInProgress){
      if (taskInProgress.isDefined && taskInProgress.get.equals(task)) return Some(node)
    }
    None //throw new RuntimeException(s"There is no node which is allocated to $task")
  }

  /**
   * Returns an allocation based on the consumptions
   */
  def consumptions2allocation : ExecutedAllocation = {
    var finalBundles : Map[ComputingNode, List[Task]] = Map[ComputingNode, List[Task]]()
    for ((node, consumedTasks) <- consumedBundles) {
      val orderedConsumedTasks = ListMap(consumedTasks.toSeq.sortBy(_._2):_*).keys.toList
        finalBundles = finalBundles+ (node -> orderedConsumedTasks)
    }
    val finalAllocation = new ExecutedAllocation(stap, delay)
    finalAllocation.simulatedCost = simulatedCost
    finalAllocation.bundle = finalBundles
    finalAllocation
  }

  /**
   * The effective completion date of a job
   */
  private def realCompletionDate(job: Job) : LocalDateTime = {
    var maxDate = job.releaseTime
    val jobTasks = stap.tasksOf(stap.tasks.toSet, job)
    for ((_, tasks) <- consumedBundles){
      for ((task, dateOfTask) <- tasks) {
        if (jobTasks.contains(task) && dateOfTask.isAfter(maxDate)) maxDate = dateOfTask
      }
    }
    maxDate
  }

  /**
   * The effective completion time of a job
   */
  private def realCompletionTime(job: Job) : Double = {
    val jobDate = realCompletionDate(job)
    val duration = new Duration(job.releaseTime.toDateTime(), jobDate.toDateTime())
    duration.getMillis.toDouble
  }

  /**
   * Returns the effective global flowtime
   */
  private def realGlobalFlowTime : Double = {
    var flowtime : Double = 0
    for (job <- stap.jobs) {
      flowtime += realCompletionTime(job)
    }
    flowtime
  }

  /**
   * Returns the real global mean flowtime
   */
  def realGlobalMeanFlowtime : Double = {
    realGlobalFlowTime / stap.jobs.size
  }

  /**
   * Returns the simulated completion time of the task for the computing node
   */
  private def simulatedCompletionTime(task: Task, node: ComputingNode): Double = {
    val duration = new Duration(stap.jobOf(task).releaseTime.toDateTime(), time.toDateTime())
    delay(task, node) + simulatedCost.simulatedCost(stap.cost(task, node), node) + duration.getMillis
  }

  /**
    * Returns the simulated completion time of the task
    */
  private def simulatedCompletionTime(task: Task): Double = {
    if (node(task).isDefined) return simulatedCompletionTime(task, node(task).get)
    0.0
  }

  /**
    * Returns the simulated completion time of the job
    */
  private def simulatedCompletionTime(job : Job) : Double = job.tasks.foldLeft(0.0){
    (maxCompletionTime,task) => Math.max(simulatedCompletionTime(task),maxCompletionTime)
  }

    /**
    * Returns the simulated global flowtime
    */
  private def simulatedGlobalFlowtime: Double = jobs.foldLeft(0.0)(
    (sum: Double, job: Job) => sum + simulatedCompletionTime(job)
  )

  /**
   * Returns the simulated mean global flowtime
   */
  def simulatedMeanGlobalFlowtime: Double = simulatedGlobalFlowtime / stap.l

  /**
   * Returns the ongoing completion time of the task for the computing node
   */
  private def ongoingCompletionTime(task: Task, node: ComputingNode): Double = {
    // Either the task is already consumed
    for((_, consumptions) <- consumedBundles){
      for ((t, completionTime)<- consumptions){
        if (t == task) return new Duration(stap.jobOf(task).releaseTime.toDateTime(), completionTime.toDateTime()).getMillis.toDouble
      }
    }
    val past = new Duration(stap.jobOf(task).releaseTime.toDateTime(), time.toDateTime()).getMillis
    val current = if (tasksInProgress(node).isDefined) simulatedCost.simulatedCost(stap.cost(task, node), node)
    else 0.0
    // Either the task is in progress
    if (tasksInProgress(node).contains(task)) {
      return past + current
    }
    // Or the task is waiting
    past + current + delay(task, node) + simulatedCost.simulatedCost(stap.cost(task, node), node)
  }

  /**
   * Returns the ongoing completion time of the task
   */
  private def ongoingCompletionTime(task: Task): Double = {
    if (node(task).isDefined) return ongoingCompletionTime(task, node(task).get)
    0.0
  }

  /**
   * Returns the ongoing completion time of the job
   */
  private def ongoingCompletionTime(job: Job): Double = job.tasks.foldLeft(0.0) {
    (maxCompletionTime, task) => Math.max(ongoingCompletionTime(task), maxCompletionTime)
  }

  /**
   * Returns the ongoing global flowtime
   */
  def ongoingGlobalFlowtime: Double = jobs.foldLeft(0.0)(
    (sum: Double, job: Job) => sum + ongoingCompletionTime(job)
  )

  /**
   * Returns the ongoing mean global flowtime
   */
  def ongoingMeanGlobalFlowtime: Double = ongoingGlobalFlowtime / jobs.size

  /**
   * Returns the delay of the task for the node, eventually +∞
   * The delay is the waiting time from the current time before the task is processed
   */
  private def exPostDelay(task: Task, node: ComputingNode): Double = {
    bundle(node).takeWhile(_ != task).foldLeft(0.0) { (sum, previousTask) => sum + simulatedCost.simulatedCost(stap.cost(previousTask, node), node) }
  }

  /**
   * Returns the completion time of the task for the computing node
   * Since we consider time-extended allocation,
   * the completion time of a task depends on the release time of its job
   * WARNING the cost unit is a millisecond
   */
  private def exPostCompletionTime(task: Task, node: ComputingNode): Double = {
    val duration = new Duration(stap.jobOf(task).releaseTime.toDateTime(), time.toDateTime())
    val theTime =
      duration.getMillis + exPostDelay(task, node) + simulatedCost.simulatedCost(stap.cost(task, node), node)
    theTime
  }

  /**
   * Returns the completion time of the task, eventually +∞
   */
  private def exPostCompletionTime(task: Task): Double = {
    if (node(task).isDefined) return exPostCompletionTime(task, node(task).get)
    0.0
  }


  /**
   * Returns the completion time of the job, i.e. its makespan
   */
  private def exPostCompletionTime(job: Job): Double = job.tasks.foldLeft(0.0) {
    (maxCompletionTime, task) => Math.max(exPostCompletionTime(task), maxCompletionTime)
  }

  /**
   * Returns the mean globalFlowtime of the jobs
   */
  private def exPostGlobalFlowtime: Double = stap.jobs.foldLeft(0.0)(
    (sum: Double, job: Job) => sum + exPostCompletionTime(job)
  )

  /**
   * Returns the mean localFlowtime of the jobs
   */
  def exPostMeanGlobalFlowtime: Double = exPostGlobalFlowtime / stap.l

}

  /**
  * Factory for [[ExecutedAllocation]] instances
  */
object ExecutedAllocation {
  val debug = false

    /**
      * Generate an unfair allocation
      */
    @unused
    def unfairAllocation(stap: STAP): ExecutedAllocation = {
      val allocation = new ExecutedAllocation(stap)
      allocation.bundle += (stap.ds.computingNodes.head -> stap.tasks.toList)
      stap.ds.computingNodes.tail.foreach{ node =>
        allocation.bundle += (node -> List.empty[Task])
      }
      allocation
    }

    /**
     * Generate a random allocation
     */
    def randomAllocation(stap: STAP): ExecutedAllocation =
      randomAllocation(stap, stap.ds.computingNodes)

    /**
     * Generate a random allocation for a specific job
     */
    def randomAllocation(stap: STAP, job : Job): ExecutedAllocation =
      randomAllocation(stap, job, stap.ds.computingNodes)

    /**
      * Generate a random allocation restricted to a non-empty subset of nodes
      */
    @throws[RuntimeException]
    def randomAllocation(stap: STAP, loadedNodes : SortedSet[ComputingNode]): ExecutedAllocation = {
      if (loadedNodes.isEmpty)
        throw new RuntimeException(s"No random allocation can be generated if the set of loaded nodes is empty")
      val allocation = new ExecutedAllocation(stap)
      stap.tasks.foreach { task : Task =>
        val randomNode = RandomUtils.random[ComputingNode](loadedNodes)
        allocation.bundle += (randomNode -> (task :: allocation.bundle(randomNode)))
      }
      allocation
    }

    /**
     * Generate a random allocation of a job restricted to a non-empty subset of nodes
     */
    @throws[RuntimeException]
    def randomAllocation(stap: STAP, job : Job, loadedNodes: SortedSet[ComputingNode]): ExecutedAllocation = {
      if (loadedNodes.isEmpty)
        throw new RuntimeException(s"No random allocation can be generated if the set of loaded nodes is empty")
      val allocation = new ExecutedAllocation(stap)
      job.tasks.foreach { task: Task =>
        val randomNode = RandomUtils.random[ComputingNode](loadedNodes)
        allocation.bundle += (randomNode -> (task :: allocation.bundle(randomNode)))
      }
      allocation
    }

    /**
      * Generate a half part allocation
      */
    @unused
    def halfPartAllocation(stap: STAP): ExecutedAllocation = {
      val allocation = new ExecutedAllocation(stap)
      stap.tasks.foreach { task : Task =>
        val randomNode = RandomUtils.random[ComputingNode](stap.ds.computingNodes.splitAt(stap.m/2)._1)
        allocation.bundle += (randomNode -> (task :: allocation.bundle(randomNode)))
      }
      allocation
    }

    /**
      * Generate an allocation where single node is responsible all tasks
      */
    @unused
    def singleNodeAllocation(stap: STAP): ExecutedAllocation = {
      val allocation = new ExecutedAllocation(stap)
      val node = stap.ds.computingNodes.head
      stap.tasks.foreach { task : Task =>
        allocation.bundle += (node -> (task :: allocation.bundle(node)))
      }
      allocation
    }

    /**
    * Build an executed allocation
      *
      * @param path of the OPL output
    * @param stap   MATA
    */
  def apply(path: String, stap: STAP): ExecutedAllocation = {
    val allocation = new ExecutedAllocation(stap)
    val bufferedSource = Source.fromFile(path)
    var lineNumber = 0
    for (line <- bufferedSource.getLines()) { // foreach line
      if (lineNumber == 0) {
        val u = line.toDouble
        if (debug) println(s"Rule = $u")
      }
      if (lineNumber == 1) {
        val t = line.toDouble
        if (debug) println(s"T (ms) = $t")
      }
      if (lineNumber > 1) {
        val task: Task = stap.tasks.toVector(lineNumber - 2)
        val agentNumber = line.toInt
        val agent = stap.ds.computingNodes.toVector(agentNumber - 1)
        if (debug) println(s"${agent.name} -> ${task.name}")
        allocation.bundle += (agent -> (task :: allocation.bundle(agent)))
      }
      lineNumber += 1
    }
    allocation
  }

    /**
     * Build an an executed allocation from an allocation
     */
    def apply(allocation: Allocation): ExecutedAllocation = {
      val executedAllocation = new ExecutedAllocation(allocation.stap, allocation.delay)
      allocation.bundle.foreach {
        case (node: ComputingNode, task: List[Task]) =>
          executedAllocation.bundle = allocation.bundle.updated(node, task)
        case _ => throw new RuntimeException("Not able to copy bundle")
      }
      // Initialize the execution
      allocation.stap.ds.computingNodes.foreach {
        node => executedAllocation.consumedBundles += node -> Map[Task, LocalDateTime]()
      }
      allocation.stap.ds.computingNodes.foreach {
        node => executedAllocation.tasksInProgress += node -> None
      }
    executedAllocation
    }
  }
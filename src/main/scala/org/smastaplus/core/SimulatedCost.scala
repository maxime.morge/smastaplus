// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2023
package org.smastaplus.core

import scala.annotation.unused

/**
 * Trait representing a simulated cost
 */
trait SimulatedCost {
  /**
   * Return the simulated cost for the node
   */
  def simulatedCost(cost: Double, node: ComputingNode): Double
}

/**
 * Class representing a simulated cost with a perfect information of the running environment
 */
class SimulatedCostE extends SimulatedCost {
  /**
   * Returns the simulated cost for the node
   */
  override def simulatedCost(cost: Double, node: ComputingNode): Double = cost
  override def toString: String = "costE"

}

/**
 * Class representing a simulated cost with with half slow down nodes
 */
class SimulatedCostH extends SimulatedCost {
  /**
   * Returns the simulated cost for the node
   */
  override def simulatedCost(cost: Double, node: ComputingNode): Double = {
    Integer.parseInt(String.valueOf(node.name.takeRight(1)))
    if ((Integer.parseInt(String.valueOf(node.name.takeRight(1))) % 2) == 1)
      return 2 * cost
    cost
  }

  override def toString: String = "costH"
}

/**
 * Class representing a simulated cost with a slow down node
 */
@unused
class SimulatedCostA(slowerNode: ComputingNode) extends SimulatedCost {
  /**
   * Returns the simulated cost for the node
   */
  override def simulatedCost(cost: Double, node: ComputingNode): Double = {
    if (node.name.equals(slowerNode.name))
      return 2 * cost
    cost
  }

  override def toString: String = "costA"

}

// Copyright (C) Maxime MORGE, Anne-Cécile CARON, 2020, 2022, 2023
package org.smastaplus.core

import org.smastaplus.strategy.consumption.{Bundle, Mind}
import org.smastaplus.utils.RandomUtils

import scala.io.Source
import org.joda.time.{Duration, LocalDateTime}
import scala.annotation.unused
import scala.collection.SortedSet

/**
 * Class representing a time-extended allocation as
 * an assignment of the tasks to some computing nodes.
 * @param stap instance
 * @param delay since the initial boot time of the distributed system [[DistributedSystem.t0]]
 */
class Allocation(val stap: STAP,
                 val delay: Duration = Duration.ZERO) {

  /**
   * Timestamp of the allocation
   */
  val time : LocalDateTime = stap.ds.t0.plus(delay)

  var bundle: Map[ComputingNode, List[Task]] = Map[ComputingNode, List[Task]]()
  stap.ds.computingNodes.foreach{
    node => bundle += node -> List[Task]()
  }

  override def toString: String =
    stap.ds.computingNodes.toList.map(node => s"$node: " +
      bundle(node).mkString(", ")).mkString("\n")

  override def equals(that: Any): Boolean =
    that match {
      case that: Allocation => that.canEqual(this) && this.bundle == that.bundle
      case _ => false
    }

  override def hashCode(): Int = this.bundle.hashCode()

  def canEqual(a: Any): Boolean = a.isInstanceOf[Allocation]

  /**
   * Returns a copy
   */
  @throws(classOf[RuntimeException])
  def copy(): Allocation = {
    val allocation = new Allocation(stap, Duration.ZERO)
    this.bundle.foreach {
      case (node: ComputingNode, task: List[Task]) =>
        allocation.bundle = allocation.bundle.updated(node, task)
      case _ => throw new RuntimeException("Not able to copy bundle")
    }
    allocation
  }

  /**
   * Return the bundle of the node in a list of tasks
   */
  def getBundle(node: ComputingNode): List[Task] = bundle(node)

  /**
   * Return the bundle of the node in a list of tasks
   */
  def getTheBundle(node: ComputingNode): Bundle = {
    val newBundle = new Bundle(stap, node)
    newBundle.setBundle(bundle(node).toSet)
  }

  /**
   * Return true if all the bundles are empty, false otherwise
   */
  def areBundlesEmpty: Boolean = {
    for ((_: ComputingNode, tasks: List[Task]) <- this.bundle) {
      if (tasks.nonEmpty) return false
    }
    true
  }

  /**
   * Updates an allocation with a new bundle for a computing node
   */
  def update(node: ComputingNode, taskList: List[Task]) : Allocation = {
    val allocation = this.copy()
    allocation.bundle = allocation.bundle.updated(node, taskList)
    allocation
  }

  /**
   * Returns the tasks of the allocation
   */
  def tasks: Set[Task] = bundle.values.foldLeft(Set[Task]())((acc, bundle) => acc ++ bundle.toSet)

  /**
   * Returns the jobs which are released
   */
  def jobs: Set[Job] = stap.jobsOf(tasks)

  /**
   * Sorts the allocation according to the consumption strategy
   */
  def sort(): Allocation = {
    var sortedAllocation = copy()
    sortedAllocation.stap.ds.computingNodes.foreach{ peer =>
      val peerMind = Mind(sortedAllocation.stap, peer, this)
      sortedAllocation = sortedAllocation.update(peer, peerMind.sortedTasks)
    }
    sortedAllocation
  }

  /**
   * Returns true if the allocation is a partition, i.e. the intersection of any two distinct bundle is empty
   */
  def isPartition: Boolean =
    bundle.values.forall(b1 => bundle.values.filter(_ != b1).forall(b2 => b2.toSet.intersect(b1.toSet).isEmpty))

  /**
   * Returns true if the allocation is complete, any task is performed by at most one task
   */
  def isComplete: Boolean =
    stap.tasks == bundle.values.foldLeft(Set[Task]())((acc, bundle) => acc ++ bundle.toSet)

  /**
   * Returns true if the allocation is sound, i.e a complete partition of the tasks
   */
  def isSound: Boolean = isPartition && isComplete

  /**
   * Returns the tasks assigned to a computing node
   */
  def tasks(node : ComputingNode) : Set[Task] = bundle(node).toSet

  /**
   * Returns the set of jobs such that at least one of its tasks is in the bundle of the computing node
   */
  def jobs(node: ComputingNode) : Set[Job] =
    bundle(node).foldLeft(Set[Job]())( (acc, task) => acc + stap.jobOf(task))

  /**
   * Returns the set of tasks for a job assigned to the computing node
   */
  def tasks(job: Job, node: ComputingNode) : Set[Task] =
    bundle(node).filter( stap.jobOf(_) == job ).toSet

  /**
   * Returns the bundle which contains the tasks
   */
  @throws(classOf[RuntimeException])
  def bundle(task: Task): List[Task] =
    bundle.values.find(_.contains(task)) match {
      case Some(b) => b
      case None => throw new RuntimeException(s"None bundle contains $task")
    }

  /**
   * Returns the computing node which has the task in its bundle
   */
  @throws(classOf[RuntimeException])
  def node(task : Task) : Option[ComputingNode] = {
    for( (node: ComputingNode, tasks: List[Task]) <- this.bundle){
      if (tasks.contains(task)) return Some(node)
    }
    None //throw new RuntimeException(s"There is no node which is allocated to $task")
  }

  /**
   * Returns the loaded nodes
   */
  def loadedNodes : SortedSet[ComputingNode]= {
    stap.ds.computingNodes.filter(node => bundle(node).nonEmpty)
  }

  /**
   * Returns the workload of the computing node
   */
  def workload(node: ComputingNode): Double =
    bundle(node).foldLeft(0.0)((sum: Double, task: Task) => sum + stap.cost(task, node))

  /**
   * Returns the delay of the task for the node, eventually +∞
   * The delay is the waiting time from the current time before the task is processed
   */
  def delay(task: Task, node : ComputingNode ): Double = {
    bundle(node).takeWhile(_ != task).foldLeft(0.0){ (sum,previousTask)  => sum + stap.cost(previousTask, node) }
  }

  /**
   * Returns the completion time of the task, eventually +∞
   */
  def completionTime(task: Task): Double = {
    if (node(task).isDefined) return completionTime(task, node(task).get)
    0.0
  }

  /**
   * Returns the completion time of the task for the computing node
   * Since we consider time-extended allocation,
   * the completion time of a task depends on the release time of its job
   * WARNING the cost unit is a millisecond
   */
  def completionTime(task: Task, node : ComputingNode): Double = {
    val duration = new Duration(stap.jobOf(task).releaseTime.toDateTime(), time.toDateTime())
    val theTime =
      duration.getMillis + delay(task, node) + stap.cost(task, node)
    theTime
  }

  /**
   * Returns the cost of the job for a node
   */
  def cost(job : Job, node : ComputingNode) : Double =
    job.tasks.intersect(bundle(node).toSet).foldLeft(0.0){ (sum,task) => sum + stap.cost(task, node) }

  /**
   * Returns the completion time of the job, i.e. its makespan
   */
  def completionTime(job : Job) : Double = job.tasks.foldLeft(0.0){
    (maxCompletionTime,task) => Math.max(completionTime(task),maxCompletionTime)
  }

  /**
   * Returns the completion time of the job for a node
   */
  def completionTime(job : Job, node :ComputingNode) : Double =
    stap.tasksOf(bundle(node).toSet,job).foldLeft(0.0){
      (maxCompletionTime,task) => Math.max(completionTime(task,node),maxCompletionTime)
    }

  /**
   * Returns the completion date of the job
   */
  def completionDate(job : Job) : LocalDateTime = job.releaseTime.plusMillis(completionTime(job).toInt)

  /**
   * Returns the mean globalFlowtime of the jobs
   */
  def globalFlowtime: Double = stap.jobs.foldLeft(0.0)(
    (sum: Double, job: Job) => sum + completionTime(job)
  )

  /**
   * Returns the mean localFlowtime of the jobs
   */
  def meanGlobalFlowtime: Double = globalFlowtime/ stap.l

  /**
   * Returns the completion date of the last job to perform
   * The makespan measures the maximum completion time of the job
   * i.e.the maximum workload of the nodes.
   */
  def makespan: Double = stap.ds.computingNodes.foldLeft(0.0){
    (maxWorkload: Double, node: ComputingNode) => Math.max(maxWorkload, workload(node))
  }
  /* Previously,
    pb.jobs.foldLeft(0.0){(maxCompletionDate: Double, job: Job) =>
    Math.max(maxCompletionDate, completionDate(job))}
  */

  /**
   * Returns the size of local resources for a task
   */
  private def sizeOfLocalResources(task: Task): Double = task.resources.foldLeft(0.0) {
    case (sum: Double, resource: Resource) =>
      val localSize : Double = if (stap.ds.local(resource,node(task).get) ) resource.size else 0.0
      sum + localSize
  }

  /**
   * Returns the size of local resources for all the tasks
   */
  private def sizeOfLocalResources : Double = tasks.map(sizeOfLocalResources).sum

  /**
   * Returns the locality rate of the allocation which
   * measures the proportion of locally processed resources
   */
  def localAvailabilityRatio : Double = sizeOfLocalResources / stap.ds.sizeOfResources

  /**
   * Return a JSON description of the concatenation of the bundles
   */
  def bundles2JSON: String = bundle.values.flatten.mkString("[ '", "' , '", "' ]")

  /**
   * Return a JSON description of the allocation
   * complete mode is used for monitoring
   * basic mode is used for one visualization, e.g. in AllocationDrawerVega
   */
  def toJSON(mode: String = "basic"): String = {
    if (mode == "basic") {
      stap.ds.computingNodes.map(node =>
        bundle(node).map(task =>
          s"{'node': '${node.name}', 'task': '${task.name}', 'job': '${stap.jobOf(task).name}', 'cost': ${stap.cost(task, node)} }"
        ).mkString("", ",\n\t", "")
      ).mkString("[\n\t", ",\n\t", "\n]")
    } else { // complete mode
      val orderedTasks = this.bundle.values.flatten

      // function with accumulators in order to avoid too much reading of task and node lists
      def generateJSon(tasks2JSON: String, jobs2JSON: String, jobNames2JSON: String,
                       costs2JSON: String, nodes: List[ComputingNode], tasks: Iterable[Task],
                       seenNodes: Set[ComputingNode], seenJobs: Set[Job], firstNode: Boolean): String =
        nodes match {
          case Nil => "{\"tasks\": " + tasks2JSON + ",\n \"jobs\": " + jobs2JSON + ",\n \"jobNames\": " + jobNames2JSON + ",\n \"costs\": [" + costs2JSON + "\n]\n}" // we have finished
          case n :: nl =>
            val seenNode = seenNodes.contains(n)
            if (firstNode) { // it is the first node so we build tasks2JSON, jobs2JSON and jobNames2JSON (for the following nodes it is not necessary)
              val jsonResult = tasks match {
                case Nil => generateJSon(
                  "[" + tasks2JSON.substring(1) + "]", // substring(1) remove the first useless coma
                  "[" + jobs2JSON.substring(1) + "]",
                  "[" + jobNames2JSON.substring(1) + "]",
                  costs2JSON + "}",
                  nl,
                  orderedTasks,
                  seenNodes,
                  seenJobs,
                  firstNode = false) // it is finished for the first node
                case t :: tl =>
                  val j = this.stap.jobOf(t)
                  val seenJob = seenJobs.contains(j)
                  generateJSon(
                    tasks2JSON + ",\"" + t.toString + "\"",
                    jobs2JSON + "," + j.toString.substring(1), // substring(1) remove letter 'J'
                    jobNames2JSON + (if (seenJob) "" else ",\"" + j.name + "\""),
                    costs2JSON + (if (seenNode) "" else "\n{\"node\": \"" + n.name + "\"") + ", \"" + t.name + "\":" + (if (this.tasks(n).contains(t)) this.stap.cost(t, n).toString else "0.0"),
                    nodes,
                    tl,
                    seenNodes.+(n),
                    seenJobs.+(j),
                    firstNode = true)
                case _ => "problem in generateJSon"
              }
              jsonResult
            }
            else { // tasks2JSON, jobs2JSON and jobNames2JSON never change
              val jsonResult = tasks match {
                case Nil => generateJSon(tasks2JSON,
                  jobs2JSON,
                  jobNames2JSON,
                  costs2JSON + "}",
                  nl, orderedTasks, seenNodes, seenJobs, firstNode = false)
                case t :: tl =>
                  generateJSon(tasks2JSON,
                    jobs2JSON,
                    jobNames2JSON,
                    costs2JSON + (if (seenNode) "" else ",\n{\"node\": \"" + n.name + "\"") + ", \"" + t.name + "\":" + (if (this.tasks(n).contains(t)) this.stap.cost(t, n).toString else "0.0"),
                    nodes, tl, seenNodes.+(n), seenJobs, firstNode = false)
                case _ => "problem in generateJSon"
              }
              jsonResult
            }
        } // end inner function generateJSon

      generateJSon("", "",
        "", "",
        this.stap.ds.computingNodes.toList,
        orderedTasks,
        Set[ComputingNode](), Set[Job](), firstNode = true
      )

    } // end complete mode
  } // end toJSON

  /**
   * Returns a JSON representation of the node, for the trace of monitoring
   * Similar to the toJSON method in the Bundle class (here the bundle is a List[Task] and not a Bundle object)
 *
   * @param node to expand
   */
  def bundleToJSON(node : ComputingNode) : String = {
    val nodeBundle = this.getBundle(node)
    val str_init = "{ \"node\": \"" + node.name + "\""
    val str_tasks = nodeBundle.foldLeft("")((str, task) => str + ", \"" + task.toString + "\":" + this.stap.cost(task, node))
    if (str_tasks.nonEmpty) {
      str_init + "," + nodeBundle.foldLeft("")((str, task) => str + ", \"" + task.toString + "\":" + this.stap.cost(task, node)).substring(1) + "}"
    } else {
      str_init + "}"
    }
  }
}

/**
 * Factory for [[Allocation]] instances
 */
object Allocation {
  val debug = false

  /**
   * Generate an unfair allocation
   */
  @unused
  def unfairAllocation(stap: STAP): Allocation = {
    val allocation = new Allocation(stap)
    allocation.bundle += (stap.ds.computingNodes.head -> stap.tasks.toList)
    stap.ds.computingNodes.tail.foreach{ node =>
      allocation.bundle += (node -> List.empty[Task])
    }
    allocation
  }

  /**
   * Generate a random allocation
   */
  def randomAllocation(stap: STAP): Allocation =
    randomAllocation(stap, stap.ds.computingNodes)

  /**
   * Generate a random allocation restricted to a non-empty subset of nodes
   */
  @throws[RuntimeException]
  def randomAllocation(stap: STAP, loadedNodes : SortedSet[ComputingNode]): Allocation = {
    if (loadedNodes.isEmpty)
      throw new RuntimeException(s"No random allocation can be generated if the set of loaded nodes is empty")
    val allocation = new Allocation(stap)
    stap.tasks.foreach { task : Task =>
      val randomNode = RandomUtils.random[ComputingNode](loadedNodes)
      allocation.bundle += (randomNode -> (task :: allocation.bundle(randomNode)))
    }
    allocation
  }

  /**
   * Generate a half part allocation
   */
  @unused
  def halfPartAllocation(stap: STAP): Allocation = {
    val allocation = new Allocation(stap)
    stap.tasks.foreach { task : Task =>
      val randomNode = RandomUtils.random[ComputingNode](stap.ds.computingNodes.splitAt(stap.m/2)._1)
      allocation.bundle += (randomNode -> (task :: allocation.bundle(randomNode)))
    }
    allocation
  }

  /**
   * Generate an allocation where single node is responsible all tasks
   */
  @unused
  def singleNodeAllocation(stap: STAP): Allocation = {
    val allocation = new Allocation(stap)
    val node = stap.ds.computingNodes.head
    stap.tasks.foreach { task : Task =>
      allocation.bundle += (node -> (task :: allocation.bundle(node)))
    }
    allocation
  }

  /**
   * Build an allocation
   *
   * @param path of the OPL output
   * @param stap   MATA
   */
  def apply(path: String, stap: STAP): Allocation = {
    val allocation = new Allocation(stap)
    val bufferedSource = Source.fromFile(path)
    var lineNumber = 0
    for (line <- bufferedSource.getLines()) { // foreach line
      if (lineNumber == 0) {
        val u = line.toDouble
        if (debug) println(s"Rule = $u")
      }
      if (lineNumber == 1) {
        val t = line.toDouble
        if (debug) println(s"T (ms) = $t")
      }
      if (lineNumber > 1) {
        val task: Task = stap.tasks.toVector(lineNumber - 2)
        val agentNumber = line.toInt
        val agent = stap.ds.computingNodes.toVector(agentNumber - 1)
        if (debug) println(s"${agent.name} -> ${task.name}")
        allocation.bundle += (agent -> (task :: allocation.bundle(agent)))
      }
      lineNumber += 1
    }
    allocation
  }

  /**
   * Test Example 1
   */
  /*
def main(args: Array[String]): Unit = {
  import org.smastaplus.example.stap.ex1.stap
  val allocation : Allocation = Allocation("experiments/opl/lpOutput.txt", stap)
  println(allocation)
  println(s"meanGlobalFlowtime= ${allocation.meanGlobalFlowtime}")
  println(s"Locality= ${allocation.localAvailabilityRatio}")
}
*/

}
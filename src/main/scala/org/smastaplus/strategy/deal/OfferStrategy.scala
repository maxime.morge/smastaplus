// Copyright (C) Maxime MORGE 2021, 2023
package org.smastaplus.strategy.deal

import org.smastaplus.core._

/**
  * Abstract strategy for proposing/counter-proposing a bilateral deal (e.g. swap)
 * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class OfferStrategy[Swap](val minimalImprovement: Double = 0.0) {
  var debug = false
  val debugAcceptability = false
  var debugAbstract = false

  implicit val order: Ordering[Double] = Ordering.Double.TotalOrdering
  // eventually Ordering.Double.IeeeOrdering

  /**
    * Returns true if the acceptability criteria of the deal is validated by the mind (of the initiator)
    */
  @throws(classOf[RuntimeException])
  def acceptabilityCriteria(deal: Swap, mind: MindWithPeerModelling, rule: SocialRule): Boolean

  /**
    * Returns true if the acceptability criteria of the deal is validated by the mind (of the responder)
    */
  def triggerable(deal: Swap, mind: MindWithPeerModelling, rule: SocialRule): Boolean =
    acceptabilityRule(deal, mind, rule)

  /**
    * Returns true if the acceptability rule of the deal wrt the rule is validated by the mind (of the responder)
    */
  @throws(classOf[RuntimeException])
  def acceptabilityRule(deal: Swap, mind: MindWithPeerModelling, rule: SocialRule): Boolean = {
    rule match {
      case Makespan =>
        acceptabilityCriteria(deal, mind, Makespan)
      case LocalFlowtime =>
        acceptabilityCriteria(deal, mind, LocalFlowtime)
      case GlobalFlowtime =>
        acceptabilityCriteria(deal, mind, GlobalFlowtime)
      case LocalFlowtimeAndMakespan =>
        acceptabilityCriteria(deal, mind, LocalFlowtime) && acceptabilityCriteria(deal, mind, Makespan)
      case LocalFlowtimeAndGlobalFlowtime =>
        acceptabilityCriteria(deal, mind, LocalFlowtime) && acceptabilityCriteria(deal, mind, GlobalFlowtime)
      case rule =>
        throw new RuntimeException(s"DelegationStrategy>${mind.me}  is not able to manage $rule ")
    }
  }
}

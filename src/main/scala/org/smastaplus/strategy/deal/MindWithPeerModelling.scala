// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022
package org.smastaplus.strategy.deal

import org.smastaplus.core._
import org.smastaplus.provisioning.{Active, ProvisioningUsage}
import org.smastaplus.strategy.consumption._
import org.smastaplus.strategy.consumption.BeliefBase
import org.smastaplus.utils.MathUtils._

/**
  *  Class representing the agent's state of mind with peer modelling
  * @param me the node I manage
  * @param peers are nodeAgents managed by the other agents
  * @param usage are the the provisioning usages of the computing nodes
  * @param stap instance
  * @param taskSet of tasks assigned to the agent
  * @param beliefBase about the other agents
  */
class MindWithPeerModelling(me: ComputingNode,
                            peers: Set[ComputingNode],
                            val usage: Map[ComputingNode, ProvisioningUsage],
                            stap: STAP,
                            taskSet: Set[Task],
                            val beliefBase: BeliefBase = new BeliefBase())
  extends Mind(me, peers, stap, taskSet) {

  debug = false
  val debugAcceptability = false
  val debugOrder = false
  val debugSwap = false

  bundle = new Bundle(stap, me).setBundle(taskSet)

  /**
    * Auxiliary  constructor
    */
  def this(me: ComputingNode,
           peers: Set[ComputingNode],
           usage: Map[ComputingNode, ProvisioningUsage],
           stap: STAP,
           bundle: Bundle,
           beliefBase: BeliefBase) = {
    this(me, peers, usage, stap, Set.empty[Task], beliefBase)
    this.bundle = bundle
  }

  override def toString: String = super.toString + s" and the belief base $beliefBase"

  /**
    * Print the current bundle of the agent (useful for debug)
    */
  def printBundle(): Unit = {
    println(s"$me : $bundle")
  }

  /**
    * Returns a copy
    */
  override def copy(): MindWithPeerModelling = {
    val copyOfBeliefBase: BeliefBase = beliefBase.copy() //beliefBase
    val copyOfBundle = bundle.copy()
    new MindWithPeerModelling(me, peers, usage, stap, copyOfBundle, copyOfBeliefBase)
  }

  /**
    * Updates the belief base with the insight of a node
    */
  def updateBeliefBase(node: ComputingNode, insight: Insight): MindWithPeerModelling = {
    val updatedBeliefBase = beliefBase.update(node, insight)
    if (debug) println(s"Mind>$me 's belief base: $updatedBeliefBase")
    new MindWithPeerModelling(me, peers, usage, stap, bundle, updatedBeliefBase)
  }

  /**
    * Sets and sorts the bundle of the mind
    */
  override def setTaskSet(updatedBundle: Set[Task]): MindWithPeerModelling =
    new MindWithPeerModelling(me, peers, usage, stap, bundle.setBundle(updatedBundle), beliefBase)

  /**
   * Sets the bundle of the mind
   */
  override def setBundle(updatedBundle: Bundle): MindWithPeerModelling =
    new MindWithPeerModelling(me, peers, usage, stap, updatedBundle, beliefBase)

  /**
    * Adds a task to the bundle and sorts it
    */
  override def addTask(task: Task): MindWithPeerModelling =
    new MindWithPeerModelling(me, peers, usage, stap, bundle.addTask(task), beliefBase)

  /**
    * Adds an endowment to the bundle and sorts it
    */
  override def addTasks(endowment: List[Task]): MindWithPeerModelling =
    new MindWithPeerModelling(me, peers, usage, stap, bundle.addTasks(endowment), beliefBase)

  /**
    * Removes a task to the bundle and sorts it
    */
  override def removeTask(task: Task): MindWithPeerModelling =
    new MindWithPeerModelling(me, peers, usage, stap, bundle.removeTask(task), beliefBase)

  /**
    * Remove an endowment to the bundle and sorts it
    */
  override def removeTasks(endowment: List[Task]): MindWithPeerModelling =
    new MindWithPeerModelling(me, peers, usage, stap, bundle.removeTasks(endowment), beliefBase)

  /**
    * Replaces a task by a counterpart in the bundle and sorts it
    */
  override def replaceTask(task: Task, counterpart: Task): MindWithPeerModelling =
    new MindWithPeerModelling(me, peers, usage, stap, bundle.replaceTask(task, counterpart), beliefBase)

  /**
    * Replaces an endowment by a counterpart in the bundle and sorts it
    */
  override def replaceTasks(endowment: List[Task], counterpart: List[Task]): MindWithPeerModelling =
    new MindWithPeerModelling(me, peers, usage, stap, bundle.replaceTasks(endowment, counterpart), beliefBase)

  /**
    * Returns what the agent believes about the cost of the job for the node
    */
  def beliefCost(job: Job, node: ComputingNode): Double = {
    //if (node == me) return cost(job)
    beliefBase.belief(node).cost(job)
  }

  /**
    * Returns what the agent believes about the cost of some jobs for the node
    */
  def beliefCost(jobs: Set[Job], node: ComputingNode): Double =
    jobs.foldLeft(0.0)((sum, job) => sum + beliefCost(job, node))

  /**
    * Returns what the agent believes about the maximum cost for the job
    */
  def beliefMaxCost(job: Job): Double = stap.ds.computingNodes.map(node => beliefCost(job, node)).max

  /**
    * Returns what the agent believes about the workload for the node
    */
  def beliefWorkload(node: ComputingNode): Double = if (node == me) workload() else beliefBase.workload(node)

  /**
    * Returns the most loaded node with some jobs
    */
  def mostLoadedNode(jobs: Set[Job]): ComputingNode =
    stap.ds.computingNodes.foldLeft(stap.ds.computingNodes.head) { case (mostLoadedNode, currentNode) =>
      if (beliefCost(jobs, currentNode) > beliefCost(jobs, mostLoadedNode)) currentNode
      else mostLoadedNode
    }

  /**
    * Returns the belief about the completion time for the job by a node
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTime(job: Job, node: ComputingNode): Double = {
    // 1- Either I am the node and so I know my completion time of the job
    if (node == me) return completionTime(job)
    beliefBase.beliefCompletionTime(job, node)
  }

  /**
    * Returns the belief about the completion time for the job in the allocation
    */
  def beliefCompletionTime(job: Job): Double =
    stap.ds.computingNodes.map(node => beliefCompletionTime(job: Job, node: ComputingNode)).max

  /**
    * Returns the belief about the global flowtime for the job in the allocation
    */
  def beliefGlobalFlowtime: Double =
    stap.jobs.foldLeft(0.0)((sum, job) => sum + beliefCompletionTime(job))


  /**
    * Returns true if the agent believes the node is a limiting factor for the job
    */
  def isNodeMax(node: ComputingNode, job: Job): Boolean =
    beliefCompletionTime(job, node) ~= beliefCompletionTime(job: Job)

  /**
    * Returns what the belief about the completion time of a recipient for a job after the delegation from the donor
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTimeOfRecipient(job: Job, donor: ComputingNode, recipient: ComputingNode, task: Task): Double = {
    val jobOfTask = stap.jobOf(task)
    var updatedMind = this.copy()

    // Either I am the recipient
    if (recipient == me) {
      if (debugAcceptability)
        println(s"MindWithPeerModelling>$me 's belief base about $donor (before):" +
          s" ${updatedMind.beliefBase.belief(donor)}")

      // 1 - Update the belief base by removing the cost of the task in the corresponding job for the donor
      val beforeCostJobDonor = updatedMind.beliefCost(jobOfTask, donor)
      val afterCostJobDonor = beforeCostJobDonor - stap.cost(task, donor)
      val updatedInsight: Insight = updatedMind.beliefBase.belief(donor).update(jobOfTask, afterCostJobDonor)
      updatedMind = updatedMind.updateBeliefBase(donor, updatedInsight)
      if (debugAcceptability)
        println(s"MindWithPeerModelling>$me 's belief base about $donor (after): ${updatedMind.beliefBase.belief(donor)}")
      // 2 - Add the task to the bundle of the recipient which is sorted
      updatedMind = updatedMind.addTask(task)
      if (debugAcceptability)
        println(s"MindWithPeerModelling>$me 's bundle (after): ${updatedMind.taskSet} with ${bundle.orderedJobs}")
      // 3 - Returns the completion time of the recipient according to the new mind
      return updatedMind.completionTime(job)
    }

    // Otherwise I am the donor
    // 1 - Add the cost of the task for the recipient in the belief base
    if (debugAcceptability)
      println(s"MindWithPeerModelling>$me 's belief base about $recipient (before): ${updatedMind.beliefBase.belief(recipient)}")
    val beforeCostJobRecipient = updatedMind.beliefCost(jobOfTask, recipient)
    val afterCostJobRecipient = beforeCostJobRecipient + stap.cost(task, recipient)
    val updatedInsight = updatedMind.beliefBase.belief(recipient).update(jobOfTask, afterCostJobRecipient)
    updatedMind = updatedMind.updateBeliefBase(recipient, updatedInsight)
    if (debugAcceptability)
      println(s"MindWithPeerModelling>$me 's belief base about $recipient (after): ${updatedMind.beliefBase.belief(recipient)}")
    // 2 - Remove the task to the bundle which is sorted
    updatedMind = updatedMind.removeTask(task)
    val cost4Job = updatedMind.beliefCost(job, recipient)
    // 3 a - Either the cost of the job is 0.0 then the completion time is 0.0
    if (cost4Job ~= 0.0) return 0.0
    // 3 b - Or it is not the case and the jobs must be sorted according to the peer strategy and the belief base
    // in order to consider the delay 3a
    val orderedJob = updatedMind.stap.jobs.toList.sortWith((a, b) =>
      updatedMind.beliefCost(a: Job, recipient) ~< updatedMind.beliefCost(b: Job, recipient) ||
        ((updatedMind.beliefCost(a: Job, recipient) ~= updatedMind.beliefCost(b: Job, recipient)) && a.name < b.name)
    ) // since we assume LocallyCheapestJobFirst
    if (debugOrder) println(s"MindWithPeerModelling>$me deduces the job order of $recipient after delegation of $task: $orderedJob")
    //4 - Returns the completion time of the recipient according to the new mind
    val previousJob = orderedJob.takeWhile(_ != job)
    val delay = previousJob.foldLeft(0.0)((sum, previousJob) => sum + updatedMind.beliefCost(previousJob, recipient))
    val beliefCompletionTime = delay + cost4Job
    if (debug) println(s"MindWithPeerModelling>$me deduces the completion time of the job $job for the recipient $recipient with task $task: $beliefCompletionTime")
    beliefCompletionTime
  }

  /**
    * Returns what the belief about the completion time of a recipient for a job after the multi delegation from the donor
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTimeOfRecipient(job: Job, donor: ComputingNode, recipient: ComputingNode, endowment: List[Task]): Double = {
    var updatedMind = this.copy()
    // Either I am the recipient
    if (recipient == me) {
      if (debugAcceptability)
        println(s"MindWithPeerModelling>$me 's belief base about $donor (before):" +
          s" ${updatedMind.beliefBase.belief(donor)}")
      // 1 - Update the belief base by removing the cost of the task in the corresponding job for the donor
      for (task <- endowment) {
        val jobOfTask = stap.jobOf(task)
        val beforeCostJobDonor = updatedMind.beliefCost(jobOfTask, donor)
        val afterCostJobDonor = beforeCostJobDonor - stap.cost(task, donor)
        val updatedInsight: Insight = updatedMind.beliefBase.belief(donor).update(jobOfTask, afterCostJobDonor)
        updatedMind = updatedMind.updateBeliefBase(donor, updatedInsight)
      }
      if (debugAcceptability)
        println(s"MindWithPeerModelling>$me 's belief base about $donor (after): ${updatedMind.beliefBase.belief(donor)}")
      // 2 - Add the task to the bundle of the recipient which is sorted
      updatedMind = updatedMind.addTasks(endowment)
      if (debugAcceptability)
        println(s"MindWithPeerModelling>$me 's bundle (after): ${updatedMind.taskSet} with ${bundle.orderedJobs}")
      // 3 - Returns the completion time of the recipient according to the new mind
      return updatedMind.completionTime(job)
    }

    // Otherwise I am the donor
    // 1 - Add the cost of the task for the recipient in the belief base
    if (debugAcceptability)
      println(s"MindWithPeerModelling>$me 's belief base about $recipient (before): ${updatedMind.beliefBase.belief(recipient)}")
    for (task <- endowment) {
      val jobOfTask = stap.jobOf(task)
      val beforeCostJobRecipient = updatedMind.beliefCost(jobOfTask, recipient)
      val afterCostJobRecipient = beforeCostJobRecipient + stap.cost(task, recipient)
      val updatedInsight = updatedMind.beliefBase.belief(recipient).update(jobOfTask, afterCostJobRecipient)
      updatedMind = updatedMind.updateBeliefBase(recipient, updatedInsight)
    }
    if (debugAcceptability)
      println(s"MindWithPeerModelling>$me 's belief base about $recipient (after): ${updatedMind.beliefBase.belief(recipient)}")
    // 2 - Remove the task to the bundle which is sorted
    updatedMind = updatedMind.removeTasks(endowment)
    val cost4Job = updatedMind.beliefCost(job, recipient)
    // 3 a - Either the cost of the job is 0.0 then the completion time is 0.0
    if (cost4Job ~= 0.0) return 0.0
    // 3 b - Or it is not the case and the jobs must be sorted according to the peer strategy and the belief base
    // in order to consider the delay 3a
    val orderedJob = updatedMind.stap.jobs.toList.sortWith((a, b) =>
      updatedMind.beliefCost(a: Job, recipient) ~< updatedMind.beliefCost(b: Job, recipient) ||
        ((updatedMind.beliefCost(a: Job, recipient) ~= updatedMind.beliefCost(b: Job, recipient)) && a.name < b.name)
    ) // since we assume LocallyCheapestJobFirst
    if (debugOrder) println(s"MindWithPeerModelling>$me deduces the job order of $recipient after delegation of $endowment: $orderedJob")
    //4 - Returns the completion time of the recipient according to the new mind
    val previousJob = orderedJob.takeWhile(_ != job)
    val delay = previousJob.foldLeft(0.0)((sum, previousJob) => sum + updatedMind.beliefCost(previousJob, recipient))
    val beliefCompletionTime = delay + cost4Job
    if (debug) println(s"MindWithPeerModelling>$me deduces the completion time of the job $job for the recipient $recipient with task $endowment: $beliefCompletionTime")
    beliefCompletionTime
  }

  /**
    * Returns what the belief about the completion time of a donor for a job after the multi delegation to the recipient
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTimeOfDonor(job: Job, donor: ComputingNode, recipient: ComputingNode, endowment: List[Task]): Double = {
    var updatedMind = this.copy()

    // Either I am the donor
    if (donor == me) { // Add the cost of the task for the recipient in the belief base
      if (debugAcceptability)
        println(s"$me 's belief base about $recipient (before): ${updatedMind.beliefBase.belief(recipient)}")
      // 1 - Update the belief base by adding the cost of the task in the corresponding job for the recipient
      for (task <- endowment) {
        val jobOfTask = stap.jobOf(task)
        val beforeCostJobRecipient = updatedMind.beliefCost(jobOfTask, recipient)
        val afterCostJobRecipient = beforeCostJobRecipient + stap.cost(task, recipient)
        val updatedInsight =
          updatedMind.beliefBase.belief(recipient).update(jobOfTask, afterCostJobRecipient)
        updatedMind = updatedMind.updateBeliefBase(recipient, updatedInsight)
      }
      if (debugAcceptability) println(s"MindWithPeerModelling>$me 's belief base about $recipient (after): ${updatedMind.beliefBase.belief(recipient)}")
      // 2 - Remove  the task to the bundle which is sorted
      updatedMind = updatedMind.removeTasks(endowment)
      // 3 - Returns the completion time in the new mind
      return updatedMind.completionTime(job)
    }

    // Otherwise I am the recipient
    // 1 - Remove the cost of the task for the donor in the belief base
    if (debugAcceptability)
      println(s"MindWithPeerModelling>$me 's belief base about $donor (before): ${updatedMind.beliefBase.belief(donor)}")
    for (task <- endowment) {
      val jobOfTask = stap.jobOf(task)
      val beforeCostJobDonor = updatedMind.beliefCost(jobOfTask, donor)
      val afterCostJobDonor = beforeCostJobDonor - stap.cost(task, donor)
      val updatedInsight = updatedMind.beliefBase.belief(donor).update(jobOfTask, afterCostJobDonor)
      updatedMind = updatedMind.updateBeliefBase(donor, updatedInsight)
    }
    if (debugAcceptability) println(s"MindWithPeerModelling>$me 's belief base about $donor (after): ${updatedMind.beliefBase.belief(donor)}")
    // 2 - Add the task to the bundle which is sorted
    updatedMind = updatedMind.addTasks(endowment)
    val cost4Job = updatedMind.beliefCost(job, donor)
    // 3 a - Either the cost of the job is 0.0 then the completion time is 0.0
    if (cost4Job ~= 0.0) {
      if (debugAcceptability) println(s"MindWithPeerModelling>$me deduces the completion time of the job $job for the donor $donor without task $endowment: " +
        s"0.0 since cost is belief cost is 0.0")
      return 0.0
    }
    // 3 b - Or it is not the case and the jobs must be sorted according to the peer strategy and the belief base
    // in order to consider the delay 3a
    val orderedJob = updatedMind.stap.jobs.toList.sortWith((a, b) =>
      updatedMind.beliefCost(a: Job, donor) ~< updatedMind.beliefCost(b: Job, donor) ||
        ((updatedMind.beliefCost(a: Job, donor) ~= updatedMind.beliefCost(b: Job, donor)) && a.name < b.name)
    ) // since we assume LocallyCheapestJobFirst
    if (debugOrder) println(s"MindWithPeerModelling>$me deduces the job order of $donor after delegation of $endowment: $orderedJob")
    //4 - Returns the completion time of the donor according to the new mind
    val previousJob = orderedJob.takeWhile(_ != job)
    val delay = previousJob.foldLeft(0.0)((sum, previousJob) => sum + updatedMind.beliefCost(previousJob, donor))
    val beliefCompletionTime = delay + cost4Job
    if (debugAcceptability) println(s"MindWithPeerModelling>$me deduces the completion time of the job $job for the donor $donor without task $endowment: " +
      s"$beliefCompletionTime since delay $delay and previous jobs $previousJob")
    beliefCompletionTime
  }

  /**
    * Returns what the belief about the completion time of a donor for a job after the delegation to the recipient
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTimeOfDonor(job: Job, donor: ComputingNode, recipient: ComputingNode, task: Task): Double = {
    val jobOfTask = stap.jobOf(task)
    var updatedMind = this.copy()

    // Either I am the donor
    if (donor == me) { // Add the cost of the task for the recipient in the belief base
      if (debugAcceptability)
        println(s"$me 's belief base about $recipient (before): ${updatedMind.beliefBase.belief(recipient)}")
      // 1 - Update the belief base by adding the cost of the task in the corresponding job for the recipient
      val beforeCostJobRecipient = updatedMind.beliefCost(jobOfTask, recipient)
      val afterCostJobRecipient = beforeCostJobRecipient + stap.cost(task, recipient)
      val updatedInsight =
        updatedMind.beliefBase.belief(recipient).update(jobOfTask, afterCostJobRecipient)
      updatedMind = updatedMind.updateBeliefBase(recipient, updatedInsight)
      if (debugAcceptability) println(s"MindWithPeerModelling>$me 's belief base about $recipient (after): ${updatedMind.beliefBase.belief(recipient)}")
      // 2 - Remove  the task to the bundle which is sorted
      updatedMind = updatedMind.removeTask(task)
      // 3 - Returns the completion time in the new mind
      return updatedMind.completionTime(job)
    }

    // Otherwise I am the recipient
    // 1 - Remove the cost of the task for the donor in the belief base
    if (debugAcceptability)
      println(s"MindWithPeerModelling>$me 's belief base about $donor (before): ${updatedMind.beliefBase.belief(donor)}")
    val beforeCostJobDonor = updatedMind.beliefCost(jobOfTask, donor)
    val afterCostJobDonor = beforeCostJobDonor - stap.cost(task, donor)
    val updatedInsight = updatedMind.beliefBase.belief(donor).update(jobOfTask, afterCostJobDonor)
    updatedMind = updatedMind.updateBeliefBase(donor, updatedInsight)
    if (debugAcceptability) println(s"MindWithPeerModelling>$me 's belief base about $donor (after): ${updatedMind.beliefBase.belief(donor)}" +
      s"since $beforeCostJobDonor - ${stap.cost(task, donor)} = $afterCostJobDonor")
    // 2 - Add the task to the bundle which is sorted
    updatedMind = updatedMind.addTask(task)
    val cost4Job = updatedMind.beliefCost(job, donor)
    // 3 a - Either the cost of the job is 0.0 then the completion time is 0.0
    if (cost4Job ~= 0.0) {
      if (debugAcceptability) println(s"MindWithPeerModelling>$me deduces the completion time of the job $job for the donor $donor without task $task: " +
        s"0.0 since cost is belief cost is 0.0")
      return 0.0
    }
    // 3 b - Or it is not the case and the jobs must be sorted according to the peer strategy and the belief base
    // in order to consider the delay 3a
    val orderedJob = updatedMind.stap.jobs.toList.sortWith((a, b) =>
      updatedMind.beliefCost(a: Job, donor) ~< updatedMind.beliefCost(b: Job, donor) ||
        ((updatedMind.beliefCost(a: Job, donor) ~= updatedMind.beliefCost(b: Job, donor)) && a.name < b.name)
    ) // since we assume LocallyCheapestJobFirst
    if (debugOrder) println(s"MindWithPeerModelling>$me deduces the job order of $donor after delegation of $task: $orderedJob")
    //4 - Returns the completion time of the donor according to the new mind
    val previousJob = orderedJob.takeWhile(_ != job)
    val delay = previousJob.foldLeft(0.0)((sum, previousJob) => sum + updatedMind.beliefCost(previousJob, donor))
    val beliefCompletionTime = delay + cost4Job
    if (debugAcceptability) println(s"MindWithPeerModelling>$me deduces the completion time of the job $job for the donor $donor without task $task: " +
      s"$beliefCompletionTime since delay $delay and previous jobs $previousJob")
    beliefCompletionTime
  }

  /**
    * Returns what the belief about the completion time of an initiator for a job after the swap
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTimeOfInitiator(job: Job, initiator: ComputingNode, responder: ComputingNode, task: Task, counterpart: Task): Double = {
    val jobOfTask = stap.jobOf(task)
    val jobOfCounterpart = stap.jobOf(counterpart)
    var updatedMind = this.copy()

    // Either I am the initiator
    if (initiator == me) { // Add the cost of the task for the responder in the belief base
      if (debugAcceptability || debugSwap)
        println(s"$me 's belief base about $responder (before): ${updatedMind.beliefBase.belief(responder)}")
      // 1a - Update the belief base by adding the cost of the task in the corresponding job for the responder
      val beforeCostJobOfTask4Responder = updatedMind.beliefCost(jobOfTask, responder)
      val afterCostJobOfTask4Responder = beforeCostJobOfTask4Responder + stap.cost(task, responder)
      var updatedInsight =
        updatedMind.beliefBase.belief(responder).update(jobOfTask, afterCostJobOfTask4Responder)
      updatedMind = updatedMind.updateBeliefBase(responder, updatedInsight)
      if (debugAcceptability || debugSwap) println(s"MindWithPeerModelling>$me 's belief base about $responder (after task addition): ${updatedMind.beliefBase.belief(responder)}")
      // 1b - Update the belief base by removing the cost of the counterpart taken in the corresponding job for the responder
      val beforeCostJobOfCounterpart4Responder = updatedMind.beliefCost(jobOfCounterpart, responder)
      val afterCostJobOfCounterpart4Responder = beforeCostJobOfCounterpart4Responder - stap.cost(counterpart, responder)
      updatedInsight =
        updatedMind.beliefBase.belief(responder).update(jobOfCounterpart, afterCostJobOfCounterpart4Responder)
      updatedMind = updatedMind.updateBeliefBase(responder, updatedInsight)
      if (debugAcceptability || debugSwap) println(s"MindWithPeerModelling>$me 's belief base about $responder (after counterpart removal): ${updatedMind.beliefBase.belief(responder)}")
      // 2 - Replace the task by the counterpart in the bundle which is sorted
      updatedMind = updatedMind.replaceTask(task, counterpart)
      // 3 - Returns the completion time in the new mind
      return updatedMind.completionTime(job)
    }

    // Otherwise I am the responder
    // 1a - Remove the cost of the task proposed in the initiator's belief base
    if (debugAcceptability || debugSwap)
      println(s"MindWithPeerModelling>$me 's belief base about $initiator (before): ${updatedMind.beliefBase.belief(initiator)}")
    val beforeCostJobOfTask4Initiator = updatedMind.beliefCost(jobOfTask, initiator)
    val afterCostJobOfTask4Initiator = beforeCostJobOfTask4Initiator - stap.cost(task, initiator)
    var updatedInsight = updatedMind.beliefBase.belief(initiator).update(jobOfTask, afterCostJobOfTask4Initiator)
    updatedMind = updatedMind.updateBeliefBase(initiator, updatedInsight)
    // 1b - Add the cost of the counterpart in the initiator's belief base
    val beforeCostJobOfCounterpartInitiator = updatedMind.beliefCost(jobOfCounterpart, initiator)
    val afterCostJobOfCounterpartInitiator = beforeCostJobOfCounterpartInitiator + stap.cost(counterpart, initiator)
    updatedInsight = updatedMind.beliefBase.belief(initiator).update(jobOfCounterpart, afterCostJobOfCounterpartInitiator)
    updatedMind = updatedMind.updateBeliefBase(initiator, updatedInsight)
    if (debugAcceptability || debugSwap) println(s"MindWithPeerModelling>$me 's belief base about $initiator (after): ${updatedMind.beliefBase.belief(initiator)}")
    // 2 - Replace the counterpart by the task in the bundle which is sorted
    updatedMind = updatedMind.replaceTask(counterpart, task)
    val cost4Job = updatedMind.beliefCost(job, initiator)
    // 3 a - Either the cost of the job is 0.0 then the completion time is 0.0
    if (cost4Job ~= 0.0) {
      if (debugAcceptability || debugSwap) println(s"MindWithPeerModelling>$me deduces the completion time of the job $job for the donor $initiator after swap of $task and $counterpart: " +
        s"0.0 since cost is belief cost is 0.0")
      return 0.0
    }
    // 3 b - Or it is not the case and the jobs must be sorted according to the peer strategy and the belief base
    // in order to consider the delay 3a
    val orderedJob = updatedMind.stap.jobs.toList.sortWith((a, b) =>
      updatedMind.beliefCost(a: Job, initiator) ~< updatedMind.beliefCost(b: Job, initiator) ||
        ((updatedMind.beliefCost(a: Job, initiator) ~= updatedMind.beliefCost(b: Job, initiator)) && a.name < b.name)
    ) // since we assumeLocallyCheapestJobFirst
    if (debugOrder) println(s"MindWithPeerModelling>$me deduces the job order of $initiator after swap of $task and $counterpart: $orderedJob")
    //4 - Returns the completion time of the initiator according to the new mind
    val previousJob = orderedJob.takeWhile(_ != job)
    val delay = previousJob.foldLeft(0.0)((sum, previousJob) => sum + updatedMind.beliefCost(previousJob, initiator))
    val beliefCompletionTime = delay + cost4Job
    if (debugAcceptability || debugSwap) println(s"MindWithPeerModelling>$me deduces the completion time of the job $job for the initiator $initiator after swap of $task and $counterpart: " +
      s"$beliefCompletionTime since delay $delay and previous jobs $previousJob")
    beliefCompletionTime
  }

  /**
    * Returns what the belief about the completion time of a responder for a job after the swap
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTimeOfResponder(job: Job, initiator: ComputingNode, responder: ComputingNode, task: Task, counterpart: Task): Double = {
    beliefCompletionTimeOfInitiator(job, responder, initiator, counterpart, task)
  }

  /**
    * Returns the ordered list of jobs for which the agent is a bottleneck
    */
  def bottleneckJobs(): List[Job] = bundle.orderedJobs.filter(this.isNodeMax(this.me, _))

  /**
    * Returns the jobs sorted by a peer
    * since we assume LocallyCheapestJobFirst
    */
  def sortedJobs(peer: ComputingNode): List[Job] = stap.jobs.toList.sortWith((a, b) =>
    beliefCost(a: Job, peer) < beliefCost(b: Job, peer) ||
      (beliefCost(a: Job, peer) == beliefCost(b: Job, peer) && a.name < b.name))

}
  /**
  * Factory for [[MindWithPeerModelling]] instances
  */
object MindWithPeerModelling {
  val debug = false

  /**
    * Build a mind with an updated belief base from
    * @param stap instance
    * @param me is the computing node
    * @param allocation an allocation
    */
  def apply(stap: STAP, me: ComputingNode, allocation: ExecutedAllocation): MindWithPeerModelling = {
    new MindWithPeerModelling(me,
      stap.ds.computingNodes.filter(_ != me).toSet,
      Map[ComputingNode, ProvisioningUsage]() ++ stap.ds.computingNodes.view.map(i => i -> Active),
      stap,
      allocation.bundle(me).toSet,
      BeliefBase(stap, me, allocation)
    )
  }
}
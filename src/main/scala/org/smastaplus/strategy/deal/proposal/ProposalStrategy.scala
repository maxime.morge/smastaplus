// Copyright (C) Maxime MORGE 2021, 2022, 2023
package org.smastaplus.strategy.deal.proposal

import org.smastaplus.consumer.deal.mas.nodeAgent.negotiator.NegotiatingMind
import org.smastaplus.core._
import org.smastaplus.process._
import org.smastaplus.provisioning.{Active, Inactive, Suspended}
import org.smastaplus.strategy.deal.{MindWithPeerModelling, OfferStrategy}

/**
  * Abstract proposal strategy
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class ProposalStrategy(override val minimalImprovement: Double) extends
  OfferStrategy[Delegation](minimalImprovement) {

  /**
    * Returns the offer which has been previously selected by selectOffer,
    * eventually none
    */
  var previousOffer: Option[Delegation] = None

  /**
    * Returns the offer which has been previously accepted,
    * eventually none
    */
  private var previousAcceptance: Option[Delegation] = None

  /**
    * Returns a potential bilateral deal eventually none
    */
  def selectOffer(mind: MindWithPeerModelling, rule: SocialRule): Option[Delegation]

  /**
    * Returns a list of tasks eventually empty, i.e.
    * the acceptable endowment from the delegation wrt the rule validated by the mind (of the recipient)
    */
  @throws(classOf[RuntimeException])
  def acceptableEndowment(delegation: Delegation, mind: NegotiatingMind, rule: SocialRule): List[Task] = {
    //println(s"INSIGHT : ${mind.beliefBase}")
    var endowment = delegation.endowment.reverse
    while (endowment.nonEmpty) {
      val partialDelegation = new Delegation(delegation.stap, delegation.donor, delegation.recipient, endowment)
      //println(s"$partialDelegation")
      val acceptability = rule match {
        case Makespan =>
          acceptabilityCriteria(partialDelegation, mind, Makespan)
        case LocalFlowtime =>
          acceptabilityCriteria(partialDelegation, mind, LocalFlowtime)
        case GlobalFlowtime =>
          acceptabilityCriteria(partialDelegation, mind, GlobalFlowtime)
        case LocalFlowtimeAndMakespan =>
          acceptabilityCriteria(partialDelegation, mind, LocalFlowtime) &&
            acceptabilityCriteria(partialDelegation, mind, Makespan)
        case LocalFlowtimeAndGlobalFlowtime =>
          acceptabilityCriteria(partialDelegation, mind, LocalFlowtime) &&
            acceptabilityCriteria(partialDelegation, mind, GlobalFlowtime)
        case rule =>
          throw new RuntimeException(s"DelegationStrategy>${mind.me} is not able to manage $rule ")
      }
      if (acceptability) {
        previousAcceptance = Some(partialDelegation)
        if (debug) println(s"DelegationStrategy>${mind.me} has found an acceptable endowment $endowment")
        if (debug) println(s"DelegationStrategy>${mind.me} has found an acceptance $previousAcceptance")
        //println(s"acceptable endowment (non empty) : $endowment")
        //println(s"result : $endowment")
        return endowment
      }
      endowment = endowment.tail
    }
    previousAcceptance = None
    if (debug) println(s"DelegationStrategy>${mind.me} has found no acceptable endowment")
    if (debug) println(s"DelegationStrategy>${mind.me} has found no acceptance $previousAcceptance")
    //println(s"acceptable endowment : ${List.empty[Task]}")
    List.empty[Task]
  }

  /**
    * Returns true if the delegation is compliant with the provisioning usage according to the mind
    */
  @throws(classOf[RuntimeException])
  def provisioningCompliance(delegation: Delegation, mind: MindWithPeerModelling): Boolean = {
    val donor = delegation.donor
    val recipient = delegation.recipient
    mind.usage(delegation.recipient) match {
      case Inactive =>
        if (debug) println(s"ProposalStrategy>${mind.me} consider $delegation as not provisioning-compliant " +
          s"since the recipient $recipient is Inactive")
        false
      case Suspended =>
        if (debug) println(s"ProposalStrategy>${mind.me} consider $delegation as not provisioning-compliant " +
          s"since the recipient $recipient is Suspended")
        false
      case Active =>
        mind.usage(delegation.donor) match {
          case Suspended =>
            if (debug) println(s"ProposalStrategy>${mind.me} consider $delegation as provisioning-compliant " +
              s"since the recipient $recipient is Active" +
              s"and the donor $donor is Suspended")
            true
          case Inactive => throw new RuntimeException(s"ERROR: ProposalStrategy of node ${mind.me} cannot evaluate " +
            s"provisioning-compliance since the donor $donor is Inactive ")
          case Active =>
            if (debug) println(s"ProposalStrategy>${mind.me} consider $delegation as provisioning-compliant " +
              s"since the recipient $recipient is Active" +
              s"and the donor $donor is Active")
            true
        }
    }
  }
}

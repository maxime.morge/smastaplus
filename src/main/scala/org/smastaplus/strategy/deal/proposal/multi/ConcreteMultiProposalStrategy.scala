// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2021, 2023
package org.smastaplus.strategy.deal.proposal.multi

import org.smastaplus.core._
import org.smastaplus.strategy.deal.MindWithPeerModelling

/**
  * Concrete strategy for proposing a (multi) delegation with the bad strategy for endowment selection:
  * the agent adds one-by-one all the tasks in a job or in previous ones that improves the global flowtime
  * and stops to consider the candidate as soon as adding a task does not improve the global flowtime
  * @param minimalImprovement is the minimal required reduction for acceptability, 0.0 by default
  */
case class ConcreteMultiProposalStrategy(override val minimalImprovement: Double = 0.0) extends
  AbstractMultiProposalStrategy(minimalImprovement) {

  /**
    * Adds one-by-one all the tasks in a job or in previous ones that improves the global flowtime
    * and stops to consider the candidate as soon as adding a task does not improve the global flowtime
    */
  override def selectEndowment(mind: MindWithPeerModelling, recipient: ComputingNode, job: Job, rule: SocialRule): List[Task] =  {
    var endowment = List[Task]()
    // Sort the candidate tasks
    //Sorting the candidate tasks
    val previousJobs: List[Job] = mind.previousJobs(job) ::: List(job)
    val previousTasks = mind.tasks.intersect(previousJobs.foldLeft(Set[Task]())((acc, j) => acc.union(j.tasks)))
    var tasks: List[Task] = sortByCostPayoff(mind, recipient, previousTasks)
    // Build the endowment
    var bestBeliefCompletionTime = mind.beliefGlobalFlowtime
    while (tasks.nonEmpty) {
      val task = tasks.head
      if (debug) println(s"ConcreteMultiProposalStrategy>${mind.me} tries the task $task in $tasks")
      tasks = tasks.tail
      if (debug) println(s"ConcreteMultiProposalStrategy>${mind.me} computes the global completion time with the new endowment : ")
      val afterDelegation = globalFlowtimeWithEndowment(mind, recipient, task :: endowment)
      if (bestBeliefCompletionTime > afterDelegation) {
        endowment ::= task
        bestBeliefCompletionTime = afterDelegation
        if (debug) println(s"ConcreteMultiProposalStrategy>${mind.me} adds the task $task in the endowment")
      }else{
        if (debug) println(s"ConcreteMultiProposalStrategy>${mind.me} doesn't add the task $task  " +
          s"and it proposes the endowment $endowment" )
        return endowment
      }
    }
    endowment
  }
}
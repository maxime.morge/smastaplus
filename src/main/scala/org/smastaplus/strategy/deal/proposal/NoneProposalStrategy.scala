// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2021, 2022, 2023
package org.smastaplus.strategy.deal.proposal

import org.smastaplus.consumer.deal.mas.nodeAgent.negotiator.NegotiatingMind
import org.smastaplus.core._
import org.smastaplus.process._
import org.smastaplus.strategy.deal._

/**
  * Silent strategy
  * @param minimalImprovement is the minimal required reduction for acceptability, 0.0 by default
  */
class NoneProposalStrategy(override val minimalImprovement: Double = 0.0)
  extends ProposalStrategy(minimalImprovement) {

override def toString: String = "NoneProposalStrategy"

  /**
    * Returns true if the acceptability criteria of the deal is validated by the mind (of the initiator)
    */
  @throws(classOf[RuntimeException])
  def acceptabilityCriteria(deal : Swap, mind: NegotiatingMind, rule: SocialRule): Boolean = false

    /**
    * Returns true if the acceptability criteria of the deal is validated by the mind (of the initiator)
    */
  @throws(classOf[RuntimeException])
  def acceptabilityCriteria(deal: Delegation, mind: MindWithPeerModelling, rule: SocialRule): Boolean = false


  /**
    * Returns a potential bilateral deal eventually none
    */
  def selectOffer(mind: MindWithPeerModelling, rule: SocialRule): Option[Delegation] = None

}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2023
package org.smastaplus.strategy.deal.proposal.multi

import org.smastaplus.core._
import org.smastaplus.strategy.deal.MindWithPeerModelling

/**
  * Concrete strategy for proposing a (multi) delegation with the good strategy for endowment selection:
  * the agent adds one-by-one all the tasks in a job or in previous ones that improves the global flowtime
  * @param minimalImprovement is the minimal required reduction for acceptability
  * @deprecated replaced by ConcreteMultiProposalStrategy previously BadConcreteHeuristicMultiProposalStrategy
  */
@Deprecated case class GoodConcreteMultiProposalStrategy(override val minimalImprovement: Double) extends
  AbstractMultiProposalStrategy(minimalImprovement) {

  /**
    * Adds one-by-one all the tasks in a job or in previous ones that improves the global flowtime
    */
  override def selectEndowment(mind: MindWithPeerModelling, recipient: ComputingNode, job: Job, rule: SocialRule): List[Task] =  {
    var endowment = List[Task]()
    //Sorting the candidate tasks
    val previousJobs: List[Job] = mind.previousJobs(job) ::: List(job)
    val previousTasks = mind.tasks.intersect(previousJobs.foldLeft(Set[Task]())((acc, j) => acc.union(j.tasks)))
    var tasks: List[Task] = sortByCostPayoff(mind, recipient, previousTasks)
    // Build the endowment
    var bestBeliefCompletionTime = mind.beliefGlobalFlowtime
    while (tasks.nonEmpty) {
      val task = tasks.head
      if (debug) println(s"ConcreteHeuristicMultiProposalStrategy1>${mind.me} tries the task $task in $tasks")
      tasks = tasks.tail
      if (debug) println(s"GoodConcreteMultiProposalStrategy>${mind.me} computes the global completion time with the new endowment : ")
      val afterDelegation = globalFlowtimeWithEndowment(mind, recipient, task :: endowment)
      if (debugAcceptability) println(s"GoodConcreteMultiProposalStrategy>${mind.me} endowment without/with $task : $bestBeliefCompletionTime/$afterDelegation ")
      if (bestBeliefCompletionTime > afterDelegation + minimalImprovement) {
        endowment ::= task
        bestBeliefCompletionTime = afterDelegation
        if (debug) println(s"GoodConcreteMultiProposalStrategy>${mind.me} adds the task $task in endowment")
      }
    }
    if (debug) println(s"GoodConcreteMultiProposalStrategy>${mind.me} proposes endowment $endowment")
    endowment
  }
}

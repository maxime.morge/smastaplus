// Copyright (C) Maxime MORGE 2020, 2021, 2022, 2023
package org.smastaplus.strategy.deal.proposal.single

import org.smastaplus.core._
import org.smastaplus.process.{Delegation, SingleDelegation}
import org.smastaplus.strategy.deal.MindWithPeerModelling

/**
  * Abstract conservative strategy for proposing a single delegation
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class AbstractConservativeProposalStrategy(override val minimalImprovement: Double)
  extends SingleProposalStrategy(minimalImprovement){
  /**
    * Returns the job which should be partially delegated
    * according to the donor mind
    */
  def select(mind : MindWithPeerModelling, jobs : Set[Job] ) : Option[Job]

  /**
    * Returns the node which should be the recipient for the job delegation
    * according to the donor mind
    */
  def select(mind : MindWithPeerModelling, nodes : Set[ComputingNode], job : Job) : Option[ComputingNode]

  /**
    * Returns the task which should be delegated to the recipient for the job
    * according to the donor mind
    */
  def select(mind : MindWithPeerModelling, tasks : Set[Task], recipient : ComputingNode, job : Job) : Option[Task]

  /**
    * Returns the proposal of a potential (single) delegation eventually none
    * and update the previous offer
    */
  override def selectOffer(mind: MindWithPeerModelling, rule: SocialRule): Option[Delegation] = {
    var (job, recipient, task) : (Option[Job], Option[ComputingNode], Option[Task]) = (None, None, None)
    var jobs : Set [Job] = mind.stap.jobsOf(mind.tasks)
    while (jobs.nonEmpty){
      job = select(mind, jobs)
      if (debug) println(s"AbstractSingleProposalStrategy>${mind.me} tries the job $job in $jobs")
      if (job.isEmpty) return None
      var nodes : Set[ComputingNode] =  mind.peers.filter( peer => mind.usage(peer).isActive)
      while (nodes.nonEmpty) {
        recipient = select(mind, nodes, job.get)
        if (debug) println(s"AbstractSingleProposalStrategy>${mind.me} tries the recipient $recipient in $nodes")
        if (recipient.isDefined) {
          var tasks: Set[Task] = mind.tasks
          while (tasks.nonEmpty) {
            task = select(mind, tasks, recipient.get, job.get)
            if (debug) println(s"AbstractSingleProposalStrategy>${mind.me} tries the task $task in $tasks")
            if (task.isDefined) {
              val delegation = new SingleDelegation(mind.stap, donor = mind.me, recipient = recipient.get, task.get)
              if (debug) println(s"AbstractSingleProposalStrategy>${mind.me} is calculating triggering criteria : ")
              if (acceptabilityRule(delegation, mind, rule)) {
                previousOffer = Some(delegation)
                return Some(delegation)
              }
              tasks = tasks.filter(_ != task.get)
            } else tasks = Set[Task]()
          }
          nodes = nodes.filter(_ != recipient.get)
          if (debug) println(s"AbstractSingleProposalStrategy>${mind.me} removes the recipient $recipient in $nodes")
        } else nodes = Set[ComputingNode]()
      }
      jobs = jobs.filter( _ != job.get)
    }
    previousOffer = None
    None
  }
}
// Copyright (C) Maxime MORGE, 2023
package org.smastaplus.strategy.deal.proposal.multi

import org.smastaplus.core.{ComputingNode, Job, SocialRule, Task}
import org.smastaplus.strategy.deal.MindWithPeerModelling

import scala.annotation.unused

/**
 * Silent strategy
 * @param minimalImprovement is the minimal required reduction for acceptability
 */
@unused
class NoneMultiProposalStrategy(override val minimalImprovement: Double)
  extends AbstractMultiProposalStrategy(minimalImprovement ) {
  /**
   * Returns the endowment
   */
  override def selectEndowment(mind: MindWithPeerModelling, recipient: ComputingNode, job: Job, rule: SocialRule): List[Task] =  Nil
}

// Copyright (C) Maxime MORGE 2020, 2021, 2023
package org.smastaplus.strategy.deal.proposal.multi

import org.smastaplus.core._
import org.smastaplus.process.Delegation
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.strategy.deal.proposal.ProposalStrategy
import org.smastaplus.utils.MathUtils._

import scala.annotation.switch

/**
  * Abstract strategy for proposing a (multi)-delegation
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class MultiProposalStrategy(override val minimalImprovement: Double)
  extends ProposalStrategy(minimalImprovement){

  override def toString: String = s"MultiProposalStrategy"

  /**
    * Returns true if the acceptability criteria of the delegation is validated by any mind
    */
  @throws(classOf[RuntimeException])
  override def acceptabilityCriteria(delegation: Delegation, mind: MindWithPeerModelling, rule: SocialRule): Boolean = {
    //println(s"${mind.beliefBase}")
    if (!provisioningCompliance(delegation, mind)) {
      if (debugAcceptability) println(s"MultiProposalStrategy>${mind.me} consider the delegation $delegation as not provisioning-compliant")
      return false
    }
    if (debugAcceptability) println(s"MultiProposalStrategy>${mind.me} with ${mind.tasks} calculating acceptability criteria for delegation $delegation")
    val  donor = delegation.donor
    val recipient = delegation.recipient
    val endowment = delegation.endowment
    var (before, after) = (0.0, 0.0)
    rule match {
      case Makespan => // The makespan rule
        before = mind.beliefWorkload(donor)
        after = mind.beliefWorkload(recipient) + endowment.map(task => mind.stap.cost(task, recipient)).sum
      case LocalFlowtime => // The localFlowtime rule
        before = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
          val donorBefore = mind.beliefCompletionTime(job, donor)
          val recipientBefore = mind.beliefCompletionTime(job, recipient)
          val tmp = sum + Math.max(
            donorBefore,
            recipientBefore
          )
          if (debugAcceptability)
            println(s"MultiProposalStrategy>${mind.me} with ${mind.tasks}: " +
              s"$job for $delegation -> AccSum=$sum  ${donor}Before=$donorBefore  ${recipient}Before=$recipientBefore")
          tmp
        }
        after = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
          val donorAfter = mind.beliefCompletionTimeOfDonor(job, donor, recipient, endowment)
          val recipientAfter = mind.beliefCompletionTimeOfRecipient(job, donor, recipient, endowment)
          val tmp = sum + Math.max(
            donorAfter,
            recipientAfter
          )
          if (debugAcceptability)
            println(s"MultiProposalStrategy>${mind.me} with ${mind.tasks}: " +
              s"$job , AccSum=$sum  ${donor}After=$donorAfter  ${recipient}After=$recipientAfter")
          tmp
        }
        if (debugAcceptability)
          println(s"MultiProposalStrategy>${mind.me} acceptability of $delegation =" +
            s" ${after < before} since after=$after and before=$before with ${mind.orderedJobs}")
      case GlobalFlowtime => // The globalFlowtime rule
        before = mind.beliefGlobalFlowtime
        after = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
          sum + mind.stap.ds.computingNodes.map{ node : ComputingNode =>
            (node: @switch) match {
              case n if n == donor =>
                mind.beliefCompletionTimeOfDonor(job, donor, recipient, endowment)
              case n if n== recipient =>
                mind.beliefCompletionTimeOfRecipient(job, donor, recipient, endowment)
              case _ =>
                mind.beliefCompletionTime(job, node)
            }
          }.max
        }
        if (debugAcceptability)
          println(s"MultiProposalStrategy>${mind.me} acceptability of $delegation =" +
            s" ${after < before} since after=$after and before=$before with belief base ${mind.beliefBase} ")
      case rule =>
        throw new RuntimeException(s"MultiProposalStrategy>${mind.me}  is not able to manage $rule ")
    }
    if (debug) println(s"MultiProposalStrategy>${mind.me} acceptability of $delegation = " +
      s"${after < before} since after=$after and before=$before")
    //after ~< before
    minimalImprovement ~< (before - after)
  }
}
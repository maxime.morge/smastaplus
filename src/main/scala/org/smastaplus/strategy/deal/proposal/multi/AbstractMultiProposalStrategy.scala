// Copyright (C) Maxime MORGE 2020, 2021, 2022, 2023
package org.smastaplus.strategy.deal.proposal.multi

import org.smastaplus.consumer.deal.mas.nodeAgent.negotiator.NegotiatingMind
import org.smastaplus.core._
import org.smastaplus.process.Delegation
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.utils.MathUtils.MathUtils

import scala.annotation.switch

/**
  * Abstract strategy for proposing a (multi) delegation
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class AbstractMultiProposalStrategy(override val minimalImprovement: Double)
  extends MultiProposalStrategy(minimalImprovement) {

  /**
    * Returns the job which should be partially delegated
    * according to the donor mind
    * 
    * Selects among the jobs for which the donor is considered as a bottleneck,
    * the highest priority job according to the donor's  mind
    */
  final def select(mind: MindWithPeerModelling, jobs: Set[Job]): Option[Job] = {
    try {
      val bottleneckJobsList = mind.bottleneckJobs().filter(jobs.contains)
      Some(bottleneckJobsList.head)
    } catch {
      case _: NoSuchElementException => None
    }
  }

  /**
    * Returns the node which should be the recipient for the job delegation
    * according to the donor mind
    * Selects the peer for whom the sum of the differences between
    * the completion time for the allocation and the completion time for the peer is the largest for the jobs
    * after the job in the peer bundle according to the donor
    */
  final def select(mind: MindWithPeerModelling, nodes: Set[ComputingNode], job: Job): Option[ComputingNode] = {
    var maxDistance = Double.MinValue
    var maxNode: Option[ComputingNode] = None
    nodes.foreach { peer =>
      val peerJobsList: List[Job] = mind.sortedJobs(peer)
      val nextPeerJobsList = peerJobsList.dropWhile(_ != job).filterNot(_ == job)
      val distance = nextPeerJobsList.foldLeft(0.0)((sum, j) =>
        sum + mind.beliefCompletionTime(j) - mind.beliefCompletionTime(j, peer)
      )
      if (distance ~> maxDistance) {
        maxDistance = distance
        maxNode = Some(peer)
      }
    }
    maxNode
  }

  /**
    * Returns the endownment
    */
  def selectEndowment(mind: MindWithPeerModelling, recipient: ComputingNode, job: Job, rule: SocialRule): List[Task]

  /**
    * Returns the proposal of a potential multi delegation eventually none
    * and update the previous offer
    */
  final override def selectOffer(mind: MindWithPeerModelling, rule: SocialRule): Option[Delegation] = {
    var (job, recipient): (Option[Job], Option[ComputingNode]) = (None, None)
    var jobs: Set[Job] = mind.stap.jobsOf(mind.tasks)
    while (jobs.nonEmpty) {
      job = select(mind, jobs)
      if (debug || debugAbstract) println(s"AbstractMultiProposalStrategy>${mind.me} tries the job $job in $jobs")
      if (job.isEmpty) return None
      var nodes: Set[ComputingNode] = mind.peers.filter( peer => mind.usage(peer).isActive)
      while (nodes.nonEmpty) {
        recipient = select(mind, nodes, job.get)
        if (debug || debugAbstract) println(s"AbstractMultiProposalStrategy>${mind.me} tries the recipient $recipient in $nodes")
        if (recipient.isDefined) {
          val endowment = selectEndowment(mind, recipient.get, job.get, rule)
          if (endowment.nonEmpty) {
            previousOffer = Some(new Delegation(mind.stap, donor = mind.me, recipient = recipient.get, endowment))
            return previousOffer
          }
          nodes = nodes.filter(_ != recipient.get)
          if (debug || debugAbstract) println(s"AbstractMultiProposalStrategy>${mind.me} removes the recipient $recipient in $nodes")
        } else nodes = Set[ComputingNode]()
      }
      jobs = jobs.filter(_ != job.get)
    }
    previousOffer = None
    None
  }

  /**
    * Return the list of task by decreasing order of payoff for the delegation to the recipient
    */
  def sortByCostPayoff(mind: MindWithPeerModelling, recipient: ComputingNode, tasks: Set[Task]) : List[Task] = tasks.toList.sortWith{
    (t1,t2) =>
      val diff1 : Double = mind.stap.cost(t1, mind.me ) -  mind.stap.cost(t1, recipient)
      val diff2 : Double = mind.stap.cost(t2, mind.me ) -  mind.stap.cost(t2, recipient)
      diff1 > diff2
  }

  /**
    * Returns the believed global flowtime obtained with the delegation of the endownment
    */
  def globalFlowtimeWithEndowment(mind: MindWithPeerModelling, recipient: ComputingNode, endowment: List[Task]) : Double = {
    //val delegation = new Delegation(mind.pb, donor = mind.me, recipient = recipient, endowment)
    val afterDelegation = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
      sum + mind.stap.ds.computingNodes.map { node: ComputingNode =>
        (node: @switch) match {
          case n if n == mind.me =>
            mind.beliefCompletionTimeOfDonor(job, mind.me, recipient, endowment)
          case n if n == recipient =>
            mind.beliefCompletionTimeOfRecipient(job, mind.me, recipient, endowment)
          case _ =>
            mind.beliefCompletionTime(job, node)
        }
      }.max
    }
    afterDelegation
  }

  /**
    * Returns true if adding the task improves the delegation
    * according to the responder's mind
    */
  def betterWithTask(task: Task, delegation: Delegation, mind: NegotiatingMind, rule: SocialRule): Boolean = {
    val donor = delegation.donor
    val recipient = delegation.recipient
    val endowment = delegation.endowment
    val endowmentWithTask = task :: delegation.endowment
    var (afterDelegation, afterTask) = (0.0, 0.0)
    rule match {
      case Makespan =>
        val donorBeforeDelegation = mind.beliefWorkload(donor)
        val donorAfterDelegation = donorBeforeDelegation - endowment.map(t => mind.stap.cost(t, donor)).sum
        val donorAfterTask = donorAfterDelegation - mind.stap.cost(task, donor)
        val recipientBeforeDelegation = mind.beliefWorkload(recipient)
        val recipientAfterDelegation = recipientBeforeDelegation + endowment.map(t => mind.stap.cost(t, delegation.recipient)).sum
        val recipientAfterTask = recipientAfterDelegation + mind.stap.cost(task, recipient)
        val afterDelegation = Math.max(donorAfterDelegation, recipientAfterDelegation)
        val afterTask = Math.max(donorAfterTask, recipientAfterTask)
        val isBetter = afterTask ~< afterDelegation
        isBetter

      case LocalFlowtime =>
        for (job <- mind.stap.jobs) {
          val donorAfterDelegation = mind.beliefCompletionTimeOfDonor(job, donor, recipient, endowment)
          val recipientAfterDelegation = mind.beliefCompletionTimeOfRecipient(job, donor, recipient, endowment)
          afterDelegation += math.max(donorAfterDelegation, recipientAfterDelegation)
          val donorAfterTask = mind.beliefCompletionTimeOfDonor(job, donor, recipient, endowment)
          val recipientAfterTask = mind.beliefCompletionTimeOfRecipient(job, donor, recipient, endowmentWithTask)
          afterTask += Math.max(donorAfterTask, recipientAfterTask)
        }
        val isBetter = afterTask ~< afterDelegation
        isBetter

      case GlobalFlowtime =>
        val afterDelegation = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
          sum + mind.stap.ds.computingNodes.map { node: ComputingNode =>
            (node: @switch) match {
              case n if n == donor =>
                mind.beliefCompletionTimeOfDonor(job, donor, recipient, endowment)
              case n if n == recipient =>
                mind.beliefCompletionTimeOfRecipient(job, donor, recipient, endowment)
              case _ =>
                mind.beliefCompletionTime(job, node)
            }
          }.max
        }
        val afterTask = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
          sum + mind.stap.ds.computingNodes.map { node: ComputingNode =>
            (node: @switch) match {
              case n if n == donor =>
                mind.beliefCompletionTimeOfDonor(job, donor, recipient, endowmentWithTask)
              case n if n == recipient =>
                mind.beliefCompletionTimeOfRecipient(job, donor, recipient, endowmentWithTask)
              case _ =>
                mind.beliefCompletionTime(job, node)
            }
          }.max
        }
        val isBetter = afterTask ~< afterDelegation
        isBetter

      case rule =>
        throw new RuntimeException(s"AbstractMultiProposalStrategy>${mind.me} is not able to manage $rule ")
    }
  }
}
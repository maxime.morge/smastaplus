// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2021, 2023
package org.smastaplus.strategy.deal.proposal.multi

import org.smastaplus.core._
import org.smastaplus.strategy.deal.MindWithPeerModelling

/**
  * Concrete heuristic for proposing a (multi) delegation with the first strategy for endowment selection:
  * the agent adds one-by-one all the tasks in a job or in previous ones that improves the global flowtime
  * and ,as soon as adding a task does not improve the global flowtime, it removes the candidate in the same job
  * @param minimalImprovement is the minimal required reduction for acceptability
  * @deprecated replaced by ConcreteMultiProposalStrategy previously BadConcreteHeuristicMultiProposalStrategy
  */
@Deprecated case class UglyConcreteMultiProposalStrategy(override val minimalImprovement: Double) extends
  AbstractMultiProposalStrategy(minimalImprovement) {

  /**
    * Adds one-by-one all the tasks in a job or in previous ones that improves the global flowtime
    * and ,as soon as adding a task does not improve the global flowtime, it removes the candidate in the same job
    */
  override def selectEndowment(mind: MindWithPeerModelling, recipient: ComputingNode, job: Job, rule: SocialRule): List[Task] =  {
    var endowment = List[Task]()
    //Sorts the candidate tasks
    val previousJobs: List[Job] = mind.previousJobs(job) ::: List(job)
    val previousTasks = mind.tasks.intersect(previousJobs.foldLeft(Set[Task]())((acc, j) => acc.union(j.tasks)))
    var tasks: List[Task] = sortByCostPayoff(mind, recipient, previousTasks)
    // Build the endowment
    var bestBeliefCompletionTime = mind.beliefGlobalFlowtime
    while (tasks.nonEmpty) {
      val task = tasks.head
      if (debug) println(s"UglyConcreteMultiProposalStrategy>${mind.me} tries the task $task in $tasks")
      tasks = tasks.tail
      if (debug) println(s"UglyConcreteMultiProposalStrategy>${mind.me} computes the global completion time with the new endowment : ")
      val afterDelegation = globalFlowtimeWithEndowment(mind, recipient, task :: endowment)
      if (bestBeliefCompletionTime > afterDelegation + minimalImprovement) {
        endowment ::= task
        bestBeliefCompletionTime = afterDelegation
        if (debug) println(s"UglyConcreteMultiProposalStrategy>${mind.me} adds the task $task in endowment")
      } else {
        val job = mind.stap.jobOf(task)
        tasks = tasks.diff(job.tasks.toList) // removes the other tasks in the same job
        if (debug) println(s"UglyConcreteMultiProposalStrategy>${mind.me} doesn't add the task $task in endowment and remove the candidate from job $job")
      }
    }
    if (debug) println(s"UglyConcreteMultiProposalStrategy>${mind.me} proposes endowment $endowment")
    endowment
  }
}
// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2021, 2023
package org.smastaplus.strategy.deal.counterproposal.single

import org.smastaplus.consumer.deal.mas.nodeAgent.negotiator.NegotiatingMind
import org.smastaplus.core._
import org.smastaplus.process._
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.utils.MathUtils.MathUtils

/**
  * Abstract heuristic for counter-proposing a single swap
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class AbstractSingleCounterProposalStrategy(override val minimalImprovement: Double)
  extends SingleCounterProposalStrategy(minimalImprovement) {

  /**
    * Returns the counterpart task which should be delegated to the initiator for lighting
    * the job according to the  mind
    */
  def select(mind: MindWithPeerModelling, tasks: Set[Task], initiator: ComputingNode): Option[Task]

  /**
    * Returns a potential swap eventually none after an offer
    */
  override def selectCounterOffer(offer: Delegation, mind: NegotiatingMind, rule: SocialRule): Option[Swap] = {
    var task: Option[Task] = None
    var tasks: Set[Task] = mind.tasks
    while (tasks.nonEmpty) {
      task = select(mind, tasks, offer.donor)
      if (debugAbstract) println(s"AbstractSingleCounterProposalStrategy>${mind.me} tries the task $task in $tasks")
      if (task.isDefined) {
        val swap = new SingleSwap(mind.stap, offer.donor, mind.me, offer.endowment.head, task.get) // Not very prudent
        if (debugAbstract) println(s"AbstractSingleCounterProposalStrategy>${mind.me} is calculating triggering criteria : ")
        if (triggerable(swap, mind, rule)) {
          if (debugAbstract) println(s"AbstractSingleCounterProposalStrategy>${mind.me} selects $swap")
          previousCounterOffer = Some(swap)
          return Some(swap)
        }
        tasks = tasks.filter(_ != task.get)
      } else tasks = Set[Task]()
    }
    None
  }

  /**
    * Returns true if the swap is better than the original delegation
    * according to the responder's mind
    */
  def betterLocalFlowtimeThanDelegation(swap: Swap, mind: MindWithPeerModelling): Boolean = {
    val initiator = swap.initiator
    val responder = swap.responder

    swap match {
      case singleSwap: SingleSwap =>
        val task = singleSwap.task
        val counterpart = singleSwap.counterpart
        val delegation = new SingleDelegation(mind.stap, initiator, responder, task)
        var (afterDelegation, afterSwap) = (0.0, 0.0)
        if (debugAbstract)  println(s"AbstractSingleCounterProposalStrategy>${mind.me} believes ${mind.beliefBase}")
        for (job <- mind.stap.jobs){
          val initiatorAfterDelegation = mind.beliefCompletionTimeOfDonor(job, initiator, responder, task)
          val responderAfterDelegation = mind.beliefCompletionTimeOfRecipient(job, initiator, responder, task)
          afterDelegation += math.max(initiatorAfterDelegation, responderAfterDelegation)
          val initiatorAfterSwap = mind.beliefCompletionTimeOfInitiator(job, initiator, responder, task, counterpart)
          val responderAfterSwap = mind.beliefCompletionTimeOfResponder(job, initiator, responder, task, counterpart)
          afterSwap += math.max(initiatorAfterSwap, responderAfterSwap)
          if (debugAbstract)  println(s"AbstractSingleCounterProposalStrategy(>${mind.me} job $job : initiatorAfterDelegation=$initiatorAfterDelegation responderAfterDelegation=$responderAfterDelegation")
          if (debugAbstract)  println(s"AbstractSingleCounterProposalStrategy(>${mind.me} job $job : initiatorAfterSwap=$initiatorAfterSwap responderAfterSwap=$responderAfterSwap")
        }
        val  isBetter = minimalImprovement ~<  (afterDelegation - afterSwap)
        if (debugAbstract){
          val justification =s"since swapLocalFlowTime=$afterSwap and delegationLocalFlowTime=$afterDelegation"
          if (isBetter)
            println(s"AbstractSingleCounterProposalStrategy>${mind.me} the $swap is better than the delegation $delegation $justification")
          else println(s"AbstractSingleCounterProposalStrategy(>${mind.me} the $swap is not better than the delegation $delegation $justification")
        }
        isBetter

      case swap : Swap =>
        throw new RuntimeException(s"AbstractMultiProposalStrategy>${mind.me} is not able to manage $swap ")
    }
  }
}

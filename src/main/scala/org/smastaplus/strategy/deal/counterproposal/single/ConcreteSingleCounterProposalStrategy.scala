// Copyright (C) Maxime MORGE 2021, 2023
package org.smastaplus.strategy.deal.counterproposal.single

import org.smastaplus.core._
import org.smastaplus.process.Swap
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.utils.MathUtils.MathUtils

/**
  * Concrete heuristic for counter-proposing a single swap
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
case class ConcreteSingleCounterProposalStrategy(override val minimalImprovement: Double= 0.0)
  extends AbstractSingleCounterProposalStrategy(minimalImprovement) {

  val debugConcrete = false

  /**
    * Selects a task for which the payout is the highest in
    * either the job or in one of the jobs preceding it within the responder bundle
    * according to the responder's mind
    */
  override def select(mind: MindWithPeerModelling, tasks: Set[Task], initiator: ComputingNode): Option[Task] = {
    for(candidate <- mind.sortedTasks if tasks.contains(candidate)){
      val improvement = mind.stap.cost(candidate, mind.me) - mind.stap.cost(candidate, initiator)
      if (improvement ~> minimalImprovement) return Some(candidate)
    }
    None
  }

  /**
    * Returns true if the swap is triggerable by the mind
    */
  override def triggerable(swap: Swap, mind: MindWithPeerModelling, rule: SocialRule): Boolean =
    acceptabilityRule(swap, mind, rule) && betterLocalFlowtimeThanDelegation(swap, mind)

}

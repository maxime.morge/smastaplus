// Copyright (C) Maxime MORGE, Luc BIGAND 2021, 2023
package org.smastaplus.strategy.deal.counterproposal.single

import org.smastaplus.core._
import org.smastaplus.process.{SingleSwap, Swap}
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.utils.MathUtils.MathUtils

import scala.annotation.switch
import scala.math.max

/**
  * Abstract single swap strategy
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class SingleCounterProposalStrategy(override val minimalImprovement: Double)
  extends CounterProposalStrategy(minimalImprovement) {

  override def toString: String = "SingleCounterProposalStrategy"

  /**
    * Returns true if the acceptability criteria of the swap is validated by the mind (of the recipient)
    */
  @throws(classOf[RuntimeException])
  override def acceptabilityCriteria(swap: Swap, mind: MindWithPeerModelling, rule: SocialRule): Boolean = {
    val initiator = swap.initiator
    val responder = swap.responder
    swap match {
      case singleSwap: SingleSwap =>
        val task = singleSwap.task
        val counterpart = singleSwap.counterpart
        if (debugAcceptability) println(s"SingleCounterProposalStrategy>${mind.me}  with  ${mind.tasks} calculating Acceptability Criteria for swap $swap")
        var (before, after) = (0.0, 0.0)
        rule match {
          case Makespan => // The makespan rule
            before = max(
              mind.beliefWorkload(initiator),
              mind.beliefWorkload(responder)
            )
            after = max(
              mind.beliefWorkload(initiator) - mind.stap.cost(task, initiator) + mind.stap.cost(counterpart, initiator),
              mind.beliefWorkload(responder) - mind.stap.cost(counterpart, responder) + mind.stap.cost(task, responder)
            )
          case LocalFlowtime => // The localFlowtime rule
            for (job <- mind.stap.jobs) {
              val initiatorBefore = mind.beliefCompletionTime(job, initiator)
              val responderBefore = mind.beliefCompletionTime(job, responder)
              before += max(initiatorBefore, responderBefore)
              val initiatorAfter = mind.beliefCompletionTimeOfInitiator(job, initiator, responder, task, counterpart)
              val responderAfter = mind.beliefCompletionTimeOfResponder(job, initiator, responder, task, counterpart)
              after += max(initiatorAfter, responderAfter)
              if (debugAcceptability) println(s"SingleCounterProposalStrategy>$job initiatorBefore = $initiatorBefore, responderBefore = $responderBefore, initiatorAfter = $initiatorAfter, responderAfter = $responderAfter")
            }
          case GlobalFlowtime => // The globalFlowtime rule
            before = mind.beliefGlobalFlowtime
            after = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
              sum + mind.stap.ds.computingNodes.map { node: ComputingNode =>
                (node: @switch) match {
                  case n if n == initiator =>
                    mind.beliefCompletionTimeOfInitiator(job, initiator, responder, task, counterpart)
                  case n if n == responder =>
                    mind.beliefCompletionTimeOfResponder(job, initiator, responder, task, counterpart)
                  case _ =>
                    mind.beliefCompletionTime(job, node)
                }
              }.max
            }
            if (debugAcceptability)
              println(s"SingleCounterProposalStrategy>${mind.me} acceptability of $swap =" +
                s" ${after < before} since after=$after and before=$before")

          case rule =>
            throw new RuntimeException(s"SingleCounterProposalStrategy>${mind.me}  is not able to manage $rule ")
        }
        if (debugAcceptability) println(s"SingleCounterProposalStrategy>${mind.me} acceptability of $swap = " +
          s"${after < before} since after=$after and before=$before")
        //after ~< before
        minimalImprovement  ~< (before - after)
      case swap: Swap =>
        throw new RuntimeException(s"SingleCounterProposalStrategy>${mind.me} cannot handle $swap")
    }
  }
}
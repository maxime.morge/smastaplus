// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2021, 2022
package org.smastaplus.strategy.consumption

import org.smastaplus.core._
import org.smastaplus.provisioning.{Active, ProvisioningUsage}

import org.joda.time.LocalDateTime

/**
  *  Agent's state of mind
  *  @param me is the node managed
  *  @param peers are computing node managed by the other agents
  *  @param stap instance
  *  @param taskSet is the set of tasks assigned to the agent
  */
class Mind(val me: ComputingNode,
           val peers: Set[ComputingNode],
           val stap: STAP,
           val taskSet: Set[Task]) {

  var debug = false

  protected var bundle: Bundle = new Bundle(stap, me).setBundle(taskSet)

  /**
    * Auxiliary  constructor
    */
  def this(me: ComputingNode, peers: Set[ComputingNode], pb: STAP, bundle: Bundle) = {
    this(me, peers, pb, Set.empty[Task])
    this.bundle = bundle
  }

  override def toString : String =
    s"$me knows the peers $peers, the bundle $bundle"

  /**
    * Returns a copy
    */
  def copy(): Mind = {
    val copyOfBundle = bundle.copy()
    new Mind(me, peers, stap, copyOfBundle)
  }

  /**
    * Sets and sorts the bundle of the mind
    */
  def setTaskSet(updatedBundle: Set[Task]) : Mind =
    new Mind(me, peers, stap, bundle.setBundle(updatedBundle))

  /**
   * Sets the bundle of the mind
   */
  def setBundle(updatedBundle: Bundle): Mind =
    new Mind(me, peers, stap, updatedBundle)

  /**
    * Adds a task to the bundle and sorts it
    */
  def addTask(task: Task): Mind =
    new Mind(me, peers, stap, bundle.addTask(task))

  /**
    * Adds an endowment to the bundle and sorts it
    */
  def addTasks(endowment: List[Task]): Mind =
    new Mind(me, peers, stap, bundle.addTasks(endowment))

  /**
    * Removes a task to the bundle and sorts it
    */
  def removeTask(task: Task): Mind =
    new Mind(me, peers, stap, bundle.removeTask(task))

  /**
    * Remove an endowment to the bundle and sorts it
    */
  def removeTasks(endowment: List[Task]): Mind =
    new Mind(me, peers, stap, bundle.removeTasks(endowment))

  /**
    * Replaces a task by a counterpart in the bundle and sorts it
    */
  def replaceTask(task: Task, counterpart: Task): Mind =
    new Mind(me, peers, stap, bundle.replaceTask(task, counterpart))

  /**
    * Replaces an endowment by a counterpart in the bundle and sorts it
    */
  def replaceTasks(endowment: List[Task], counterpart: List[Task]): Mind =
    new Mind(me, peers, stap, bundle.replaceTasks(endowment, counterpart))

  /**
    * Returns the ordered list of tasks in the bundle
    * according to the consumption strategy LCJF
    */
  def sortedTasks : List[Task] = bundle.sortedTasks

  /**
    * Returns the set of tasks in the bundle
    */
  def tasks : Set[Task] = bundle.sortedTasks.toSet

  /**
    * Returns the list of jobs ordered according to their local cost
    */
  def orderedJobs : List[Job] = bundle.orderedJobs

  /**
    * Returns the list of previous jobs
    */
  def previousJobs(job: Job) : List[Job]= bundle.previousJobs(job)

  /**
    * Returns the delay of the task within a potential bundle
    */
  def delay(task: Task, potentialBundle: Bundle = bundle): Double = {
    if (!potentialBundle.contains(task)){
      throw new RuntimeException(s"Mind>$me realizes $task does not belong to the bundle $potentialBundle")
    }
    val job = stap.jobOf(task)
    val delayJob : Double = potentialBundle.previousJobs(job).foldLeft(0.0) { (sum, previousJob) =>
      sum + potentialBundle.costOf(previousJob)
    }
    potentialBundle.previousTasks(task, job).foldLeft(delayJob)((sum, previousTask) => sum + stap.cost(previousTask, me))
  }

  /**
    * Returns the completion time of the task within a potential bundle
    * @note since Scala compiler disallow overloaded methods with default arguments use Java style
    */
  def completionTime(task: Task, potentialBundle: Bundle): Double =
    delay(task, potentialBundle) + stap.cost(task, me)  //to update with t-t0

  /**
    * Returns the completion time of the task within the current bundle
    * @note since Scala compiler disallow overloaded methods with default arguments use Java style
    */
  def completionTime(task: Task): Double =
    delay(task) + stap.cost(task, me) //to update with t-t0

  /**
    * Returns the workload within a potential bundle
    */
  def workload(potentialBundle: Bundle = bundle): Double =
    potentialBundle.sortedTasks.foldLeft(0.0)((sum, task) => sum + stap.cost(task, me))

  /**
    * Returns the cost of the job for me within a potential bundle
    */
  def cost(job: Job, potentialBundle: Bundle = bundle): Double = potentialBundle.costOf(job)

  /**
    * Returns the completion time of the job within a potential bundle
    * @note since Scala compiler disallow overloaded methods with default arguments use Java style
    */
  def completionTime(job: Job, potentialBundle : Bundle = bundle): Double =
    potentialBundle.previousJobs(job).foldLeft(0.0)((sum, previousJob) =>
      sum + potentialBundle.costOf(previousJob)) + potentialBundle.costOf(job)

  /**
    * Returns the completion time of the job within my bundle
    * @note since Scala compiler disallow overloaded methods with default arguments use Java style
    */
  def completionTime(job: Job): Double =
    bundle.previousJobs(job).foldLeft(0.0)((sum, previousJob) =>
      sum + bundle.costOf(previousJob)) + bundle.costOf(job)

    /**
    * Returns the insight of the agent for a given timestamp
    * If not precised, the default timestamp is the date of the creation of the insight
    */
  def insight(timestamp: LocalDateTime = LocalDateTime.now()): Insight = new Insight(bundle.costOfJobs, bundle.completionTimesOfJobs(timestamp), timestamp)
  

  /**
    * Returns true if the bundle is empty
    */
  def isEmptyBundle : Boolean = bundle.isEmpty

  /**
    * Returns the next task to execute
    */
  def nextTaskToExecute : Task = sortedTasks.head

  /**
    * Consume the next task
    */
  def consume : (Task, Mind) = {
  val (task, updatedBundle) = bundle.consume
    (task, new Mind(me, peers, stap, updatedBundle))
  }
}

/**
  * Factory for [[Mind]] instances
  */
object Mind {
  val debug = false
  /**
    * Build a mind with an updated belief base from
    * @param stap instance
    * @param me with the mind
    * @param allocation an allocation
    */
  def apply(stap: STAP, me: ComputingNode, allocation: Allocation, usage: ProvisioningUsage = Active): Mind = {
    val peers = stap.ds.computingNodes.filter(_ != me).toSet
    new Mind(me, peers, stap, allocation.bundle(me).toSet)
  }
}
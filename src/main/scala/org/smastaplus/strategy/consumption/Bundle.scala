// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, Anne-Cécile CARON, 2020, 2021, 2023
package org.smastaplus.strategy.consumption

import org.smastaplus.core._
import org.smastaplus.utils.MathUtils._

import org.joda.time.{Duration, LocalDateTime}
import scala.language.postfixOps
import scala.annotation.unused

/**
 * Task bundle which is managed according to the
 * "Locally Cheapest Job First" (LCJF)
 * consumption strategy
 * @param stap instance
 * @param node is the computing node which owns the bundle
 */
class Bundle(val stap: STAP, node: ComputingNode){

  // Initiates the internal data structure
  private var tasks : Map[Job, SubBundle] = Map[Job, SubBundle](stap.jobs.toSeq map { job =>
    (job, new SubBundle(stap, node, job))} :_*) // The SubBundles
  private var costs :  Map[Job, Double] = Map[Job, Double](stap.jobs.toSeq map { job =>
    (job, 0.0)} : _*) // The local cost for each job
  private var jobs : List[Job] = stap.jobs.toList.sortWith((a, b) =>
    a.name < b.name) // An ordered list of jobs according to their local cost

  var taskInProgress: Option[Task] = None
  var remainingWork: Option[Double] = None

  /**
   * Returns a JSON description of the bundle
   */
  def toJSON: String = {
    val str_init = "{ \"node\": \"" + this.node.name + "\"" // + "\","
    val str_tasks = sortedTasks.foldLeft("")((str, task) => str + ", \"" + task.toString + "\":" + this.stap.cost(task, this.node))
    if (str_tasks.nonEmpty){
      str_init + "," + sortedTasks.foldLeft("")((str, task) => str + ", \"" + task.toString + "\":" + this.stap.cost(task, this.node)).substring(1) + "}"
    }else{
      str_init + "}"
    }
  }

  override def toString: String = sortedTasks.mkString("[",", ","]")

  def describe: String = {
    jobs.foldLeft("")( (s, job) =>
      s+s"${job.name} (${costs(job)}): $tasks \n"
    )
  }

  /**
   * Returns the local cost of a job
   */
  def costOf(job: Job) : Double = costs(job)

  /**
   * Returns the local cost of each jobs
   */
  def costOfJobs : Map[Job, Double] = costs

  /**
   * Returns the list of jobs ordered according to their local cost
   */
  def orderedJobs : List[Job] = jobs

  /**
   * A secondary constructor
   */
  def this(pb: STAP, node: ComputingNode, tasks : Map[Job, SubBundle], costs :  Map[Job, Double], jobs : List[Job]) = {
    this(pb, node)
    this.tasks = tasks
    this.costs = costs
    this.jobs = jobs
  }

  /**
   * A secondary constructor when a task is in progress
   */
  def this(pb: STAP, node: ComputingNode, tasks : Map[Job, SubBundle], costs :  Map[Job, Double], jobs : List[Job], taskInProgress: Option[Task], remainingWork: Option[Double]) = {
    this(pb, node)
    this.tasks = tasks
    this.costs = costs
    this.jobs = jobs
    this.taskInProgress = taskInProgress
    this.remainingWork = remainingWork
  }

  /**
   * Returns a copy
   */
  def copy(): Bundle= {
    var copyOfCost = Map[Job, Double]()
    var copyOfTasks = Map[Job, SubBundle]()
    for( job <- stap.jobs) {
      copyOfCost += (job -> costs(job))
      copyOfTasks += (job -> tasks(job).copy())
    }
    new Bundle(stap, node, copyOfTasks, copyOfCost, jobs, this.taskInProgress, this.remainingWork)
  }

  /**
   * Returns the delay of a task
   */
  private def delayOfTask(task: Task) : Double = {
    val tasksBefore : List[Task] = sortedTasks.takeWhile( ! task.equals(_))
    var delay: Double = 0.0
    if (taskInProgress.isDefined) delay += remainingWork.get
    for (t <- tasksBefore) {
      delay += stap.cost(t, node)
    }
    delay
  }

  /**
   * Returns the completion time of a task for a given time
   */
  private def completionTimeOfTask(task: Task, time: LocalDateTime) : Double = {
    if (taskInProgress.isDefined && taskInProgress.get.equals(task)){
      val pastDuration = new Duration(stap.jobOf(task).releaseTime.toDateTime(), time.toDateTime()).getMillis
      return pastDuration + remainingWork.get
    }
    val pastDuration = new Duration(stap.jobOf(task).releaseTime.toDateTime(), time.toDateTime()).getMillis
    pastDuration + delayOfTask(task) + stap.cost(task, node)
  }

  /**
   * Returns the completion time of a job locally to the concerned node for a given time
   */
  def completionTimeOfJob(job: Job, time: LocalDateTime) : Double = {
    if (tasks(job).sortedTasks().nonEmpty) {
      val lastTask = tasks(job).sortedTasks().last
      return completionTimeOfTask(lastTask, time)
    }
    else if (taskInProgress.isDefined && stap.jobOf(taskInProgress.get).equals(job) ){
      return completionTimeOfTask(taskInProgress.get, time)
    }
    0.0
  }

  /**
   * Returns the completion time of a job locally to the concerned node for a given time
   */
  def completionTimesOfJobs(time: LocalDateTime) : Map[Job, Double] = {
    stap.jobs.toSeq map { job => (job, completionTimeOfJob(job, time))} toMap
  }

  /**
   * Returns the set of tasks in the bundle
   */
  def sortedTasks : List[Task] = jobs.foldLeft(List.empty[Task]){
    (taskList, job)  =>
      taskList ++ tasks(job).sortedTasks()
  }

  /**
   * Returns the list of previous jobs
   */
  def previousJobs(job: Job) : List[Job] = {
    var listOfPrevious = jobs.takeWhile(_ != job)
    if (taskInProgress.isDefined && !listOfPrevious.contains(stap.jobOf(taskInProgress.get)))
      listOfPrevious = stap.jobOf(taskInProgress.get) :: listOfPrevious
    listOfPrevious
  }

  /**
   * Returns the list of previous tasks for a job
   */
  def previousTasks(task: Task, job : Job) : List[Task] = {
    var listOfPrevious = tasks(job).previousTasks(task)
    if (taskInProgress.isDefined) listOfPrevious = taskInProgress.get :: listOfPrevious
    listOfPrevious
  }

  /**
   * Sort the job according to their local cost
   */
  private def sortJobs(): List[Job] =
    jobs.sortWith( (a, b) =>
      (costs(a: Job) ~< costs(b: Job)) ||
        ((costs(a: Job) ~= costs(b: Job)) && a.name < b.name)
    )

  /**
   * Returns the most expensive job
   */
  @unused
  def mostExpensiveJob : Job = jobs.last

  /**
   * Add a new task
   */
  @throws(classOf[RuntimeException])
  def addTask(task : Task) : Bundle ={
    val updatedBundle = copy()
    val job = stap.jobOf(task)
    val updatedTasks : SubBundle =  updatedBundle.tasks(job).addTask(task)
    updatedBundle.tasks += ( job -> updatedTasks)
    updatedBundle.costs += ( job -> (updatedBundle.costs(job) + stap.cost(task,node)))
    updatedBundle.jobs = updatedBundle.sortJobs()
    updatedBundle
  }

  /**
   * Add an endowment
   */
  def addTasks(endowment : List[Task]) : Bundle ={
    val updatedBundle = copy()
    endowment.foreach { task =>
      val job = stap.jobOf(task)
      val updatedTasks: SubBundle = updatedBundle.tasks(job).addTask(task)
      updatedBundle.tasks += (job -> updatedTasks)
      updatedBundle.costs += (job -> (updatedBundle.costs(job) + stap.cost(task, node)))
    }
    updatedBundle.jobs = updatedBundle.sortJobs()
    if (updatedBundle.isEmpty) println(s"Empty bundle after adding endowment $endowment")
    updatedBundle
  }

  /**
   * Remove a new task
   */
  def removeTask(task : Task) : Bundle ={
    val updatedBundle = copy()
    val job = stap.jobOf(task)
    val updatedTasks : SubBundle = updatedBundle.tasks(job).removeTask(task)
    updatedBundle.tasks += ( job -> updatedTasks)
    updatedBundle.costs += ( job -> (updatedBundle.costs(job) - stap.cost(task,node)))
    updatedBundle.jobs = updatedBundle.sortJobs()
    updatedBundle
  }

  /**
   * Remove an endowment
   */
  def removeTasks(endowment : List[Task]) : Bundle ={
    val updatedBundle = copy()
    endowment.foreach { task =>
      val job = stap.jobOf(task)
      val updatedTasks: SubBundle = updatedBundle.tasks(job).removeTask(task)
      updatedBundle.tasks += (job -> updatedTasks)
      updatedBundle.costs += (job -> (updatedBundle.costs(job) - stap.cost(task, node)))
    }
    updatedBundle.jobs = updatedBundle.sortJobs()
    updatedBundle
  }

  /**
   * Replaces a task by a counterpart
   */
  def replaceTask(task: Task, counterpart: Task): Bundle = {
    val updatedBundle = copy()

    val job = stap.jobOf(task)
    val updatedTasks : SubBundle = updatedBundle.tasks(job).removeTask(task)
    updatedBundle.tasks += ( job -> updatedTasks)
    updatedBundle.costs += ( job -> (updatedBundle.costs(job) - stap.cost(task,node)))

    val jobCounterpart = stap.jobOf(counterpart)
    val updatedCounterparts : SubBundle = updatedBundle.tasks(jobCounterpart).addTask(counterpart)
    updatedBundle.tasks += (jobCounterpart -> updatedCounterparts)
    updatedBundle.costs += (jobCounterpart -> (updatedBundle.costs(jobCounterpart) + stap.cost(counterpart, node)))

    updatedBundle.jobs = updatedBundle.sortJobs()
    updatedBundle
  }

  /**
   * Replaces an endowment by counterparts
   */
  def replaceTasks(endowment: List[Task], counterparts: List[Task]): Bundle = {
    val updatedBundle = copy()
    for(task <- endowment){
      val job = stap.jobOf(task)
      val updatedTasks: SubBundle = updatedBundle.tasks(job).removeTask(task)
      updatedBundle.tasks += (job -> updatedTasks)
      updatedBundle.costs += (job -> (updatedBundle.costs(job) - stap.cost(task, node)))
    }
    for(counterpart <- counterparts) {
      val jobCounterpart = stap.jobOf(counterpart)
      val updatedCounterparts: SubBundle = updatedBundle.tasks(jobCounterpart).addTask(counterpart)
      updatedBundle.tasks += (jobCounterpart -> updatedCounterparts)
      updatedBundle.costs += (jobCounterpart -> (updatedBundle.costs(jobCounterpart) + stap.cost(counterpart, node)))
    }
    updatedBundle.jobs = updatedBundle.sortJobs()
    updatedBundle
  }

  /**
   * Set  the bundle
   */
  def setBundle(updatedBundle : Set[Task]) : Bundle = {
    if (updatedBundle.isEmpty) return this
    addTask(updatedBundle.head).setBundle(updatedBundle.tail)
  }

  /**
   * Returns true if the bundle contains  the tasks
   */
  def contains(task: Task) : Boolean = tasks(stap.jobOf(task)).sortedTasks().contains(task)

  /**
   * Returns true if the bundle if empty
   */
  def isEmpty : Boolean = sortedTasks.isEmpty

  /**
   * Returns true if the bundle if empty
   */
  def nonEmpty: Boolean = sortedTasks.nonEmpty


  /**
   * Returns the next task to execute
   */
  @throws(classOf[RuntimeException])
  def nextTaskToExecute : Task = {
    if (isEmpty)
      throw new RuntimeException(s"Bundle>$node cannot execute a task since the bundle $this is empty")
    sortedTasks.head
  }

  /**
   * when a task has been given to the worker
   */
  def updateInProgressTask(task: Option[Task]) : Bundle =   {
    if (task.isDefined) {
      val cost = stap.cost(task.get, node)
      val updatedBundle = removeTask(task.get) // attention à garder le job concerné !
      updatedBundle.taskInProgress = task
      updatedBundle.remainingWork = Some(cost)
      //update the cost of the corresponding job
      val job = stap.jobOf(task.get)
      updatedBundle.costs += (job -> (updatedBundle.costs(job) + cost))
      updatedBundle
    }
    else{ //do not happen normally
      val updatedBundle = copy()
      updatedBundle.taskInProgress = None
      updatedBundle.remainingWork = None
      updatedBundle
    }
  }

  def updateRemainingWork(cost: Double) : Bundle = {
    val updatedBundle = copy()
    val oldCost = updatedBundle.remainingWork.get
    updatedBundle.remainingWork = Some(cost)
    //update the cost of the corresponding job
    val job = stap.jobOf(taskInProgress.get)
    updatedBundle.costs += (job -> (updatedBundle.costs(job) - oldCost + cost))
    updatedBundle
  }

  def resetTaskInProgress : Bundle = {
    val updatedBundle = copy()
    val oldCost = updatedBundle.remainingWork.getOrElse(0.0)
    val job = stap.jobOf(taskInProgress.get)
    taskInProgress = None
    remainingWork = None
    //update the cost of the corresponding job
    updatedBundle.costs += (job -> (updatedBundle.costs(job) - oldCost))
    updatedBundle
  }

  /**
   * Consume the next task
   */
  def consume : (Task, Bundle) = {
    if (isEmpty) (NoTask, this)
    else {
      val task = nextTaskToExecute
      (task, this.removeTask(task))
    }
  }
}

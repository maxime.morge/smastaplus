// Copyright (C) Maxime MORGE  2020, 2021
package org.smastaplus.strategy.consumption

import org.smastaplus.core._

import org.joda.time.LocalDateTime

/**
  *  Agent's belief base containing the
  *  insight and the ordered list of job for each peer
  *  @param belief about insight for each peer recorded after each inform
  *  @param orderedJobs  ordered list of jobs for each peer computed  after each inform
  *  @param completionTime  of each node/job computed  after each inform
  *  @param timestamp of the last update for each node
  */
class BeliefBase(val belief: Map[ComputingNode, Insight] = Map[ComputingNode, Insight](),
                 val orderedJobs: Map[ComputingNode, List[Job]] = Map[ComputingNode, List[Job]](),
                 val completionTime: Map[ComputingNode, Map[Job, Double]] = Map[ComputingNode, Map[Job, Double]](),
                 val timestamp: Map[ComputingNode, LocalDateTime] =  Map[ComputingNode, LocalDateTime](),
                 taskInProgress : Option[Task] = None, 
                 remainingWork : Option[Double] = None){

  val debug = false

  // var taskInProgress : Option[Task] = None
  // var remainingWork : Option[Double] = None

  override def toString: String = orderedJobs.map { case (node, jobs) =>
    jobs.map(job =>
      s"$job (cost=${belief(node).cost(job)}, completionTime=${completionTime(node)(job)})")
      .mkString(s"$node (", ", ", ")\n")
  }.mkString("[",", ","]\n")

  /**
    * Auxiliary  constructor
    */
  // def this(belief: Map[ComputingNode, Insight] = Map[ComputingNode, Insight](),
  //          orderedJobs: Map[ComputingNode, List[Job]] = Map[ComputingNode, List[Job]](),
  //          completionTime: Map[ComputingNode, Map[Job, Double]] = Map[ComputingNode, Map[Job, Double]](),
  //          timestamp: Map[ComputingNode, LocalDateTime] =  Map[ComputingNode, LocalDateTime](),
  //          taskInProgress : Option[Task] = None, 
  //          remainingWork : Option[Double] = None) = {
  //   this(belief, orderedJobs, completionTime, timestamp)
  //   this.taskInProgress = taskInProgress
  //   this.remainingWork = remainingWork
  // }

  /**
    * Returns a copy
    */
  def copy(): BeliefBase = {
    var copyBelief = Map[ComputingNode, Insight]()
    for ((node, insight) <- belief)
      copyBelief += (node -> insight)
    var copyOrderedJobs =  Map[ComputingNode, List[Job]]()
    for ((node, jobs) <- orderedJobs)
      copyOrderedJobs += (node -> jobs)
    var copyCompletionTime =  Map[ComputingNode, Map[Job,Double]]()
    for ((node, times) <- completionTime){
      var copyTimes  = Map[Job,Double]()
      for ((job,time) <- times){
      copyTimes += (job -> time)
      }
      copyCompletionTime  += (node -> copyTimes)
    }
    var copyTimestamp = Map[ComputingNode, LocalDateTime]()
    for ((node, time) <- timestamp)
      copyTimestamp += (node -> time)
    new BeliefBase(copyBelief, copyOrderedJobs, copyCompletionTime, copyTimestamp, taskInProgress, remainingWork)
  }

  /**
    * Updates the belief base with the insight of a node
    */
  def update(node: ComputingNode, newInsight: Insight): BeliefBase = {
    if ( ! timestamp.contains(node) || newInsight.timestamp.isAfter(timestamp(node)) ) { // update with the most recent insight
      val updatedBelief = belief.removed(node) +
        (node -> newInsight)
      val updatedOrderedJobs = orderedJobs.removed(node) +    //on récupère le completion time directement depuis l'insight
        (node -> newInsight.orderedJob)
      /*var times = Map[Job, Double]()
      var delay = 0.0
      for (job <- updatedOrderedJobs(node)) {
        delay += newInsight.cost(job)
        times += (job -> delay)
      }*/
      val updatedCompletionTime = completionTime.removed(node) + (node -> newInsight.completionTimes)
      val updatedTimestamp = timestamp.removed(node) + (node -> newInsight.timestamp)
      return new BeliefBase(updatedBelief, updatedOrderedJobs, updatedCompletionTime, updatedTimestamp, taskInProgress, remainingWork)
    }
  this
  }

  /**
    * Updates the belief base with the task in progress and its remaining work
    */
  def updateWorkInProgress(task: Option[Task], remaining: Option[Double]) =
    new BeliefBase(belief, orderedJobs, completionTime, timestamp, task, remaining)


  /**
    * Returns the workload of a peer
    */
  def workload(node: ComputingNode) : Double = belief(node).values.sum

  /**
    * Returns the most expensive job for a node
    */
  def mostExpensiveJob(node: ComputingNode) : Job = orderedJobs(node).last

  /**
    * Returns the belief about the completion time for the job by a node
    */
  def beliefCompletionTime(job: Job, node: ComputingNode): Double = completionTime(node)(job)

}

/**
  * Factory for [[BeliefBase]] instances
  */
object BeliefBase{
  val debug = false

  /**
    * Build a mind with an updated belief base from
    * @param stap  instance
    * @param me the computing node
    * @param allocation an allocation
    */
  def apply(stap: STAP, me: ComputingNode, allocation: ExecutedAllocation): BeliefBase = {
    var belief = Map[ComputingNode, Insight]()
    var orderedJobs =  Map[ComputingNode, List[Job]]()
    var completionTime = Map[ComputingNode, Map[Job, Double]]()
    var timestamp = Map[ComputingNode, LocalDateTime]()
    val peers = stap.ds.computingNodes.filter(_ != me).toSet
    peers.foreach { peer =>
      var insight: Insight = Insight()
      stap.jobs.foreach { job =>
        insight = insight.update(job, allocation.cost(job, peer))
      }
      belief+= (peer -> insight)
      timestamp += (peer -> insight.timestamp)
      orderedJobs+= (peer -> belief(peer).orderedJob)
      var times  = Map[Job,Double]()
      var delay : Double= 0.0
      for (job <- orderedJobs(peer)){
        delay += belief(peer).cost(job)
        times += (job -> delay)
      }
      completionTime += (peer -> times)
    }
    new BeliefBase(belief, orderedJobs, completionTime, timestamp)
  }
}
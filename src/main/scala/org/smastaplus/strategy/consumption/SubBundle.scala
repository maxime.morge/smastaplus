// Copyright (C) Maxime MORGE 2021
package org.smastaplus.strategy.consumption

import org.smastaplus.core._
import org.smastaplus.utils.MathUtils._

/**
  * Task SubBundle for an agent according to the Locally Cheapest Task First" (LCTF) consumption strategy
  *
  * @param stap instance, in particular the cost
  * @param node which must execute the tasks
  * @param job which contains the tasks
  */
class SubBundle(stap: STAP,
                node: ComputingNode,
                job: Job) {

  // Internal data structure
  private var tasks = List.empty[Task]

  override def toString: String = tasks.mkString(", ")

  /**
    * A secondary constructor
    */
  def this(pb:  STAP, node : ComputingNode, job: Job, tasks: List[Task]) = {
    this(pb, node, job)
    this.tasks = tasks
  }

  /**
    * Returns a copy of the subBundle
    */
  def copy() : SubBundle = {
    val copyOfTasks = tasks map {t => t}
    new SubBundle(stap, node, job, copyOfTasks)
  }

  /**
    * Returns the list of tasks in the bundle
    */
  def sortedTasks() : List[Task] = tasks

  /**
    * Returns the list of previous tasks
    */
  def previousTasks(task: Task) : List[Task] = tasks.takeWhile(_ != task)

  /**
    * Add some tasks
    */
  @throws(classOf[RuntimeException])
  def addTasks(someTasks : List[Task]) : SubBundle ={
    if (someTasks.isEmpty) this
    else addTask(someTasks.head).addTasks(someTasks.tail)
  }

  /**
    * Add a new task
    */
  @throws(classOf[RuntimeException])
  def addTask(task : Task) : SubBundle ={
    if (!job.tasks.contains(task))
      throw new RuntimeException(s"SubBundle>$node cannot add $task to the SubBundle $job")
    new SubBundle(stap, node, job, add(task, tasks))
  }

  /**
    * Remove a task
    */
  @throws(classOf[RuntimeException])
  def removeTask(task : Task) : SubBundle ={
    if (!job.tasks.contains(task))
      throw new RuntimeException(s"SubBundle>$node cannot remove $task to the SubBundle $job")
    new SubBundle(stap, node, job, remove(task, tasks))
  }

  /**
    * Add a task to a list of tasks
    * sorted by on the local cost of tasks
    */
  @throws(classOf[RuntimeException])
  def add(task : Task, list : List[Task]): List[Task] = {
    if (list.isEmpty) return List(task)
    if (task == list.head)
      throw new RuntimeException(s"SubBundle>$node already has $task in the SubBundle $job")
    if (
      stap.cost(task, node) > stap.cost(list.head, node) ||
        ((stap.cost(task, node) ~= stap.cost(list.head, node)) && list.head.name < task.name)
    ) list.head :: add(task, list.tail)
    else task :: list
  }

  /**
    * Remove a task from a list of tasks
    */
  @throws(classOf[RuntimeException])
  def remove(task : Task, list : List[Task]): List[Task] = {
    if (!list.contains(task))
      throw new RuntimeException(s"SubBundle>$node cannot remove $task from the SubBundle $job which does not contain it")
    list.filterNot(_ == task)
  }
}

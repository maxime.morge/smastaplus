// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021
package org.smastaplus

/**
  * Positions are float number
  * @note It is worth noticing that the position
  *       is floating point to allow insertion
  */
package object deputation {
  type Position = Double
}

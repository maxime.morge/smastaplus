// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021
package org.smastaplus.deputation

import org.smastaplus.utils.MathUtils._

/**
  * Class representing a assignment, i.e a node agent and a position
  */
class VariableAssignment(val nodeAgent: NodeAgent,
                         val position: Position) extends Ordered[VariableAssignment] {
    override def toString : String = s"($nodeAgent,$position)"

    /**
      * Returns true if that variable assignment represents the same node agent and the same position
      */
    override def equals(that: Any): Boolean =
        that match {
            case that: VariableAssignment => that.canEqual(this) &&
              this.nodeAgent == that.nodeAgent &&
              (this.position ~= that.position)
            case _ => false
        }

    def canEqual(a: Any): Boolean = a.isInstanceOf[VariableAssignment]

    /**
      * Returns 0 if this and that are the same, negative if this < that, and positive otherwise
      * VariableAssignment are sorted with their node id  and the position
      */
    def compare(that: VariableAssignment): Int = {
        val comparisonNode = this.nodeAgent.compare(that.nodeAgent)
        if (comparisonNode !=0 ) return comparisonNode
        val comparisonPosition = this.position - that.position
        if (comparisonPosition ~> 0.0) return 1
        if (comparisonPosition ~< 0.0) return -1
        0
    }
}

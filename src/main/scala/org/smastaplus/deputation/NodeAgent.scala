// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021
package org.smastaplus.deputation

import org.smastaplus.utils.MathUtils._
import org.smastaplus.core.{Task, ComputingNode}

/**
  * Class representing a node agent
  *
  * @param node  node object associated to the node agent
  * @param costs the cost of all the tasks for the node
  * @param initialTasks the sorted tasks initially assigned to the node
  */
class NodeAgent(val node: ComputingNode,
                val costs: Map[Task, Position],
                private val initialTasks: List[Task]) extends Ordered[NodeAgent] {

  /**
    * The bundle of the node agent is represented a map (task-> target)
    */
  var bundle: Map[Task, Position] = Map()
  /**
    * The bundle is initially sorted according to the array
    */
  for (i <- initialTasks.indices) bundle = bundle + (initialTasks(i) -> (i + 1))

  override def toString: String = node.name

  /**
    * Returns true if that node agent represents the same node as this node agent
    */
  override def equals(that: Any): Boolean =
    that match {
      case that: NodeAgent => that.canEqual(this) && this.node == that.node
      case _ => false
    }

  def canEqual(a: Any): Boolean = a.isInstanceOf[NodeAgent]

  /**
    * Returns 0 if this and that are the same, negative if this < that, and positive otherwise
    * Node agents are sorted with their node id
    */
  def compare(that: NodeAgent): Int = this.node.compare(that.node)

  /**
    * Returns a copy of the node agent
    */
  def copy(): NodeAgent = new NodeAgent(node, costs, sortedTasks())

  /**
    * Returns the cost of a task for this node
    */
  def cost(task: Task): Double = costs(task)

  /**
    * Returns the sorted tasks
    */
  def sortedTasks(): List[Task] = bundle.toSeq.sortWith(_._2 < _._2).map(_._1).toList // A REVOIR ?

  /**
    * Returns the position of a task
    */
  @throws(classOf[RuntimeException])
  def positionOfTask(task: Task): Position = {
    if (!bundle.contains(task))
      throw new RuntimeException(s"ERROR: The node agent ${node.name} is not assigned to task $task")
    bundle(task)
  }

  /**
    * Remove a task
    */
  def removeTask(task: Task): Unit = {
    bundle = bundle - task
  }

  /**
    * Add a task at a certain target
    */
  def addTask(task: Task, position: Position): Unit = {
    bundle = bundle + (task -> position)
  }

  /**
    * Update the target of a task
    */
  def updatePositionOfTask(task: Task, position: Position): Unit = {
    bundle = bundle + (task -> position)
  }

  /**
    * Returns the number of tasks after a given task in the bundle
    */
  def nbTasksAfter(task: Task) : Int = sortedTasks().dropWhile(_ != task ).tail.length

  /**
    * Returns the completion time of a task for the node
    */
  @throws(classOf[RuntimeException])
  def completionTime(task: Task): Double = {
    if (!bundle.contains(task))
      throw new RuntimeException(s"ERROR: The node agent ${node.name} is not assigned to task $task")
    var completionTime = 0.0
    for (t <- bundle.keys) {
      if (bundle(t) < bundle(task)) {
        completionTime += cost(t)
      }
    }
    completionTime += cost(task)
    completionTime
  }

  /**
    * Returns a string representation of the bundle
    */
  def describeBundle: String = s"$this : " + sortedTasks().mkString("[", ", ", "]")


  /**
    * Returns a string representation of the costs
    */
  def describeCosts: String = s"$this : " + sortedTasks().map(task => s"c($task)=${cost(task)}").mkString("\t")

  /**
    * Returns a string representation of the completion times
    */
  def describeCompletionTimes: String = s"$this: " + sortedTasks().map(task => s"C($task)=${completionTime(task)}").mkString("\t")

  /**
    * Returns a new position which immediately precedes the target one on the node
    */
  def immediatelyPrecedingPosition(target: Position) : Position = {
    // If the target is the first position, returns a previous one
    val firstPosition = bundle.values.min
    if (target ~= firstPosition) return target / 2.0
    // Otherwise returns a position between the target and the immediately preceding one
    val previousPosition = bundle.values.filter(_ ~< target).max
    (previousPosition + target) / 2
  }

  /**
    * Returns a new position from the original position beyond the target one
    */
  @throws(classOf[RuntimeException])
  def justBeyond(original: Position, target: Position): Position = {
    if (original ~= target) throw new RuntimeException(s"ERROR: The position cannot be moved beyond itself")
    if (original ~< target) { // If the target is after the original position
      // If the target is the last position, then returns the incrementation of the target
      if (target ~= bundle.values.max) return target + 1.0
      // Otherwise returns a position between the target and the immediately succeeding one
      val nextPosition = bundle.values.filter(_ ~> target).min
      return (nextPosition + target) / 2.0
    }
    // Otherwise the target is before the original position
    // If the target is the first position, then returns a position before the target
    if (target ~= bundle.values.min) return target / 2.0
    // Otherwise returns a position between the target and the immediately preceding one
    val previousPosition = bundle.values.filter(_ ~< target).max
    (previousPosition + target) / 2.0
  }
}
// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021
package org.smastaplus.deputation

import org.smastaplus.core.{Task, NoTask, Job}
import org.smastaplus.utils.MathUtils._

import scala.collection.SortedSet

/**
  * Class representing a Job Agent
  * @param job represented by the agent
  */
class JobAgent(val job: Job) extends Ordered[JobAgent] {
  val debug = false

  /**
    * The variableAssignments of the job agent is represented a map (taskId, value)
    */
  var variableAssignments: Map[Task, VariableAssignment] = Map()

  override def toString: String = job.name

  /**
    * Returns true if that job agent represents the same job as this job agent
    */
  override def equals(that: Any): Boolean =
    that match {
      case that: JobAgent => that.canEqual(this) && this.job == that.job
      case _ => false
    }

  def canEqual(a: Any): Boolean = a.isInstanceOf[JobAgent]

  /**
    * Returns 0 if this and that are the same, negative if this < that, and positive otherwise
    * Job agents are sorted with their job id
    */
  def compare(that: JobAgent): Int = this.job.compare(that.job)

  /**
    * Returns a copy of the job agent
    */
  def copy(): JobAgent = new JobAgent(job)

  /**
    * Returns a string representation of the tasks belonging to the job
    */
  def describeTasks: String = {
    var str = "[ "
    for (task <- job.tasks) {
      str = str + task + " "
    }
    str = str + "]"
    str
  }

  /**
    * Return the variable assignments where the task variable has changed
    */
  def assignVariable(task: Task, variableAssignment: VariableAssignment): Map[Task, VariableAssignment] = {
    variableAssignments = variableAssignments + (task -> variableAssignment)
    variableAssignments
  }

  /**
    * Returns a string representation of variableAssignments
    */
  def describeVariables: String = s"$this : " + variableAssignments.foldLeft("")((s, kv) => s + s"${kv._1}=${kv._2} \t")

  /**
    * Returns the last task for the job performed by the node agent
    * eventually NoTask if the node is not assigned to a task for this job
    */
  def last(node: NodeAgent): Task = {
    var maxPos: Position = 0.0
    var lastTask: Task = NoTask
    for ((task, pos) <- node.bundle) {
      if (variableAssignments.contains(task)) {
        if (maxPos < pos) {
          maxPos = pos
          lastTask = task
        }
      }
    }
    lastTask
  }

  /**
    * Returns the completion time of the job according to a set of agents
    */
  def completionTimeOfJob(nodes: SortedSet[NodeAgent]): Double = {
    var maxCompletionTime = 0.0
    for (node <- nodes) { // For each node
      val lastTask = last(node)
      if (!lastTask.equals(NoTask)) { // If the node is assigned to a task of the job
        val completionTimeLastTask = node.completionTime(lastTask)
        if (completionTimeLastTask > maxCompletionTime) maxCompletionTime = completionTimeLastTask
      }
    }
    maxCompletionTime
  }

  /**
    * Returns the best assignment reassignment (taskId, value, payoff) for the job
    * according to the heuristic (or not)
    */
  def selectBestReassignment(deputation: Deputation, heuristic: Boolean): (Task, VariableAssignment, Double) = {
    var bestReassignment = new VariableAssignment(deputation.nodeAgents.head, 0.0)
    var bestPayoff = Double.MinValue
    var bestTask: Task = NoTask
    for (task <- job.tasks) {
      val variableAssignment = variableAssignments(task)
      if (debug) println(s"Task, assignment: $task, $variableAssignment")
      val potentialReassignments = deputation.potentialReassignments(variableAssignment)
      for (reassignment <- potentialReassignments) {
        val payoff = deputation.payoff(task, reassignment)
        if (debug) println(s"Reassignment: $reassignment $payoff")
        //if equality with an other task, applying the rule of decision
        if ((payoff ~= bestPayoff) && !task.equals(bestTask)) {
          if (deputation.breakTieTasks(bestTask, task, heuristic).equals(task)){
            bestTask = task
            bestPayoff = payoff
            bestReassignment = reassignment
          }
        }
        else if (payoff > bestPayoff) {
          bestTask = task
          bestPayoff = payoff
          bestReassignment = reassignment
        }
      }
    }
    (bestTask, bestReassignment, bestPayoff)
  }
}

// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.utils

import scala.collection.SortedSet
import java.util.concurrent.TimeUnit
import scala.annotation.unused

/**
  * Compare floating-point numbers in Scala
  *
  */
object MathUtils {

  /**
    * Implicit class for classical list functions
    */
  implicit class Count[T](list: List[T]) {
    def count(n: T): Int = list.count(_ == n)
  }

  /**
    * Implicit class for classical mathematical functions
    */
  implicit class MathUtils(x: Double) {
    private val precision = 0.000001

    /**
      * Returns true if x and y are equals according to an implicit precision parameter
      */
    def ~=(y: Double): Boolean = {
      if ((x - y).abs <= precision) true else false
    }

    /**
      * Returns true if x is greater than y according to an implicit precision parameter
      */
    def ~>(y: Double): Boolean = {
      if (x - y > precision) true else false
    }

    /**
      * Returns true if y is greater than x according to an implicit precision parameter
      */
    def ~<(y: Double): Boolean = {
      if (y - x  > precision) true else false
    }

    /**
      * Returns true if x is greater or equal than y according to an implicit precision parameter
      */
    @unused
    def ~>=(y: Double): Boolean =  (x~>y) || (x~=y)

    /**
      * Returns true if y is greater than x according to an implicit precision parameter
      */
    def ~<=(y: Double): Boolean =  (x~<y) || (x~=y)
  }
}

/**
  * Random weight in Scala
  *
  */
object RandomUtils {

  val r: scala.util.Random = scala.util.Random // For reproducible XP new scala.util.Random(42)

  /**
    * Returns true with a probability p in [0;1]
    */
  def randomBoolean(p: Double): Boolean = {
    if (p < 0.0 || p > 1) throw new RuntimeException(s"The probability $p must be in [0 ; 1]")
    if (math.random() < p) return true
    false
  }

  /**
    * Returns a pseudo-randomly generated Double in ]0;1]
    */
  def strictPositiveWeight(): Double = {
    val number = r.nextDouble() // in [0.0;1.0[
    1.0 - number
  }

  /**
    * Returns the next pseudorandom, normally distributed
    *  double value with mean 0.0 and standard deviation 1.0
    */
  @unused
  def nextGaussian(): Double = {
    r.nextGaussian()
  }

  /**
    * Returns a pseudo-randomly generated Double in  [-1.0;1.0[
    */
  def weight(): Double = {
    val number = r.nextDouble() // in [0.0;1.0[
    number * 2 - 1
  }

  /**
   * Returns a shuffle list
   */
  def shuffle[T](s: List[T]): List[T] = {
    r.shuffle(s)
  }

  /**
   * Returns a shuffle list
   */
  def shuffle[T](s: SortedSet[T]): List[T] = {
    r.shuffle(s.toList)
  }


  /**
    * Returns a random element in a non-empty list
    */
  def random[T](s: Iterator[T]): T = {
    val n = r.nextInt(s.size)
    s.iterator.drop(n).next()
  }

  /**
    * Returns a random element in a non-empty set
    */
  def random[T](s: Set[T]): T = {
    val n = r.nextInt(s.size)
    s.iterator.drop(n).next()
  }

  /**
    * Returns a random element in a non-empty set
    */
  def random[T](s: SortedSet[T]): T = {
    val n = r.nextInt(s.size)
    s.iterator.drop(n).next()
  }

  /**
    * Returns a pseudo-randomly generated Double in [min, max]
    */
  @unused
  def randomDouble(min: Int, max: Int): Double = {
    (min + util.Random.nextInt((max - min) + 1)).toDouble
  }

  /**
    * Returns a pseudo-randomly generated Double in [min, max]
    */
  def random(min: Int, max: Int): Int = {
    min + util.Random.nextInt((max - min) + 1)
  }

  /*
  * Returns  a pseudo-randomly generated subset of n elm
   */
  def pick[T](s: SortedSet[T], n: Int): Set[T] = r.shuffle(s.toList).take(n).toSet

}

/**
  * Matrix in Scala
  *
  */
object Matrix {

  /**
    * Print
    *
    * @param matrix is an array of array
    * @tparam T type of content
    * @return string representation
    **/
  def show[T](matrix: Array[Array[T]]): String = matrix.map(_.mkString("[", ", ", "]")).mkString("\n")

  /**
    * Print
    *
    * @tparam T type of content
    * @param f function
    * @param L line number
    * @param C column number
    * @return string representation
    **/
  def show[T](f: (Integer, Integer) => T, L: Integer, C: Integer): String = {
    (for (i <- 0 until L) yield {
      (for (j <- 0 until C) yield f(i, j).toString).mkString("[", ", ", "]")
    }).mkString("[\n", ",\n", "]\n")
  }
}

/**
  * List in Scala
  */
object MyList {
  def insert[T](list: List[T], i: Int, value: T): List[T] = list match {
    case head :: tail if i > 0 => head :: insert(tail, i - 1, value)
    case _ => value :: list
  }
}

/**
  * Time in Scala
  */
object MyTime {
  def show(nanoseconds: Long): String = {
    s"${TimeUnit.NANOSECONDS.toHours(nanoseconds)}h " +
      s"${TimeUnit.NANOSECONDS.toMinutes(nanoseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(nanoseconds))}min " +
      s"${TimeUnit.NANOSECONDS.toSeconds(nanoseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(nanoseconds))}sec " +
      s"${TimeUnit.NANOSECONDS.toMillis(nanoseconds) - TimeUnit.SECONDS.toMillis(TimeUnit.NANOSECONDS.toSeconds(nanoseconds))}ms " +
      s"${TimeUnit.NANOSECONDS.toNanos(nanoseconds) - TimeUnit.MILLISECONDS.toNanos(TimeUnit.NANOSECONDS.toMillis(nanoseconds))}ns "
  }
}

/**
  * Statistical tools
  */
object Stat {

  /**
    * Returns the mean of a random variable
    */
  def mean(values: List[Double]): Double = values.sum / values.length

  /**
    * Returns the variance of a random variable with a Gaussian distribution (i.e. normally distributed)
    *
    * @param values of the random variable
    */
  private def variance(values: List[Double]): Double = {
    val mean: Double = Stat.mean(values)
    values.map(a => math.pow(a - mean, 2)).sum / values.length
  }

  /**
    * Returns the mean and the variance of a random variable with a Gaussian distribution (i.e. normally distributed)
    *
    * @param values of the random variable
    */
  private def normal(values: List[Double]): (Double, Double) = (mean(values), variance(values))


  /**
    * Returns the statistic t for Welch's t-test
    */
  @unused
  def statistic(values1: List[Double], values2: List[Double]): Double = {
    val (mean1, var1) = normal(values1)
    val (mean2, var2) = normal(values2)
    (mean1 - mean2) / math.sqrt(var1 / values1.length + var2 / values2.length)
  }

  /**
   * Returns the first, the second (median) and the third quartile of data
   */
  def quartiles(data: List[Double]): (Double, Double, Double) = {
    val sortedData = data.sortWith(_ < _)
    val dataSize = data.size
    (sortedData(dataSize / 4), sortedData(dataSize / 2), sortedData(dataSize * 3 / 4))
  }

}
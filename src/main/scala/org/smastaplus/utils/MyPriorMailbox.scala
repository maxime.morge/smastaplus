// Copyright (C) Maxime MORGE 2023
package org.smastaplus.utils

import org.smastaplus.consumer.deal.mas.Done

import akka.actor.ActorSystem
import akka.dispatch.{PriorityGenerator, UnboundedPriorityMailbox}
import com.typesafe.config.Config

/**
 * Mailbox for actor with priority
 */
class MyPriorMailbox(settings: ActorSystem.Settings, config: Config) extends UnboundedPriorityMailbox(
  PriorityGenerator {
    // The messages Done from the worker to the manager have priority
    case Done(_)   => 0
    // Other messages have no priority
    case _  => 1
  }
)
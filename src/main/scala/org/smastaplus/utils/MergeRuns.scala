// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2023
package org.smastaplus.utils

import scala.io.{BufferedSource, Source}
import java.io._

/**
 * Merge the outcome of runs for different configurations/experiments
 */
class MergeRuns(val inputFilenames: List[String],
                val outputFilename : String) {

    val debug = false
    // The list of configurations
    var configurations : List[String] = List[String]()
    // The list of metric
    var metrics : List[String] = List[String]()

    /**
     * Parse the outcomes of different runs in a file and
     * returns a map which contains the list of values for each metric
     */
    private def parseRuns(filename: String) : Map[String, List[Double]] = {
        val bufferedSource : BufferedSource = Source.fromFile(filename)
        val lines = bufferedSource.getLines()
        metrics = lines.next().split(",").toList.drop(4)
        val nbMetrics = metrics.size
        var outcomes = Map[String, List[Double]]()
        for (metric <- metrics) {
            outcomes = outcomes + (metric -> List[Double]())
        }
        var line = ""
        while (lines.hasNext) {
            line = lines.next()
            if (debug) println(s"Parse line: $line")
            val data = line.split(',').toList.drop(4)
            if (debug) println(s"Nb metrics: $nbMetrics")
            for (i <- 0 until nbMetrics) {
                if (debug) println(s"Metric $i: ${metrics(i)}")
                val updatedResult = outcomes(metrics(i)) :+ data(i).toDouble
                outcomes = outcomes + (metrics(i) -> updatedResult)
            }
        }
        val configuration = line.split(',').slice(0,4).mkString(",")
        configurations = configurations :+ configuration
        outcomes
    }

    /**
     * Returns the line which aggregates the values of metrics for all the runs of the same experiment/configuration
     */
    private def aggregate(outcomes: Map[String, List[Double]]): String = {
        var line = ""
        for (metric <- metrics) {
            val q = Stat.quartiles(outcomes(metric))
            line += "," + q._1 + "," + q._2 + "," + q._3
        }
        line + "\n"
    }

    /**
     * Return the head line which contains the names of metrics
     * @return
     */
    def header() : String = {
        var head = "m,l,n,randomGenerationRule"
        for (metric <- metrics) {
            head += ","+metric+"Open,"+metric+"Mean,"+metric+"Close"
        }
        head + "\n"
    }

    /**
     * Parse the files of the different runs to generate a single file
     */
    private def merge() : Unit = {
        var lines = List[String]()
        for (file <- inputFilenames) {
            if (debug) println(s"Parse file $file")
            val outcome = parseRuns(file)
            lines = lines :+ aggregate(outcome)
        }
        var outcome = header()
        val nbFiles = inputFilenames.size
        for (i <- 0 until nbFiles) {
            outcome += configurations(i) + lines(i)
        }
        val writer = new PrintWriter(new File(outputFilename))
        writer.write(outcome)
        writer.flush()
        writer.close()
    }

}
/**
 * Companion object
 */
private object MergeRuns extends App{
    private val directoryName =  "experiments/consumer/data/"

    private var inputFilenames = List.apply(
        "consumption4jobs8nodes5staps5allocations040-320tasksDetailed8-40.csv",
        "consumption4jobs8nodes5staps5allocations040-320tasksDetailed8-80.csv",
        "consumption4jobs8nodes5staps5allocations040-320tasksDetailed8-120.csv",
        "consumption4jobs8nodes5staps5allocations040-320tasksDetailed8-160.csv",
        "consumption4jobs8nodes5staps5allocations040-320tasksDetailed8-200.csv",
        "consumption4jobs8nodes5staps5allocations040-320tasksDetailed8-240.csv",
        "consumption4jobs8nodes5staps5allocations040-320tasksDetailed8-280.csv",
        "consumption4jobs8nodes5staps5allocations040-320tasksDetailed8-320.csv",
    )
      .map(f => directoryName + f)
    var outputFilename = "consumption4jobs8nodes5staps5allocations040-320tasks.csv"
    new MergeRuns(inputFilenames, directoryName + outputFilename).merge()

    inputFilenames = List.apply(
        "consumption4jobs8nodes5staps5allocationsFromStable040-320tasksDetailed8-40.csv",
        "consumption4jobs8nodes5staps5allocationsFromStable040-320tasksDetailed8-80.csv",
        "consumption4jobs8nodes5staps5allocationsFromStable040-320tasksDetailed8-120.csv",
        "consumption4jobs8nodes5staps5allocationsFromStable040-320tasksDetailed8-160.csv",
        "consumption4jobs8nodes5staps5allocationsFromStable040-320tasksDetailed8-200.csv",
        "consumption4jobs8nodes5staps5allocationsFromStable040-320tasksDetailed8-240.csv",
        "consumption4jobs8nodes5staps5allocationsFromStable040-320tasksDetailed8-280.csv",
        "consumption4jobs8nodes5staps5allocationsFromStable040-320tasksDetailed8-320.csv",
    )
      .map(f => directoryName + f)
    outputFilename = "consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.csv"
    new MergeRuns(inputFilenames, directoryName + outputFilename).merge()

    inputFilenames = List.apply(
        "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasksDetailed8-40.csv",
        "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasksDetailed8-80.csv",
        "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasksDetailed8-120.csv",
        "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasksDetailed8-160.csv",
        "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasksDetailed8-200.csv",
        "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasksDetailed8-240.csv",
        "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasksDetailed8-280.csv",
        "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasksDetailed8-320.csv",
    )
        .map(f => directoryName + f)
    outputFilename = "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks.csv"
    new MergeRuns(inputFilenames, directoryName + outputFilename).merge()

    /*
    inputFilenames = List.apply(
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed8-120.csv",
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed9-120.csv",
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed10-120.csv",
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed11-120.csv",
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed12-120.csv",
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed13-120.csv",
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed14-120.csv",
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed15-120.csv",
        "consumption4jobs5staps5allocationsByNodes08-16nodesDetailed16-120.csv"
    )
      .map(f => directoryName + f)
    outputFilename = "consumption4jobs5staps5allocationsByNodes.csv"
    new MergeRuns(inputFilenames, directoryName + outputFilename).merge()
    */
}

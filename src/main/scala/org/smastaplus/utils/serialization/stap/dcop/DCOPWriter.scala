// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.utils.serialization.stap.dcop

import org.smastaplus.core._
import org.smastaplus.utils.serialization.stap.STAPWrapper

import java.io.{BufferedWriter, File, FileWriter}

/**
  * Write a STAP as a DCOP problem in a yaml file
  * @param fileName of the text file
  * @param stap instance to be written
  * @param allocation for the STAP problem
  * @param rule to be optimized
  */
class DCOPWriter(fileName: String, stap: STAP, allocation: Allocation, rule: SocialRule)
  extends STAPWrapper(fileName, stap, rule){
  val debug = false

  val file = new File(fileName)
  val bw = new BufferedWriter(new FileWriter(file))

  private val weightHighConstraints: Int = Int.MaxValue

  /**
  * Writes the head of the YAML file
  */
  def head() : Unit = bw.write(
    s"name: 'STAP instance with ${stap.m} nodes, ${stap.n} tasks and ${stap.l} jobs'\n" +
    "description: |\n" +
    s"  nodes: ${stap.ds.computingNodes}\n" +
    s"  tasks: ${stap.tasks}\n" +
    s"  jobs: ${stap.jobs}\n\n" +
    s"# Objective\n" +
    s"objective: min\n\n" +
    s"# Domain\n" +
    s"domains:\n" +
    s"  presence:\n" +
    s"    values: [0, 1]\n" +
    s"    type: non_semantic\n\n"
  )

  /**
    * Writes the variables
    */
  def variables() : Unit = {
    bw.write(s"# ${stap.n*stap.n*stap.m} Variables\n")
    bw.write(s"# xiko, i in [1, ${stap.n}], k in [1, ${stap.n}], o in [1, ${stap.m}]\n")
    bw.write("variables:\n")
    for (i <- 1 to stap.n; k <- 1 to stap.n; o <- 1 to stap.m) {
      bw.write(s"  x$i$k$o:\n")
      bw.write("      domain: presence\n")
    }
  }

  /**
    * Writes the variables
    * with initial values iin accordance with an allocation
    */
  def variablesWithInitialValues() : Unit = {
    bw.write(s"# ${stap.n*stap.n*stap.m} Variables\n")
    bw.write(s"# xiko, i in [1, ${stap.n}], k in [1, ${stap.n}], o in [1, ${stap.m}]\n")
    bw.write("variables:\n")
    for (i <- 1 to stap.n; k <- 1 to stap.n; o <- 1 to stap.m) {
      bw.write(s"  x$i$k$o:\n")
      if (debug) print(s"x$i$k$o : ")
      if (debug) println(s"${allocation.bundle(node(o-1)).size}")
      bw.write("      domain: presence\n")
      val initialValue = if (allocation.bundle(node(o-1)).size >= k  &&
        allocation.bundle(node(o-1))(k-1) == task(i-1)) 1
        else 0
      bw.write(s"      initial_value: $initialValue\n")
    }
  }

  /**
    * Writes the constraints
    */
  def constraints() : Unit = {
    bw.write("\n# Constraints\n")
    bw.write("constraints:\n")
    bw.write("# The hard constraints\n")
    bw.write(s"  # ${stap.n} constraints to assign each task to exactly 1 position on a single node\n")
    for (i <- 1 to stap.n) {
      bw.write(s"  constraintPosition4Task$i:\n")
      bw.write("    type: intention\n")
      bw.write("    function: 0 if(")
      for (o <- 1 to stap.m; k <- 1 to stap.n){
        bw.write(s"x$i$k$o ")
        if (o != stap.m || k != stap.n ) bw.write("+ ")
      }
      bw.write(s" == 1) else $weightHighConstraints\n")
    }
    bw.write(s"  #  ${stap.n * stap.m} constraints to assign each task to at most one position on a single node\n")
    for (k <- 1 to stap.n; o <- 1 to stap.m) {
      bw.write(s"  constraintTask4Position$k$o:\n")
      bw.write("    type: intention\n")
      bw.write("    function: 0 if (")
        for (i <- 1 to stap.n) {
          bw.write(s"x$i$k$o ")
          if (i != stap.n) bw.write("+ ")
        }
        bw.write(s" <= 1) else $weightHighConstraints\n")
    }
  }

  /**
    * Writes the makespan
    */
  def makespan() : Unit = {
    bw.write("\n  # Objective function for the makespan\n")
    bw.write(s"  # max(o in [1, ${stap.m}]) sum(i in [1, ${stap.n}])  c[o][i] * (sum (k in [1, ${stap.n}])  x[i][k][o])\n")
    bw.write("  cost:\n")
    bw.write("    type: intention\n")
    bw.write("    function: ")
    bw.write("max([")
    for (o <- 1 to stap.m){
      for (i <- 1 to stap.n){
        bw.write(costArray(i-1)(o-1).toString +" * (")
        for (k <- 1 to stap.n){
          bw.write(s"x$i$k$o ")
          if (k != stap.n) bw.write("+ ")
        }
        bw.write(")")
        if (i != stap.n) bw.write(" + ")
      }
      if (o != stap.m) bw.write(" , ")
    }
    bw.write("])")
  }

  /**
    * Writes the globalFlowtime function
    */
  def globalFlowtime() : Unit = {
    bw.write("\n  # Objective function for the globalFlowtime\n")
    bw.write(s"  # sum(j in [1, ${stap.l}]) max(o in [1, ${stap.m}]) max(i in [1, ${stap.n}]) sum(k in [1, ${stap.n}], i2 in [1, ${stap.n}],  ${stap.n}>= m > k) y[i][j] * x[i][k][o] * x[i2][m][o] * C[i2][t]\n")
    bw.write("  cost:\n")
    bw.write("    type: intention\n")
    bw.write("    function: ")
    for (j <- 1 to stap.l){
      bw.write("max([")
      for (o <- 1 to stap.m){
        bw.write("max([")
        for (i <- 1 to stap.n if z(i-1)(j-1) == 1){
          bw.write("(")
          for (k <- 1 to stap.n; i2 <- 1 to stap.n; m <- k to stap.n){
            bw.write( costArray(i2-1)(o-1).toString +" * " + s"x$i$k$o" + " * " + s"x$i2$m$o" )
            if (k != stap.n || i2 != stap.n || m != stap.n ) bw.write(" + ")
          }
          bw.write(")")
          if (i != stap.n) bw.write(" , ")
        }
        bw.write("])")
        if (o != stap.m) bw.write(" , ")
      }
      bw.write("])")
      if (j != stap.l) bw.write(" + ")
    }
    bw.write("\n")
  }

  /**
    * Write the objective function
    */
  private def objective(): Unit = if (rule == Makespan) makespan() else globalFlowtime()

  /**
    * Write the agents
    */
  def agents() : Unit = {
    bw.write("\n# Agents\n")
    bw.write("agents: [")
    for (a <- 1 to stap.n * stap.n * stap.m){
      bw.write(s"a$a")
      if (a != stap.n * stap.n * stap.m) bw.write(", ")
    }
    bw.write("]\n")
  }

  /**
    * Write the whole YAML representation
    **/
  def write() : Unit = {
      head()
      variablesWithInitialValues()
     constraints()
      objective()
      agents()
      bw.close()
  }
}

/**
  * Companion object for testing
  */
object DCOPWriter extends App {
  import org.smastaplus.example.stap.ex1._
  import org.smastaplus.example.allocation.stap1.ex1Allocation._
  // Run with the command line
  // pydcop --output examples/dcop/balancer/ex1.json --timeout 10 solve --algo mgm2 examples/dcop/balancer/ex1.yml
  new DCOPWriter("examples/dcop/balancer/ex1.yml", stap = stap, a, GlobalFlowtime).write()
}
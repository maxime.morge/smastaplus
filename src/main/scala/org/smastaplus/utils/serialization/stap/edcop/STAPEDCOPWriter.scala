// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.utils.serialization.stap.edcop

import org.smastaplus.core._
import org.smastaplus.utils.serialization.stap.STAPWrapper

import java.io.{BufferedWriter, File, FileWriter}

/**
  * Write a STAP problem as an Extended DCOP problem in a yaml file
  * @param fileName of the text file
  * @param stap instance to be written
  * @param allocation for the STAP problem
  * @param rule to be optimized
  */
class STAPEDCOPWriter(fileName: String,
                      stap : STAP,
                      allocation: ExecutedAllocation,
                      rule : SocialRule)
  extends STAPWrapper(fileName, stap, rule){
  val debug = false

  val file = new File(fileName)
  val bw = new BufferedWriter(new FileWriter(file))

  val weightHighConstraints: Int = Int.MaxValue

  /**
    * Writes the problem in a text file for python parsing
    */
  def writeProblem() : Unit = {
      //defining name of the problem file
      val pathArray = fileName.split("/")
      pathArray(pathArray.length-1) = "allocation.txt"
      val problemFile = pathArray.mkString("/")

      val dimension = s"${stap.n}, ${stap.l}, ${stap.m}\n" // dimension of the problem

      var jobsToString = ""
      for (j <- stap.jobs){ // A line per job
        val tasksIDs : Set[String] = for {task <- j.tasks} yield task.toString.replaceAll("[^0-9]", "") // the numbers of the tasks in the job
        jobsToString = jobsToString + tasksIDs.mkString(", ")+"\n"
      }

      var costsToString = ""
      for (node <- stap.ds.computingNodes){ // A line per node
        val listOfTasks = stap.tasks.toList
        val costsForNode : List[Double] = for {task <- listOfTasks} yield stap.cost(task, node) // the cost of the tasks for all the nodes
        costsToString = costsToString + costsForNode.mkString(", ")+"\n"
      }

      val bufferedWriter = new BufferedWriter(new FileWriter(problemFile))
      bufferedWriter.write(
      dimension +
      jobsToString +
      costsToString
      )
      bufferedWriter.close()
  }

  /**
  * Writes the head of the YAML file
  */
  def head() : Unit = bw.write(
    s"name: 'STAP instance with ${stap.m} nodes, ${stap.n} tasks and ${stap.l} jobs'\n" +
    "description: |\n" +
    s"  nodes: ${stap.ds.computingNodes}\n" +
    s"  tasks: ${stap.tasks}\n" +
    s"  jobs: ${stap.jobs}\n\n" +
    s"# Objective\n" +
    s"objective: min\n\n" +
    s"# Domain\n" +
    s"domains:\n" +
    s"  position:\n" +
    s"    values: [0..${stap.n*stap.m-1}]\n" +
    s"    type: non_semantic\n\n"
  )

  /**
    * Writes the variables
    */
  def variables() : Unit = {
    bw.write(s"# ${stap.n} Variables\n")
    bw.write(s"# ti, i in [1, ${stap.n}]\n")
    bw.write("variables:\n")
    for (i <- 1 to stap.n) {
      bw.write(s"  t$i:\n")
      bw.write("      domain: position\n")
    }
  }

  /**
    * Returns the position of a task to a value for the eDCOP model
    */
  def position(task: Task) : Int = {
    val nodeId = allocation.node(task).toString.replaceAll("[^0-9]", "").toInt
    val position = allocation.bundle(task).indexOf(task)
    (nodeId-1) * stap.n + position
  }

  /**
    * Writes the variables
    * with initial values iin accordance with an allocation
    */
  def variablesWithInitialValues() : Unit = {
    bw.write(s"# ${stap.n} Variables\n")
    bw.write(s"# ti, i in [1, ${stap.n}]\n")
    bw.write("variables:\n")
    for (i <- 1 to stap.n) {
      bw.write(s"  t$i:\n")
      if (debug) print(s"t$i : ")
      bw.write("      domain: position\n")
      //initial value to determine
      val task = stap.tasks.toVector(i-1)
      val initialValue = position(task)
      bw.write(s"      initial_value: $initialValue\n")
    }
  }

  /**
    * Writes the constraints
    */
  def constraints() : Unit = {
    bw.write("\n# Constraints\n")
    bw.write("constraints:\n")
    bw.write(s"  #  Hard constraint to ensure there is at most one task per position\n")
    bw.write(s"  constraintOneTaskPerPosition :\n")
    bw.write("      type: intention\n")
    bw.write("      source: \"./edcop.py\"\n")
    val variablesToStrings : List[String] = (for{i <- 1 to stap.n} yield s"t$i").toList
    bw.write(s"      function: source.all_different(${variablesToStrings.mkString(",")})\n")
    val function = if (rule == GlobalFlowtime) "flowtime" else  "makespan"
    bw.write(s"\n  # Objective function for the $function\n")
    bw.write(s"  $function:\n")
    bw.write("      type: intention\n")
    bw.write("      source: \"./edcop.py\"\n")
    bw.write(s"      function: source.$function(${variablesToStrings.mkString(",")})\n")
  }

  /**
    * Write the agents
    */
  def agents() : Unit = {
    bw.write("\n# Agents\n")
    bw.write("agents: [")
    for (a <- 1 to stap.n){
      bw.write(s"a$a")
      if (a != stap.n) bw.write(", ")
    }
    bw.write("]\n")
  }

  /**
    * Write the whole YAML representation
    **/
  def write() : Unit = {
      writeProblem()
      head()
      variablesWithInitialValues()
      constraints()
      agents()
      bw.close()
  }
}

/**
  * Companion object for testing
  */
object STAPEDCOPWriter extends App {
  import org.smastaplus.example.stap.ex1._
  import org.smastaplus.example.allocation.stap1.ex1Allocation._
  // Run with the command line
  //pydcop --output examples/edcop/balancer/ex1.json --timeout 10 solve --algo mgm2 examples/edcop/balancer/ex1.yml
  new STAPEDCOPWriter("examples/edcop/balancer/ex1.yml",stap = stap, a, GlobalFlowtime).write()
}
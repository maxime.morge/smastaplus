//Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.utils.serialization.stap.dcop

import org.smastaplus.core._
import org.smastaplus.utils.MathUtils._
import org.smastaplus.utils.serialization.stap.STAPWrapper

import scala.io.Source

/**
  * Parse an allocation for a STAP in a JSON file
  * @param fileName of the text file
  * @param stap instance to be written
  * @param rule to be optimized
  */
class DCOPParser(fileName: String, stap : STAP, rule : SocialRule)
  extends STAPWrapper(fileName, stap, rule){
  val debug = false
  var allocation = new ExecutedAllocation(stap)

  /**
    *Parse the JSON file
    */
  def parse() : ExecutedAllocation = {
    val txtSource = Source.fromFile(fileName)
    val fileContent = txtSource.getLines().mkString
    txtSource.close()
    val data = ujson.read(fileContent)
    val assignment = data.obj("assignment")
    val cost = data.obj("cost").value
    if (debug) println(s"size: ${assignment.obj.size}")
    if (debug) println(s"keys: ${assignment.obj.keys}")
    if (debug) println(s"Objective function to minimize ($rule): " + cost)
    for (o <- 1 to stap.m) {
      val bundle: IndexedSeq[Task] = for (k <- 1 to stap.n; i <- 1 to stap.n
                                          if assignment(s"x$i$k$o").value.asInstanceOf[Double] ~= 1)
        yield taskArray(i-1)
      allocation = allocation.update(nodeArray(o-1), bundle.reverse.toList)
    }
    allocation
  }
}

/**
  * Companion object for testing
  */
object DCOPParser extends App {
  import org.smastaplus.example.stap.ex1._
  val allocation = new DCOPParser("examples/dcop/balancer/ex1.json",stap = stap, GlobalFlowtime).parse()
  println(s"Allocation:\n$allocation")
  println(s"GlobalFlowtime: ${allocation.globalFlowtime}")
}
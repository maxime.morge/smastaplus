// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.utils.serialization.stap.edcop

import org.smastaplus.core._
import org.smastaplus.utils.serialization.stap.STAPWrapper

import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
  * Parse an allocation for a STAP in a JSON file
  * representing an assignment for an Extended DCOP
  * @param fileName of the text file
  * @param stap is the problem instance
  * @param rule to be optimized
  */
class STAPEDCOPParser(fileName: String,
                      stap : STAP,
                      rule : SocialRule)
  extends STAPWrapper(fileName, stap, rule){
  val debug = false
  var allocation = new ExecutedAllocation(stap)

  /**
    * Returns the global position (node, position) according to the variable assignment
    */
  def value2position(value: Double) : (ComputingNode, Int) ={
    val nodeId = value.toInt / stap.n + 1
    val node = nodeArray(nodeId-1)
    val position = value.toInt % stap.n + 1
    if (debug) println(s"value=$value -> ($node, $position)")
    (node, position)
  }

  /**
    *Parse the JSON file
    */
  def parse() : ExecutedAllocation = {
    val txtSource = Source.fromFile(fileName)
    val fileContent = txtSource.getLines().mkString
    txtSource.close()
    val data = ujson.read(fileContent)
    val assignment = data.obj("assignment")
    val cost = data.obj("cost").value
    if (debug) println(s"size: ${assignment.obj.size}")
    if (debug) println(s"keys: ${assignment.obj.keys}")
    if (debug) println(s"Objective function to minimize ($rule): " + cost)

    var translatedAssignments : Map[Task, (ComputingNode, Int)] = Map[Task, (ComputingNode, Int)]()
    for (i <- 1 to stap.n){
      translatedAssignments = translatedAssignments + (task(i-1) -> value2position(assignment(s"t$i").value.asInstanceOf[Double]))
    }
    for (node <- nodeArray){
      val bundle = ListBuffer[Task]()
      for ((task, (nodeOfTask, _)) <- translatedAssignments){
        if (nodeOfTask.equals(node)){
          bundle += task
        }
      }
      val sortedBundle = bundle.toList.sortWith(translatedAssignments(_)._2 < translatedAssignments(_)._2)
      allocation = allocation.update(node, sortedBundle)
    }
    allocation
  }
}

/**
  * Companion object for testing
  */
object STAPEDCOPParser extends App {
  import org.smastaplus.example.stap.ex1._
  val rule: SocialRule = GlobalFlowtime
  val allocation = new STAPEDCOPParser("examples/edcop/balancer/ex1.json",stap = stap, rule).parse()
  println(s"Allocation:\n$allocation")
  if (rule == GlobalFlowtime) println(s"GlobalFlowtime: ${allocation.globalFlowtime}")
  else println(s"Makespan: ${allocation.makespan}")
}
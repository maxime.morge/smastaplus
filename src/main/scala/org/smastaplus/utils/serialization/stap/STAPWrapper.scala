// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.utils.serialization.stap

import org.smastaplus.core._

/**
  * Wrapper to parse/write a STAP instance/allocation as a file
  * @param fileName of the input/output
  * @param stap instance to be written/parsed
  * @param rule to be optimized
  */
class STAPWrapper(fileName: String,
                  stap : STAP,
                  rule : SocialRule) {

  val taskArray : Array[Task] = stap.tasks.toArray // tasks as an array
  val nodeArray : Array[ComputingNode] = stap.ds.computingNodes.toArray // nodes as array
  val jobArray : Array[Job] = stap.jobs.toArray // jobs as an array
  var costArray : Array[Array[Double]] = // costs as an array
    Array.tabulate(stap.n, stap.m)((t, n) => stap.cost(task(t), node(n)))
  var z: Array[Array[Int]] = // contains as an array
    Array.tabulate(stap.n, stap.l)((i, j) => if (job(j).tasks.contains(task(i))) 1 else 0)

  /**
    * Returns the ith task with 0 <= i < pb.n
    */
  def task(i : Int): Task = {
    if (i >= stap.n) throw new RuntimeException(s"Cannot returns the $i th task with ${stap.n} tasks")
    taskArray(i)
  }

  /**
    * Returns the ith node with 0 <= i < pb.m
    */
  def node(i : Int): ComputingNode = {
    if (i >= stap.m) throw new RuntimeException(s"Cannot returns the $i th node with ${stap.m} nodes")
    nodeArray(i)
  }

  /**
    * Returns the jth job with 0 <= j < pb.n
    */
  def job(j : Int): Job = {
    if (j >= stap.l) throw new RuntimeException(s"Cannot returns the $j th job with ${stap.l} jobs")
    jobArray(j)
  }
}


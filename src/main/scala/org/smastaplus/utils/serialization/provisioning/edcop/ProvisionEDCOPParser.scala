// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022
package org.smastaplus.utils.serialization.provisioning.edcop

import org.smastaplus.core._
import org.smastaplus.provisioning.{Configuration, ProvisioningProblem, QoS, QoSFlowtime}
import org.smastaplus.utils.MathUtils.MathUtils
import org.smastaplus.utils.serialization.provisioning.ProvisionWrapper

import scala.collection.SortedSet
import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
  * Parse an configuration for a provision problem in a JSON file
  * representing an assignment for an Extended DCOP
  * @param fileName of the text file
  * @param problem is the provisioning problem instance
  */
class ProvisionEDCOPParser(fileName: String, problem: ProvisioningProblem)
  extends ProvisionWrapper(fileName, problem){
  val debug = true
  var allocation = new ExecutedAllocation(problem.stap)
  var configuration = new Configuration(problem, problem.stap.ds.computingNodes, new ExecutedAllocation(problem.stap))

  /**
    * Returns the global position (node, position) according to the variable assignment
    */
  def value2position(value: Double) : (ComputingNode, Int) ={
    val nodeId = value.toInt / problem.stap.n + 1
    val node = nodeArray(nodeId-1)
    val position = value.toInt % problem.stap.n + 1
    if (debug) println(s"value=$value -> ($node, $position)")
    (node, position)
  }

  /**
    *Parse the JSON file
    */
  def parse() : Configuration = {
    val txtSource = Source.fromFile(fileName)
    val fileContent = txtSource.getLines().mkString
    txtSource.close()
    val data = ujson.read(fileContent)
    val assignment = data.obj("assignment")
    //val cost = data.obj("cost").value
    if (debug) println(s"size: ${assignment.obj.size}")
    if (debug) println(s"keys: ${assignment.obj.keys}")
    //if (debug) println(s"Objective function to minimize ($problem.rule): " + cost)

    var translatedAssignments : Map[Task, (ComputingNode, Int)] = Map[Task, (ComputingNode, Int)]()
    for (i <- 1 to problem.stap.n){
      translatedAssignments = translatedAssignments + (task(i-1) -> value2position(assignment(s"t$i").value.asInstanceOf[Double]))
    }
    for (node <- nodeArray){
      val bundle = ListBuffer[Task]()
      for ((task, (nodeOfTask, _)) <- translatedAssignments){
        if (nodeOfTask.equals(node)){
          bundle += task
        }
      }
      val sortedBundle = bundle.toList.sortWith(translatedAssignments(_)._2 < translatedAssignments(_)._2)
      allocation = allocation.update(node, sortedBundle)
    }
    var nodes = SortedSet[ComputingNode]()
    for (j <- 1 to problem.stap.m){
      if (assignment(s"n$j").value.asInstanceOf[Double] ~= 1)  nodes = nodes.union(Set(node(j-1)))
    }
    new Configuration(problem, nodes, allocation)
  }
}

/**
  * Companion object for testing
  */
object ProvisionEDCOPParser extends App {
  import org.smastaplus.example.provisioning.ex4Elasticity._
  val rule: QoS = QoSFlowtime
  val configuration = new ProvisionEDCOPParser("examples/edcop/scaler/ex1.json", problem).parse()
  println(s"Allocation:\n${configuration.allocation}")
  println(s"Nodes: ${configuration.nodes}")
  if (rule == QoSFlowtime) println(s"GlobalFlowtime: ${configuration.allocation.meanGlobalFlowtime}")
  else println(s"Makespan: ${configuration.allocation.makespan}")
}
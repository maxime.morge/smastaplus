// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.utils.serialization.provisioning

import org.smastaplus.core._
import org.smastaplus.provisioning.{ProvisioningProblem, QoS, QoSFlowtime, QoSMakespan}
import org.smastaplus.utils.serialization.core.DistributedSystemParser

import scala.collection.SortedSet
import scala.io.{BufferedSource, Source}

/**
  * Build a Provisioning from a text file
  * See [[org.smastaplus.utils.serialization.core.STAPParser]]
  * @param fileName of the STAP
  */
class ProvisioningProblemParser(val fileName: String) {
  val debug = false
  val parseDebug = false

  private var lineNumber = 0
  private var nbParsedJobs = 0
  private var ds : DistributedSystem = _
  private var problem : ProvisioningProblem = _
  private var n = 0 // number of tasks
  private var l = 0 // number of jobs
  var tasks: SortedSet[Task] = SortedSet[Task]()
  var jobs: SortedSet[Job] = SortedSet[Job]()
  private var rule : QoS = _
  private var (maxThreshold,  minThreshold) = (0.0, 0.0)
  private val dsParser : DistributedSystemParser = new DistributedSystemParser(fileName)

  /**
    * Returns a STAP by parsing file
    */
  def parse(): ProvisioningProblem = {
    val bufferedSource : BufferedSource = Source.fromFile(fileName)
    ds = dsParser.parseDS(bufferedSource)
    problem = parseProvisioningProblem(bufferedSource)
    bufferedSource.close()
    problem
  }

  /**
    * Returns a provisioning problem by parsing a bufferedSource after the STAP is setup
    */
  private def parseProvisioningProblem(bufferedSource : BufferedSource) : ProvisioningProblem = {
    for (line <- bufferedSource.getLines()) {
      lineNumber += 1
      if (debug || parseDebug) println(s"parseProvisioningProblem line $lineNumber: $line")
      if (line.startsWith("//")) { //Drop comment
        if (debug) println(s"parse $fileName line$lineNumber: comment $line")
      } else parseLine(line)
    }
    if (debug) println(s"$minThreshold, $maxThreshold, $rule")
    val problem = new ProvisioningProblem(new STAP(ds, jobs, tasks), maxThreshold,  minThreshold, rule )
    if (debug) println(problem)
    problem
  }

  /**
    * Parse a line
    *
    * @param line of the file to parse
    */
  def parseLine(line: String): Unit = {
    val couple = line.split(":").map(_.trim)
    if (couple.length != 2) throw new RuntimeException(s"ERROR parseLine $fileName line$lineNumber: comment $line")
    val (key, value) = (couple(0), couple(1))
    //Firstly, the size (m,n) should be setup as strict positive integer
    if (n == 0 || l == 0) parseSize(key, value)
    //Secondly, the entities should be setup as non-empty
    else {
      if (tasks.isEmpty || jobs.isEmpty) {
        parseEntities(key, value)
      }
      else if (tasks.nonEmpty && jobs.nonEmpty) {
        parseEntity(key, value)
      }
    }
  }

  /**
    * Parse the size of the MATA Problem (m,n,l)
    * @param key   of the line
    * @param value of the line
    */
  def parseSize(key: String, value: String): Unit = key match {
    case "n" => n = value.toInt
    case "l" => l = value.toInt
    case _ => throw new RuntimeException(s"ERROR parseSize $fileName L$lineNumber=$key, $value")
  }


  /**
    * Parse the entities (nodeAgents, tasks, jobs) of the provisioning problem
    * @param key   of the line
    * @param value of the line
    */
  def parseEntities(key: String, value: String): Unit = key match {
    case "tasks" => parseTasks(value)
    case "jobs" => parseJobs(value)
    case _ => throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $key")
  }

  /**
    * Parse the tasks
    * @param names e.g. a string "t1, t2"
    */
  def parseTasks(names: String): Unit = {
    val array: Array[String] = names.split(", ").map(_.trim)
    if (array.length != n) throw new RuntimeException(s"ERROR parseTasks $fileName line$lineNumber: the number of tasks  $n is wrong: ${array.length}")
    val pattern = """(\w+)\s?""".r
    array.foreach { str: String =>
      str match {
        case pattern(task) =>
          if (debug) println(s"parseTasks: $task")
          tasks = tasks.union(Set(new Task(task, Set[Resource]()))) // Wait for resources
          if (debug) println(s"Tasks = $tasks")
        case _ => throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $str")
      }
    }
  }

  /**
    * Parse the jobs
    * @param names e.g. a string "j1, j2, j3"
    */
  def parseJobs(names: String): Unit = {
    val array: Array[String] = names.split(", ").map(_.trim)
    array.foreach { str: String =>
      if (debug) println(s"parseJobs: $str")
      jobs = jobs.union(Set(new Job(str, tasks = Set[Task](),  ds.t0)))// Wait for release time  and tasks
    }
  }

  /**
    * Parse the entity (job, node)
    * @param key   of the line
    * @param value of the line
    */
  def parseEntity(key: String, value: String): Unit = {
    if (debug) println(s"parseEntity $key")
    if (tasks.map(n => n.name).contains(key))
      parseTask(key, value)
    else {
      if (nbParsedJobs < l)
        parseJob(key, value)
      else {
        key match {
          case "maxThreshold" =>
            if (debug) println(value)
            maxThreshold = value.toDouble
          case "minThreshold" =>
            if (debug) println(value)
            minThreshold = value.toDouble
          case "rule" =>
            if (debug) println(value)
            value match {
              case "flowtime" =>
                rule = QoSFlowtime
              case "makespan" =>
                rule = QoSMakespan
              case _ => throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $value")
            }
        }
      }
    }
  }

  /**
    * Parse the tasks, e.g. "t1: r1"
    */
  def parseTask(key: String, value: String): Unit = {
    if (debug) println(s"parseTask: $key $value")
    val task : Task = tasks.find(_.name == key) match {
      case Some(t) => t
      case None => throw new RuntimeException(s"No task $key found")
    }
    val array: Array[String] = value.split(", ").map(_.trim)
    array.foreach { str: String =>
      val resource = ds.resources.find(_.name == str) match {
        case Some(r) => r
        case None => throw new RuntimeException(s"No resource $key found")
      }
      task.resources += resource
    }
  }

  /**
    * Parse the job
    */
  def parseJob(key: String, value: String): Unit = {
    if (debug) println(s"parseJob $key")
    if (key == "releaseTime") {
      val couple: Array[String] = value.split(" ").map(_.trim)
      if (couple.length != 2) throw new RuntimeException(s"ERROR parse $fileName line$lineNumber: $value")
      val job = jobs.find(_.name == couple(0)) match {
        case Some(j) => j
        case None => throw new RuntimeException(s"No job $key found")
      }
      val releaseTime: Int = try {
        couple(1).toInt
      } catch {
        case _ : NumberFormatException => throw new RuntimeException(s"ERROR parseJob $fileName line$lineNumber: ${couple(1)}")
      }
      job.releaseTime = ds.t0.plusMillis(releaseTime)
      nbParsedJobs += 1
    } else {
      val job = jobs.find(_.name == key) match {
        case Some(t) => t
        case None => throw new RuntimeException(s"No task $key found")
      }
      val array: Array[String] = value.split(", ").map(_.trim)
      val pattern = """(\w+)\s?""".r
      array.foreach { str: String =>
        str match {
          case pattern(task) =>
            if (debug) println(s"parseTasks: $task")
            tasks.find(_.name == task) match {
              case Some(t) => job.tasks += t
              case None =>  throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $str")
            }
          case _ => throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $str")
        }
      }
    }
  }
}

/**
  * Test ProvisioningProblemParser
  */
object ProvisioningProblemParser extends App {
  var parser = new ProvisioningProblemParser("examples/provisioning/ex4Elasticity.txt")
  var pb = parser.parse() // parse first problem
  println(pb)
}






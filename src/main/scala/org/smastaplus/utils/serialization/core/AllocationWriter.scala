// Copyright (C) Maxime MORGE, Anne-Cécile CARON, 2020, 2021, 2023
package org.smastaplus.utils.serialization.core

import com.typesafe.config.{Config, ConfigFactory}
import org.smastaplus.core._
import org.smastaplus.example.allocation.stap1._
import org.smastaplus.example.allocation.stap2._
import org.smastaplus.example.allocation.stap4._
import org.smastaplus.example.allocation.stap4.jobRelease._
import org.smastaplus.example.allocation.stap4.swap._

import java.io.{BufferedWriter, File, FileWriter}

/**
 * Write an allocation in a text file
 * @param pathName of the text file
 * @param allocation to be written
 * @param mode "simple" => simple text serialization (default),
 *             "json" => Simple JSon serialization,
 *             "monitoring" => Complete JSon serialization, used for monitoring
 */

class AllocationWriter(pathName: String, allocation : Allocation, mode : String = "simple") {
  val file = new File(pathName)
  def write() : Unit = {
    val bw = new BufferedWriter(new FileWriter(file))
    if (mode == "json")
      bw.write(allocation.toJSON("basic"))
    else if (mode == "monitoring") {
      // write the initial allocation
      bw.write(allocation.toJSON("complete"))
      val config: Config = ConfigFactory.load()
      val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
        config.getString("path.monitoringDirectory")
      // write one file per node, in the trace directory
      allocation.stap.ds.computingNodes.foreach(
        n => {
          val fileName = monitoringDirectoryName + "/trace/" + n.name + ".json"
          val bbw = new BufferedWriter(new FileWriter(fileName))
          bbw.write(allocation.bundleToJSON(n))
        }
      )
    } else
      bw.write(allocation.toString)
    bw.close()
  }

}

/**
 * Test AllocationWriter
 */
object AllocationWriter extends App {
  // First STAP example
  new AllocationWriter("examples/allocation/stap1/ex1.txt", ex1Allocation.a).write()
  new AllocationWriter("examples/allocation/stap1/ex1Delegation.txt", ex1Delegation.a).write()
  new AllocationWriter("examples/allocation/stap1/ex1Delegation2.txt", ex1Delegation2.a).write()
  new AllocationWriter("examples/allocation/stap1/ex1Delegation3.txt", ex1Delegation3.a).write()
  new AllocationWriter("examples/allocation/stap1/ex1Delegation4.txt", ex1Delegation4.a).write()
  new AllocationWriter("examples/allocation/stap1/ex1lcjf.txt", ex1lcjf.a).write()
  // Second STAP example
  new AllocationWriter("examples/allocation/stap2/ex2.txt", ex2Allocation.a).write()
  new AllocationWriter("examples/allocation/stap2/ex2Delegation.txt", ex2Delegation.a).write()
  new AllocationWriter("examples/allocation/stap2/ex2lcjf.txt", ex2lcjf.a).write()
  new AllocationWriter("examples/allocation/stap2/ex2lcjfDelegation1.txt", ex2lcjfDelegation1.a).write()
  new AllocationWriter("examples/allocation/stap2/ex2lcjfDelegation2.txt", ex2lcjfDelegation2.a).write()
  new AllocationWriter("examples/allocation/stap2/ex2lcjfDelegation3.txt", ex2lcjfDelegation3.a).write()
  new AllocationWriter("examples/allocation/stap2/ex2lcjfDelegation4.txt", ex2lcjfDelegation4.a).write()
  new AllocationWriter("examples/allocation/stap2/ex2lcjfDelegationStrategy1.txt", ex2lcjfDelegationStrategy1.a).write()
  // Fourth STAP example
  new AllocationWriter("examples/allocation/stap4/ex4.txt", ex4Allocation.a).write()
  new AllocationWriter("examples/allocation/stap4/ex4Swap.txt", ex4Swap.a).write()
  // Fifth STAP example
  new AllocationWriter("examples/allocation/stap5/ex5Swap.txt", ex5Swap.a).write()
}
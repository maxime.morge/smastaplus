//Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.utils.serialization.core

import org.smastaplus.core._

import java.io.{BufferedWriter, File, FileWriter}

/**
  * Draw an allocation with the
  * [[https://d3js.org/ D3]]
  * @param pathName of the text file
  * @param allocation to be written
  * @param problem STAP instance
 *  @param maxWorkload which is represented, 0 by default if  maximum is dynamically fixed
  */
class AllocationDrawerD3(pathName: String,
                         allocation: ExecutedAllocation,
                         problem: STAP,
                         maxWorkload: Int = 0) {
  val file = new File(pathName)

  val htmlHead: String =
    """<!DOCTYPE html>
      |<html>
      |<meta charset="utf-8">
      |<head>
      |    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
      |    <script src="https://d3js.org/d3.v5.min.js"></script>
      |    <script src="../d3-save-svg.min.js"></script>
      |    <title>Allocation Example</title>
      |</head>
      |<body>
      |    <div id="charts">
      |        <svg width="800" height="500"></svg>
      |    </div>
      |    <input id="dl"
      |           name="downloadButton"
      |           type="button"
      |           value="Save as a SVG file" />
      |</body>
      |<script type="text/javascript">""".stripMargin

  val htmlTail: String =
    """</script></html>""".stripMargin

  private val d3Head: String =
    """
      |// Copyright (C) Maxime MORGE 2021
      |// Data
      |""".stripMargin

  private val d3Tail: String =
   s"""
      |// Color style
      |const jobColors = ["#e41a1c", "#377eb8", "#4daf4a", "#31047c"];// Red, blue, green, purple
      |
      |// The legend
      |function createChartLegend(mainDiv, tasks) {
      |    $$(mainDiv).before("<div id='Legend' style='margin-top:0; margin-bottom:0;'></div>");
      |    jobNames.forEach(function(d, i) {
      |        $$("#Legend").append(
      |            "<span style='display: inline-block; margin-right:10px;'>"
      |                + "<span style='background:"
      |                + jobColors[i] // colorCode
      |                + ";opacity: 0.6; width: 10px;height: 10px;display: inline-block;vertical-align: middle;'>&nbsp;</span>"
      |                + "<span style='padding-top: 0;font-family:Source Sans Pro, sans-serif;font-size: 13px;display: inline;'>"
      |                + d
      |                + "</span></span>");
      |    });
      |}
      |
      |
      |var mainDiv = "#charts";// Name of the div in HTML
      |var divName = "charts";
      |createChartLegend(mainDiv, tasks);
      |
      |costs.forEach(function(d) {
      |    d = type(d);
      |});
      |var layers = d3.stack()
      |    .keys(tasks)
      |    .offset(d3.stackOffsetDiverging)(costs);
      |
      |var svg = d3.select("svg"),
      |    margin = {
      |        top: 20,
      |        right: 30,
      |        bottom: 60,
      |        left: 60
      |    },
      |    width = +svg.attr("width"),
      |    height = +svg.attr("height");
      |
      |var x = d3.scaleBand()
      |    .rangeRound([margin.left, width - margin.right])
      |    .padding(0.1);
      |
      |x.domain(costs.map(function(d) {
      |    return d.node;
      |}))
      |
      |var y = d3.scaleLinear()
      |    .rangeRound([height - margin.bottom, margin.top]);
      |
      |y.domain([d3.min(layers, stackMin), ${if (maxWorkload == 0) "d3.max(layers, stackMax)" else maxWorkload}])
      |
      |function stackMin(layers) {
      |    return d3.min(layers, function(d) {
      |        return d[0];
      |    });
      |}
      |
      |function stackMax(layers) {
      |    return d3.max(layers, function(d) {
      |        return d[1];
      |    });
      |}
      |
      |var maing = svg.append("g")
      |    .selectAll("g")
      |    .data(layers);
      |
      |var g = maing.enter().append("g")
      |    .attr("fill", function(d,i) {
      |        return jobColors[jobs[i]-1]; // color of tasks
      |    });
      |
      |var rect = g.selectAll("rect")
      |    .data(function(d) {
      |        d.forEach(function(d1) {
      |            d1.key = d.key;
      |            return d1;
      |        });
      |        return d;
      |    })
      |    .enter().append("rect")
      |    .attr("data", function(d) {
      |        var data = {};
      |        data["key"] = d.key;
      |        data["value"] = d.data[d.key];
      |        var total = 0;
      |        tasks.map(function(d1) {
      |            total = total + d.data[d1]
      |        });
      |        data["total"] = total;
      |        return JSON.stringify(data);
      |    })
      |    .attr("width", x.bandwidth)
      |    .attr("x", function(d) {
      |        return x(d.data.node);
      |    })
      |    .attr("y", function(d) {
      |        return y(d[1]);
      |    })
      |    .attr("height", function(d) {
      |        return y(d[0]) - y(d[1]);
      |    })
      |    .style('opacity',0.6)
      |    .style("stroke", "black")
      |    .style("stroke-width", 1);
      |
      |var text = g.selectAll("text")
      |    .data(function(d) {
      |        d.forEach(function(d1) {
      |            d1.key = d.key;
      |            return d1;
      |        });
      |        return d;
      |    })
      |    .enter().append("text")
      |    .attr("data", function(d) {
      |        var data = {};
      |        data["key"] = d.key;
      |        data["value"] = d.data[d.key];
      |        var total = 0;
      |        tasks.map(function(d1) {
      |            total = total + d.data[d1]
      |        });
      |        data["total"] = total;
      |        return JSON.stringify(data);
      |    })
      |    .attr("class","text")
      |    .attr("x", function(d) {
      |        return x(d.data.node) + x.bandwidth()/2;
      |    })
      |    .attr("y", function(d) {
      |        return y(d[1]);
      |    })
      |    .style("text-anchor", "middle")
      |    .style("font-size", "12px")
      |    .style("fill", "black")
      |    .text(function(d) {
      |        if (d.data[d.key]!=0) return d.key;
      |        return "";
      |    });
      |
      |
      |// Interaction
      |// rect.on("mouseover", function() {
      |//     var currentEl = d3.select(this);
      |//     var fadeInSpeed = 120;
      |//     d3.select("#recttooltip_" + divName)
      |//         .transition()
      |//         .duration(fadeInSpeed)
      |//         .style("opacity", function() {
      |//             return 1;
      |//         });
      |//     d3.select("#recttooltip_" + divName).attr("transform", function(d) {
      |//         var mouseCoords = d3.mouse(this.parentNode);
      |//         var xCo = 0;
      |//         if (mouseCoords[0] + 10 >= width * 0.80) {
      |//             xCo = mouseCoords[0] - d3.selectAll("#recttooltipRect_" + divName)
      |//                                               .attr("width");
      |//         } else {
      |//             xCo = mouseCoords[0] + 10;
      |//         }
      |//         var x = xCo;
      |//         var yCo = 0;
      |//         if (mouseCoords[0] + 10 >= width * 0.80) {
      |//             yCo = mouseCoords[1] + 10;
      |//         } else {
      |//             yCo = mouseCoords[1];
      |//         }
      |//         var x = xCo;
      |//         var y = yCo;
      |//         return "translate(" + x + "," + y + ")";
      |//     });
      |//     //CBT:calculate tooltips text
      |//     var tooltipData = JSON.parse(currentEl.attr("data"));
      |//     var tooltipsText = "";
      |//     d3.selectAll("#recttooltipText_" + divName).text("");
      |//     var yPos = 0;
      |//     d3.selectAll("#recttooltipText_" + divName).append("tspan").attr("x", 0).attr("y", yPos * 10).attr("dy", "1.9em").text(tooltipData.key + ":  " + tooltipData.value);
      |//     yPos = yPos + 1;
      |//     d3.selectAll("#recttooltipText_" + divName).append("tspan").attr("x", 0).attr("y", yPos * 10).attr("dy", "1.9em").text("Workload" + ":  " + tooltipData.total);
      |//     //CBT:calculate width of the text based on characters
      |//     var dims = helpers.getDimensions("recttooltipText_" + divName);
      |//     d3.selectAll("#recttooltipText_" + divName + " tspan")
      |//         .attr("x", dims.w + 4);
      |
      |//     d3.selectAll("#recttooltipRect_" + divName)
      |//         .attr("width", dims.w + 10)
      |//         .attr("height", dims.h + 20);
      |
      |// });
      |
      |// rect.on("mousemove", function() {
      |//     var currentEl = d3.select(this);
      |//     currentEl.attr("r", 7);
      |//     d3.selectAll("#recttooltip_" + divName)
      |//         .attr("transform", function(d) {
      |//             var mouseCoords = d3.mouse(this.parentNode);
      |//             var xCo = 0;
      |//             if (mouseCoords[0] + 10 >= width * 0.80) {
      |//                 xCo = mouseCoords[0] - parseFloat(d3.selectAll("#recttooltipRect_" + divName)
      |//                                                   .attr("width"));
      |//             } else {
      |//                 xCo = mouseCoords[0] + 10;
      |//             }
      |//             var x = xCo;
      |//             var yCo = 0;
      |//             if (mouseCoords[0] + 10 >= width * 0.80) {
      |//                 yCo = mouseCoords[1] + 10;
      |//             } else {
      |//                 yCo = mouseCoords[1];
      |//             }
      |//             var x = xCo;
      |//             var y = yCo;
      |//             return "translate(" + x + "," + y + ")";
      |//         });
      |// });
      |// rect.on("mouseout", function() {
      |//     var currentEl = d3.select(this);
      |//     d3.select("#recttooltip_" + divName)
      |//         .style("opacity", function() {
      |//             return 0;
      |//         })
      |//         .attr("transform", function(d, i) {
      |//             // klutzy, but it accounts for tooltip padding which could push it onscreen
      |//             var x = -500;
      |//             var y = -500;
      |//             return "translate(" + x + "," + y + ")";
      |//         });
      |// });
      |
      |svg.append("g")
      |    .attr("transform", "translate(0," + y(0) + ")")
      |    .call(d3.axisBottom(x))
      |    .append("text")
      |    .attr("x", width / 2)
      |    .attr("y", margin.bottom * 0.5)
      |    .attr("dx", "0.32em")
      |    .attr("fill", "#000")
      |    .attr("font-weight", "bold")
      |    .attr("text-anchor", "start")
      |    .text("Nodes");
      |
      |svg.append("g")
      |    .attr("transform", "translate(" + margin.left + ",0)")
      |    .call(d3.axisLeft(y))
      |    .append("text")
      |    .attr("transform", "rotate(-90)")
      |    .attr("x", 0 - (height / 2))
      |    .attr("y", 15 - (margin.left))
      |    .attr("dy", "0.32em")
      |    .attr("fill", "#000")
      |    .attr("font-weight", "bold")
      |    .attr("text-anchor", "middle")
      |    .text("Workload");
      |
      |// gridlines in y axis function
      |function make_y_gridlines() {
      |    return d3.axisLeft(y)
      |        .ticks(5)
      |}
      |
      |svg.append("g")
      |    .attr("class", "grid")
      |    .attr('transform', `translate($${margin.left}, 0)`)
      |    .attr("stroke-width", .5)
      |    .attr("fill", "none")
      |    .call(make_y_gridlines()
      |          .tickSize(-width)
      |          .tickFormat("")
      |         )
      |    .lower();
      |
      |// var rectTooltipg = svg.append("g")
      |//     .attr("font-family", "sans-serif")
      |//     .attr("font-size", 10)
      |//     .attr("text-anchor", "end")
      |//     .attr("id", "recttooltip_" + divName)
      |//     .attr("style", "opacity:0")
      |//     .attr("transform", "translate(-500,-500)");
      |
      |// rectTooltipg.append("rect")
      |//     .attr("id", "recttooltipRect_" + divName)
      |//     .attr("x", 0)
      |//     .attr("width", 120)
      |//     .attr("height", 80)
      |//     .attr("opacity", 0.71)
      |//     .style("fill", "#000000");
      |
      |// rectTooltipg
      |//     .append("text")
      |//     .attr("id", "recttooltipText_" + divName)
      |//     .attr("x", 30)
      |//     .attr("y", 15)
      |//     .attr("fill", function() {
      |//         return "#fff"
      |//     })
      |//     .style("font-size", function(d) {
      |//         return 10;
      |//     })
      |//     .style("font-family", function(d) {
      |//         return "arial";
      |//     })
      |//     .text(function(d, i) {
      |//         return "";
      |//     });
      |
      |
      |function type(d) {
      |    tasks.forEach(function(c) {
      |        d[c] = +d[c];
      |    });
      |    return d;
      |}
      |
      |var helpers = {
      |    getDimensions: function(id) {
      |        var el = document.getElementById(id);
      |        var w = 0,
      |            h = 0;
      |        if (el) {
      |            var dimensions = el.getBBox();
      |            w = dimensions.width;
      |            h = dimensions.height;
      |        } else {
      |            console.log("error: getDimensions() " + id + " not found.");
      |        }
      |        return {
      |            w: w,
      |            h: h
      |        };
      |    }
      |};
      |
      |d3.select('#dl').on('click', function() {
      |    var config = {
      |        filename: 'ex3swap',
      |    }
      |    d3_save_svg.save(d3.select('svg').node(), config);
      |});
      |
      |
      |""".stripMargin


  /**
    * Draw an allocation
    */
  def draw() : Unit =  {
    val bw = new BufferedWriter(new FileWriter(file))
    val orderedTasks =  allocation.bundle.values.flatten
    val tasks = orderedTasks.map(task => s""""$task"""").mkString("const tasks =[",", ","];\n")
    val jobs = orderedTasks.map(task => allocation.stap.jobOf(task).toString.substring(1)).mkString("const jobs =[",", ","];\n")
    val jobNames = allocation.stap.jobs.map(job => s""""$job"""").mkString("const jobNames =[",", ","];\n")
    val costs = allocation.stap.ds.computingNodes.map(node =>
      s"\n\t{ 'node': '$node',\n" +
      orderedTasks.map(task => s"\t$task:" + (if (allocation.node(task).get == node){allocation.stap.cost(task,node)}  else "0.0") ).mkString(",\n")
      +"\t}"
    ).mkString("const costs =[",", ","];\n")
    val contentHtml: String =  htmlHead + d3Head + tasks + jobs + jobNames + costs + d3Tail + htmlTail
    bw.write(contentHtml)
    bw.close()
   }
}


/**
  * Test AllocationDrawerD3
  */
object AllocationDrawerD3 extends App {

  import org.smastaplus.example.allocation.stap1._
  import org.smastaplus.example.allocation.stap2._
  import org.smastaplus.example.allocation.stap4._
  import org.smastaplus.example.allocation.stap4.elasticity._
  import org.smastaplus.example.allocation.stap4.jobRelease._
  import org.smastaplus.example.allocation.stap4.multiDelegation._
  import org.smastaplus.example.allocation.stap4.swap._
  import org.smastaplus.example.stap._

  // First STAP example
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap1/ex1Allocation.html", ex1Allocation.a, ex1.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap1/ex1Delegation.html", ex1Delegation.a, ex1.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap1/ex1Delegation2.html", ex1Delegation2.a, ex1.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap1/ex1Delegation3.html", ex1Delegation3.a, ex1.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap1/ex1Delegation4.html", ex1Delegation4.a, ex1.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap1/ex1lcjf.html", ex1lcjf.a, ex1.stap)
    .draw()

  // Second STAP example
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap2/ex2Allocation.html", ex2Allocation.a, ex2.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap2/ex2Delegation.html", ex2Delegation.a, ex2.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap2/ex2lcjf.html", ex2lcjf.a, ex2.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap2/ex2lcjfDelegation1.html", ex2lcjfDelegation1.a, ex2.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap2/ex2lcjfDelegation2.html", ex2lcjfDelegation2.a, ex2.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap2/ex2lcjfDelegation3.html", ex2lcjfDelegation3.a, ex2.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap2/ex2lcjfDelegation4.html", ex2lcjfDelegation4.a, ex2.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap2/ex2lcjfDelegationStrategy1.html", ex2lcjfDelegationStrategy1.a, ex2.stap)
    .draw()

  // STAP example for swap
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4Allocation.html", ex4Allocation.a, ex3.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4Swap.html", ex4Swap.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4Consumption.html", ex4Slides_AfterConsumption.a, ex3.stap)
    .draw()


  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap5/ex5Swap.html", ex5Swap.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap5/ex5SwapB.html", ex5SwapB.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap5/ex5SwapC.html", ex5SwapC.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap5/ex5SwapBeforeSwap.html", ex5SwapBeforeSwap.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap5/ex5SwapAfterSwap.html", ex5SwapAfterSwap.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap5/ex5SwapAfterDelegation.html", ex5SwapAfterDelegation.a, ex4.stap)
    .draw()

  // Stap example for multi-delegation
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4AfterDelegation.html", ex4AfterDelegation.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4AfterSwap.html", ex4AfterSwap.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4MultipleDelegationBefore.html", ex4MultiDelegationBefore.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4MultipleDelegationStep1.html", ex4MultiDelegationStep1.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4MultipleDelegationStep2.html", ex4MultiDelegationStep2.a, ex4.stap)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4MultipleDelegationStep3.html", ex4MultiDelegationStep3.a, ex4.stap)
    .draw()

  // Fourth STAP example
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4Stable.html", ex4Stable.a, ex4.stap)
     .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4t3.html", ex4t3.a, ex4with4Jobs.stap)
     .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4t3step2.html", ex4t3step2.a, ex4with4Jobs.stap)
     .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4Final.html", ex4Final.a, ex4with4Jobs.stap)
     .draw()

  // STAP example for elasticity
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4AllocationWith2Nodes.html", ex4AllocationWith2Nodes.a, ex4Elasticity.stap, 24)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4AllocationWith3Nodes.html", ex4AllocationWith3Nodes.a, ex4Elasticity.stap, 24)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4AllocationWith4Nodes.html", ex4AllocationWith4Nodes.a, ex4Elasticity.stap, 24)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4AllocationWith5Nodes.html", ex4AllocationWith5Nodes.a, ex4Elasticity.stap, 24)
    .draw()
  new AllocationDrawerD3("examples/dataviz/allocation/d3/stap4/ex4AllocationWith6Nodes.html", ex4AllocationWith6Nodes.a, ex4Elasticity.stap, 24)
    .draw()
}
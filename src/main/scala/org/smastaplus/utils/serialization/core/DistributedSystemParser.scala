// Copyright (C) Maxime MORGE 2020-2021
package org.smastaplus.utils.serialization.core

import org.smastaplus.core._

import scala.collection.SortedSet
import scala.io.{BufferedSource, Source}

/**
  * Build a distributed system from a text file
  * @param fileName of the distributed system
  *
  */
class DistributedSystemParser(val fileName: String ) {
  val debug = false
  val parseDebug = false
  private var lineNumber = 0

  private var m = 0 // number of computing nodes
  private var m2= 0 // number of resource nodes
  private var k = 0 // number of resources
  private var computingNodes: SortedSet[ComputingNode] = SortedSet[ComputingNode]()
  private var resourcesNodes: SortedSet[ResourceNode] = SortedSet[ResourceNode]()
  private var acquaintance = Map[(ComputingNode, ResourceNode), Boolean]()
  private var resources: SortedSet[Resource] = SortedSet[Resource]()
  private var locations = Map[Resource, Set[ResourceNode]]()
  var ds: DistributedSystem = _
  private var hasAcquaintance = false

  /**
    * Returns a DistributedSystem by parsing file
    */
  def parse(): DistributedSystem = {
    val bufferedSource: BufferedSource = Source.fromFile(fileName)
    val ds = parseDS(bufferedSource)
    bufferedSource.close()
    ds
  }

  /**
    * Returns a DistributedSystem by parsing a bufferedSource
    */
  @throws(classOf[RuntimeException])
  def parseDS(bufferedSource: BufferedSource): DistributedSystem = {
    for (line <- bufferedSource.getLines()) {
      lineNumber += 1
      if (debug || parseDebug) println(s"parseDS line $lineNumber: $line")
      if (line.startsWith("//")) { //Drop comment
        if (debug) println(s"parse $fileName line$lineNumber: comment $line")
      } else parseLine(line)
      if (hasAcquaintance) {
        ds = new DistributedSystem(computingNodes, resourcesNodes, acquaintance, resources, locations)
        if (debug) println(ds)
        if (ds.isFullySpecified) return ds
        else throw new RuntimeException(s"ERROR parse incomplete ds")
      }
    }
    ds
  }

  /**
    * Parse a line
    *
    * @param line of the file to parse
    */
  @throws(classOf[RuntimeException])
  def parseLine(line: String): Unit = {
    val couple = line.split(":").map(_.trim)
    if (couple.length != 2) throw new RuntimeException(s"ERROR parseLine $fileName line$lineNumber: comment $line")
    val (key, value) = (couple(0), couple(1))
    //Firstly, the size (m,n) should be setup as strict positive integer
    if (m == 0 || m2 == 0 || k == 0) parseSize(key, value)
    //Secondly, the entities should be setup as non-empty
    else{
      if (debug) println(s"parseLine m,m2,k=$m,$m2,$k")
      if (computingNodes.isEmpty || resources.isEmpty) {
        parseEntities(key, value)
      }
      else if (computingNodes.nonEmpty && resources.nonEmpty) {
        ds = new DistributedSystem(computingNodes, resourcesNodes, acquaintance, resources, locations)
        parseEntity(key, value)
      }
    }
  }

  /**
    * Parse the size of the MATA Problem (m,n,l)
    *
    * @param key   of the line
    * @param value of the line
    */
  @throws(classOf[RuntimeException])
  def parseSize(key: String, value: String): Unit = key match {
    case "m" => m = value.toInt
    case "m2" => m2 = value.toInt
    case "k" => k = value.toInt
    case _ => throw new RuntimeException("ERROR parseSize" + fileName + " L" + lineNumber + "=" + key + "," + value)
  }

  /**
    * Parse the entities (nodeAgents, tasks, jobs) of the MATA Problem
    *
    * @param key   of the line
    * @param value of the line
    */
  @throws(classOf[RuntimeException])
  def parseEntities(key: String, value: String): Unit = key match {
    case "computingNodes" => parseComputingNodes(value)
    case "resourceNodes" => parseResourcesNodes(value)
    case "resources" => parseResources(value)
    case _ => throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $key")
  }

  /**
    * Parse the resources
    *
    * @param names e.g. a string "r1 (6.0), r2 (5.0)"
    */
  @throws(classOf[RuntimeException])
  private def parseResources(names: String): Unit = {
    val array: Array[String] = names.split(", ").map(_.trim)
    if (array.length != k) throw new RuntimeException(s"ERROR parseTasks $fileName line$lineNumber: the number of resources  $k is wrong: ${array.length}")
    val pattern = """(\w+)\s?\((\d+.\d+)\)\s?""".r
    array.foreach { str: String =>
      str match {
        case pattern(resource, size) =>
          if (debug) println(s"parseResources: $resource with size $size")
          resources = resources.union(Set(new Resource(resource, size.toDouble)))
        case _ => throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $str")
      }
    }
  }

  /**
    * Parse the computing nodes
    * @param names e.g. a string "n1, n2, n3"
    */
  private def parseComputingNodes(names: String): Unit = {
    val array: Array[String] = names.split(", ").map(_.trim)
    array.foreach { str: String =>
      if (debug) println(s"parseComputingNodes: $str")
      computingNodes = computingNodes  ++ SortedSet(new ComputingNode(str))
    }
  }

  /**
    * Parse the resource nodes
    * @param names e.g. a string "n1, n2, n3"
    */
  private def parseResourcesNodes(names: String): Unit = {
    val array: Array[String] = names.split(", ").map(_.trim)
    array.foreach { str: String =>
      if (debug) println(s"parseResourcesNodes: $str")
      resourcesNodes = resourcesNodes  ++ SortedSet(new ResourceNode(str))
    }
    if (computingNodes.size == m && resourcesNodes.size == m2)
      computingNodes.foreach(cn => resourcesNodes.foreach(rn => acquaintance = acquaintance + ((cn, rn) -> false)))
  }


  /**
    * Parse the entity (location, acqauaitance)
    *
    * @param key   of the line
    * @param value of the line
    */
  def parseEntity(key: String, value: String): Unit = {
    if (debug) println(s"parseEntity $key")
    if (ds.resources.map(r => r.name).contains(key))
      parseLocations(key, value)
    else if (! hasAcquaintance)  parseAcquaintance(key, value)
  }

  /**
    * Parse the acquaitance
    *
    * @param key   of the line
    * @param value of the line
    */
  @throws(classOf[RuntimeException])
  private def parseAcquaintance(key: String, value: String): Unit = {
    if (debug) println(s"parseAcquaintance $value")
    if (key == "acquaintance") {
      val couples: Array[String] = value.split(";").map(_.trim)
      val pattern = """\s?\((\w+\d*),(\w+\d*)\)\s?""".r
      couples.foreach { couple: String =>
        if (debug) println(s"parseAcquaintance $couple")
        couple match {
          case pattern(n1, n2) =>
            if (debug) println(s"*$n1*,*$n2*")
            acquaintance += ((ds.getComputingNode(n1), ds.getResourceNode(n2)) -> true)
          case _ => throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $value")
        }
      }
    } else throw new RuntimeException(s"ERROR parseEntities $fileName line$lineNumber: $value")
    hasAcquaintance = true
  }

  /**
    * Parse the cost
    *
    * @param key   of the line
    * @param value of the line
    */
  @throws(classOf[RuntimeException])
  private def parseLocations(key: String, value: String): Unit = {
    if (debug) println(s"parseLocation $key")
    val resource = ds.getResource(key)
    val places: Array[String] = value.split(",").map(_.trim)
    if (places.length < 1) throw new RuntimeException(s"ERROR parse $fileName line$lineNumber: $value")
    val someNodes = places.map(ds.getResourceNode)
    locations += (resource -> someNodes.toSet)
    if (debug) println(s"parseLocations $resource: "+someNodes.mkString(", "))
  }
}

/**
  * Test Extensive Distributed system parser
  */
object  DistributedSystemParser extends App {
  var parser = new DistributedSystemParser("examples/ds/ex1.txt")
  var pb = parser.parse() // parse problem
  println(pb)
  parser = new DistributedSystemParser("examples/ds/ex2.txt")
  pb = parser.parse() // parse problem
  println(pb)
  parser = new DistributedSystemParser("examples/ds/ex3.txt")
  pb = parser.parse() // parse problem
  println(pb)
}


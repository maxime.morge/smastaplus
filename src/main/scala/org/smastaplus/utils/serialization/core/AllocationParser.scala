// Copyright (C) Maxime MORGE 2022
package org.smastaplus.utils.serialization.core

import org.smastaplus.core._

import scala.io.{BufferedSource, Source}

/**
  * Build an allocation from a text file
  * param stap is the stap problem instance
  * @param fileName of the allocation
  *
  *
  */
class AllocationParser(val stap: STAP, val fileName: String) {
  val debug = false

  private var lineNumber = 0

  var allocation = new ExecutedAllocation(stap)

  /**
    * Returns an allocation by parsing file
    */
  def parse(): ExecutedAllocation = {
    val bufferedSource : BufferedSource = Source.fromFile(fileName)
    val allocation =parse(bufferedSource)
    bufferedSource.close()
    allocation
  }

  /**
    * Returns a stap by parsing a bufferedSource
    */
  def parse(bufferedSource : BufferedSource) : ExecutedAllocation = {
      for (line <- bufferedSource.getLines()) {
        lineNumber += 1
        if (debug) println(s"parse $fileName line$lineNumber: $line")
        if (line.startsWith("//")) { //Drop comment
          if (debug) println(s"parse $fileName line$lineNumber: comment $line")
        } else parseLine(line)
      }
      if (debug) println(allocation)
      if (allocation.isSound) return allocation
      throw new RuntimeException(s"ERROR parse unsound allocation")
  }

  /**
    * Parse a line
    *
    * @param line of the file to parse
    */
  def parseLine(line: String): Unit = {
    val couple = line.split(":").map(_.trim)
    val (key, value) = if (couple.length == 2) (couple(0), couple(1))
    else (couple(0), "")
    parseBundle(key, value)
  }

  /**
    * Parse the bundles e.g. "ν1: τ5, τ2, τ7, τ8"
    */
  private def parseBundle(key: String, value: String): Unit = {
    if (debug) println(s"parseBundle: $key -$value")
    var bundle = List[Task]()
    val node : ComputingNode = stap.ds.computingNodes.find(_.name == key) match {
      case Some(t) => t
      case None => throw new RuntimeException(s"No node $key found")
    }
    val array: Array[String] = value.split(", ").map(_.trim)
    if (debug) array.foreach(println)
    if (array.length!=1 || array(0).nonEmpty){// If there is at least one task
      array.foreach { str: String =>
      val task = stap.tasks.find(_.name == str) match {
        case Some(r) => r
       case None => throw new RuntimeException(s"No task $key found")
      }
      bundle ::= task
      }
    }
    allocation = allocation.update(node,bundle)
  }
}

/**
  * Test allocation parser
  */
object AllocationParser extends App {
  import org.smastaplus.example.stap.ex4Elasticity._
  var parser = new AllocationParser(stap, "examples/allocation/stap4/ex4AllocationWith4Nodes.txt")
  var allocation = parser.parse()
  println(allocation)
}





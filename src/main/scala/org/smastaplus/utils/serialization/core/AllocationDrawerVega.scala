//Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.utils.serialization.core

import org.smastaplus.core._
import org.smastaplus.example.allocation.stap1._
import org.smastaplus.example.allocation.stap2._
import org.smastaplus.example.allocation.stap4.ex4Allocation
import org.smastaplus.example.allocation.stap4.jobRelease.ex4Swap
import org.smastaplus.example.allocation.stap4.swap.ex5Swap

import java.io.{BufferedWriter, File, FileWriter}

/**
  * Draw an allocation with the
  * [[https://vega.github.io/vega-lite/ Vega-Lite grammar]]
  * @deprecated See [[AllocationDrawerD3]]
  * @param pathName of the text file
  * @param allocation to be written
  * @param problem STAP instance
  */
class AllocationDrawerVega(pathName: String,
                           allocation: ExecutedAllocation,
                           problem: STAP) {
  val file = new File(pathName)

  val htmlHead: String =
    """<!DOCTYPE html>
      |<html>
      |  <head>
      |    <title>Allocation</title>
      |    <meta charset="utf-8" />
      |        <script src="https://cdn.jsdelivr.net/npm/vega@5"></script>
      |        <script src="https://cdn.jsdelivr.net/npm/vega-lite@4"></script>
      |       <script src="https://cdn.jsdelivr.net/npm/vega-embed@6"></script>
      |    <style media="screen">
      |      /* Add space between Vega-Embed links  */
      |      .vega-actions a {
      |        margin-right: 5px;
      |      }
      |    </style>
      |  </head>
      |  <body>
      |    <!-- Container for the visualization -->
      |    <div id="vis"></div>
      |    <script>""".stripMargin

  val htmlTail: String =
    """
      |    </script>
      |  </body>
      |</html>""".stripMargin

  private val vegaHead: String =
    """
      |var vlSpec =
      |{
      |    'schema': {'language': 'vega', 'version': '3.0'},
      |    'width': 500, 'height': 250, 'padding': 5,
      |    'data': {
      |	   'values':
      |""".stripMargin

  private val vegaTail: String =
    """
      |      }
      |    }
      |  ]
      |}
      |// Embed the visualization in the container with id `vis`
      |vegaEmbed('#vis', vlSpec);""".stripMargin

  /**
    * Return a Vega-lite representation of the data transformation
    * according to the potential job order
    */
  private def vegaDataTransformation(jobOrder: Option[List[Job]]) : String = {
    jobOrder match {
      case None => ""
      case Some(list) =>
        s"""
           |    "transform": [{
           |    "calculate": "${ifJSON(list,1)}",
           |    "as": "jobOrder"
           |  }],
           |""".stripMargin
    }
  }

  /**
    * Return a Vega-lite representation of the bar order
    * according to the potential job order
    */
  private def vegaOrder(jobOrder: Option[List[Job]]) : String = {
    jobOrder match {
      case None =>
        s"""'order': {'aggregate': 'sum', 'field': 'cost', 'type': 'quantitative'},
           |        'sort': ${allocation.bundles2JSON}""".stripMargin
      case Some(_) =>
        """"order": {"field": "jobOrder", "type": "quantitative"}"""
    }
  }

  /**
    *
    */
  private def vegaDomain(max : Option[Int]) : String = max match{
    case m : Some[Int] => s"  'scale': {'domain': [0, ${m.get}]}"
    case None => ""
  }

  /**
    * Returns the first part of the Vega-lite representation
    * according to the potential job order
    */
  private def vegaFirstPart(jobOrder: Option[List[Job]], max : Option[Int]): String =
    s"""
       |    },
       |    ${vegaDataTransformation(jobOrder)}
       |    'resolve': {'scale': {'color': 'independent'}},
       |    'layer': [
       |	{'mark': 'bar',
       |	 'encoding': {
       |          'y': {
       |	      'aggregate': 'sum',
       |	      'field': 'cost',
       |	      'type': 'quantitative',
       |	      'stack': 'zero',
       |	      'title': 'Workload',
       |       ${vegaDomain(max)}
       |      },
       |    'x': {'field': 'node', 'type': 'nominal', 'title': 'Nodes'},
       |	   'color': {'field': 'job', 'type': 'nominal', 'title': 'Jobs', 'scale': {'scheme': 'set1'}},
       |	   ${vegaOrder(jobOrder)}""".stripMargin

  /**
    * Return the second part of the Vega-lite representation
    * according to the potential job order
    */
  private def vegaSecondPart(jobOrder: Option[List[Job]]): String =
    s"""
       |     }
       |     },
       |     {'mark': {'type': 'text', 'dy': -5, 'dx': 3},
       |      'encoding': {
       |          'y': {
       |	      'aggregate': 'sum',
       |	      'field': 'cost',
       |	      'type': 'quantitative',
       |	      'stack': 'zero',
       |	      'title': 'Workload'
       |	  },
       |    'x': {'field': 'node', 'type': 'nominal', 'axis': {'labelAngle': 0, 'labelPadding': 10, 'tickSize': 0}},
       |    'color': {'field': 'job', 'type': 'nominal', 'scale': {'range': ['black']}, 'legend': null},
       |	  'text': {'field': 'task', 'type': 'nominal'},
       |	   ${vegaOrder(jobOrder)}""".stripMargin

  /**
    * Return the Vega-lite representation of the order among the jobs
    * @return
    */
  private def ifJSON(jobs: List[Job], position : Int) : String = {
    if (jobs.size == 1) return s"$position"
    s"if(datum.job === '${jobs.head}', $position, ${ifJSON(jobs.tail,position+1)})"
  }

  /**
    * Draw an allocation with the
    * [[https://vega.github.io/vega-lite/ Vega-Lite]]  grammar
    * @param jobOrder eventually the order of the jobs
    * @param max workload
    */
  private def drawOrder(jobOrder: Option[List[Job]] = None, max: Option[Int] = None) : Unit = {
    val bw = new BufferedWriter(new FileWriter(file))
    val vegaScript: String = vegaHead + allocation.toJSON() +
      vegaFirstPart(jobOrder,max) + vegaSecondPart(jobOrder) + vegaTail
    val contentHtml: String =  htmlHead + vegaScript + htmlTail
    bw.write(contentHtml)
    bw.close()
  }

  /**
    * Draw an allocation with the
    * [[https://vega.github.io/vega-lite/ Vega-Lite]]  grammar
    * @param max workload
    */
  def draw(max: Option[Int] = None): Unit = drawOrder(None, max)
}

/**
  * Test AllocationDrawerVega
  */
object AllocationDrawerVega extends App {
  import org.smastaplus.example.stap._
  // First STAP example
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap1/ex1Allocation.html", ex1Allocation.a, ex1.stap)
      .draw(max = Some(20))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap1/ex1Delegation.html", ex1Delegation.a, ex1.stap)
      .draw(max = Some(20))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap1/ex1Delegation2.html", ex1Delegation2.a, ex1.stap)
      .draw(max = Some(20))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap1/ex1Delegation3.html", ex1Delegation3.a, ex1.stap)
      .draw(max = Some(20))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap1/ex1Delegation4.html", ex1Delegation4.a, ex1.stap)
      .draw(max = Some(20))
  // Second STAP example
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap2/ex2Allocation.html", ex2Allocation.a, ex2.stap)
      .draw(max = Some(10))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap2/ex2Delegation.html", ex2Delegation.a, ex2.stap)
      .draw(max = Some(10))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap2/ex2lcjf.html", ex2lcjf.a, ex2.stap)
      .draw(max = Some(10))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap2/ex2lcjfDelegation1.html", ex2lcjfDelegation1.a, ex2.stap)
      .draw(max = Some(10))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap2/ex2lcjfDelegation2.html", ex2lcjfDelegation2.a, ex2.stap)
      .draw(max = Some(10))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap2/ex2lcjfDelegation3.html", ex2lcjfDelegation3.a, ex2.stap)
      .draw(max = Some(10))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap2/ex2lcjfDelegation4.html", ex2lcjfDelegation4.a, ex2.stap)
      .draw(max = Some(10))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap2/ex2lcjfDelegationStrategy1.html", ex2lcjfDelegationStrategy1.a, ex2.stap)
      .draw(max = Some(10))
  // STAP example for swap
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap4/ex4Allocation.html", ex4Allocation.a, ex3.stap)
      .draw(max = Some(12))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap4/ex4Swap.html", ex4Swap.a, ex4.stap)
      .draw(max = Some(12))
  new AllocationDrawerVega("examples/dataviz/allocation/vega/stap5/ex5Swap.html", ex5Swap.a, ex4.stap)
      .draw(max = Some(12))
}


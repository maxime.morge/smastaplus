// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.utils.serialization.core

import org.smastaplus.core._

import java.io.{BufferedWriter, File, FileWriter}

/**
  * Draw a STAP instance with the
  * [[https://vega.github.io/vega-lite/ Vega-Lite grammar]]
  * @param pathName of the text file
  * @param problem STAP instance
  */
class STAPDrawer(pathName: String,
                 problem: STAP) {
  val file = new File(pathName)

  val head: String =
    """<!DOCTYPE html>
      |<html>
      |  <head>
      |    <title>Allocation</title>
      |    <meta charset="utf-8" />
      |      <script src="https://cdn.jsdelivr.net/npm/vega@5"></script>
      |      <script src="https://cdn.jsdelivr.net/npm/vega-lite@4"></script>
      |      <script src="https://cdn.jsdelivr.net/npm/vega-embed@6"></script>
      |    <style media="screen">
      |      /* Add space between Vega-Embed links  */
      |      .vega-actions a {
      |        margin-right: 5px;
      |      }
      |    </style>
      |  </head>
      |  <body>
      |    <!-- Container for the visualization -->
      |    <div id="vis"></div>
      |    <script>
      |var vlSpec =
      |{
      |  "schema": {"language": "vega", "version": "3.0"},
      |  "width": 700, "height": 250, "padding": 5,
      |  "data": [
      |    {
      |      "name": "table",
      |      "values":
      |""".stripMargin

  val tail: String =
    """
      |    }
      |  ],
      |  "scales": [
      |    {
      |      "name": "xscale",
      |      "type": "band",
      |      "domain": {"data": "table", "field": "node"},
      |      "range": "width",
      |      "padding": 0.2
      |    },
      |    {
      |      "name": "yscale",
      |      "type": "linear",
      |      "domain": {"data": "table", "field": "size"},
      |      "range": "height",
      |      "round": false,
      |      "zero": true,
      |      "nice": true
      |    },
      |    {
      |      "name": "color",
      |      "type": "ordinal",
      |      "domain": {"data": "table", "field": "job"},
      |      "range": {"scheme": "set1"}
      |    }
      |  ],
      |  "legends": [
      |        {
      |          "title": "Jobs",
      |          "fill": "color",
      |          "labelColor": "black",
      |        }
      |      ],
      |  "axes": [
      |    {"orient": "bottom", "scale": "xscale", "tickSize": 0, "labelPadding": 4, "zindex": 1, "title": "Nodes"},
      |    {"orient": "left", "scale": "yscale", "title":"Resource size"}
      |  ],
      |  "marks": [
      |    {
      |      "type": "group",
      |      "from": {
      |        "facet": {
      |          "data": "table",
      |          "name": "facet",
      |          "groupby": "node"
      |        }
      |      },
      |      "encode": {
      |        "enter": {
      |          "x": {"scale": "xscale", "field": "node"},
      |        }
      |      },
      |      "signals": [
      |        {"name": "width", "update": "bandwidth('xscale')"}
      |      ],
      |      "scales": [
      |        {
      |          "name": "pos",
      |          "type": "band",
      |          "range": "width",
      |          "domain": {"data": "facet", "field": "resource"}
      |        }
      |      ],
      |      "marks": [
      |        {
      |          "name": "bars",
      |          "from": {"data": "facet"},
      |          "type": "rect",
      |          "encode": {
      |            "enter": {
      |              "x": {"scale": "pos", "field": "resource"},
      |              "width": {"scale": "pos", "band": 1},
      |              "y": {"scale": "yscale", "field": "size"},
      |              "y2": {"scale": "yscale", "value": 0},
      |              "fill": {"scale": "color", "field": "job", "title": "Jobs"}
      |            }
      |          }
      |        },
      |        {
      |          "type": "text",
      |          "from": {"data": "bars"},
      |          "encode": {
      |            "enter": {
      |              "x": {"field": "x", "offset": {"field": "width", "mult": 0.75}},
      |              "y": {"field": "y", "offset": -10},
      |              "fill": {"value": "black"},
      |              "align": {"value": "right"},
      |              "baseline": {"value": "middle"},
      |              "text": {"field": "datum.resource"}
      |            }
      |          }
      |        }
      |      ]
      |    }
      |  ]
      |}
      |// Embed the visualization in the container with id `vis`
      |vegaEmbed('#vis', vlSpec);
      |    </script>
      |  </body>
      |</html>""".stripMargin

  /**
    * Draw a STAP instance with the
    * [[https://vega.github.io/vega-lite/ Vega-Lite]]  grammar
    */
  def draw() : Unit = {
    val bw = new BufferedWriter(new FileWriter(file))
    val content: String = head + problem.toJSON + tail
    bw.write(content)
    bw.close()
  }
}

/**
  * Test MASTAPlusDrawer
  */
object STAPDrawer extends App {
  import org.smastaplus.example.stap._
  new STAPDrawer("examples/dataviz/stap/ex1.html", ex1.stap).draw()
  new STAPDrawer("examples/dataviz/stap/ex2.html", ex2.stap).draw()
  new STAPDrawer("examples/dataviz/stap/ex3.html", ex3.stap).draw()
}
// Copyright (C) Maxime MORGE 2023
package org.smastaplus.utils.serialization.core

import scala.io.{BufferedSource, Source}
import scala.util.matching.Regex

/**
  * Build a Sankey diagram from a log file
  * @param fileName of the log file
  *
  */
class SankeyDrawer(val fileName: String ) {
  val debug = false

  var nodes : Array[String]= Array[String]()
  private var delegations :  List[(String,String,List[String])] =  List[(String,String,List[String])]()
  //var delegationSize = Array[Int][Int]()

  //var label = Array[String]()
  var target: Array[Int] = Array[Int]()
  var value: Array[Int] = Array[Int]()

  private val regexConfirm: Regex = """(\w+)\s->\s(\w+)\s:\sConfirm\(δ\(\w+,\w+,(.*?)\)\)""".r

  private val regexParticipant : Regex = """^participant\s+(?!Supervisor\b)(\w+)$""".r

  override def toString: String =
    "nodes: " + nodes.mkString(", ") + "\n" +
    "delegations: " + delegations.mkString(", ")

  /**
    * Parse a log file
    */
  private def parseLog() : Unit = {
    var lineNumber = 0
    val bufferedSource: BufferedSource = Source.fromFile(fileName)
    for (line <- bufferedSource.getLines()) {
      lineNumber += 1
      if (debug) println(s"parseLog line $lineNumber: $line")
      parseLine(line)
    }
    println(s"parseLog $this")
  }

  /**
    * Parse a line
    * @param line of the file to parse
    */
  def parseLine(line: String): Unit = line match {
      //case _ => ("", List.empty[String])
    case regexParticipant(x) => nodes = nodes :+ x
    case regexConfirm(donor, recipient, tasks) =>
      delegations = delegations :+ (donor, recipient, tasks.split (", ").toList)
    case _ =>
    }
}

/**
  * Test STAP parser
  */
object SankeyDrawer extends App {
  private val drawer = new SankeyDrawer("log.txt")
  drawer.parseLog() // parse first problem
}




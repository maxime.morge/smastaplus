// Copyright (C) Maxime MORGE, Anne-Cécile CARON, 2020, 2021, 2023
package org.smastaplus.utils

import org.smastaplus.core._
import org.smastaplus.utils.serialization.core.{AllocationWriter, STAPParser}
import org.smastaplus.balancer._
import org.smastaplus.balancer.deal._
import org.smastaplus.balancer.dual._
import org.smastaplus.balancer.dual.constraint._
import org.smastaplus.balancer.dual.lp._
import org.smastaplus.balancer.deal.mas.AgentBasedBalancer
import org.smastaplus.balancer.local.HillClimbingBalancer
import org.smastaplus.strategy.deal.proposal.multi.ConcreteMultiProposalStrategy
import org.smastaplus.strategy.deal.proposal.single.{ConcreteConservativeProposalStrategy, ConcreteLiberalSingleProposalStrategy}
import org.smastaplus.strategy.deal.counterproposal.single.{ConcreteSingleCounterProposalStrategy, NoneSingleCounterProposalStrategy}

import java.nio.file.{Files, Paths}
import akka.actor.ActorSystem
import akka.dispatch.MessageDispatcher

/**
 * Solve a particular STAP instance using :
 *
 * - either  sbt "run org.smastaplus.utils.MASTAPlusBalancer  -d -g -v examples/stap/ex1.txt ex1AllocationFound.txt"
 *
 * - or java -jar smastaplus-assembly-X.Y.jar org.smastaplus.utils.MASTAPlusBalancer -d -g -v examples/stap/ex1.txt ex1AllocationFound.txt
 *
 * With monitoring (-t option), give the path to the NodeJS monitoring directory
 */
object MASTAPlusBalancer extends App {

  val debug = false

  val centralizedDispatcherId = "single-thread-dispatcher"
  val decentralizedDispatcherId = "akka.actor.default-dispatcher"
  val centralizedSystem : ActorSystem = ActorSystem("CentralizedBalancer" + AgentBasedBalancer.id)
  implicit val centralizedContext: MessageDispatcher = centralizedSystem.dispatchers.lookup(centralizedDispatcherId)
  AgentBasedBalancer.id += 1
  val decentralizedSystem : ActorSystem = ActorSystem("DecentralizedBalancer" + AgentBasedBalancer.id)
  implicit val decentralizedContext: MessageDispatcher = decentralizedSystem.dispatchers.lookup(decentralizedDispatcherId)

  val usage =
    """
    Usage: java -jar smastasplus-assembly-X.Y.jar org.smastaplus.utils.MASTAPlusBalancer [-acdfghijlmnopsvw] inputFilename outputFilename
    The following options are available:
    -a: allocation method based on sequential single-issue auctions (false by default)
    -c: constraint programming (false by default) with a 15 seconds timeout
    -d: distributed (false by default)
    -f: LocalFlowtime (Makespan by default)
    -g: GlobalFlowtime (Makespan by default)
    -h: heuristic approach (false by default)
    -i: LocalFlowtimeAndMakespan (Makespan by default)
    -j: LocalFlowtimeAndGlobalFlowtime (Makespan by default)
    -l: linear programming (false by default)
    -m: MGM2 (false by default) with a 15 seconds timeout
    -n: multi-delegation without swap (false by default)
    -o: dcop-like approach (false by default) with the specific heuristic
    -s: swap (false by default)
    -r: reallocation method based on sequential single-issue auctions (false by default)
    -t: trace for interaction protocol (false by default)
    -v: verbose (false by default)
    -w: monitor (false by default)
  """

  // Default parameters for the balancer
  var verbose = false
  var trace = false
  var monitor = true
  private var socialRule: SocialRule = Makespan
  private var distributed: Boolean = false
  var swap: Boolean = false
  var multiDelegation = false
  private var linearProgramming : Boolean = false
  private var constraintProgramming : Boolean = false
  var dcop : Boolean = false
  private var heuristicApproach : Boolean = false
  private var mgm2 : Boolean = false
  private var assi : Boolean = false
  private var rssi : Boolean = false

  // Default fileNames/path for the input/output
  private var inputFilename = new String()
  var outputFilename = new String()
  if (args.length <= 2) {
    println(usage)
    System.exit(1)
  }

  // drop class name in the java command line
  var argList = if (args.head == "org.smastaplus.utils.MASTAPlusBalancer") args.toList.drop(1) else args.toList
  parseFilenames() // parse filenames
  if (!nextOption(argList)) {
    println(s"ERROR MASTAPlusBalancer: options cannot be parsed ")
    System.exit(1)
  }

  // fail if options cannot be parsed
  val parser = new STAPParser(inputFilename)
  val pb = parser.parse() // parse problem
  val balancer : Balancer = selectBalancer(pb) // select balancer
  if (verbose){
    balancer match{
      case s: DealBalancer => // Trace MAS balancers
        s.trace = true
      case _ => // Debug other balancers
        balancer.debug = true
    }
  }
  if (verbose) println(pb)
  if (debug) println(
    s"""
    Run balancer with the following parameters:

    verbose:$verbose
    socialRule:$socialRule
    distributed:$distributed
    swap:$swap
    multiDelegation:$multiDelegation
    heuristicApproach:$heuristicApproach
    linearProgramming:$linearProgramming
    constraintProgramming:$linearProgramming
    dcop:$dcop
    mgm2:$mgm2
    assi:$assi
    rssi:$rssi
    ...
  """)

  balancer.run() match{
    case None =>
      println(s"$balancer schedules none allocation")
    case Some(allocation) =>
      println(s"$balancer schedules the allocation:\n$allocation")
      socialRule match {
        case Makespan => println(s"Makespan: ${allocation.makespan}")
        case LocalFlowtime  => println(s"GlobalFlowtime: ${allocation.meanGlobalFlowtime}")
        case GlobalFlowtime => println(s"GlobalFlowtime: ${allocation.meanGlobalFlowtime}")
        case rule => throw new RuntimeException(s"ERROR: Rule $rule was not expected")
      }
      println(s"Local availability ratio: ${allocation.localAvailabilityRatio}")
      // write the result in the outputFile, in JSon
      val writer = new AllocationWriter(outputFilename, allocation, "monitoring")
      writer.write()
  }
  System.exit(0)

  /**
   * Parse filenames at first
   */
  def parseFilenames(): Unit = {
    outputFilename = argList.last.trim
    argList = argList.dropRight(1) // drop outputFile
    if (Files.exists(Paths.get(outputFilename))) {
      println(s"ERROR MASTAPlusBalancer parseFilename: $outputFilename already exist")
      System.exit(1)
    }

    inputFilename = argList.last.trim
    argList = argList.dropRight(1) //drop inputFile
    if (!Files.exists(Paths.get(inputFilename))) {
      println(s"ERROR MASTAPlusBalancer parseFilename: $inputFilename does not exist")
      System.exit(1)
    }
  }

  /**
   * Parse options at second
   * @param tags is the list of options
   */
  @scala.annotation.tailrec
  def nextOption(tags: List[String]): Boolean = {
    if (tags.isEmpty) return true
    val tag: String = tags.head.substring(1) // remove '-'
    tag match {
      case "v" =>
        if (debug) println("Verbose mode")
        verbose = true
      case "w" =>
        if (debug) println("Monitor mode")
        monitor = true
      case "t" =>
        if (debug) println("Trace mode")
        trace = true
      case "f" =>
        if (debug) println("LocalFlowtime mode")
        socialRule = LocalFlowtime
      case "g" =>
        if (debug) println("GlobalFlowtime mode")
        socialRule = GlobalFlowtime
      case "i" =>
        if (debug) println("LocalFlowtimeAndMakespan mode")
        socialRule = LocalFlowtimeAndMakespan
      case "j" =>
        if (debug) println("LocalFlowtimeAndGlobalFlowtime mode")
        socialRule = LocalFlowtimeAndGlobalFlowtime
      case "d" =>
        if (debug) println("Distributed mode")
        distributed = true
      case "s" =>
        if (debug) println("SingleSwap mode")
        swap = true
      case "n" =>
        if (debug) println("Multi delegation mode")
        multiDelegation = true
      case "l" =>
        if (debug) println("Linear programming mode")
        linearProgramming = true
      case "c" =>
        if (debug) println("Constraint programming mode")
        constraintProgramming = true
      case "m" =>
        if (debug) println("MGM2 mode")
        mgm2 = true
      case "o" =>
        if (debug) println("DCOP mode")
        dcop= true
      case "h" =>
        if (debug) println("Heuristic approach mode")
        heuristicApproach = true
      case "a" =>
        if (debug) println("ASSI mode")
        assi = true
      case "r" =>
        if (debug) println("RSSI mode")
        rssi = true
      case _ => return false
    }
    nextOption(tags.tail)
  }

  /**
   * Returns the balancer for the stap instance pb
   */
  private def selectBalancer(pb: STAP): Balancer = {

    if (dcop) return new DeputationBalancer(pb, socialRule, heuristic = true)

    if (constraintProgramming) return new ConstraintBalancer(pb, socialRule)

    if (mgm2) return new DCOPBalancer(pb, socialRule)

    if (linearProgramming) return new NonLinearBalancer(pb, socialRule)

    if (heuristicApproach) return new HillClimbingBalancer(pb, socialRule)

    if (assi) return new ASSIBalancer(pb, socialRule)

    if (rssi) return new RSSIBalancer(pb, socialRule)

    val dispatcherId = if (distributed) decentralizedDispatcherId else centralizedDispatcherId
    val name = if (distributed)  "DecentralizedBalancer" else "CentralizedBalancer"

    val delegationStrategy = if (multiDelegation) ConcreteMultiProposalStrategy() else new ConcreteConservativeProposalStrategy

    if (swap && !multiDelegation)
      new AgentBasedBalancer(pb, socialRule, delegationStrategy, Some(ConcreteLiberalSingleProposalStrategy()), ConcreteSingleCounterProposalStrategy(), centralizedSystem, name, dispatcherId, monitor)

    new AgentBasedBalancer(pb, socialRule, delegationStrategy, None, new NoneSingleCounterProposalStrategy(), centralizedSystem, name, dispatcherId,  monitor)
  }
}
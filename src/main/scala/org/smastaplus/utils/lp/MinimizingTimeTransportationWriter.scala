// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.utils.lp

import org.smastaplus.core._

import java.io.{BufferedWriter, File, FileWriter}

/**
  * Write a STAP instance as a time minimizing
  * transportation problem with bundles in a text file
  * @param pathName of the text file
  * @param stap instance to be written
  */
class MinimizingTimeTransportationWriter(pathName: String, stap: STAP) {
  val file = new File(pathName)
  def write() : Unit = {
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(stap.toMinimizingTimeTransportation)
    bw.close()
  }
}


// Copyright (C) Maxime MORGE 2021
package org.smastaplus.utils.thread

import scala.concurrent._

/**
  * Custom class to intterrupt a thread
  */
class MyCustomExecutionContext extends AnyRef with ExecutionContext {
  //  import ExecutionContext.Implicits.global
  @volatile var lastThread: Option[Thread] = None
  override def execute(runnable: Runnable): Unit = {
    ExecutionContext.Implicits.global.execute(new Runnable() {
      override def run(): Unit = {
        lastThread = Some(Thread.currentThread)
        try {
          runnable.run()
          if (Thread.interrupted())
            throw new InterruptedException()
        }catch {
          case _: InterruptedException =>
        }
      }
    })
  }
  override def reportFailure(t: Throwable): Unit = ???
}

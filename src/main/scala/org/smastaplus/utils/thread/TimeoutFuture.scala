// Copyright (C) Maxime MORGE 2021
package org.smastaplus.utils.thread

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent._
import scala.concurrent.duration._

/**
  * Future that delegates to another but will finish early if the specified duration expires.
  * The delegate future is interrupted and cancelled if it times out.
  */
object TimeoutFuture {
  def apply[A](timeout: FiniteDuration)(block: => A): Future[A] = {

    val prom = Promise[A]()

    val system = akka.actor.ActorSystem("system")
    // timeout logic
    system.scheduler.scheduleOnce(timeout) {
      prom tryFailure new java.util.concurrent.TimeoutException
    }

    // business logic
    Future {
      prom success block
    }

    prom.future
  }
}
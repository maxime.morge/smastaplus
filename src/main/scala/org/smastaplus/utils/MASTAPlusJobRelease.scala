// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, Anne-Cécile CARON 2023
package org.smastaplus.utils

import org.smastaplus.core._
import org.smastaplus.utils.Timer.{Start, Timeout}
import org.smastaplus.consumer.nodeal.AsynchronousDecentralizedConsumer
import org.smastaplus.consumer.deal.mas.{AsynchronousAgentBasedConsumer, Init, Outcome, Update}
import org.smastaplus.strategy.deal.proposal.single.ConcreteLiberalSingleProposalStrategy
import org.smastaplus.strategy.deal.proposal.multi.ConcreteMultiProposalStrategy
import org.smastaplus.strategy.deal.counterproposal.single.ConcreteSingleCounterProposalStrategy

import scala.annotation.tailrec
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.dispatch.MessageDispatcher
import com.typesafe.config.{Config, ConfigFactory}
import org.joda.time.{Duration, LocalDateTime}

/**
 * Consume and negotiate a random allocation and job release for a random STAP instance using :
 *
 * - either  sbt "run org.smastaplus.utils.MASTAPlusJobRelease -v -m"
 *
 * - or java -jar smastaplus-assembly-X.Y.jar org.smastaplus.utils.MASTAPlusJobRelease -v -m
 */
class MASTAPlusJobRelease(val debug: Boolean,
                          val monitor: Boolean,
                          val verbose: Boolean,
                          val system: ActorSystem,
                          val dispatcherId: String) extends Actor {

  println(s"Run MASTAPlusJobRelease")
  val config: Config = ConfigFactory.load()
  val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
    config.getString("path.monitoringDirectory")

  implicit val decentralizedContext: MessageDispatcher = system.dispatchers.lookup(dispatcherId)
  private var consumptionSystem: ActorRef = _
  private var assiSystem: ActorRef = _
  private var rssiSystem: ActorRef = _
  private var negotiationSystem: ActorRef = _
  private var timer : ActorRef = _

  private var pb : STAP= _
  private var aInit: ExecutedAllocation = _
  private var a0 : ExecutedAllocation = _
  private var job : Job = _
  private var cs0 : Double = 0.0
  private var cr0 : Double = 0.0
  private var cs : Double = 0.0
  private var cr : Double = 0.0
  private var step = 1

  val l = 4 // with l jobs
  val m = 8 // with m nodes, eventually 16
  val n = 128 // with n tasks, eventually 128
  val o = 10// with o resources per task, eventually 10
  val d = 3 // with d instances per resource, eventually 3

  println(s"Initial STAP with $l jobs, $m nodes, $n tasks, $o resources per tasks replicated $d times")
  pb = STAP.randomProblem(l = l, m = m, n = n, o, d = d, Uncorrelated)
  job = pb.getJob("J1")
  private val releaseDelayInMillis: Long = 10000//1s
  private val releaseDelayInNanos: Long = releaseDelayInMillis * 1000000
  val duration: Duration = Duration.millis(releaseDelayInMillis)
  job.updateReleaseTime(pb.ds.t0.plus(duration))

  aInit = ExecutedAllocation.randomAllocation(pb)
  a0 = aInit.removeJob(job)
  a0 = a0.setDelay(pb.ds.t0)

  var additionalAllocation: ExecutedAllocation = ExecutedAllocation.randomAllocation(pb, job)
  additionalAllocation = additionalAllocation.setDelay(pb.ds.t0.plus(duration))
  println("Random allocation")
  if (verbose) println(a0)

  //Thread.sleep(5000) // wait for 5000 millisecond if demonstration is required

  consumptionSystem = system.actorOf(Props(classOf[AsynchronousDecentralizedConsumer],
    pb,
    system,
    "DecentralizedConsumer",
    dispatcherId,
    monitor,
    new SimulatedCostE(),
    false, false)
    withDispatcher dispatcherId, name = "ConsumptionSystem")

  assiSystem = system.actorOf(Props(classOf[AsynchronousDecentralizedConsumer],
    pb,
    system,
    "ASSI",
    dispatcherId,
    monitor,
    new SimulatedCostE(),
    true, false)
    withDispatcher dispatcherId, name = "SSISystem")

  rssiSystem = system.actorOf(Props(classOf[AsynchronousDecentralizedConsumer],
    pb,
    system,
    "RSSI",
    dispatcherId,
    monitor,
    new SimulatedCostE(),
    false, true)
    withDispatcher dispatcherId, name = "PSISystem")

  negotiationSystem =  system.actorOf(Props(classOf[AsynchronousAgentBasedConsumer],
    pb,
    system,
    "AgentBasedConsumer",
    GlobalFlowtime,
    ConcreteMultiProposalStrategy(),
    Some(ConcreteLiberalSingleProposalStrategy()),
    new ConcreteSingleCounterProposalStrategy,
    dispatcherId,
    monitor,
    new SimulatedCostE())
  withDispatcher dispatcherId, name = "NegotiationSystem")

  println("Consume without negotiation...")
  timer = context.actorOf(Props(classOf[Timer], releaseDelayInNanos).withDispatcher("akka.actor.default-dispatcher"), "ConsumerTimer")
  timer ! Start
  pb.ds.t0 = LocalDateTime.now()
  a0.setDelay(pb.ds.t0)
  consumptionSystem ! Init(a0)


  /**
   * Handle messages
   */
  override def receive: Receive = {
    // When the allocation is consumed
    case Outcome(consumedAe,_, _, _) if step == 1 =>
      step = 2
      cs0 = a0.simulatedMeanGlobalFlowtime / 1000 / 60
      println(s"C^S(A0):\t$cs0 mins")
      cr0 = consumedAe.realGlobalMeanFlowtime / 1000 / 60
      println(s"C^R(A0):\t$cr0 mins")
      println("Consume with bargaining...")
      timer = context.actorOf(Props(classOf[Timer], releaseDelayInNanos).withDispatcher("akka.actor.default-dispatcher"), "NegotiatorTimer")
      timer ! Start
      pb.ds.t0 = LocalDateTime.now()
      a0.setDelay(pb.ds.t0)
      negotiationSystem ! Init(a0)

    case Outcome(consumedAe, _, _, _) if step == 2 =>
      step = 3
      cs = consumedAe.simulatedMeanGlobalFlowtime / 1000 / 60
      println(s"C^S(Ae):\t$cs mins")
      cr = consumedAe.realGlobalMeanFlowtime / 1000 / 60
      println(s"C^R(AE):\t$cr mins")
      val gammaNeg = (cr0 - cr) / cr0 * 100
      println(s"GammaNEG:\t$gammaNeg %")
      println("Consume with RSSI...")
      timer = context.actorOf(Props(classOf[Timer], releaseDelayInNanos).withDispatcher("akka.actor.default-dispatcher"), "RSSITimer")
      timer ! Start
      pb.ds.t0 = LocalDateTime.now()
      a0.setDelay(pb.ds.t0)
      rssiSystem ! Init(a0)

    case Outcome(consumedAe, _, _, _) if step == 3 =>
      step = 4
      cs = consumedAe.simulatedMeanGlobalFlowtime / 1000 / 60
      println(s"C^S(Ae):\t$cs mins")
      cr = consumedAe.realGlobalMeanFlowtime / 1000 / 60
      println(s"C^R(Ae):\t$cr mins")
      val gammaPsi = (cr0 - cr) / cr0 * 100
      println(s"GammaPSI:\t$gammaPsi %")
      println("Consume with ASSI...")
      timer = context.actorOf(Props(classOf[Timer], releaseDelayInNanos).withDispatcher("akka.actor.default-dispatcher"), "ASSITimer")
      timer ! Start
      pb.ds.t0 = LocalDateTime.now()
      a0.setDelay(pb.ds.t0)
      assiSystem ! Init(a0)

    case Outcome(consumedAe,_, _, _) if step == 4 =>
      step = 5
      cs = consumedAe.simulatedMeanGlobalFlowtime / 1000 / 60
      println(s"C^S(Ae):\t$cs mins")
      cr = consumedAe.realGlobalMeanFlowtime / 1000 / 60
      println(s"C^R(AE):\t$cr mins")
      val gammaPsi = (cr0 - cr) / cr0 * 100
      println(s"GammaSSI:\t$gammaPsi %")
      system.terminate()
      sys.exit()

    // When the timeout is reached
    case Timeout if step == 1=>
      println("MASTAPlusJobRelease: job release")
      consumptionSystem ! Update(additionalAllocation)

    // When the timeout is reached
    case Timeout if step == 2 =>
      println("MASTAPlusJobRelease: job release")
      negotiationSystem ! Update(additionalAllocation)

    // When the timeout is reached
    case Timeout if step == 3 =>
      println("MASTAPlusJobRelease: job release")
      rssiSystem ! Update(additionalAllocation)

    // When the timeout is reached
    case Timeout if step == 4 =>
      println("MASTAPlusJobRelease: job release")
      assiSystem ! Update(additionalAllocation)

  }
}

/**
 * Companion object
 */
object MASTAPlusJobRelease {
  val debug: Boolean = false
  var monitor: Boolean = true
  var verbose: Boolean = true

  var argList: List[String] = List[String]()

  val system: ActorSystem = ActorSystem("HelloSystem")
  val dispatcherId = "akka.actor.default-dispatcher"
  implicit val decentralizedContext: MessageDispatcher = system.dispatchers.lookup(dispatcherId)
  var jobRelease: ActorRef = _
  def main(args: Array[String]): Unit = {
    // Drop class name in the java command line
    if (!args.isEmpty) {
      argList = if (args.head == "org.smastaplus.utils.MASTAPlusJobRelease") args.toList.drop(1) else args.toList
      parseCommandLine()
      jobRelease = system.actorOf(Props(classOf[MASTAPlusJobRelease], debug, monitor, verbose, system, dispatcherId)
        withDispatcher dispatcherId, name = "JobRelease")

    }
  }

  /**
   * Parse the command line
   */
  private def parseCommandLine(): Unit = {
    if (!nextOption(argList)) {
      println(s"ERROR MASTAPlusConsumer: options cannot be parsed ")
      System.exit(1)
    }
  }

  /**
   * Parse options at second
   *
   * @param tags is the list of options
   */
  @tailrec
  def nextOption(tags: List[String]): Boolean = {
    if (tags.isEmpty) return true
    val tag: String = tags.head.substring(1) // remove '-'
    tag match {
      case "v" =>
        if (debug) println("Verbose mode")
        verbose = true
      case "m" =>
        if (debug) println("Monitoring mode")
        monitor = true
      case _ => return false
    }
    nextOption(tags.tail)
  }
}
// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, Anne-Cécile CARON 2023
package org.smastaplus.utils

import akka.actor.ActorSystem
import akka.dispatch.MessageDispatcher
import com.typesafe.config.{Config, ConfigFactory}
import org.joda.time.LocalDateTime
import org.smastaplus.consumer.deal.mas.AgentBasedConsumer
import org.smastaplus.core._
import org.smastaplus.strategy.deal.counterproposal.single.ConcreteSingleCounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.multi.ConcreteMultiProposalStrategy
import org.smastaplus.strategy.deal.proposal.single.ConcreteLiberalSingleProposalStrategy

/**
 * Consume and negotiate a random  allocation for a random  STAP instance using :
 */
object MASTAPlusSankey extends App {
  private val decentralizedDispatcherId = "akka.actor.default-dispatcher"
  private val decentralizedSystem1: ActorSystem = ActorSystem("DecentralizedSystem1")
  private val decentralizedSystem2: ActorSystem = ActorSystem("DecentralizedSystem2")
  implicit val decentralizedContext: MessageDispatcher = decentralizedSystem1.dispatchers.lookup(decentralizedDispatcherId)

  // Load monitoring setup
  val config: Config = ConfigFactory.load()
  val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
    config.getString("path.monitoringDirectory")

  // Manage the command line argument
  var verbose: Boolean = false
  var monitor: Boolean = true

  val l = 4 // with l jobs
  val m = 8 // with m nodes, eventually 8
  val n = 32 //128 // with n tasks, eventually 128
  val o = 5 //10 // with o resources per task, eventually 10
  val d = 3 // with d instances per resource, eventually 3

  println(s"STAP with $l jobs, $m nodes, $n tasks, $o resources per tasks replicated $d times")
  val pb = STAP.randomProblem(l = l, m = m, n = n, o, d = d, Uncorrelated)
  if (verbose)  println(pb)
  private val a0 = ExecutedAllocation.randomAllocation(pb)
  println("Random allocation")
  if (verbose)  println(a0)

  /*
  println("Consume with parallel single-item auction ...")
  pb.ds.t0 = LocalDateTime.now()
  a0.setDelay(pb.ds.t0)
  private val psiSystem = new SynchronousDecentralizedConsumer(pb,
    decentralizedSystem1,
    name = "PSIConsumption",
    dispatcherId = decentralizedDispatcherId,
    monitor = true,
    ssi = false,
    psi = true)
  consumedA2 = psiSystem.consume(a0)
  decentralizedSystem1.terminate()
  */

  println("Consume with negotiation...")
  pb.ds.t0 = LocalDateTime.now()
  a0.setDelay(pb.ds.t0)
  val consumer = new AgentBasedConsumer(pb,
    GlobalFlowtime,
    ConcreteMultiProposalStrategy(),
    Some(ConcreteLiberalSingleProposalStrategy()),
    new ConcreteSingleCounterProposalStrategy,
    decentralizedSystem2,
    name = "AgentBasedConsumer",
    dispatcherId = decentralizedDispatcherId,
    monitor = true)
  consumer.trace = true
  consumer.consume(a0)

  sys.exit()

}
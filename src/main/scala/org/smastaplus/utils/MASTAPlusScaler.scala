// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.utils

import org.smastaplus.provisioning.{Configuration, QoSFlowtime, QoSMakespan}
import org.smastaplus.scaler.ReScaler
import org.smastaplus.scaler.dual.EDCOPScaler
import org.smastaplus.utils.serialization.core.{AllocationParser, AllocationWriter}
import org.smastaplus.utils.serialization.provisioning.ProvisioningProblemParser

import java.nio.file.{Files, Paths}

/**
  * Solve a particular provisioning problem instance using :
  *
  * - either  sbt "run org.smastaplus.utils.MASTAPlusScaler -v examples/provisioning/ex4Elasticity.txt examples/allocation/stap4/ex4AllocationWith4Nodes.txt provisioning.txt"
  *
  * - or java -jar smastaplus-assembly-X.Y.jar org.smastaplus.utils.MASTAPlusScaler -v examples/provisioning/ex4Elasticity.txt examples/allocation/stap4/ex4AllocationWith4Nodes.txt provisioning.txt
  *
  */
object MASTAPlusScaler extends App {

  val debug = true

  val usage =
    """
    Usage: java -jar smastasplus-assembly-X.Y.jar [-v] problemFilename allocationFilename outputFilename
    The following options are available:
    -v: verbose (false by default)
  """

  // Default parameters for the balancer
  var verbose = false
  var dcop : Boolean = true

  // Default fileNames/path for the input/output
  private var problemFilename = new String()
  private var allocationFilename = new String()
  var outputFilename = new String()

  if (args.length <= 3) {
    println(usage)
    System.exit(1)
  }

  // drop class name in the java command line
  var argList = if (args.head == "org.smastaplus.utils.MASTAPlusScaler") args.toList.drop(1) else args.toList
  parseFilenames() // parse filenames
  if (!nextOption(argList)) {
    println(s"ERROR MASTAPlusScaler: options cannot be parsed $argList")
    System.exit(1)
  }
  if (verbose) {
    println(s"problemFilename:$problemFilename")
    println(s"allocationFile:$allocationFilename")
    println(s"output:$outputFilename")
  }

  // fail if options cannot be parsed
  private val problemParser = new ProvisioningProblemParser(problemFilename)
  val problem = problemParser.parse() // parse problem
  private val allocationParser = new AllocationParser(problem.stap, allocationFilename)
  val allocation = allocationParser.parse() // parse allocation
  val configuration = new Configuration(problem, allocation.loadedNodes, allocation)

  val scaler : ReScaler = selectReScaler // select scaler
  if (verbose){
    scaler.debug = true
  }
  if (verbose) println(problem)
  if (debug) println(
    s"""
    Run scaler with the following parameters:

    verbose:$verbose
    dcop:$dcop
  """)

  scaler.run(configuration) match{
    case None =>
      println(s"$scaler rescales none allocation")
    case Some(configuration) =>
        println(s"$scaler rescales the allocation:\n${configuration.allocation}")
        println(s"Is the configuration just-in-need ? ${configuration.isJustInNeed} since\n" +
          s"minThreshold= ${problem.minThreshold},\n" +
          s"maxThreshold= ${problem.maxThreshold} and")
        problem.rule match {
          case QoSMakespan => println(s"Makespan= ${configuration.allocation.makespan}")
          case QoSFlowtime  => println(s"GlobalFlowtime= ${configuration.allocation.meanGlobalFlowtime}")
          case rule =>
            println(s"ERROR MASTAPlusScaler: rule cannot be parsed $rule")
            System.exit(1)
      }
      println(s"Active nodes: ${configuration.nodes}")
      val writer = new AllocationWriter(outputFilename, allocation)
      writer.write()
  }
  System.exit(0)

  /**
    * Parse filenames at first
    */
  def parseFilenames(): Unit = {
    outputFilename = argList.last.trim
    argList = argList.dropRight(1) // drop outputFile
    allocationFilename = argList.last.trim
    argList = argList.dropRight(1) //drop allocationFile
    problemFilename = argList.last.trim
    argList = argList.dropRight(1) //drop inputFile
    if (!Files.exists(Paths.get(problemFilename)) || !Files.exists(Paths.get(allocationFilename)) || Files.exists(Paths.get(outputFilename))) {
      println(s"ERROR parseFilename: either $problemFilename does not exist or $allocationFilename does not exist or $outputFilename already exist")
      System.exit(1)
    }
  }

  /**
    * Parse options at second
    * @param tags is the list of options
    */
  @scala.annotation.tailrec
  def nextOption(tags: List[String]): Boolean = {
    if (tags.isEmpty) return true
    val tag: String = tags.head.substring(1) // remove '-'
    tag match {
      case "v" =>
        if (debug) println("Verbose mode")
        verbose = true
      case _ => return false
    }
    nextOption(tags.tail)
  }

  /**
    * Returns the rescaler for the stap instance pb
    */
  private def selectReScaler: ReScaler = {
    new EDCOPScaler(problem)
  }
}
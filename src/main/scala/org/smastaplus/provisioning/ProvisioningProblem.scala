//Copyright (C) Maxime MORGE, 2022
package org.smastaplus.provisioning

import org.smastaplus.core.STAP

//import org.smastaplus.core.ComputingNode
//import scala.collection.SortedSet


/**
  * The provisioning problem consists of finding a minimal configuration,
  * i.e. a just-in-need configuration such that there is no computing node
  * which can be deactivated without being under-provisioning
  * @param stap instance
  * @param maxThreshold is the the maximum threshold
  * @param minThreshold is the the minimum threshold
  * @param rule is the QoS rule (QoSFlowtime or QoSMakespan)
  */
class ProvisioningProblem(val stap: STAP,
                          val maxThreshold: Double,
                          val minThreshold: Double,
                          val rule : QoS = QoSFlowtime) {

  /**
    * Returns a string describing the provisioning problem
    */
  override def toString: String = {
    s"$stap\n" +
      s"maxThreshold: $maxThreshold\n" +
      s"minThreshold: $minThreshold\n" +
      s"rule: $rule\n"
  }

  /**
    * Returns all the potential configurations
    */
  def allConfigurations: Set[Configuration] = {
    stap.ds.computingNodes.subsets().filter(_.nonEmpty).flatMap { nodes =>
      stap.allAllocations(nodes).map { allocation =>
        new Configuration(this, nodes, allocation)
      }
    }.toSet
  }

  /**
    * Returns all the potential just-in-need configurations
    */
  def allJustInNeedConfigurations: Set[Configuration] = {
    allConfigurations.filter(_.isJustInNeed)
  }
}

  /**
  * Companion object for alternative  constructor
  */
object ProvisioningProblem {
  /**
    * Builder where minThreshold = maxThreshold/3
    * @param stap instance
    * @param maxThreshold is the the maximum threshold
    */
  def apply(stap: STAP, maxThreshold: Double, rule : QoS = QoSFlowtime) : ProvisioningProblem =
    new ProvisioningProblem(stap, maxThreshold, maxThreshold/3, rule)
}
//Copyright (C) Maxime MORGE, 2022
package org.smastaplus.provisioning

import org.smastaplus.core.{Allocation, ComputingNode}
import org.smastaplus.utils.MathUtils._
import org.smastaplus.utils.RandomUtils

import scala.annotation.unused
import scala.collection.SortedSet

/**
  * A configuration (at time t) represents a specific task allocation
  * for a non-empty set of computing nodes
  * @param problem is the provisioning problem
  * @param nodes are the computing nodes which are active
  * @param allocation is the task distribution over the active nodes
  */
class Configuration(val problem: ProvisioningProblem,
                    val nodes: SortedSet[ComputingNode],
                    val allocation: Allocation) {
  val debug = true

  /**
    * Returns a string describing the configuration
    */
  override def toString: String = {
    s"$problem\n" +
      s"active nodes: " + nodes.mkString(", ") + "\n" +
      s"allocation:\n$allocation"
  }

  override def equals(that: Any): Boolean =
    that match {
      case that: Configuration => that.canEqual(this) &&
        this.nodes == that.nodes &&
        this.allocation == that.allocation
      case _ => false
    }
  def canEqual(a: Any): Boolean = a.isInstanceOf[Configuration]

  /**
    * A configuration is sound if only the active agents are loaded
    */
  def isSound: Boolean =
    allocation.loadedNodes.subsetOf(nodes)

  /**
    * A configuration is under-provisioning if
    * the QoS is not reached
    */
  def isUnderProvisioning: Boolean = problem.rule match {
    case QoSMakespan => allocation.makespan ~> problem.maxThreshold
    case QoSFlowtime => allocation.meanGlobalFlowtime ~> problem.maxThreshold
    case rule => throw new RuntimeException(s"Configuration: QoS rule $rule was not expected")
  }

  /**
    * A configuration is over-provisioning if
    * not all the computing nodes are useful
    */
  def isOverProvisioning: Boolean = problem.rule match {
    case QoSMakespan => allocation.makespan ~< problem.minThreshold
    case QoSFlowtime => allocation.meanGlobalFlowtime ~< problem.minThreshold
    case rule => throw new RuntimeException(s"Configuration: QoS rule $rule was not expected")
  }

  /**
    * A configuration is just-in-need if
    * there is no computing node which can be deactivated without being under-provisioning
    */
    @throws
  def isJustInNeed: Boolean = problem.rule match {
    case QoSMakespan =>
      (problem.minThreshold ~< allocation.makespan) &&
        (allocation.makespan ~< problem.maxThreshold)
    case QoSFlowtime =>
      (problem.minThreshold ~< allocation.meanGlobalFlowtime) &&
        (allocation.meanGlobalFlowtime ~< problem.maxThreshold)
    case rule => throw new RuntimeException(s"Configuration: QoS rule $rule was not expected")
  }

  /**
    * A configuration, is minimal if
    * this is a just-in-need configuration such that
    * there is no computing node which can be deactivated without being under-provisioning.
    *
    */
  @unused
  def isMinimal: Boolean = {
    if (!isJustInNeed) return false
    val activeNodes = allocation.loadedNodes
    activeNodes.foreach{ removedNodes =>
      val nodesSubset = activeNodes.filter(_ == removedNodes)
      allocation.stap.allAllocations(nodesSubset).foreach { a =>
        val qos = problem.rule match {
          case QoSMakespan => a.makespan
          case QoSFlowtime => a.meanGlobalFlowtime
          case rule => throw new RuntimeException(s"Configuration: QoS rule $rule was not expected")
        }
        if (qos ~<= problem.maxThreshold) {
          if (debug) println(s"The following allocation with the active nodes $nodesSubset is not under-provisioning $a")
          return false
        }
      }
    }
    true
  }
}

/**
  * Factory for [[Configuration]] instances
  */
object Configuration {
  val debug = false

  /**
    * Generate a random configuration where all the nodes are actives
    */
  @unused def randomFullConfiguration(pb: ProvisioningProblem): Configuration =
    randomConfiguration(pb: ProvisioningProblem,  pb.stap.ds.computingNodes)

  /**
    * Generate a random configuration
    */
  def randomConfiguration(pb: ProvisioningProblem): Configuration = {
    val nonEmptySubSetOfNodes : Set[SortedSet[ComputingNode]] = pb.stap.ds.computingNodes.subsets().filter(_.nonEmpty).toSet
    val nodes = RandomUtils.random[SortedSet[ComputingNode]](nonEmptySubSetOfNodes)
    randomConfiguration(pb: ProvisioningProblem,  nodes)
  }

  /**
    * Generate a random allocation with a set of active nodes
    */
  def randomConfiguration(pb: ProvisioningProblem, activeNodes : SortedSet[ComputingNode] ): Configuration =
    new Configuration(pb, activeNodes, Allocation.randomAllocation(pb.stap, activeNodes))
}

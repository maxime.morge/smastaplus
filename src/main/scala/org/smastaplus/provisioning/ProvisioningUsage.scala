// Copyright (C) Maxime MORGE 2022
package org.smastaplus.provisioning

/**
 *  A node agent is active, suspended when it is currently dispatching its bundle, or inactive.
 */
class ProvisioningUsage{
  @throws(classOf[RuntimeException])
  override def toString: String = this match{
    case Active => "active"
    case Inactive => "inactive"
    case Suspended => "suspended"
    case usage => throw new RuntimeException(s"ERROR : Provisioning usage $usage is neither active nor inactive nor suspended")
  }

  /**
    * Returns true if the provisioning usage is active
    * @return
    */
  def isActive: Boolean = this match{
    case Active => true
    case Inactive => false
    case Suspended => false
    case usage => throw new RuntimeException(s"ERROR : Provisioning usage $usage is neither active nor inactive nor suspended")
  }
}
case object Active extends ProvisioningUsage
case object Inactive extends ProvisioningUsage
case object Suspended extends  ProvisioningUsage

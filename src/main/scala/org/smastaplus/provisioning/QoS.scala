// Copyright (C) Maxime MORGE 2022
package org.smastaplus.provisioning

/**
 * Class representing a Quality of Service (QoS) rule
 */
class QoS{
  @throws(classOf[RuntimeException])
  override def toString: String = this match{
    case QoSMakespan => "makespan"
    case QoSFlowtime => "flowtime"
    case rule => throw new RuntimeException(s"ERROR : QoS rule $rule is neither makespan nor flowtime")
  }
}
case object QoSFlowtime extends QoS
case object QoSMakespan extends QoS
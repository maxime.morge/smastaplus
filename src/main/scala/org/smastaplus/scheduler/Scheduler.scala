// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.scheduler

import org.smastaplus.core._
import org.smastaplus.utils.MyTime

/**
  * Scheduler which sorts the bundle according to consumption strategy
  */
class Scheduler() {
  var debug = false

  var solvingTime : Long = 0

  override def toString: String = s"Scheduler : ${MyTime.show(solvingTime)}"

  /**
    * Returns an allocation and update solving time
    */
  def schedule(allocation: Allocation): Allocation = {
    val startingTime = System.nanoTime()
    val result = allocation.sort()
    solvingTime = System.nanoTime() - startingTime
    if (result.isSound) allocation
    else throw new RuntimeException(s"Solver: the outcome\n $allocation\nis not sound for\n ${allocation.stap}")
  }
}

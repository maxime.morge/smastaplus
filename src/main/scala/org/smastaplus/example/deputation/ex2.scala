// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
package org.smastaplus.example.deputation

import org.smastaplus.deputation.Deputation
import org.smastaplus.example.allocation.stap2.ex2Allocation._

/**
  * Deputation example for the second allocation of the second stap
  */
object ex2 {
  val deputation : Deputation = Deputation.stap2deputation(a)
}

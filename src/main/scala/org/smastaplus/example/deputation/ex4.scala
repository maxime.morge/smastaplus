// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
package org.smastaplus.example.deputation

import org.smastaplus.core._
import org.smastaplus.deputation.Deputation
import org.smastaplus.example.stap.ex3._

/**
  * Deputation example for the third stap
  */
object ex4 {
  val a = new ExecutedAllocation(stap)
  a.bundle += ( cn1 -> List(t4, t7, t14, t17) )
  a.bundle += ( cn2 -> List(t8, t5, t2, t1, t18, t15, t12, t11) )
  a.bundle += ( cn3 -> List(t9, t3, t6, t10, t13, t16) )

  val deputation : Deputation = Deputation.stap2deputation(a)
}

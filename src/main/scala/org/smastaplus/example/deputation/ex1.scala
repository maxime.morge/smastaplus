// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
package org.smastaplus.example.deputation

import org.smastaplus.deputation.Deputation
import org.smastaplus.example.allocation.stap1.ex1Allocation._

/**
  * Deputation example for the first allocation of the first stap
  */
object ex1 {
  val deputation : Deputation = Deputation.stap2deputation(a)
}

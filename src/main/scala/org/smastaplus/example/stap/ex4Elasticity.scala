// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022
package org.smastaplus.example.stap

import org.smastaplus.core._

import scala.collection.SortedSet

/**
  *  Example for Elasticity paper
  */
object ex4Elasticity{
  val cn1 = new ComputingNode("cn1")
  val cn2 = new ComputingNode("cn2")
  val cn3 = new ComputingNode("cn3")
  val cn4 = new ComputingNode("cn4")
  val cn5 = new ComputingNode("cn5")
  val cn6 = new ComputingNode("cn6")
  val computingNodes : SortedSet[ComputingNode] = SortedSet(cn1, cn2, cn3, cn4, cn5, cn6)
  val rn1 = new ResourceNode("rn1")
  val rn2 = new ResourceNode("rn2")
  val rn3 = new ResourceNode("rn3")
  val rn4 = new ResourceNode("rn4")
  val rn5 = new ResourceNode("rn5")
  val rn6 = new ResourceNode("rn6")
  val resourcesNodes : SortedSet[ResourceNode] = SortedSet(rn1, rn2, rn3, rn4, rn5, rn6)

  val r1 = new Resource("r1", 5.0)
  val r2 = new Resource("r2", 3.0)
  val r3 = new Resource("r3", 1.0)
  val r4 = new Resource("r4", 4.0)
  val r5 = new Resource("r5", 2.0)
  val r6 = new Resource("r6",  5.0)
  val r7 = new Resource("r7", 2.0)
  val r8 = new Resource("r8", 2.0)
  val r9 = new Resource("r9", 4.0)
  val location = Map(
    r1 -> Set(rn1, rn4),
    r2 -> Set(rn1, rn6),
    r3 -> Set(rn3, rn5),
    r4 -> Set(rn2, rn4),
    r5 -> Set(rn1, rn6),
    r6 -> Set(rn3, rn6),
    r7 -> Set(rn2, rn5),
    r8 -> Set(rn2, rn5),
    r9 -> Set(rn3, rn6)
  )
  val resources : SortedSet[Resource] = SortedSet(r1, r2, r3, r4, r5, r6, r7, r8, r9)
  val acquaintance = Map(
    (cn1,rn1) -> true, (cn2,rn2) -> true, (cn3,rn3) -> true, (cn4, rn4) -> true, (cn5, rn5) -> true, (cn6, rn6) -> true)
  val ds = new DistributedSystem(computingNodes, resourcesNodes, acquaintance, resources, location)

  val t1 = new Task("t1", Set(r1))
  val t2 = new Task("t2", Set(r2))
  val t3 = new Task("t3", Set(r3))
  val t4 = new Task("t4", Set(r4))
  val t5 = new Task("t5", Set(r5))
  val t6 = new Task("t6", Set(r6))
  val t7 = new Task("t7", Set(r7))
  val t8 = new Task("t8", Set(r8))
  val t9 = new Task("t9", Set(r9))
  val tasks : SortedSet[Task] = SortedSet(t1, t2, t3, t4, t5, t6, t7, t8, t9)
  val j1 = new Job("J1", Set(t1, t2, t3), ds.t0)
  val j2 = new Job("J2", Set(t4, t5, t6), ds.t0)
  val j3 = new Job("J3", Set(t7, t8, t9), ds.t0)
  val jobs: SortedSet[Job] = SortedSet[Job](j1,j2,j3)

  val stap = new STAP(ds, jobs, tasks)
}


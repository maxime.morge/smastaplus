// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.stap

import org.smastaplus.core._

import scala.collection.SortedSet

/**
  * Minimal STAP example
  */
object exMinimal{
  val cn1 = new ComputingNode("cn1")
  val cn2 = new ComputingNode("cn2")
  val computingNodes : SortedSet[ComputingNode] = SortedSet(cn1, cn2)
  val rn1 = new ResourceNode("rn1")
  val rn2 = new ResourceNode("rn2")
  val resourcesNodes : SortedSet[ResourceNode] = SortedSet(rn1, rn2)
  val r1 = new Resource("r1", 6.0)
  val r2 = new Resource("r2", 5.0)
  val location = Map(
    r1 -> Set(rn1),
    r2 -> Set(rn2)
  )
  val resources : SortedSet[Resource] = SortedSet(r1, r2)
  val acquaintance = Map( (cn1,rn1) -> true, (cn2,rn2) -> true)
  val ds = new DistributedSystem(computingNodes, resourcesNodes, acquaintance, resources, location)

  val t1 = new Task("t1", Set(r1))
  val t2 = new Task("t2", Set(r2))
  val tasks : SortedSet[Task] = SortedSet(t1, t2)
  val j1 = new Job("J1", Set(t1), ds.t0)
  val j2 = new Job("J2", Set(t2), ds.t0)
  val jobs: SortedSet[Job] = SortedSet[Job](j1,j2)

  val stap = new STAP(ds, jobs, tasks)
}

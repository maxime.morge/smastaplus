// Copyright (C) Maxime MORGE, Luc BIGAND 2021, 2022
package org.smastaplus.example.stap

import org.smastaplus.core._

import scala.collection.SortedSet

/**
  *  STAP example
  */
object ex4with4Jobs{
  val cn1 = new ComputingNode("cn1")
  val cn2 = new ComputingNode("cn2")
  val cn3 = new ComputingNode("cn3")
  val computingNodes : SortedSet[ComputingNode] = SortedSet(cn1, cn2, cn3)
  val rn1 = new ResourceNode("rn1")
  val rn2 = new ResourceNode("rn2")
  val rn3 = new ResourceNode("rn3")
  val resourcesNodes : SortedSet[ResourceNode] = SortedSet(rn1, rn2, rn3)
  val r1 = new Resource("r1", 5.0)
  val r2 = new Resource("r2", 3.0)
  val r3 = new Resource("r3", 1.0)
  val r4 = new Resource("r4", 4.0)
  val r5 = new Resource("r5", 2.0)
  val r6 = new Resource("r6",  5.0)
  val r7 = new Resource("r7", 2.0)
  val r8 = new Resource("r8", 2.0)
  val r9 = new Resource("r9", 4.0)
  val r10 = new Resource("r10", 3.0)
  val r11 = new Resource("r11", 1.0)
  val r12 = new Resource("r12", 4.0)
  val location = Map(
    r1 -> Set(rn1, rn3),
    r2 -> Set(rn1, rn2),
    r3 -> Set(rn1, rn3),
    r4 -> Set(rn2, rn3),
    r5 -> Set(rn1, rn2),
    r6 -> Set(rn2, rn3),
    r7 -> Set(rn2, rn3),
    r8 -> Set(rn1, rn2),
    r9 -> Set(rn1, rn3),
    r10 -> Set(rn2, rn3),
    r11 -> Set(rn1, rn3),
    r12 -> Set(rn1, rn2)
  )
  val resources : SortedSet[Resource] = SortedSet(r1, r2, r3, r4, r5, r6, r7, r8, r9)
  val acquaintance = Map( (cn1,rn1) -> true, (cn2,rn2) -> true, (cn3,rn3) -> true)
  val ds = new DistributedSystem(computingNodes, resourcesNodes, acquaintance, resources, location)

  val t1 = new Task("t1", Set(r1))
  val t2 = new Task("t2", Set(r2))
  val t3 = new Task("t3", Set(r3))
  val t4 = new Task("t4", Set(r4))
  val t5 = new Task("t5", Set(r5))
  val t6 = new Task("t6", Set(r6))
  val t7 = new Task("t7", Set(r7))
  val t8 = new Task("t8", Set(r8))
  val t9 = new Task("t9", Set(r9))
  val t10 = new Task("t10", Set(r10))
  val t11 = new Task("t11", Set(r11))
  val t12 = new Task("t12", Set(r12))
  val tasks : SortedSet[Task] = SortedSet(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11,t12)
  val j1 = new Job("J1", Set(t1, t2, t3), ds.t0)
  val j2 = new Job("J2", Set(t4, t5, t6), ds.t0)
  val j3 = new Job("J3", Set(t7, t8, t9), ds.t0)
  val j4 = new Job("J4", Set(t10, t11, t12), ds.t0)
  val jobs: SortedSet[Job] = SortedSet[Job](j1,j2,j3,j4)

  val stap = new STAP(ds, jobs, tasks)
}

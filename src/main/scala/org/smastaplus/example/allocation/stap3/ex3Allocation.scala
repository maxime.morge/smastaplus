// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap3

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation for the third example
  */
object ex3Allocation {

  import org.smastaplus.example.stap.ex3._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t7, t4))
  a.bundle += (cn2 -> List(t8, t5, t2, t1))
  a.bundle += (cn3 -> List(t9, t3, t6))

}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stapMinimal

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation example for the minimals STAP example
  */
object exMinimalAllocation {

  import org.smastaplus.example.stap.exMinimal._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t1))
  a.bundle += (cn2 -> List(t2))
}

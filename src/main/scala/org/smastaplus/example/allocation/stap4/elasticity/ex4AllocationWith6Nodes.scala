// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022
package org.smastaplus.example.allocation.stap4.elasticity

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation example for elasticity with 6 active nodes
  */
object ex4AllocationWith6Nodes {

  import org.smastaplus.example.stap.ex4Elasticity._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t3, t2))
  a.bundle += (cn2 -> List(t8, t4))
  a.bundle += (cn3 -> List(t9))
  a.bundle += (cn4 -> List(t1))
  a.bundle += (cn5 -> List(t7, t5))
  a.bundle += (cn6 -> List(t6))
}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.jobRelease

import org.smastaplus.core.ExecutedAllocation

/**
  * Initial allocation for the fourth STAP example with job release
  */
object ex4Stable {

  import org.smastaplus.example.stap.ex4._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t3, t8, t9))
  a.bundle += (cn2 -> List(t7, t2, t5, t4))
  a.bundle += (cn3 -> List(t1, t6))

}

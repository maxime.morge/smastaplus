// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.swap

import org.smastaplus.core.ExecutedAllocation
import org.smastaplus.process.SingleDelegation

/**
  * Allocation example after a delegation
  */
object ex5SwapAfterDelegation {

  import org.smastaplus.example.stap.ex4._

  var a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t2, t5, t6))
  a.bundle += (cn2 -> List(t4, t7, t8))
  a.bundle += (cn3 -> List(t9, t3, t1))
  val delegation = new SingleDelegation(stap, cn1, cn2, t6)
  a = delegation.execute(a)
}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4

import org.smastaplus.core.ExecutedAllocation

/**
  * First allocation for the fourth STAP example
  * with no social rational delegation but with a socially rational swap
  */
object ex4Slides_AfterConsumption {

  import org.smastaplus.example.stap.ex4Slides._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t3, t2, t5))
  a.bundle += (cn2 -> List(t4, t9))
  a.bundle += (cn3 -> List(t7, t1, t6))

}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022
package org.smastaplus.example.allocation.stap4.elasticity

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation example for elasticity with 3 active nodes
  */
object ex4AllocationWith3Nodes {

  import org.smastaplus.example.stap.ex4Elasticity._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t5, t2, t7, t8))
  a.bundle += (cn2 -> List(t4, t9))
  a.bundle += (cn3 -> List(t6, t3, t1))
  a.bundle += (cn4 -> List())
  a.bundle += (cn5 -> List())
  a.bundle += (cn6 -> List())
}

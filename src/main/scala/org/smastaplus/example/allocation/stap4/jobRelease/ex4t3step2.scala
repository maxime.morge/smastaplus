// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.jobRelease

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation for the fourth STAP example with job release
  * after the arrival of the job J4 when tasks 11 and 10 are already delegated to executing agents
  */
object ex4t3step2 {

  import org.smastaplus.example.stap.ex4withDispatcher._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t3, t8, t9))
  a.bundle += (cn2 -> List(t7, t2, t5, t4, t10))
  a.bundle += (cn3 -> List(t1, t6, t11))
  a.bundle += (cn4 -> List(t12))

}

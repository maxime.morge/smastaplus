// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.swap

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation example for testing swap
  */
object ex5SwapB {

  import org.smastaplus.example.stap.ex4._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t5, t3, t2))
  a.bundle += (cn2 -> List(t7, t8, t4, t6))
  a.bundle += (cn3 -> List(t9, t1))

}

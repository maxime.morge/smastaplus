// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.swap

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation example for testing swap
  */
object ex5SwapC {

  import org.smastaplus.example.stap.ex4._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t2, t4))
  a.bundle += (cn2 -> List(t7, t8, t5, t6))
  a.bundle += (cn3 -> List(t9, t3, t1))
}

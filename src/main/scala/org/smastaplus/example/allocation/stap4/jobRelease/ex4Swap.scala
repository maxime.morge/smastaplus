// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.jobRelease

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation for the fourth STAP example
  */
object ex4Swap {

  import org.smastaplus.example.stap.ex4._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t5, t8, t3, t2))
  a.bundle += (cn2 -> List(t4, t6))
  a.bundle += (cn3 -> List(t1, t7, t9))

}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation after the swap σ((τ9), (τ5) (ν2, ν1, A))
  */
object ex4Slides_AfterSwap {

  import org.smastaplus.example.stap.ex4Slides._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t3, t2, t8, t9))
  a.bundle += (cn2 -> List(t5, t4))
  a.bundle += (cn3 -> List(t7, t1, t6))

}

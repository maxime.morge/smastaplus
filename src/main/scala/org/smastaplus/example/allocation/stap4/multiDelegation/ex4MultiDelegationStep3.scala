// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.multiDelegation

import org.smastaplus.core.ExecutedAllocation

/**
  * Third step allocation of the multi-delegation scenario
  */
object ex4MultiDelegationStep3 {

  import org.smastaplus.example.stap.ex4._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t5, t9, t3, t2, t1))
  a.bundle += (cn2 -> List(t7, t8))
  a.bundle += (cn3 -> List(t4, t6))
}

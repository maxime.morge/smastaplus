// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation after the delegation δ((τ9), ν2, ν1, A))
  */
object ex4AfterDelegation {

  import org.smastaplus.example.stap.ex4._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t5, t3, t2, t8, t9))
  a.bundle += (cn2 -> List(t4))
  a.bundle += (cn3 -> List(t7, t1, t6))

}

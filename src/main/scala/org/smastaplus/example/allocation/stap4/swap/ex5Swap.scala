// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.swap

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation example for swap illustration
  */
object ex5Swap {

  import org.smastaplus.example.stap.ex4._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t8, t2, t5, t6))
  a.bundle += (cn2 -> List(t3, t4))
  a.bundle += (cn3 -> List(t1, t7, t9))
}

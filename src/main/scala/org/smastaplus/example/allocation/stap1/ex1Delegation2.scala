// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap1

import org.smastaplus.core.ExecutedAllocation

/**
  * Second delegation for the first example of STA1P
  */
object ex1Delegation2 {

  import org.smastaplus.example.stap.ex1._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t7, t1))
  a.bundle += (cn2 -> List(t4, t5, t8, t2))
  a.bundle += (cn3 -> List(t3, t6, t9))

}

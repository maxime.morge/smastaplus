// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap2

import org.smastaplus.core.ExecutedAllocation

/**
  * First allocation for the "Locally Cheap Job First" consumption strategy
  * for the second example of STAP
  */
object ex2lcjf {

  import org.smastaplus.example.stap.ex2._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t1, t4, t7))
  a.bundle += (cn2 -> List(t5, t8, t2))
  a.bundle += (cn3 -> List(t3, t9, t6))

}

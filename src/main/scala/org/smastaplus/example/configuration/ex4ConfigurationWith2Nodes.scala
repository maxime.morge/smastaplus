// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022
package org.smastaplus.example.configuration

import org.smastaplus.provisioning.Configuration

import scala.collection.SortedSet

/**
  *  Configuration example with 2 nodes for Elasticity paper
  */
object ex4ConfigurationWith2Nodes {
  import org.smastaplus.example.stap.ex4Elasticity._
  import org.smastaplus.example.provisioning.ex4Elasticity._
  import org.smastaplus.example.allocation.stap4.elasticity.ex4AllocationWith2Nodes
  val ex4ConfigurationWith2Nodes = new Configuration(problem, SortedSet(cn1, cn3), ex4AllocationWith2Nodes.a)
}

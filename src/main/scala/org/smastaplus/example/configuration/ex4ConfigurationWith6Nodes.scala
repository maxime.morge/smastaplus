// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022
package org.smastaplus.example.configuration

import org.smastaplus.provisioning.Configuration

import scala.collection.SortedSet

/**
  *  Configuration example with 6 nodes for Elasticity paper
  */
object ex4ConfigurationWith6Nodes {
  import org.smastaplus.example.allocation.stap4.elasticity.ex4AllocationWith6Nodes
  import org.smastaplus.example.provisioning.ex4Elasticity._
  import org.smastaplus.example.stap.ex4Elasticity._
  val ex4ConfigurationWith6Nodes = new Configuration(problem,  SortedSet(cn1,cn2, cn3, cn4, cn5, cn6), ex4AllocationWith6Nodes.a)
}

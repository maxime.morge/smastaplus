// Copyright (C) Maxime MORGE, Luc BIGAND 2020, 2021
package org.smastaplus.process

import org.smastaplus.core._

import scala.annotation.unused

/**
  * Deal (i.e. a transaction) between contractors (i.e. computing nodes involved)
  * which exchanges some endowments (i.e. lists of tasks) within a stap instance
  */
abstract class Deal(val stap: STAP,
                    @unused val contractors: List[ComputingNode],
                    @unused val endowments: List[List[Task]])
  extends Operation {

  /**
    * Returns the allocation the deal with respect to the consumption strategy
    */
  def execute(allocation: Allocation): Allocation
  def execute(allocation: ExecutedAllocation): ExecutedAllocation
}

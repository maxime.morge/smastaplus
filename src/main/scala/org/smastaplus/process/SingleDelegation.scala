// Copyright (C) Maxime MORGE 2020
package org.smastaplus.process

import org.smastaplus.core._

/**
  * Single task delegation
  * @param stap instance
  * @param donor computing node which gives the task
  * @param recipient computing node which takes the task
  * @param task which is delegated
  */
class SingleDelegation(stap: STAP,
                       donor: ComputingNode,
                       recipient: ComputingNode,
                       val task: Task) extends
Delegation(stap, donor, recipient, List[Task](task))  {

  override def toString: String = s"δ($donor,$recipient,$endowment)"

  override def equals(that: Any): Boolean =
    that match {
      case that: SingleDelegation => that.canEqual(this) &&
        this.donor == that.donor &&
        this.recipient == that.recipient &&
        this.task == that.task
      case _ => false
    }
  override def canEqual(a: Any): Boolean = a.isInstanceOf[SingleDelegation]
}
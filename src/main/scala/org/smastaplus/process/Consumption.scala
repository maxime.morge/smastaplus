// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.process

import org.smastaplus.core._
import org.smastaplus.strategy.consumption.Mind

import scala.annotation.unused

/**
  * Task consumption
 *
  * @param stap instance
  * @param executor computing node processing the task
 */
@unused
class Consumption(val stap: STAP,
                  val executor: ComputingNode) extends Operation {

  /**
    * Returns the stap instance and the allocation
    * according to the "Locally Cheapest Job First" (LCJF) consumption strategy
    */
  @unused
  def execute(allocation: Allocation): (STAP,Allocation) = {
    val mind = Mind(stap, executor, allocation) // The mind of the agent supervising the node
    if (mind.isEmptyBundle)
      throw new RuntimeException(s"Consumption>$executor cannot execute the next task of an empty bundle")
    val (performedTask, updatedMind)= mind.consume

    // Update the problem
    val performedJob = stap.jobOf(performedTask)
    val distributedSystem = stap.ds
    val remainingTasks = stap.tasks.filter(_ != performedTask)
    val updatedJobs = if (stap.tasksOf(remainingTasks.toSet, performedJob ).isEmpty)
      stap.jobs.filter(_ != performedJob)
    else stap.jobs.diff(Set(performedJob)).union(Set(performedJob.remove(performedTask)))
    val updatedPb= new STAP(distributedSystem, updatedJobs, remainingTasks)

    // Update the allocation
    var updatedAllocation = new Allocation(updatedPb)
    updatedAllocation.bundle = allocation.bundle
    updatedAllocation = updatedAllocation.update(executor, updatedMind.sortedTasks)
   (updatedPb,updatedAllocation)
  }
}

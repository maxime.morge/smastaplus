// Copyright (C) Maxime MORGE, Luc BIGAND 2020, 2021
package org.smastaplus.process

import org.smastaplus.core._
import org.smastaplus.strategy.consumption.Mind
import org.smastaplus.utils.MathUtils._

import scala.collection.SortedSet
import math.max

/**
  * Multiple swap between an initiator and a responder (computing nodes involved)
  * which exchanges an endowment and counterparts reciprocally
  */
class Swap(stap: STAP,
           val initiator: ComputingNode,
           val responder: ComputingNode,
           val endowment: List[Task],
           val counterparts: List[Task])
  extends Deal(stap, List(initiator, responder), List(endowment, counterparts)) with RationalDeal {

  override def equals(that: Any): Boolean =
    that match {
      case that: Swap => that.canEqual(this) &&
        this.initiator == that.initiator &&
        this.responder == that.responder &&
        this.endowment == that.endowment &&
        this.counterparts == that.counterparts
      case _ => false
    }

  def canEqual(a: Any): Boolean = a.isInstanceOf[Swap]


  /**
    * Returns the delegation from which the swap is a counter-proposal
    */
  def initialDelegation() : Delegation = new Delegation(stap, initiator, responder, endowment)


  /**
    * Returns the jobs of the initiator in an allocation
    */
  private def jobsOfInitiator(allocation: Allocation) : SortedSet[Job] =
    SortedSet(allocation.bundle(initiator).map(task => stap.jobOf(task)): _*)

  /**
    * Returns the jobs of the responder in an allocation
    */
  private def jobsOfResponder(allocation: Allocation) : SortedSet[Job] =
    SortedSet(allocation.bundle(responder).map(task => stap.jobOf(task)): _*)

  /**
    * Returns the jobs of the participants in an allocation
    */
  def jobs(allocation: Allocation)  : SortedSet[Job] =
    jobsOfInitiator(allocation) ++ jobsOfResponder(allocation)

  /**
    * Returns the completion time of a job in an allocation over the contractors
    */
  private def completionTime4Contactors(job : Job, allocation : Allocation) : Double =
    job.tasks.foldLeft(0.0){ (maxCompletionTime,task) =>
      Math.max(completionTime4Contractors(task,allocation),maxCompletionTime)
    }

  /**
    * Returns the completion time of a task in an allocation over the contractors
    */
  private def completionTime4Contractors(task: Task, allocation : Allocation) : Double = {
    val node =  allocation.node(task).get
    if  ( node  == initiator  || node  == responder)
      return allocation.delay(task,node) + stap.cost(task, node)
    0.0
  }

  /**
    * @see org.smastaplus.process.Deal#execute
    */
  def execute(allocation: Allocation): Allocation = {
    var mindInitiator = Mind(stap, initiator, allocation)
    var mindResponder = Mind(stap, responder, allocation)
    var updatedAllocation = allocation.copy()
    for (taskInitiator <- endowment){
      mindInitiator = mindInitiator.removeTask(taskInitiator)
      mindResponder = mindResponder.addTask(taskInitiator)
    }
    for (taskResponder <- counterparts){
      mindResponder = mindResponder.removeTask(taskResponder)
      mindInitiator = mindInitiator.addTask(taskResponder)
    }
    updatedAllocation = updatedAllocation.update(initiator, mindInitiator.sortedTasks)
    updatedAllocation = updatedAllocation.update(responder, mindResponder.sortedTasks)
    updatedAllocation
  }

  /**
   * @see org.smastaplus.process.Deal#execute
   */
  def execute(allocation: ExecutedAllocation): ExecutedAllocation = {
    var mindInitiator = Mind(stap, initiator, allocation)
    var mindResponder = Mind(stap, responder, allocation)
    var updatedAllocation = allocation.copy()
    for (taskInitiator <- endowment) {
      mindInitiator = mindInitiator.removeTask(taskInitiator)
      mindResponder = mindResponder.addTask(taskInitiator)
    }
    for (taskResponder <- counterparts) {
      mindResponder = mindResponder.removeTask(taskResponder)
      mindInitiator = mindInitiator.addTask(taskResponder)
    }
    updatedAllocation = updatedAllocation.update(initiator, mindInitiator.sortedTasks)
    updatedAllocation = updatedAllocation.update(responder, mindResponder.sortedTasks)
    updatedAllocation
  }


  /**
    * @see org.smastaplus.process.RationalDeal#isSociallyRational
    */
  @throws(classOf[RuntimeException])
  def isSociallyRational(allocation: ExecutedAllocation, rule: SocialRule): Boolean = {
    var measureBefore, measureAfter : Double  = 0.0
    rule match {
      case Makespan => // The makespan rule
        val initiatorWorkloadBefore : Double = allocation.workload(initiator)
        val responderWorkloadBefore : Double = allocation.workload(responder)
        measureBefore  = max(initiatorWorkloadBefore, responderWorkloadBefore)

        var initiatorWorkloadAfter = initiatorWorkloadBefore
        var responderWorkloadAfter = responderWorkloadBefore
        for (taskInitiator <- endowment){
          initiatorWorkloadAfter -= stap.cost(taskInitiator, initiator)
          responderWorkloadAfter += stap.cost(taskInitiator, responder)
        }
        for (taskResponder <- counterparts){
          responderWorkloadAfter -= stap.cost(taskResponder, responder)
          initiatorWorkloadAfter += stap.cost(taskResponder, initiator)
        }
        measureAfter = max(initiatorWorkloadAfter, responderWorkloadAfter)

      case LocalFlowtime => // The localFlowtime rule
        val updatedAllocation = execute(allocation)
        measureBefore = jobs(allocation).foldLeft(0.0)((sum: Double, job: Job) => sum + completionTime4Contactors(job,allocation))
        measureAfter = jobs(allocation).foldLeft(0.0)((sum: Double, job: Job) => sum + completionTime4Contactors(job,updatedAllocation))

      case GlobalFlowtime => // The globalFlowtime rule
        val updatedAllocation = execute(allocation)
        measureBefore = allocation.globalFlowtime
        measureAfter = updatedAllocation.globalFlowtime

      case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
    }
    measureAfter ~< measureBefore
  }
}

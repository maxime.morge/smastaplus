// Copyright (C) Maxime MORGE 2020, 2021
package org.smastaplus.process

import org.smastaplus.core._

/**
  * Task delegation
  * @param stap instance
  * @param donor computing node which gives the task
  * @param recipient computing node which takes the task
  * @param endowment which is delegated
  */
class Delegation(override val stap: STAP,
                 val donor: ComputingNode,
                 val recipient: ComputingNode,
                 endowment: List[Task]) extends
Swap(stap, donor, recipient, endowment, List.empty[Task])  {

  override def toString: String = s"δ($donor,$recipient,$endowment)"

  override def equals(that: Any): Boolean =
    that match {
      case that: Delegation => that.canEqual(this) &&
        this.donor == that.donor && this.recipient == that.recipient && this.endowment== that.endowment
      case _ => false
    }
  override def canEqual(a: Any): Boolean = a.isInstanceOf[Delegation]
}
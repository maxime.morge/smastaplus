// Copyright (C) Maxime MORGE, Luc BIGAND 2021
package org.smastaplus.process

import org.smastaplus.core._

/**
  * Trait for the validation of the  rationality of a deal
  */
trait RationalDeal{
    /**
      * Returns true if the deal is socially rational according to the rule (with the consumption strategy)
      */
    def isSociallyRational(allocation: ExecutedAllocation, rule: SocialRule): Boolean
}

// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.process

/**
  * An operation is
  * - either a task consumption
  * - or a tasks reallocation
  * - or the activation of a computing node
  * - or the deactivation of a computing node
  */
trait Operation

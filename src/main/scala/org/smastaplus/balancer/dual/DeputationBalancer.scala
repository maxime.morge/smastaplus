// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
package org.smastaplus.balancer.dual

import org.smastaplus.core._
import org.smastaplus.deputation._
import org.smastaplus.utils.MathUtils._

/**
  * Deputation balancer which adopts consists of exploring all the delegations
  * @param stap instance to tackle
  * @param rule is the social rule to be applied
  * @param heuristic is true if the a specific heuristic is used to choose the best delegation
  * @param name of the balancer
  */
class DeputationBalancer(stap: STAP, rule: SocialRule, val heuristic: Boolean,
                         name: String = "ExhaustiveDelegationBalancer")
  extends DualBalancer(stap,rule, name) {
  debug = false

  if (rule != GlobalFlowtime) throw new RuntimeException(s"$name can only tackle the social rule globalFlowtime")

  var nbReassignments = 0 // The number of variable reassignments performed by the balancer

  // Internal fields with dumb initialization
  private var deputation : Deputation =
    new Deputation(scala.collection.SortedSet[NodeAgent](), scala.collection.SortedSet[JobAgent]())
  private var currentAllocation = ExecutedAllocation.randomAllocation(stap)

  /**
   * Returns an initial allocation which is random by default
   */
  def init(allocation: ExecutedAllocation = ExecutedAllocation.randomAllocation(stap)): ExecutedAllocation = {
    currentAllocation = allocation
    if (debug) println(s"$name: initial random allocation\n$allocation")
    if (debug) println(s"$name: initial globalFlowtime ${allocation.globalFlowtime}")
    val startingTime: Long = System.nanoTime()
    deputation = Deputation.stap2deputation(allocation)
    preSolvingTime = System.nanoTime() - startingTime
    if (debug) println(s"$name: Deputation\n$deputation")
    allocation
  }

  /**
    * Returns an allocation
    */
  protected override def balance(): Option[ExecutedAllocation] = {
    val initialAllocation = init()
    val finalAllocation = reallocate(initialAllocation)
    Some(finalAllocation)
  }

  /**
    * Modify the current allocation
    */
  @throws(classOf[RuntimeException])
  def reallocate(allocation: ExecutedAllocation): ExecutedAllocation = {
    // 1 - Init the allocation
    if (allocation != currentAllocation)  init(allocation)
    var isTerminated = false
    // 2 - Try delegations
    while (! isTerminated) {
      // A turn-taking of all the job agents
      val (job, task, variable, payoff) = bestVariableReassignment(deputation)
      if (payoff ~> 0.0) {
        deputation.updateAssignmentsAndBundles(job, task, variable)
        deputation.cleanPositions()
        nbReassignments += 1
        if (debug) println(s"$name: variable reassignment by the job agent $job of the task $task with payoff $payoff\n $deputation")
      } else isTerminated = true
    }
    if (debug) println(s"$name: solution found after $nbReassignments reassignments with globalFlowtime ${deputation.globalFlowtime()}")
    // 3 -- Translate deputation assignment  into an allocation
    val startingTime: Long = System.nanoTime()
    currentAllocation = deputation.assignment2Allocation(stap)
    postSolvingTime = System.nanoTime() - startingTime
    currentAllocation
  }

  /**
    * Returns the best variable reassignment for any job (jobAgent, taskId, assignment, payoff)
    */
  def bestVariableReassignment(problem: Deputation) : (JobAgent, Task, VariableAssignment, Double) = {
    var bestReassignment = new VariableAssignment(problem.nodeAgents.head, 0.0)
    var bestPayoff = Double.NegativeInfinity
    var bestJob = problem.jobAgents.head
    var bestTask : Task = NoTask

    for (job <- problem.jobAgents) { // For each job
      val (task, v, payoff) = job.selectBestReassignment(problem, heuristic)
      //if equality with an other task, applying the rule of decision
      if (payoff ~= bestPayoff) {
        if (problem.breakTieTasks(bestTask, task, heuristic).equals(task)){
          bestReassignment = v
          bestPayoff = payoff
          bestJob = job
          bestTask = task
        }
      }
      if (payoff ~> bestPayoff) {
        bestReassignment = v
        bestPayoff = payoff
        bestJob = job
        bestTask = task
      }
    }
    (bestJob, bestTask, bestReassignment, bestPayoff)
  }
}

/**
  * Companion object to test it
  */
object DeputationBalancer extends App {
  val debug = true

  import org.smastaplus.example.stap.ex1.stap

  if (debug) println(stap)
  val balancer = new DeputationBalancer(stap, GlobalFlowtime, heuristic = true)
  balancer.debug = true
  val outcome = balancer.run()
  println(outcome.toString)
  println(s"globalFlowtime = ${outcome.get.meanGlobalFlowtime}")
  println(s"localRatio = ${outcome.get.localAvailabilityRatio}")
}


// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.balancer.dual

import org.smastaplus.core._
import org.smastaplus.balancer._
import org.smastaplus.utils.MyTime

/**
  * Abstract class for balancing an STAP instance by translating the problem/outcome
  * @param stap instance to tackle
  * @param rule is the social rule to apply
  * @param name of the balancer
  */
abstract class DualBalancer(stap: STAP,
                            rule: SocialRule,
                            name : String = "DualBalancer")
  extends Balancer(stap, rule, name){
  var preSolvingTime : Long = 0
  var postSolvingTime : Long = 0

  override def toString: String =
    s"$name with $rule (${MyTime.show(solvingTime)} with pre ${MyTime.show(preSolvingTime)} and post ${MyTime.show(postSolvingTime)})"

}

// Copyright (C) Maxime MORGE 2021
package org.smastaplus.balancer.dual

import org.smastaplus.core._
import org.smastaplus.utils.serialization.stap.dcop.{DCOPParser, DCOPWriter}

import java.io.File
import com.typesafe.config.{Config, ConfigFactory}

import scala.annotation.unused
import scala.concurrent.{Await, Future, TimeoutException, blocking}
import scala.sys.process._
import scala.concurrent.duration._
import scala.language.postfixOps


/**
  * A DCOP balancer based on PyDCOP
  * It is worth noticing that the hard constraints are
  * implemented as soft constraints with high cost
  * @param stap   to be balancer
  * @param rule to be optimized
  * @param name of the balancer
  * @param timeout of the balancer, 15 seconds by default
  */
class DCOPBalancer(stap: STAP, rule: SocialRule,
                   name : String = "DCOPBalancer", val timeout: FiniteDuration = 15 seconds)
  extends DualBalancer(stap, rule, name) {
  debug = false
  var isFound = false// is an allocation found
  @unused
  val overhead = 0.1  // potential timeout overhead

  val config: Config = ConfigFactory.load()
  val inputPath: String = config.getString("path.smastaplus") + "/" + config.getString("path.dcopBalancerInput")
  val outputPath: String = config.getString("path.smastaplus") + "/" + config.getString("path.dcopBalancerOutput")
  val errorPath: String = config.getString("path.smastaplus") + "/" + config.getString("path.dcopBalancerError")

  private var currentAllocation = new Allocation(stap)
  var initializationTime: Long = 0
  var initialLocalRatio = 0.0

  /**
    * Reformulates STAP as a DCOP
    */
  def reformulate(): Unit = {
    new DCOPWriter(inputPath, stap = stap, currentAllocation, rule).write()
  }

  /**
    * Translates the assignment as an allocation
    */
  def translate(): Unit = {
    currentAllocation = new DCOPParser(outputPath, stap = stap, GlobalFlowtime).parse()
  }

  /**
    * Run the balancer
    */
  def execute(): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val remainingTime: FiniteDuration = timeout - new FiniteDuration(preSolvingTime, NANOSECONDS)
    //val command: String = s"pydcop --output $outputPath --timeout ${remainingTime.toSeconds} solve --algo mgm2 $inputPath"
    val command =Seq("sh", "-c", s"pydcop --output $outputPath --timeout ${remainingTime.toSeconds} solve --algo mgm2 $inputPath 2> /dev/null > /dev/null")
    if (debug) println(s"$name runs '$command'")
    //val success = (command #> new File(errorPath)).! Rather than synchronous run
    val process = command.run(connectInput = false) // the command is asynchronously run
    // See https://github.com/sbt/sbt/pull/3970/commits/b0f52510e0c31bb324439903563ec202e2b849f2
    val future = Future(blocking(process.exitValue())) // and wrap in a future
    val success = try {
      Await.result(future, 2*remainingTime)//
    } catch {
      case _ : TimeoutException =>
        if (debug) println(s"$name observes that pyDCOP outreaches timeout")
        process.destroy()
        process.exitValue()
    }
    if (debug) println(s"$name : command result = $success")
    // Either the pyDCOP succeed or the sub-processes must be killed
    if (success == 0) isFound = true
    else try{
      "ps -ef" #| """grep "dcop"""" #| """grep -v "grep"""" #| "awk '{print $2}'"  #| "xargs  -r kill" !<
    }catch {
      case _ : java.io.IOException =>
        if (debug) println(s"$name fails to kill pyDCOP")
    }
    // ps -ef | grep "dcop" | grep -v "grep"| awk '{print $2}' | xargs -r kill
  }

  /**
    * Reallocate method
    * @return the allocation
    */
  def reallocate(allocation: Allocation): Allocation = perform(allocation) match {
    case Some(a) => a
    case None => currentAllocation
  }

  /**
    * Balancer main method
    * @return the allocation
    */
  protected override def balance(): Option[Allocation] = perform()

  /**
    * Balancer main method
    * @return the allocation
    */
  @throws(classOf[RuntimeException])
  private def perform(allocation: Allocation = ExecutedAllocation.randomAllocation(stap)): Option[Allocation] = {
    if (rule  != GlobalFlowtime && rule != Makespan) {
      throw new RuntimeException(s"$name cannot tackle the social rule $rule")
    }
    // 0 - Init allocation
    currentAllocation = allocation
    initialLocalRatio = allocation.localAvailabilityRatio
    if (debug) println(s"$name: initial random allocation\n$allocation")
    if (debug && rule == GlobalFlowtime) println(s"$name: initial globalFlowtime ${allocation.globalFlowtime}")
    if (debug && rule == Makespan) println(s"$name: initial makespan ${allocation.makespan}")

    // 1 -- Reformulate the problem
    val startingTime: Long = System.nanoTime()
    if (debug) println(s"$name reformulates as a DCOP")
    reformulate()
    preSolvingTime = System.nanoTime() - startingTime

    //2 -- Solve the DCOP problem
    if (debug) println(s"$name solves the DCOP")
    execute()
    solvingTime = timeout.toNanos - preSolvingTime

    //3 -- Translate the assignment into an allocation
    if (debug) println(s"$name translates the assignment in an allocation")
    postSolvingTime = 0 // The translation is asynchronously performed
    if (isFound) translate()
    //4 -- Remove files
    val fileInput= new File(inputPath)
    val fileOutput= new File(outputPath)
    if (fileInput.exists()) fileInput.delete()
    if (fileOutput.exists()) fileOutput.delete()
    if (isFound) {
      println(s"$name has found an allocation")
      Some(currentAllocation)
    } else {
        println(s"$name has found no allocation")
        None
      }
  }
}

/**
  * Companion object to test it
  */
object DCOPBalancer extends App {
  val debug = true
  val rule = GlobalFlowtime
  //import org.smastaplus.example.masta.exMinimal.pb
  //import org.smastaplus.example.masta.ex1.pb
  val l = 3 // with l jobs
  val m = 2 // with m nodes
  val n = l * m // with l * m tasks, 3 * l * m
  val o = 1 // with o resources per tasks
  val d = 3 // with d duplicated instances per resource
  val stap = STAP.randomProblem(l, m, n, o, d, Uncorrelated)
  if (debug) println("Build balancer")
  val balancer = new DCOPBalancer(stap, rule, "DCOPBalancer",  10 seconds)//
  balancer.debug = true
  if (debug) println("Solve")
  val outcome = balancer.balance()
  if (outcome.isDefined) {
    println(outcome.get.toString)
    if (rule == GlobalFlowtime) println(s"GlobalFlowtime: ${outcome.get.meanGlobalFlowtime}")
    else println(s"Makespan: ${outcome.get.makespan}")
  } else println("No outcome is found before deadline")
}





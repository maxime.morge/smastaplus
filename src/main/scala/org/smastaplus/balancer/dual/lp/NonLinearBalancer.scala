// Copyright (C) Maxime MORGE 2020
package org.smastaplus.balancer.dual.lp

import org.smastaplus.core._
import org.smastaplus.balancer.dual.DualBalancer
import org.smastaplus.utils.lp.NonLinearWriter

import com.typesafe.config.{Config, ConfigFactory}
import java.io.File
import scala.sys.process._

/**
  * Solving the minimization of the localFlowtime by translating the MASTA+ instance
  * in a non-linear program
  * @param pb   to be balancer
  * @param rule to be optimized
  * @param name of the balancer
  */
class NonLinearBalancer(pb: STAP, rule: SocialRule, name: String = "NonLinearBalancer")
  extends DualBalancer(pb, rule, name) {

  val config: Config = ConfigFactory.load()
  val inputPath: String =config.getString("path.smastaplus")+"/"+config.getString("path.input")
  val outputPath: String = config.getString("path.smastaplus")+"/"+config.getString("path.output")
  val errorPath: String = config.getString("path.smastaplus")+"/"+config.getString("path.error")
  var lpPath: String = rule match {
    case GlobalFlowtime =>
      config.getString("path.smastaplus")+"/"+config.getString("path.globalFlowtime")
    case Makespan =>
      config.getString("path.smastaplus")+"/"+config.getString("path.makespan")
    case _ =>
      throw new RuntimeException(s"$name cannot tackle the social rule $rule")
  }

  /**
    * Balancer main method
    * @return the allocation
    */
  @throws[RuntimeException]
  protected override def balance(): Option[ExecutedAllocation] = {
    // 1 -- Reformulate the problem
    var startingTime: Long = System.nanoTime()
    val writer=new NonLinearWriter(inputPath, pb)
    writer.write()
    preSolvingTime = System.nanoTime() - startingTime

    //2 -- Solve the problem
    if (debug) println("Solve the flow problem")
    val command : String= "oplrun"+" "+
      lpPath+" "+
      inputPath
    if (debug) println(command)
    val success = (command #> new File(errorPath)).!
    if (success != 0) throw new RuntimeException(s"$name failed")

    //3 -- Translate into an allocation
    if (debug) println("Translate flow into an allocation")
    startingTime = System.nanoTime()
    val allocation : ExecutedAllocation = ExecutedAllocation(outputPath, pb)
    val sortedAllocation = allocation.sort()
    postSolvingTime = System.nanoTime() - startingTime
    if (debug) println("A\n"+sortedAllocation)
    Some(allocation)
  }
}

/**
  * Companion object to test it
  * sbt "run org.smastaplus.balancer.dual.lp.NonLinearBalancer"
  * since dynamic library path must be setup
  */
object NonLinearBalancer extends App {
  val debug = false

  import org.smastaplus.example.stap.ex1.stap

  if (debug) println(stap)
  val rule : SocialRule = Makespan // || GlobalFlowtime
  val balancer = new NonLinearBalancer(stap,rule)
  balancer.debug = true
  val outcome = balancer.run()
  println(outcome.toString)
  if (rule == GlobalFlowtime) println(s"GlobalFlowtime= ${outcome.get.meanGlobalFlowtime}")
  if (rule == Makespan)  println(s"Makespan= ${outcome.get.makespan}")
  println(s"LocalRatio= ${outcome.get.localAvailabilityRatio}")
}


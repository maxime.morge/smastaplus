// Copyright (C) Maxime MORGE 2021, 2022
package org.smastaplus.balancer.dual.constraint

import org.jacop.constraints.{XeqC, XlteqC}
import org.jacop.scala._
import org.smastaplus.core._
import org.smastaplus.balancer.dual.DualBalancer
import org.smastaplus.utils.thread.MyCustomExecutionContext

import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * A JaCoP balancer for minimizing the globalFlowtime for stap instances
  * It is worth noticing that the cost of the tasks are rounded
  *
  * @param stap   to be balancer
  * @param rule to be optimized
  * @param name of the balancer
  * @param timeout of the balancer, 15 seconds by default
  */
class ConstraintBalancer(stap: STAP, rule: SocialRule,
                         name: String = "ConstraintBalancer",
                         val timeout: FiniteDuration = 15 seconds)
  extends DualBalancer(stap, rule, name) {
  debug = false

  var allocation = new ExecutedAllocation(stap)
  var isFound = false
  val taskArray : Array[Task] = stap.tasks.toArray // tasks as an array
  val nodeArray : Array[ComputingNode] = stap.ds.computingNodes.toArray // nodes as array
  val jobArray : Array[Job] = stap.jobs.toArray // jobs as an array
  var costArray : Array[Array[Double]] = Array.ofDim[Double](stap.n, stap.m)
  var y: Array[Array[Int]] = Array.ofDim[Int](stap.n, stap.l)// y[i][j] = 1 if task_i is in Job_j

  var variables = List.empty[IntVar]// List of variables
  var cost : IntVar = _

  var isTimeoutReached = false

  /**
    * Returns the ith task with 0 <= i < pb.n
    */
  def task(i : Int): Task = {
    if (i >= stap.n) throw new RuntimeException(s"Cannot returns the $i th task with ${stap.n} tasks")
    taskArray(i)
  }

  /**
    * Returns the ith node with 0 <= i < pb.m
    */
  def node(i : Int): ComputingNode = {
    if (i >= stap.m) throw new RuntimeException(s"Cannot returns the $i th node with ${stap.m} nodes")
    nodeArray(i)
  }

  /**
    * Returns the jth job with 0 <= j < pb.n
    */
  def job(j : Int): Job = {
    if (j >= stap.l) throw new RuntimeException(s"Cannot returns the $j th job with ${stap.l} jobs")
    jobArray(j)
  }

  /**
    * Returns the variable x($i)($k)($o) which is added to
    * the list of variables
    */
  def variable(i: Int, k: Int, o: Int) : IntVar = {
    val newVariable = new IntVar(s"x($i)($k)($o)", 0, 1)
    variables ::= newVariable
    newVariable
  }

  /**
    * $n&#94;{4}xm/2$ decision variables x(i)(k)(o) ∈ {0,1} such that
    * x(i)(k)(o) = 1 iff
    * task_i is the  #kth task from the end over the node o
    */
  var x : Array[Array[Array[IntVar]]] = Array.ofDim[IntVar](stap.n, stap.n, stap.m)


  /**
    * Reformulate STAP as a COP
    */
  def reformulate() : Unit = {
    y = Array.tabulate(stap.n, stap.l)((i, j) => if (job(j).tasks.contains(task(i))) 1 else 0)
    costArray = Array.tabulate(stap.n, stap.m)((t, n) => stap.cost(task(t), node(n)))

    x = Array.tabulate(stap.n, stap.n, stap.m)((i, k, o) => variable(i, k, o))
    if (debug) println(s"${stap.n} constraints to assign each task to exactly 1 position on a single node")
    for (i <- 0 until stap.n) { //  foreach task
      val constraintPosition4Task: XeqC =
        sum[IntVar]((for (o <- 0 until stap.m; k <- 0 until stap.n) yield x(i)(k)(o)).toList) #= 1
      constraintPosition4Task // impose the constraint
    }

    if (debug) println(s"${stap.n} x ${stap.m} constraints to assign each task to at most one position on a single node")
    for (k <- 0 until stap.n; o <- 0 until stap.m) { //  foreach pair of tasks
      val constraintTask4Position: XlteqC =
        sum[IntVar]((for (i <- 0 until stap.n) yield x(i)(k)(o)).toList) #<= 1
      constraintTask4Position // impose the constraint
    }

    if (debug && rule == GlobalFlowtime)
      println("sum(j in J) max(o in A) max(i in T) sum(k in P, i2 in T, m > k in P) y[i][j] * x[i][k][o] * x[i2][m][o] * C[i2][t]")
    if (debug && rule != GlobalFlowtime)
      println("max(o in N) sum(t in T)  c[o][t] * (sum (k in P)  x[i][k][o])")
    implicit val exec: MyCustomExecutionContext = new MyCustomExecutionContext()
    val futureCostComputation = Future {
      cost  = if (rule == GlobalFlowtime) //
        sum[IntVar]((
          for (j <- 0 until stap.l)
            yield max[IntVar]((
              for (i <- 0 until stap.n if y(i)(j) == 1)
                yield max[IntVar]((
                  for (o <- 0 until stap.m)
                    yield sum[IntVar]((
                      for (k <- 0 until stap.n)
                        yield x(i)(k)(o) * sum[IntVar]((for (i2 <- 0 until stap.n; m <- k until stap.n)
                          yield x(i2)(m)(o) * costArray(i2)(o).toInt).toList)
                      ).toList)
                  ).toList)
              ).toList)
          ).toList
        )
      else max[IntVar]((
        for (o <- 0 until stap.m)
          yield sum[IntVar]((for (i <- 0  until stap.n; k <- 0 until stap.n)
            yield x(i)(k)(o) * costArray(i)(o).toInt).toList)
        ).toList
      )
    }
    try {
      Await.result(futureCostComputation, timeout)
    } catch {
      case _: TimeoutException =>
        if (debug) println(s"$name reaches timeout during cost definition")
        isTimeoutReached = true
        println("Stopping...")
        exec.lastThread.getOrElse(throw new RuntimeException("Not started")).interrupt() // 0% cpu here
      case _: java.lang.OutOfMemoryError =>
        if (debug) println(s"$name throws OutOfMemoryError")
        isTimeoutReached = true
    }
  }

  /**
    * Run the balancer
    */
  def execute() : Unit = {
    val remainingTime : FiniteDuration= timeout - new FiniteDuration(preSolvingTime, NANOSECONDS)
    implicit val exec: MyCustomExecutionContext = new MyCustomExecutionContext()
    val futureSearch = Future {
      timeOut(remainingTime.toSeconds.toInt)
      minimize(search(variables, first_fail, indomain_min), cost, translate)
    }
    try {
      Await.result(futureSearch, remainingTime)
    } catch {
      case _: TimeoutException =>
        if (debug) println(s"$name reaches timeout during search")
        isTimeoutReached = true
        println("Stopping...")
        exec.lastThread.getOrElse(throw new RuntimeException("Not started")).interrupt() // 0% cpu here
      case _: java.lang.OutOfMemoryError =>
        if (debug) println(s"$name throws OutOfMemoryError")
        isTimeoutReached = true
      case _: java.lang.NullPointerException  =>
        if (debug) println(s"$name throws NullPointerException")
        isTimeoutReached = true
    }
  }

  /**
    * Translate of the variable assignments as the allocation
    */
  def translate: () => Unit = () => {
    if (!isFound || cost.value < allocation.globalFlowtime) { // if a better globalFlowtime is found
      isFound = true
      if (debug) println(s"Objective function to minimize ($rule): " + cost.value)
      for (o <- 0 until stap.m) {
        val bundle: IndexedSeq[Task] = for (k <- 0 until stap.n; i <- 0 until stap.n if x(i)(k)(o).value == 1)
          yield taskArray(i) //s"t${i + 1}(${costs(i)(o)})"
        allocation = allocation.update(nodeArray(o), bundle.reverse.toList)
      }
    }
  }

  /**
    * Balancer main method
    * @return the allocation
    */
  protected override def balance(): Option[ExecutedAllocation] = {
    if (rule == Makespan || rule == LocalFlowtime)
      throw new RuntimeException(s"ConstraintBalancer does not work for the social rule $rule")

    // 1 -- Reformulate the problem
    val startingTime: Long = System.nanoTime()
    if (debug) println("Reformulate as a constraint problem")
    reformulate()
    preSolvingTime = System.nanoTime() - startingTime

    //2 -- Solve the constraint problem
    if (debug) println("Solve the constraint problem")
    if (! isTimeoutReached) execute()
    //3 -- Translate the assignment into an allocation
    postSolvingTime = 0 // The translation is asynchronously performed
    if (isFound){
      if (debug) println("An allocation if found")
      return Some(allocation)
    }
    if (debug) println("No allocation is found")
    None
  }
}


/**
  * Companion object to test it
  */
object ConstraintBalancer extends App {
  val debug = true

  val rule = GlobalFlowtime
  val m = 5 // with m nodes
  val l = 4 // with l jobs
  val n = l * m // with l * m tasks
  val o = 1 // with o resources per task
  val d = 3 // with 3 duplicated instances per resources
  val stap = STAP.randomProblem(l, m, n, o, d, Uncorrelated)
  if (debug) println(stap)
  if (debug) println("Build balancer")
  val balancer = new ConstraintBalancer(stap, rule, "ConstraintBalancer", 10 seconds)
  balancer.debug = true
  if (debug) println("Solve")
  val outcome = try {
    balancer.balance()
  } catch {
    case _: OutOfMemoryError =>
      if (debug) println(s"Constraint balancer solve raise OutOfMemoryError")
      None
  }
  if (outcome.isDefined) {
    println(outcome.get.toString)
    println(s"GlobalFlowtime ${outcome.get.meanGlobalFlowtime}")
  } else println("No outcome is found before deadline")
}



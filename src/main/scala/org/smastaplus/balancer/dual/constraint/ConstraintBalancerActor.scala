// Copyright (C) Maxime MORGE 2021, 2022
package org.smastaplus.balancer.dual.constraint

import org.smastaplus.core._

import org.jacop.constraints.{XeqC, XlteqC}
import org.jacop.scala.{IntVar, first_fail, indomain_min, max, minimize, search, sum}

import akka.actor.{Actor, ActorRef}

/**
  * ConstraintBalancerActor which solves CoP representation for STAP
  * @param stap instance to tackle
  * @param rule is the social rule to be applied
  * @param dispatcherId is the name of the dispatcher
  * */
class ConstraintBalancerActor(stap: STAP, rule: SocialRule, val dispatcherId: String = "akka.actor.default-dispatcher")
  extends Actor {

  var balancer: ActorRef = context.parent // Reference to the distributed balancer

  val debug = false

  var allocation = new Allocation(stap)
  var isFound = false
  val taskArray : Array[Task] = stap.tasks.toArray // tasks as an array
  val nodeArray : Array[ComputingNode] = stap.ds.computingNodes.toArray // nodes as array
  val jobArray : Array[Job] = stap.jobs.toArray // jobs as an array
  var costArray : Array[Array[Double]] = Array.ofDim[Double](stap.n, stap.m)
  var y: Array[Array[Int]] = Array.ofDim[Int](stap.n, stap.l)// y[i][j] = 1 if task_i is in Job_j

  var variables = List.empty[IntVar]// List of variables
  var cost : IntVar = _

  //import ExecutionContext.Implicits.global
  var isTimeoutReached = false

  /**
    * Returns the ith task with 0 <= i < pb.n
    */
  def task(i : Int): Task = {
    if (i >= stap.n) throw new RuntimeException(s"Cannot returns the $i th task with ${stap.n} tasks")
    taskArray(i)
  }

  /**
    * Returns the ith node with 0 <= i < pb.m
    */
  def node(i : Int): ComputingNode = {
    if (i >= stap.m) throw new RuntimeException(s"Cannot returns the $i th node with ${stap.m} nodes")
    nodeArray(i)
  }

  /**
    * Returns the jth job with 0 <= j < pb.n
    */
  def job(j : Int): Job = {
    if (j >= stap.l) throw new RuntimeException(s"Cannot returns the $j th job with ${stap.l} jobs")
    jobArray(j)
  }

  /**
    * Returns the variable x($i)($k)($o) which is added to
    * the list of variables
    */
  def variable(i: Int, k: Int, o: Int) : IntVar = {
    val newVariable = new IntVar(s"x($i)($k)($o)", 0, 1)
    variables ::= newVariable
    newVariable
  }

  /**
    *  (n&#94;4xm)/2 decision variables x(i)(k)(o) ∈ {0,1} such that
    * x(i)(k)(o) = 1 iff
    * task_i is the  #kth task from the end over the node o
    */
  var x : Array[Array[Array[IntVar]]] = Array.ofDim[IntVar](stap.n, stap.n, stap.m)

  /**
    * Reformulate STAP
    */
  def reformulate() : Unit = {
    y = Array.tabulate(stap.n, stap.l)((i, j) => if (job(j).tasks.contains(task(i))) 1 else 0)
    costArray = Array.tabulate(stap.n, stap.m)((t, n) => stap.cost(task(t), node(n)))

    x = Array.tabulate(stap.n, stap.n, stap.m)((i, k, o) => variable(i, k, o))
    if (debug) println(s"${stap.n} constraints to assign each task to exactly 1 position on a single node")
    for (i <- 0 until stap.n) { //  foreach task
      val constraintPosition4Task: XeqC =
        sum[IntVar]((for (o <- 0 until stap.m; k <- 0 until stap.n) yield x(i)(k)(o)).toList) #= 1
      constraintPosition4Task // impose the constraint
    }

    if (debug) println(s"${stap.n} x ${stap.m} constraints to assign each task to at most one position on a single node")
    for (k <- 0 until stap.n; o <- 0 until stap.m) { //  foreach pair of tasks
      val constraintTask4Position: XlteqC =
        sum[IntVar]((for (i <- 0 until stap.n) yield x(i)(k)(o)).toList) #<= 1
      constraintTask4Position // impose the constraint
    }

    if (debug && rule == GlobalFlowtime)
      println("sum(j in J) max(o in A) max(i in T) sum(k in P, i2 in T, m > k in P) y[i][j] * x[i][k][o] * x[i2][m][o] * C[i2][t]")
    if (debug && rule != GlobalFlowtime)
      println("sum(o in N) sum(t in T) max (k in P)  x[i][k][o] * c[o][t]")

    cost = if (rule == GlobalFlowtime) //
      sum[IntVar]((
        for (j <- 0 until stap.l)
          yield max[IntVar]((
            for (i <- 0 until stap.n if y(i)(j) == 1)
              yield max[IntVar]((
                for (o <- 0 until stap.m)
                  yield sum[IntVar]((
                    for (k <- 0 until stap.n)
                      yield x(i)(k)(o) * sum[IntVar]((for (i2 <- 0 until stap.n; m <- k until stap.n)
                        yield x(i2)(m)(o) * costArray(i2)(o).toInt).toList)
                    ).toList)
                ).toList)
            ).toList)
        ).toList
      )
    else max[IntVar]((
      for (o <- 0 until stap.m)
        yield sum[IntVar]((for (i <- 0 until stap.n; k <- 0 until stap.n)
          yield x(i)(k)(o) * costArray(i)(o).toInt).toList)
      ).toList
    )
  }

  /**
    * Run the balancer
    */
  def execute() : Unit = {
      minimize(search(variables, first_fail, indomain_min), cost, translate)
  }

  /**
    * Translate of the variable assignments as the allocation
    */
  def translate: () => Unit = () => {
    if (!isFound || cost.value < allocation.globalFlowtime) { // if a better globalFlowtime is found
      isFound = true
      if (debug) println(s"Objective function to minimize ($rule): " + cost.value)
      for (o <- 0 until stap.m) {
        val bundle: IndexedSeq[Task] = for (k <- 0 until stap.n; i <- 0 until stap.n if x(i)(k)(o).value == 1)
          yield taskArray(i) //s"t${i + 1}(${costs(i)(o)})"
        allocation = allocation.update(nodeArray(o), bundle.reverse.toList)
      }
    }
  }

  /**
    * Message processing
    */
  override def receive: Receive = {
    case Reformulate =>
      reformulate()
    case Run =>
      execute()
      sender() ! allocation
  }
}

// Copyright (C) Maxime MORGE 2021, 2022
package org.smastaplus.balancer.dual.constraint

import org.smastaplus.core._
import org.smastaplus.balancer.dual.DualBalancer

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.remote.transport.ActorTransportAdapter.AskTimeout

import scala.concurrent.{Await, TimeoutException}
import scala.concurrent.duration._
import scala.language.postfixOps

// Message
case object Reformulate
case object Run

/**
  * DecentralizedConstraintBalancer which solves CoP representation for STAP with Akka actor
 *
  * @deprecated See [[ConstraintBalancer]]
  * @param stap instance to tackle
  * @param rule is the social rule to be applied
  * @param system of actors
  * @param timeout for the resolution
  * @param name of the balancer
  * @param dispatcherId is the name of the dispatcher
  */
class DecentralizedConstraintBalancer(stap: STAP, rule: SocialRule, val system: ActorSystem, val timeout: FiniteDuration = 15 seconds,
                                      name: String = "DecentralizedConstraintBalancer", val dispatcherId: String = "akka.actor.default-dispatcher")
  extends DualBalancer(stap, rule, name){

  var isTimeoutReached = false
  var isFound = false
  var allocation = new ExecutedAllocation(stap)

  // Launch a new supervisor
  DecentralizedConstraintBalancer.id += 1
  val balancer : ActorRef =
    system.actorOf(Props(classOf[ConstraintBalancerActor], stap, rule, dispatcherId) withDispatcher dispatcherId ,
      name = "supervisor" + DecentralizedConstraintBalancer.id)

  /**
    * Balancer main method
    * @return the allocation
    */
  protected override def balance(): Option[ExecutedAllocation] = {
    if (rule == Makespan || rule == LocalFlowtime)
      throw new RuntimeException(s"DecentralizedConstraintBalancer cannot tackle the social rule $rule")

    // 1 -- Reformulate the problem
    val startingTime: Long = System.nanoTime()
    if (debug) println("Reformulate as a constraint problem")
    val future1 = balancer ? Reformulate
    try {
      Await.result(future1, timeout)
    }catch{
      case _: TimeoutException =>
        if (debug) println(s"$name reaches timeout during cost definition")
        isTimeoutReached = true
      case _: OutOfMemoryError =>
        if (debug) println(s"$name throws OutOfMemoryError")
        isTimeoutReached = true
    }
    preSolvingTime = System.nanoTime() - startingTime

    //2 -- Solve the constraint problem
    if (debug) println("Solve the constraint problem")
    val remainingTime = timeout - new FiniteDuration(preSolvingTime, NANOSECONDS)

    val future2 = balancer ? Run
    try {
      allocation = Await.result(future2, remainingTime).asInstanceOf[ExecutedAllocation]
      isFound = true
    }catch{
      case _: TimeoutException =>
        if (debug) println(s"$name reaches timeout during cost definition")
        isTimeoutReached = true
      case _: OutOfMemoryError =>
        if (debug) println(s"$name throws OutOfMemoryError")
        isTimeoutReached = true
    }
    //3 -- Translate the assignment into an allocation
    postSolvingTime = 0 // The translation is asynchronously performed
    if (isFound){
      if (debug) println("An allocation if found")
      return Some(allocation)
    }
    if (debug) println("No allocation is found")
    None
  }
}

/**
  * Companion object for testing DecentralizedBalancer
  */
object DecentralizedConstraintBalancer extends App {

  var id: Int = 1

  val r = scala.util.Random
  val system = ActorSystem("Test" + r.nextInt().toString)
  val debug = true

  val rule = GlobalFlowtime
  val m = 2 // with m nodes
  val l = 4 // with l jobs
  val n = l * m // with l * m tasks
  val o = 1 // with o resources per task
  val d = 3 // with 3 duplicated instances per resources
  val masta = STAP.randomProblem(l, m, n, o, d, Uncorrelated)
  if (debug) println(masta)
  if (debug) println("Build balancer")
  val balancer = new DecentralizedConstraintBalancer(masta, rule, system, 10 seconds)
  balancer.debug = true
  if (debug) println("Solve")
  val outcome = try {
    balancer.balance()
  } catch {
    case _: OutOfMemoryError =>
      if (debug) println(s"Constraint balancer solve raise OutOfMemoryError")
      None
  }
  if (outcome.isDefined) {
    println(outcome.get.toString)
    println(s"GlobalFlowtime ${outcome.get.meanGlobalFlowtime}")
  } else println("No outcome is found before deadline")
  system.terminate()
}



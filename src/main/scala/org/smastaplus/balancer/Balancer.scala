// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.balancer

import org.smastaplus.core._
import org.smastaplus.utils.MyTime

/**
  * Abstract class representing a load balancer
  * @param stap is a stap instance to tackle
  * @param rule is a social rule to apply
  * @param name of the balancer
  */
abstract class Balancer(val stap: STAP, val rule: SocialRule, val name: String) {
  var debug = false

  var solvingTime : Long = 0
  var localRatio : Double = 0.0

  // Metrics
  var nbDeals : Int = 0
  var nbDelegations : Int = 0
  var nbSwaps : Int = 0
  var nbTimeouts :  Int = 0
  var nbDelegatedTasks: Int = 0
  var avgEndowmentSize : Double = 0
  var nbFirstStages : Int = 0
  var nbSecondStages : Int = 0

  override def toString: String = s"$name with $rule (${MyTime.show(solvingTime)})"

  /**
    * Returns an allocation
    */
  protected def balance() : Option[Allocation]

  /**
    * Returns an allocation and update solving time
    */
  def run() : Option[Allocation] = {
    val startingTime = System.nanoTime()
    val allocation = balance()
    if (allocation.isDefined) {
      localRatio = allocation.get.localAvailabilityRatio
      solvingTime = System.nanoTime() - startingTime
      if (allocation.get.isSound) return allocation
      else throw new RuntimeException(s"Balancer: the outcome\n $allocation\nis not sound for\n ${allocation.get.stap}")
    } else {
      if (debug) println(s"Warning : Balancer $name do not return any allocation for $stap with $rule")
    }
    allocation
  }
}

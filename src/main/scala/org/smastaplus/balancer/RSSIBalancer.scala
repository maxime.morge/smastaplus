// Copyright (C) Maxime MORGE 2020, 2022, 2023
package org.smastaplus.balancer

import org.smastaplus.core._
import org.smastaplus.process.SingleDelegation
import org.smastaplus.utils.RandomUtils

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * Balancer reallocating the tasks using Sequential Single-Item (RSSI) auctions
 *
 * @param stap instance to be solved
 * @param rule is the social rule to be applied
 * @param name of the balancer
 *             See Koenig, S., Tovey, C., Lagoudakis, M., Markakis, V., Kempe, D., Keskinocak, P., ... & Jain, S. (2006, July).
 *             The power of sequential single-item auctions for agent coordination. In AAAI (Vol. 2006, pp. 1625-1629).
 */
class RSSIBalancer(stap: STAP,
                   rule: SocialRule,
                   name: String = "PSIBalancer")
  extends Balancer(stap, rule, name) {

  import ExecutionContext.Implicits.global

  debug = false

  private val TIMEOUT_VALUE: FiniteDuration = 6000 minutes // Default timeout of a run
  private val noFuture = false // true if the bids are sequential

  /**
   * Returns an allocation
   */
  @throws(classOf[RuntimeException])
  protected override def balance(): Option[ExecutedAllocation] = Some(balance(ExecutedAllocation.randomAllocation(stap)))

  /**
   * Returns the allocation by delegating a task from the auctioneer to the bidder in an allocation
   */
  private def accept(allocation: ExecutedAllocation, auctioneer: ComputingNode, bidder: ComputingNode, task: Task): ExecutedAllocation = {
    val delegation = new SingleDelegation(stap, auctioneer, bidder, task)
    delegation.execute(allocation)
  }

  /**
   * Returns an allocation with all the tasks of the allocation with the parallel single-item auction
   */
  @throws(classOf[RuntimeException])
  def balance(allocation: ExecutedAllocation): ExecutedAllocation = {
    var currentAllocation = allocation.copy()
    if (debug) println(s"$name considers the initial allocation $currentAllocation")
    rule match { // Any bid must improve the current allocation
      case GlobalFlowtime =>
        if (debug) println(s"$name considers the flowtime ${currentAllocation.ongoingGlobalFlowtime}")
      case _ =>
        throw new RuntimeException(s"$name cannot apply the social rule $rule")
    }
    var potentialDelegatedTasks = currentAllocation.pendingTasks()
    if (debug) println(s"$name considers the potential tasks $potentialDelegatedTasks")
    while (potentialDelegatedTasks.nonEmpty){ // While there is a potential delegation to evaluate
      val turnTaking: List[ComputingNode] = RandomUtils.shuffle[ComputingNode](stap.ds.computingNodes)
      for (auctioneer <- turnTaking) { // for each auctioneer
        val tasks: List[Task] = currentAllocation.bundle(auctioneer).intersect(potentialDelegatedTasks)
        if (debug) println(s"$name considers $auctioneer with  the potential tasks $tasks")
        if (tasks.nonEmpty) { // If the auctioneer has some potential delegation to evaluate
          val bidders: List[ComputingNode] = stap.ds.computingNodes.diff(Set(auctioneer)).toList
          val proposals: List[(ComputingNode, Task)] = bidders.flatMap(bidder => tasks.map(task => (bidder, task)))
          val initialBid = rule match { // Any bid must improve the current allocation
            case GlobalFlowtime =>
              (tasks.head, auctioneer, currentAllocation.ongoingGlobalFlowtime)
            case _ =>
              throw new RuntimeException(s"$name cannot apply the social rule $rule")
          }
          val (bestTask, bestBidder, bestFlowtime) : (Task, ComputingNode, Double) = if (noFuture){ // sequential computation of the best bid
            proposals.foldLeft(initialBid){
              (best: (Task, ComputingNode, Double), proposal: (ComputingNode, Task)) =>
                val (bidder, task) = proposal
                val bid = rule match {
                  case GlobalFlowtime =>
                    accept(currentAllocation, auctioneer, bidder, task).ongoingGlobalFlowtime
                  case _ =>
                    throw new RuntimeException(s"$name cannot apply the social rule $rule")
                }
                if (bid < best._3) (task, bidder, bid)
                else best
            }
          }else{ // concurrents computation of the best bid
            val futures: List[Future[(Task, ComputingNode, Double)]] = proposals.map{
              case (bidder, task) =>
                val bid = rule match {
                  case GlobalFlowtime =>
                    accept(currentAllocation, auctioneer, bidder, task).ongoingGlobalFlowtime
                  case _ =>
                    throw new RuntimeException(s"$name cannot apply the social rule $rule")
                }
                Future((task, bidder, bid))
            }
            val future: Future[(Task, ComputingNode, Double)] = Future.foldLeft(futures)(initialBid){
              case (best: (Task, ComputingNode, Double), bid: (Task, ComputingNode, Double)) =>
                if (bid._3 < best._3) bid
                else best
            }
            Await.result(future, TIMEOUT_VALUE)
          }
          if (auctioneer != bestBidder) {
            if (debug) println(s"$name reallocates $bestTask from $auctioneer to $bestBidder for flowtime $bestFlowtime ")
            val delegation = new SingleDelegation(stap, auctioneer, bestBidder, bestTask)
            currentAllocation = delegation.execute(currentAllocation)
            // Metrics
            nbDeals += 1
            nbDelegations +=1
            nbDelegatedTasks +=1
            avgEndowmentSize = 1
          } else {
            if (debug) println(s"$name does not reallocate any task from $auctioneer")
          }
          potentialDelegatedTasks = potentialDelegatedTasks.filter(_ != bestTask)
        }
      }
    }
    currentAllocation
  }
}

/**
 * Companion object to test it
 */
object RSSIBalancer extends App {
  val debug = false

  import org.smastaplus.example.stap.ex1.stap

  println(stap)
  val rule: SocialRule = GlobalFlowtime
  val balancer = new RSSIBalancer(stap, rule)
  val allocation = balancer.run()
  println(allocation)
  rule match {
    case GlobalFlowtime => println(allocation.get.globalFlowtime)
    case Makespan => println(allocation.get.makespan)
    case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
  }
}

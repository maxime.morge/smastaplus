// Copyright (C) Maxime MORGE 2022
package org.smastaplus.balancer.local

import org.smastaplus.core._
import org.smastaplus.process.{Deal, SingleDelegation, SingleSwap}
import org.smastaplus.utils.MathUtils._

import scala.util.Random

/**
  * Simulated annealing combines hill climbing with a random walk that yields both efficiency and completeness
  * @param stap instance to tackle
  * @param rule is the social rule to be applied
  * @param name of the balancer
  * @param withSwap is true if swap are allowed
  */
class SimulatedAnnealingBalancer(stap: STAP, rule: SocialRule, name: String = "SimulatedAnnealingBalancer",
                                 withSwap: Boolean = false)
  extends LocalBalancer(stap,rule, name) {

  val initialTemperature : Double = 100.0// e.g. 1, 10, 100
  val threshold : Double = 0.01 // e.g. 0.1, 0.01, 0.001
  val decayRate : Double = 0.99 // e.g. 0.9, 0.99, 0.999

  /**
    * Returns the acceptance probability that depends on the
    * @param currentEnergy energy of the current state
    * @param newEnergy energy of the new state
    * @param temperature a global time-varying parameter
    */
  def acceptanceProbability(currentEnergy : Double, newEnergy : Double, temperature : Double) : Double = {
    if (newEnergy ~< currentEnergy) return 1
    math.exp( -(newEnergy - currentEnergy) / temperature)
  }

  /**
    * Returns a random neighbor and the deal for it
    */
  def randomSuccessor(current: Allocation): (Allocation, Deal) = {
    var successors : List[(Allocation,Deal)] = List()
    stap.ds.computingNodes.foreach { initiator => // Foreach initiator
      current.bundle(initiator).foreach { task => // Foreach task in its bundle
        (stap.ds.computingNodes diff Set(initiator)).foreach { responder => // Foreach responder
          nbDelegation += 1
          val delegation = new SingleDelegation(stap, initiator, responder, task)
         successors ::= ((delegation.execute(current), delegation))
          if (withSwap) { // Let us consider the swaps from this delegation
            current.bundle(responder).foreach { counterpart => // Foreach counterpart
              nbSwap += 1
              val swap = new SingleSwap(stap, initiator, responder, task, counterpart)
              successors ::= ((swap.execute(current), swap))
            }
          }
        }
      }
    }
    successors(Random.nextInt(successors.length))
  }

  /**
    * Returns a local minima
    */
  @throws(classOf[RuntimeException])
  def reallocate(allocation: Allocation): Allocation = {
    initialLocalRatio = allocation.localAvailabilityRatio
    var current = scheduler.schedule(allocation)
    var temperature = initialTemperature
    while (temperature ~> threshold ){
      val (neighbor,deal) = randomSuccessor(current)
      val (newValue, currentValue) = rule match {
        case Makespan => (neighbor.makespan, current.makespan)
        case GlobalFlowtime => (neighbor.globalFlowtime, current.globalFlowtime)
        case rule => throw new RuntimeException(s"Error: Rule $rule does not match")
      }
      val p = acceptanceProbability(currentValue, newValue, temperature)
      if ( p > Random.nextDouble() ) {
        localRatio = current.localAvailabilityRatio
        current = neighbor
        deal match {
          case _: SingleSwap => nbSuccessfulSwap += 1
          case _: SingleDelegation => nbSuccessfulDelegation += 1
        }
      }
      temperature *= decayRate
    }
    // Update metrics
    nbDeals = nbSuccessfulDelegation + nbSuccessfulSwap
    nbDelegations = nbSuccessfulDelegation
    nbSwaps = nbSuccessfulSwap
    nbDelegatedTasks = nbDelegations + 2 * nbSwaps
    current
  }
}


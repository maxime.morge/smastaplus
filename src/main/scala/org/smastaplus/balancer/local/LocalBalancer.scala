// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.balancer.local

import org.smastaplus.balancer.Balancer
import org.smastaplus.core._
import org.smastaplus.scheduler._
import org.smastaplus.utils.MyTime

/**
  * Local-based balancer
  * @param stap instance to tackle
  * @param rule is the social rule to be applied
  * @param name of the balancer
  * @param withSwap is true if swap are allowed
  */
abstract class LocalBalancer(stap: STAP, rule: SocialRule, name: String = "LocalBalancer",
                                     withSwap: Boolean = false)
  extends Balancer(stap,rule, name) {

  // Specific metrics
  var initializationTime : Long = 0
  var initialLocalRatio = 0.0
  var nbSuccessfulSwap : Int = 0
  var nbSuccessfulDelegation : Int = 0
  var nbSwap : Int = 0
  var nbDelegation : Int = 0

  if (rule != GlobalFlowtime && rule != Makespan)
    throw new RuntimeException(s"$name cannot apply the social rule $rule")

  override def toString: String =
    s"$name with $rule (${MyTime.show(solvingTime)} with initialization ${MyTime.show(initializationTime)})"

  val scheduler = new Scheduler()

  /**
    * Returns an allocation
    */
  protected override def balance(): Option[Allocation] = {
    var initializationTime: Long = System.nanoTime()
    val initialAllocation = init()
    initialLocalRatio = initialAllocation.localAvailabilityRatio
    initializationTime = System.nanoTime() - initializationTime
    val finalAllocation = reallocate(initialAllocation)
    Some(finalAllocation)
  }

  /**
    * Returns an initial allocation which is random by default
    */
  def init(allocation: Allocation = ExecutedAllocation.randomAllocation(stap)): Allocation = {
    if (debug) println(s"$name: initial random allocation\n$allocation")
    allocation
  }

  /**
    * Modify the current allocation
    */
  @throws(classOf[RuntimeException])
  def reallocate(allocation: Allocation): Allocation
}


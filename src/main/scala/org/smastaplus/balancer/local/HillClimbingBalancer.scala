// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.balancer.local

import org.smastaplus.core._
import org.smastaplus.process.{SingleDelegation, SingleSwap}

/**
  * Hill-climbing balancer
  * @param stap instance to tackle
  * @param rule is the social rule to be applied
  * @param name of the balancer
  * @param withSwap is true if swap are allowed
  */
class HillClimbingBalancer(stap: STAP, rule: SocialRule, name: String = "HillClimbingBalancer",
                           withSwap: Boolean = false)
  extends LocalBalancer(stap,rule, name) {

  /**
    * Returns the best state which is a neighbor with the lowest objective function value
    */
  def highValueSuccessor(current: Allocation): Allocation = {
    var bestAllocation = new Allocation(stap)
    var bestValue = Double.MaxValue
    stap.ds.computingNodes.foreach { initiator => // Foreach initiator
      current.bundle(initiator).foreach { task => // Foreach task in its bundle
        (stap.ds.computingNodes diff Set(initiator)).foreach { responder => // Foreach responder
          nbDelegation += 1
          val delegation = new SingleDelegation(stap, initiator, responder, task)
          val nextAllocation = delegation.execute(current)
          val nextValue = rule match {
            case Makespan => nextAllocation.makespan
            case GlobalFlowtime => nextAllocation.globalFlowtime
            case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
          }
          if (nextValue <= bestValue) {
            nbSuccessfulDelegation += 1
            bestAllocation = nextAllocation
            bestValue = nextValue
          }
          if (withSwap) { // Let us consider the swaps from this delegation
            current.bundle(responder).foreach { counterpart => // Foreach counterpart
              nbSwap += 1
              val swap = new SingleSwap(stap, initiator, responder, task, counterpart)
              val nextAllocation = swap.execute(current)
              val nextValue = rule match {
                case Makespan => nextAllocation.makespan
                case GlobalFlowtime => nextAllocation.globalFlowtime
                case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
              }
              if (nextValue <= bestValue) {
                nbSuccessfulSwap += 1
                bestAllocation = nextAllocation
                bestValue = nextValue
              }
            }
          }
        }
      }
    }
    bestAllocation
  }

  /**
    * Returns the closest local minima from a current state
    */
  @throws(classOf[RuntimeException])
  def reallocate(allocation: Allocation): Allocation = {
    initialLocalRatio = allocation.localAvailabilityRatio
    var current = scheduler.schedule(allocation)
    var isTerminated = false
    while (!isTerminated) {
      val neighbor = highValueSuccessor(current)
      val (newValue, currentValue) = rule match {
        case Makespan => (neighbor.makespan, current.makespan)
        case GlobalFlowtime => (neighbor.globalFlowtime, current.globalFlowtime)
        case rule => throw new RuntimeException(s"Error: Rule $rule does not match")
      }
      if (currentValue <= newValue){
        localRatio = current.localAvailabilityRatio
        isTerminated = true
      }else {
        current = neighbor
      }
    }
    // Update metrics
    nbDeals = nbSuccessfulDelegation + nbSuccessfulSwap
    nbDelegations = nbSuccessfulDelegation
    nbSwaps = nbSuccessfulSwap
    nbDelegatedTasks = nbDelegations + 2 * nbSwaps
    current
  }

}

/**
  * Companion object to test it
  */
object HillClimbingBalancer extends App {
  val debug = false

  import org.smastaplus.example.allocation.stap1.ex1lcjf._
  import org.smastaplus.example.stap.ex1.stap

  println(stap)
  val rule: SocialRule = GlobalFlowtime
  val balancer = new HillClimbingBalancer(stap, rule, "HillClimbingBalancer", true)
  println(a)
  rule match {
    case GlobalFlowtime => println("GlobalFlowtime: " + a.globalFlowtime)
    case Makespan => println("Makespan: " + a.makespan)
    case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
  }
  val outcome = balancer.reallocate(a)
  println(outcome)
  rule match {
    case GlobalFlowtime => println("GlobalFlowtime: " + outcome.globalFlowtime)
    case Makespan => println("Makespan: " + outcome.makespan)
    case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
  }
  println(s"Initial locality = ${balancer.initialLocalRatio}")
  println(s"Final locality = ${balancer.localRatio}")
  println(s"nbDeals = ${balancer.nbDeals}")
  println(s"nbDelegations = ${balancer.nbDelegations}")
  println(s"nbSwaps = ${balancer.nbSwaps}")
}
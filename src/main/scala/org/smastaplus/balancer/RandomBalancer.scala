// Copyright (C) Maxime MORGE 2018, 2022
package org.smastaplus.balancer

import org.smastaplus.core._

/**
  * Random balancer
  * @param stap instance to be solved
  * @param rule is the social rule to be applied
  * @param name of the balancer
  */
class RandomBalancer(stap: STAP, rule: SocialRule, name : String = "RandomBalancer") extends Balancer(stap, rule, name) {
  debug = false

  /**
    * Returns an allocation
    */
  protected override def balance(): Option[ExecutedAllocation] = Some(ExecutedAllocation.randomAllocation(stap))
}

/**
* Companion object to test it
*/
object RandomBalancer extends App {
  val debug = false
  import org.smastaplus.example.stap.ex1.stap
  println(stap)
  val rule : SocialRule = GlobalFlowtime
  val balancer = new RandomBalancer(stap,rule)
  val allocation = balancer.run()
  println(allocation)
  rule match {
    case GlobalFlowtime => println(allocation.get.globalFlowtime)
    case Makespan => println(allocation.get.makespan)
    case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
  }
}


// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.balancer.deal

import org.smastaplus.core._
import org.smastaplus.balancer.Balancer
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.ProposalStrategy
import org.smastaplus.utils.MyTime

/**
  * Abstract balancer based on deals (delegation or swap) whatever it is decentralized or not
  * @param stap instance to tackle
  * @param rule to adopt for reducing either the localFlowtime or the makespan
  * @param firstProposalStrategy for making proposals in the first stage
  * @param secondProposalStrategy for eventually making proposals in the second stage
  * @param counterProposalStrategy for making counter-proposals in the second stage
  * @param name of the balancer
  */
abstract class DealBalancer(stap: STAP,
                            rule: SocialRule,
                            firstProposalStrategy: ProposalStrategy,
                            secondProposalStrategy: Option[ProposalStrategy],
                            counterProposalStrategy: CounterProposalStrategy,
                            name: String = "DealBalancer") extends Balancer(stap, rule, name){

  var initializationTime : Long = 0

  var initialLocalityRate = 0.0

  var trace = false

  override def toString: String = s"$name with $rule (${MyTime.show(solvingTime)} with initialization ${MyTime.show(initializationTime)})"

  /**
    * Returns an initial allocation
    */
  def init(allocation: Allocation = Allocation.randomAllocation(stap)): Allocation

  /**
    * Modifies the current allocation
    */
  def reallocate(allocation: Allocation) : Allocation

  /**
    * Returns an allocation
    */
  protected override def balance(): Option[Allocation] = {
    var initializationTime : Long = System.nanoTime()
    val initialAllocation = init()
    initialLocalityRate = initialAllocation.localAvailabilityRatio
    initializationTime = System.nanoTime() - initializationTime
    val finalAllocation = reallocate(initialAllocation)
    Some(finalAllocation)
  }

  /**
    * Closes the balancer
    */
  def close() : Unit = {
  }
}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2020, 2021, 2022
package org.smastaplus.balancer.deal.mas

import org.smastaplus.core._
import org.smastaplus.process.{Delegation, Swap}
import org.smastaplus.strategy.consumption.{Bundle, Insight}

import org.joda.time.LocalDateTime
import java.io.PrintWriter

/**
  * Class representing a message og the agent communication language
  * @param msgId of the message
  * @param replyId is eventually the id of the message to which it replies
  *
  */
class Message(msgId: Int = Message.ID, replyId: Option[Int] = None) {
  Message.ID +=1 // Increment the static variable
}

/**
  * Companion object for adding a static member
  */
object Message{
  var ID = 1
}

// Message for negotiation
case class Propose(delegation: Delegation, insight: Insight) extends Message(delegation.hashCode(), None){// Make a proposal
  override def toString: String = s"Propose($delegation,$insight)"
}
case class Reject(delegation: Delegation, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Reject a proposal
  override def toString: String = s"Reject($delegation,$insight)"
}
case class Accept(delegation: Delegation, insight: Insight, msgId :Int = Message.ID, replyId : Int) extends Message(msgId, Some(replyId)) { // Accept a proposal
  override def toString: String = s"Accept($delegation,$insight)"
}
case class CounterPropose(swap: Swap, insight: Insight, replyId: Int) extends Message(swap.hashCode(), Some(replyId)) { // Make a counter-proposal
  override def toString: String = s"CounterPropose($swap,$swap,$insight)"
}
case class Confirm(deal: Delegation, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Confirm a bilateral deal (delegation)
  override def toString: String = s"Confirm($deal,$insight)"
}
case class ConfirmSwap(deal: Swap, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Confirm a bilateral deal (swap)
  override def toString: String = s"ConfirmSwap($deal,$insight)"
}
case class Withdraw(delegation: Delegation, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Withdraw a deal (delegation)
  override def toString: String = s"Withdraw($delegation,$insight)"
}
case class WithdrawSwap(swap: Swap, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Withdraw a deal (swap)
  override def toString: String = s"Withdraw($swap,$insight)"
}
case class Inform(insight: Insight) extends Message(Message.ID, None) { // Inform the peer about the insight
  override def toString: String = s"Inform($insight)"
}

// Internal message for the negotiator
case class Next() extends Message(Message.ID, None) { // Trigger the offer strategy of an agent
  override def toString: String = s"Next"
}
case class Wakeup(delegation: Delegation) extends Message(Message.ID, None) { // Trigger the proposal of the delegation
  override def toString: String = s"WakeUp($delegation)"
}
case class Timeout() extends Message(Message.ID, None) { // Trigger the offer strategy of an agent
  override def toString: String = s"Timeout"
}
case class Close() extends Message(Message.ID, None) { // An agent is restarted
  override def toString: String = s"Close"
}

// Messages from/to the supervisor
case class Trace(writer: PrintWriter) extends Message(Message.ID, None){// Ask the agents to trace the protocol
  override def toString: String = s"Trace"
}
case class Start() extends Message(Message.ID, None){// Start the supervisor/agents
  override def toString: String = s"Start"
}
case class Light(directory : Directory) extends Message(Message.ID, None){ // Light the  agents with their acquaintances
  override def toString: String = s"Trigger($directory)"
}
case class Outcome(allocation: Allocation, metrics: Metrics) extends Message(Message.ID, None){ // the outcome of the distributed balancer
  override def toString: String = s"Outcome($allocation,$metrics)"
}
case class Init(allocation: Allocation) extends Message(Message.ID, None) { // Init the supervisor with the allocation
  override def toString: String = s"Init($allocation)"
}
case class Give(bundle: List[Task]) extends Message(Message.ID, None) { // Give a bundle to an agent
  override def toString: String = s"Give($bundle)"
}
case class Ready() extends Message(Message.ID, None) {// The agent is ready to start
override def toString: String = s"Ready"
}
case class End(bundle: List[Task]) extends Message(Message.ID, None) { // An agent is ended with a bundle
  override def toString: String = s"End($bundle)"
}
case class Restart() extends Message(Message.ID, None) { // An agent is restarted
  override def toString: String = s"Restart"
}
case class Query(msgId: Int = Message.ID) extends Message(msgId, None) { // Query the insight nbProposals to an agent
  override def toString: String = s"Query"
}
case class Answer(metrics: Metrics, replyId: Int) extends Message(Message.ID, Some(replyId)) { // Reply with some metrics
  override def toString: String = s"Answer($metrics)"
}
case class PreTrigger(stage: Int) extends Message(Message.ID, None) { // PreTrigger a stage
  override def toString: String = s"PreTrigger($stage)"
}
case class ReadyForNextStage() extends Message(Message.ID, None) { // the agent is ready for the next stage
  override def toString: String = s"ReadyForNextStage"
}
case class Trigger(stage: Int) extends Message(Message.ID, None) { // Trigger a stage
  override def toString: String = s"TriggerStage($stage)"
}
case class Kill() extends Message(Message.ID, None) { // Kill the agent
  override def toString: String = s"Kill"
}
case class Finished() extends Message(Message.ID, None) { // Response to a kill message
  override def toString: String = s"Finished"
}

// Message between the supervisor and the monitor
case class Stop() extends Message(Message.ID, None) { // Stop the monitor
  override def toString: String = s"Stop"
}
case class TriggerSecondStage() extends Message(Message.ID, None) { // Time markup
  override def toString: String = s"TriggerSecondStage"
}
case class TriggerFirstStage() extends Message(Message.ID, None) { // Time markup
  override def toString: String = s"TriggerFirstStage"
}
case class EndNegotiation() extends Message(Message.ID, None) { // Time markup
  override def toString: String = s"EndNegotiation"
}

//Message between the manager and the negotiator
//Message from the manager to the negotiator
case class GiveUpdatedBundle(bundle: Bundle, timestamp: LocalDateTime = LocalDateTime.now()) extends Message(Message.ID, None) { // Commission a negotiator to delegate a bundle
  override def toString: String = s"GiveUpdatedBundle($bundle, $timestamp)"
}
case class StartStage() extends Message(Message.ID, None) { // Tell the negotiator to start the next stage
  override def toString: String = s"StartStage"
}
//Messages from the negotiator to the manager
case class FinishedStage(metrics: Metrics) extends Message(Message.ID, None) { // Inform the manager about the end of the current stage
  override def toString: String = s"FinishedStage($metrics)"
}
case class Add(endowment: List[Task]) extends Message(Message.ID, None) { // Ask the manager to add a endowment in the bundle
  override def toString: String = s"Add($endowment)"
}
case class Remove(endowment: List[Task]) extends Message(Message.ID, None) { // Ask the manager to remove a task from the bundle
  override def toString: String = s"Remove($endowment)"
}
case class Replace(endowment: List[Task], counterpart: List[Task]) extends Message(Message.ID, None) { // Ask the manager to replace a task by an other in the bundle
  override def toString: String = s"Replace($endowment, $counterpart)"
}

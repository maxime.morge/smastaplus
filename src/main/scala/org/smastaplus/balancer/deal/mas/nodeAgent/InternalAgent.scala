// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022
package org.smastaplus.balancer.deal.mas.nodeAgent

import org.smastaplus.core._
import org.smastaplus.strategy.consumption._
import org.smastaplus.balancer.deal.mas._
import org.smastaplus.utils.Timer

import akka.actor.{Actor, ActorRef, Props, Stash}
import java.io.{OutputStream, OutputStreamWriter, PrintWriter}
import java.util.concurrent.ThreadLocalRandom

/**
  * Abstract internal agent
  * @param stap instance
  * @param node represented by the agent
  * @param monitor if any
  */
abstract class InternalAgent(stap: STAP,
                             node: ComputingNode,
                             monitor: Option[ActorRef]) extends Actor with Stash {
  var trace: Boolean = false
  var internalTrace = false
  var debug: Boolean = false
  var debugState: Boolean = false
  var debugDeadline : Boolean  = false

  var outputStream: OutputStream = OutputStream.nullOutputStream
  var outputStreamWriter = new OutputStreamWriter(outputStream)
  var writer = new PrintWriter(outputStreamWriter, true) // new PrintWriter(System.out, true)

  var nbTimers = 0

  var supervisor: ActorRef = _
  var directory: Directory = new Directory()
  val rnd : ThreadLocalRandom = ThreadLocalRandom.current()

  /**
    * Returns the deadline for  the next proposal
   */
  def deadline(): Long = {
    val time = rnd.nextLong(300) + 600
    if (debugDeadline) println(s"$node generates deadline with $time nanoseconds")
    time
  }

  /**
    * Policy for stashing received proposals
    */
  def randomStash() : Unit ={
    if (rnd.nextBoolean()) stash()
  }

  /**
    * Multicast a message to some peers
    */
  def multicast(peers : Iterable[ComputingNode], message : Message): Unit = {
    if (monitor.isDefined) monitor.get ! message
    var isFirst = true
    var log = ""
    peers.foreach { peer =>
      if (trace){
        if (isFirst) log += s"$node-> $peer : $message\n"
        else log += s"& $node -> $peer :\n"
      }
      isFirst = false
      directory.addressOf(peer) ! message
    }
    if (trace) writer.print(log)
  }

  /**
    * Broadcast a message to neighbours
    */
  def broadcast(message : Message): Unit = {
    multicast(directory.peers(node), message)
  }

  var timer : ActorRef = _
  /**
    * Start timer
    */
  def startTimer(): Unit ={
    nbTimers += 1
    val name = s"Timer${nbTimers}4$node".replace('ν','n')
    timer = context.actorOf(// build the actor
      Props(classOf[Timer],deadline()).withDispatcher("akka.actor.default-dispatcher"),name)
    timer ! Timer.Start
  }

  /**
    * Cancel timer
    */
  def cancelTimer(): Unit = timer ! Timer.Cancel

  /**
    * Broadcast insight
    */
  def broadcast(insight: Insight): Unit = broadcast(Inform(insight))
}
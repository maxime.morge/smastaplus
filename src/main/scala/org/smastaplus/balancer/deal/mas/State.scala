// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.balancer.deal.mas

/**
 * Trait representing a state in a FSM
 * for a behaviour
 */
trait State

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022
package org.smastaplus.balancer.deal.mas.nodeAgent.manager

import org.smastaplus.balancer.deal.mas.State

/**
  * Trait representing as state in a FSM
  * for the manager behaviour
  */
trait ManagerState extends State
case object Initial extends ManagerState
case object Running extends ManagerState
case object EndStage extends ManagerState
case object TransitionState extends ManagerState
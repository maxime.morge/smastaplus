// Copyright (C) Maxime MORGE 2021, 2022
package org.smastaplus.balancer.deal.mas.monitor

import org.smastaplus.balancer.deal.mas._
import org.smastaplus.core._
import org.smastaplus.strategy.consumption.BeliefBase
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.ProposalStrategy

import java.io._
import scala.concurrent.duration._
import akka.actor.{Actor, ActorRef}
import org.joda.time.LocalDateTime

/**
  * Monitor the insight during negotiation
  * @param stap instance
  * @param rule is the social rule
  * @param proposalStrategy of the negotiating agents
  * @param counterProposalStrategy of the negotiating agents
  */
class Monitor(stap: STAP,
              rule: SocialRule,
              proposalStrategy: ProposalStrategy,
              counterProposalStrategy: CounterProposalStrategy) extends Actor {

  var debug: Boolean = false

  var supervisor: ActorRef = context.parent
  var directory: Directory = new Directory()

  var informers: Set[ComputingNode] = Set.empty
  var beliefBase: BeliefBase = new BeliefBase(timestamp = Map[ComputingNode, LocalDateTime]())
  var startingTime: Long = System.nanoTime()
  var gift : Map[ComputingNode, Int] = stap.ds.computingNodes.map(n => n->0).toMap
  var waitingAllInforms = true

  def folder : String = "./"
  def fileName : String = folder + s"$rule$proposalStrategy${counterProposalStrategy}log.csv"
  val writer = new PrintWriter(new File(fileName))
  val head: String = s"timestamp,meanGlobalFlowtime,makespan," +
    stap.ds.computingNodes.toList.flatMap(node => stap.jobs.map(job =>  s"C$node($job)"))
      .mkString("",",",",") +
    stap.ds.computingNodes.toList.map(node => s"Gift($node)")
      .mkString("",",","\n")
  writer.write(head)

  /**
    * Returns the belief about the completion time for the job by a node
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTime(job: Job, node: ComputingNode): Double =
    beliefBase.beliefCompletionTime(job, node)

  /**
    * Returns the belief about the completion time for the job in the allocation
    */
  def beliefCompletionTime(job: Job): Double =
    stap.ds.computingNodes.map(node => beliefCompletionTime(job, node)).max

  /**
    * Returns the belief about the global flowtime for the job in the allocation
    */
  def beliefGlobalFlowtime: Double =
    stap.jobs.foldLeft(0.0)((sum, job) => sum + beliefCompletionTime(job))

  /**
    * Returns the belief about the global flowtime for the job in the allocation
    */
  def beliefMeanGlobalFlowtime: Double = beliefGlobalFlowtime / stap.m.toDouble

  /**
    * Returns what the monitor believes about the workload for the node
    */
  def beliefWorkload(node: ComputingNode): Double =  beliefBase.belief(node).values.sum

  /**
    * Returns what the monitor believes about the makespan
    */
  def beliefMakespan: Double = stap.ds.computingNodes.map(node => beliefWorkload(node)).max

  /**
    * Process the messages
    */
  override def receive: Receive = {
    case Light(directory)  =>
      startingTime = System.nanoTime()
      supervisor = sender()
      this.directory = directory
      if (debug) println(s"Monitor> receives the directory $directory")

    case Inform(insight) =>
      val node: ComputingNode = directory.nodeOf(sender())
      if (debug && node.name == "n1") println(s"Monitor> receives an informance from n1")
      beliefBase= beliefBase.update(node, insight)
      informers += node
      if (waitingAllInforms && informers.size == stap.ds.m) { // first log
        updateLog()
        //informers = Set.empty
        waitingAllInforms = false
      }
    case Give(endowment)  =>
      val node: ComputingNode = directory.nodeOf(sender())
      gift = stap.ds.computingNodes.map { n: ComputingNode =>
        if (n != node) n -> gift(n)
        else n -> (gift(n) + endowment.size)
      }.toMap
      updateLog()

    case TriggerSecondStage =>
      val timestamp = System.nanoTime() - startingTime
      if (debug) println(s"End first stage $proposalStrategy $counterProposalStrategy: ${timestamp.nanos.toMillis}")

    case EndNegotiation =>
      val timestamp = System.nanoTime() - startingTime
      if (debug) println(s"End negotiation $proposalStrategy $counterProposalStrategy: ${timestamp.nanos.toMillis}")

    case Stop =>
      if (debug) println(s"Monitor> stops")
      writer.close()
      context.stop(self) //stops the Monitor
  }

  /**
    * Update log
    */
    def updateLog() : Unit =  {
      val timestamp = System.nanoTime() - startingTime
      val outcome = s"$timestamp,$beliefMeanGlobalFlowtime,$beliefMakespan," +
        stap.ds.computingNodes.toList.flatMap(node => stap.jobs.toList.map(job =>  s"${beliefCompletionTime(job,node)}"))
          .mkString("",",",",") + // C$node($job)=
        stap.ds.computingNodes.toList.map(node => gift(node))
          .mkString("",",","\n")
      if (debug) print(outcome)
      writer.write(outcome)
      writer.flush()
    }
}
// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2020, 2021, 2022, 2023
package org.smastaplus.balancer.deal.mas.supervisor

import org.smastaplus.core._
import org.smastaplus.balancer.deal.mas.Metrics

/**
  * Immutable status of the supervisor
  * @param allocation i.e the current allocation
  * @param isTwoStageProcess is a boolean which is true if the negotiation process includes two stages, false otherwise
  * @param sumReady is the number of agents which are ready to receive their initial bundle, initially 0
  * @param desperateNodes nodes which have no more potential bilateral reallocation, initially none
  * @param sumReadyForNext is the number of agents which are ready to start the next stage
  * @param answerNodes is the number of received answer for the submitted query at the end of the current stage, initially 0
  * @param lastQueryId is the id of the last query message sent by the usr
  * @param sumKilled is the number of agents which are properly killed, initially 0
  * @param metrics for the negotiation process
  */
class SupervisorStatus(val allocation: Allocation,
                       val isTwoStageProcess: Boolean,
                       val sumReady: Int = 0,
                       val desperateNodes: Set[ComputingNode] = Set(),
                       val sumReadyForNext: Int = 0,
                       val answerNodes: Set[ComputingNode] = Set(),
                       val sumKilled: Int = 0,
                       val lastQueryId: Option[Int] = None,
                       val metrics: Metrics = new Metrics()
                      ) {
  val debug = false

  /**
    * Returns the status where the number of ready agents is incremented
    */
  def incrementNbReady : SupervisorStatus =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = sumReady + 1, desperateNodes = desperateNodes, sumReadyForNext = sumReadyForNext, answerNodes = answerNodes, sumKilled = sumKilled, lastQueryId = lastQueryId, metrics = metrics)

  /**
    * Returns the status where the number of ready agents for the next stage is incremented
    */
  def incrementNbReadyForNextStage : SupervisorStatus =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = sumReady, desperateNodes = desperateNodes, sumReadyForNext = sumReadyForNext + 1, answerNodes = answerNodes, sumKilled = sumKilled, lastQueryId = lastQueryId, metrics = metrics)


  /**
    * Returns the status where the number of ready agents, the desperate nodes,
    * the number agents ready for next stage, the number of answers
    * the number of reallocations during the last stage
    * and the query id is reset
    */
  def resetStage : SupervisorStatus =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = 0, desperateNodes = Set(), sumReadyForNext = 0, answerNodes = Set(), sumKilled = sumKilled, lastQueryId = None, metrics.resetNbReallocationCurrentStage())


  /**
    * Returns the status where the number of agents ready for next stage is reset
    */
  def resetForNextStage : SupervisorStatus =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = sumReady, desperateNodes = desperateNodes, sumReadyForNext = 0, answerNodes = answerNodes, sumKilled = sumKilled, lastQueryId = lastQueryId, metrics = metrics)

  /**
    * Returns the status where the number of killed agents is incremented
    */
  def incrementNbKilled : SupervisorStatus =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = sumReady, desperateNodes = desperateNodes, sumReadyForNext = sumReadyForNext, answerNodes = answerNodes, sumKilled = sumKilled + 1, lastQueryId = lastQueryId, metrics = metrics)

  /**
    * Returns the status where the metrics are updated
    */
  def updateMetrics(updatedMetrics: Metrics, node: ComputingNode) =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = sumReady, desperateNodes = desperateNodes, sumReadyForNext = sumReadyForNext, answerNodes = answerNodes + node, sumKilled = sumKilled, lastQueryId = lastQueryId, metrics = metrics.update(updatedMetrics))

  /**
    * Returns the status where the node ends and so it is added to the desperate nodes and
    * the bundle is allocated to the node
    */
  def end(node : ComputingNode, bundle : List[Task]): SupervisorStatus = {
    val updatedStatus = new SupervisorStatus(allocation  = allocation.update(node, bundle), isTwoStageProcess = isTwoStageProcess, sumReady = sumReady, desperateNodes = desperateNodes + node, sumReadyForNext = sumReadyForNext, answerNodes = answerNodes, sumKilled =sumKilled,lastQueryId = lastQueryId, metrics = metrics)
    if (debug) {
      println(s"SupervisorStatus>$node is desperate with bundle $bundle")
      println(s"SupervisorStatus>${desperateNodes.size+1} agents are desperate")
    }
    updatedStatus
  }

  /**
    * Returns the status where the node restarts and so it is remove to the desperate nodes
    */
  def restart(node : ComputingNode): SupervisorStatus =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = sumReady, desperateNodes = desperateNodes - node, sumReadyForNext = sumReadyForNext, answerNodes = Set(), sumKilled = sumKilled, lastQueryId = lastQueryId, metrics = metrics)

  /**
    * Returns the status where the last query id is setup
    */
  def setLastQueryId(id : Int): SupervisorStatus =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = sumReady, desperateNodes = desperateNodes, sumReadyForNext = sumReadyForNext, answerNodes = answerNodes, sumKilled = sumKilled, lastQueryId = Some(id), metrics = metrics)

  /**
    * Returns the status where the last query id is setup
    */
  def resetLastQueryId: SupervisorStatus =
    new SupervisorStatus(allocation = allocation, isTwoStageProcess = isTwoStageProcess, sumReady = sumReady, desperateNodes = desperateNodes, sumReadyForNext = sumReadyForNext, answerNodes = answerNodes, sumKilled = sumKilled, lastQueryId = None, metrics = metrics)
}


// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.balancer.deal.mas.nodeAgent.manager

import org.smastaplus.core._
import org.smastaplus.balancer.deal.mas.nodeAgent.InternalAgent
import org.smastaplus.balancer.deal.mas.Metrics

import akka.actor.ActorRef

/**
  * Abstract manager agent
  * @param stap instance
  * @param node represented by the agent
  * @param negotiator is the corresponding actor
  * @param monitor if any
 */
abstract class Manager(stap: STAP,
                       node: ComputingNode,
                       negotiator : ActorRef,
                       monitor: Option[ActorRef])
  extends InternalAgent(stap,node, monitor) {

    var metrics = new Metrics()  // Metrics from the negotiator
}
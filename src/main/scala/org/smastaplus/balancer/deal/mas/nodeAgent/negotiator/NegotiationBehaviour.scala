// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022, 2023
package org.smastaplus.balancer.deal.mas.nodeAgent.negotiator

import org.smastaplus.core._
import org.smastaplus.process._
import org.smastaplus.balancer.deal.mas._
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.ProposalStrategy
import org.smastaplus.strategy.consumption.Bundle
import org.smastaplus.consumer.deal.mas.nodeAgent.negotiator.NegotiatingMind
import org.smastaplus.provisioning._
import org.smastaplus.utils.Timer

import akka.actor.{ActorRef, FSM}
import akka.pattern.ask
import akka.util
import akka.util.Timeout
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Negotiating agent with a concrete behaviour
  * @param stap instance
  * @param node represented by the agent
  * @param rule to adopt for reducing either the localFlowtime or the globalFlowtime or the makespan
  * @param firstProposalStrategy for making proposals in the first stage
  * @param secondProposalStrategy for eventually making proposals in the second stage
  * @param counterProposalStrategy for making counter-proposals in the second stage
  * @param monitor if any
  */
class NegotiationBehaviour(stap: STAP, node: ComputingNode, rule: SocialRule,
                           firstProposalStrategy: ProposalStrategy,
                           secondProposalStrategy: Option[ProposalStrategy],
                           counterProposalStrategy: CounterProposalStrategy,
                           monitor: Option[ActorRef])
  extends Negotiator(stap, node, firstProposalStrategy, counterProposalStrategy, monitor)
    with FSM[NegotiationState, NegotiatingMind]{

  debug = false
  val debugCounterPropose = false
  val debugInform = false
  val debugPhase = false
  val debugTimestamp = false
  val debugSelfMessage = false

  var isFirstStage = true
  var proposalStrategy: ProposalStrategy = firstProposalStrategy

  implicit val timeout: util.Timeout = Timeout(6000 minutes)

  /**
    * Asks the manager to update the bundle and waits for the answer to update the mind
   * Synchronous message
    * @param request: the message to send to the manager
    * @param mind: the current mind of the negotiator
    * @return the mind updated with the new bundle
    */
  def askManager(request: Message, mind: NegotiatingMind) : NegotiatingMind = {
      val future = manager ? request
      val newBundle : Option[Bundle] = try {
        Some(Await.result(future, timeout.duration).asInstanceOf[GiveUpdatedBundle].bundle)
      } catch {
        case _: Exception => None
      }
      if (newBundle.isDefined){
        val updatedMind = mind.setBundle(newBundle.get) //updating the belief base
        if (trace && debugInform) writer.println(s"$node updates insight since GiveUpdatedBundle from manager")
        return updatedMind
      }else{
        // do nothing
      }
      mind
  }

  /**
    * Initially the agent
    * - is active
    * - assumes all the computing nodes are active
    * - has no task in the bundle,
    * - neither received informs,
    * - nor last offer.
    * - is neither proactive,
    * - nor desperate,
    * - and in the first stage
    */
  startWith(
    Initial,
    new NegotiatingMind(node,
      peers = stap.ds.computingNodes.toSet - node,
      usage =  Map[ComputingNode, ProvisioningUsage]() ++ stap.ds.computingNodes.view.map(i => i -> Active),
      stap,
      taskSet = Set.empty[Task],
      informers = Set.empty[ComputingNode],
      lastOffer = None,
      isProactive = false,
      isDesperate = false,
      isFirstStage = true)
  )

  /**
    * Either the agent is in the initial state where
    * it is waiting for its initial bundle to become Waiting and some
    * information to become Responder
    */
  when(Initial) {
    // An enlightenment of the agents about its acquaintances
    case Event(Light(directory), mind)  =>
      this.directory = directory
      stay() using mind

    // A gift of a bundle to the agent which updates its belief base becomes a responder
    case Event(GiveUpdatedBundle(bundle, timestamp), mind) if mind.informers.size == mind.peers.size =>
      manager = sender()
      val updatedMind = mind.setBundle(bundle)
      updatedMind.updateTimestamp(timestamp)
      // broadcast(updatedMind.insight) done by the manager
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      goto(Responder) using updatedMind

    // A gift of a bundle to the agent which takes it and waits for some Inform messages
    case Event(GiveUpdatedBundle(bundle, timestamp), mind) if mind.informers.size < mind.peers.size =>
      manager = sender()
      val updatedMind = mind.setBundle(bundle)
      updatedMind.updateTimestamp(timestamp)
      unstashAll()
      goto(Waiting) using updatedMind

    // An information of the agent which updates the belief base and
    // increments nbInforms
    case Event(Inform(insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      var updatedMind = mind.updateInformers(peer)
      if (debugInform) println(s"Negotiator $node updates insight since Inform from $peer: $insight")
      updatedMind = updatedMind.updateInsight(peer, insight)
      stay() using updatedMind

    // A proposal to the agent which updates it belief base and
    // stashes it
    case Event(Propose(_, insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Propose from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      stash()
      stay() using updatedMind
  }

  /**
    * Either the agent, which has its initial bundle, is waiting for some
    * information to become a Responder
    */
  when(Waiting) {
    // An information of the agent which updates the belief base,
    // sorts the bundle and become Responder
    case Event(Inform(insight), mind)
      if mind.informers.size == mind.peers.size - 1 &&
        !mind.informers.contains(directory.nodeOf(sender())) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      var updatedMind = mind.updateInformers(peer)
      if (debugInform) println(s"Negotiator $node updates insight since Inform from $peer: $insight")
      updatedMind = updatedMind.updateInsight(peer, insight)
      updatedMind = updatedMind.resetInformers
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      goto(Responder) using updatedMind

    // An information of the agent which updates the belief base and
    // waits for other information
    case Event(Inform(insight), mind)
      if mind.informers.size < mind.peers.size - 1
        || mind.informers.contains(directory.nodeOf(sender())) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      var updatedMind = mind.updateInformers(peer)
      if (debugInform) println(s"Negotiator $node updates insight since Inform from $peer: $insight")
      updatedMind = updatedMind.updateInsight(peer, insight)
      stay() using updatedMind

    // A proposal to the agent which updates it belief base and
    // stashes it
    case Event(Propose(_, insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Propose from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      stash()
      stay() using updatedMind
  }

  /**
    * Or the agent is a responder which can :
    * - accept a delegation and become Contractor ;
    * - propose a delegation and become Proposer ;
    * - be desperate and become in Pause.
    */
  when(Responder) {

    // A self-trigger of the negotiation strategy may lead the agent to become Proposer
    case Event(Next, mind) if proposalStrategy.selectOffer(mind, rule).isDefined =>
      // This is required since the previous offer can be empty
      // MM: I am very sorry but I do not understand why
      val delegation: Delegation = if (proposalStrategy.previousOffer.isEmpty) {
        if (debug) println(s"AgentBehaviour>WARNING ${mind.me} selects an empty offer ?!")
        proposalStrategy.selectOffer(mind, rule).get
      } else proposalStrategy.previousOffer.get
      if (debug) {
        println(s"AgentBehaviour>${mind.me} selects and proposes $delegation")
        println(s"AgentBehaviour>${mind.me} my insight= ${mind.insight(mind.getTimestamp)}")
        println(s"AgentBehaviour>${mind.me} beliefBase= ${mind.beliefBase}")
      }
      if (trace && debugTimestamp) writer.println(s"$node ->  ${delegation.recipient} : Propose($delegation) with timestamp ${mind.getTimestamp}")
      if (trace && !debugTimestamp) writer.println(s"$node ->  ${delegation.recipient} : Propose($delegation)")
      if (trace && debugInform) writer.println(s"$node proposes since\n ${mind.beliefBase} and ${mind.insight(mind.getTimestamp)}")
      val receiver = directory.addressOf(delegation.recipient)
      val proposal = Propose(delegation, mind.insight(mind.getTimestamp))
      receiver ! proposal
      metrics = metrics.incrementProposals()
      val updatedMind = mind.setLastProposalId(delegation.hashCode())
      startTimer()
      if (trace && debugPhase) writer.println(s"$node GOTO PROPOSER") //DEBUG
      goto(Proposer) using updatedMind

    // A self-trigger of the negotiation strategy may lead the agent to become in Pause
    case Event(Next, mind) if proposalStrategy.selectOffer(mind, rule).isEmpty =>
      if (debug) println(s"AgentBehaviour>$node does not consider any delegation with ${mind.tasks} in state Responder")
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Close")
      self ! Close
      if (trace && debugPhase) writer.println(s"$node GOTO PAUSE") //DEBUG
      goto(Pause) using mind

    // A wake up which leads the agent to make proposal and
    // become a proposer
    case Event(Wakeup(delegation), mind) =>
      val proposal = Propose(delegation,mind.insight(mind.getTimestamp))
      val updatedMind = mind.setLastProposalId(delegation.hashCode())
      if (debug) {
        println(s"AgentBehaviour>${updatedMind.me} my insight= ${updatedMind.insight(updatedMind.getTimestamp)}")
        println(s"AgentBehaviour>${updatedMind.me} beliefBase= ${updatedMind.beliefBase}")
      }
      if (trace && !debugTimestamp) writer.println(s"$node -> ${delegation.recipient} : Propose($delegation)")
      if (trace) writer.println(s"$node -> ${delegation.recipient} : Propose($delegation)")
      if (trace && debugInform) println(s"since\n ${updatedMind.beliefBase} and ${updatedMind.insight(updatedMind.getTimestamp)}")
      val receiver: ActorRef = directory.addressOf(delegation.recipient)
      receiver ! proposal
      metrics = metrics.incrementProposals()
      startTimer()
      if (trace && debugPhase) writer.println(s"$node GOTO PROPOSER") //DEBUG
      goto(Proposer) using updatedMind

    // A proposal which is evaluated in order to
    // counter-propose a task during the second stage and the
    // agent become a contractor
    case Event(Propose(delegation, insight), mind)
      if mind.tasks.intersect(delegation.endowment.toSet).isEmpty && !isFirstStage
        && counterProposalStrategy.selectCounterOffer(delegation, mind.updateInsight(directory.nodeOf(sender()), insight), rule).nonEmpty =>
      val swap: Swap = counterProposalStrategy.previousCounterOffer.get
      val donor = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Propose from $donor: $insight")
      val updatedMind = mind.updateInsight(donor, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Propose\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      if (debug) {
        println(s"AgentBehaviour>${updatedMind.me} with ${updatedMind.tasks} receives a proposal of ${delegation.endowment} from ${delegation.donor} (${directory.nodeOf(sender())})")
        println(s"AgentBehaviour>${updatedMind.me} sends a counter-offer with ${swap.counterparts}")
        println(s"AgentBehaviour>${updatedMind.me} my insight= ${updatedMind.insight(updatedMind.getTimestamp)}")
        println(s"AgentBehaviour>${updatedMind.me} $donor insight= $insight")
        println(s"AgentBehaviour>${updatedMind.me} beliefBase= ${updatedMind.beliefBase}")
      }
      if (trace) writer.println(s"$node -> $donor : CounterPropose($swap)")
      sender() ! CounterPropose(swap, updatedMind.insight(mind.getTimestamp), delegation.hashCode())
      metrics = metrics.incrementCounterProposals()
      if (trace && debugPhase) writer.println(s"$node GOTO CONTRACTOR")
      goto(Contractor) using updatedMind

    // A proposal which is evaluated in order to
    // be accepted and the agent becomes a contractor
    // if agent is in first stage or if there is no triggerable swap
    case Event(Propose(delegation, insight), mind)
      if mind.tasks.intersect(delegation.endowment.toSet).isEmpty
        &&(isFirstStage || counterProposalStrategy.selectCounterOffer(delegation, mind.updateInsight(directory.nodeOf(sender()), insight), rule).isEmpty)
        && proposalStrategy.acceptableEndowment(delegation, mind.updateInsight(directory.nodeOf(sender()), insight), rule).nonEmpty =>
      val donor = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Propose from $donor: $insight")
      val updatedMind = mind.updateInsight(donor, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Propose\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      if (debug) {
        println(s"AgentBehaviour>${updatedMind.me} with ${updatedMind.tasks} receives an acceptable proposal of ${delegation.endowment} from ${delegation.donor}  (${directory.nodeOf(sender())})")
        println(s"AgentBehaviour>${updatedMind.me} finds no acceptable swap")
        println(s"AgentBehaviour>${updatedMind.me} insight= ${updatedMind.insight(updatedMind.getTimestamp)}")
        println(s"AgentBehaviour>${updatedMind.me} $donor insight= $insight")
        println(s"AgentBehaviour>${updatedMind.me} beliefBase= ${updatedMind.beliefBase}")
      }
      // This is required since the previous acceptance can be empty
      // MM: I am very sorry but I do not understand why
      /*val endowment : List[Task] = if (proposalStrategy.previousAcceptance.isEmpty)
        proposalStrategy.acceptableEndowment(delegation, updatedMind, rule)
      else proposalStrategy.previousAcceptance.get.endowment*/
      val endowment : List[Task] =  proposalStrategy.acceptableEndowment(delegation, updatedMind, rule)
      val acceptance = new Delegation(delegation.stap, delegation.donor, delegation.recipient, endowment)
      if (trace) writer.println(s"$node -> $donor : Accept($acceptance)")
      sender() ! Accept(acceptance, updatedMind.insight(mind.getTimestamp), replyId = delegation.hashCode())
      if (trace && debugPhase) writer.println(s"$node GOTO CONTRACTOR") //DEBUG
      goto(Contractor) using updatedMind

    // A proposal which is evaluated in order to be rejected
    case Event(Propose(delegation, insight), mind)
      if mind.tasks.intersect(delegation.endowment.toSet).nonEmpty
        || (proposalStrategy.acceptableEndowment(delegation, mind.updateInsight(directory.nodeOf(sender()), insight), rule).isEmpty
        && (isFirstStage || counterProposalStrategy.selectCounterOffer(delegation, mind.updateInsight(directory.nodeOf(sender()), insight), rule).isEmpty)) =>
      val donor = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Propose from $donor: $insight")
      val (isUpdated,updatedMind) = mind.isUpdateInsight(donor, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Propose\ninsight timestamp : ${insight.timestamp}") //DEBUG
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}") //DEBUG
      if (debug) {
        println(s"AgentBehaviour>${updatedMind.me} with ${updatedMind.tasks} receives a non-acceptable proposal of ${delegation.endowment} from ${delegation.donor} (${directory.nodeOf(sender())})")
        println(s"AgentBehaviour>${updatedMind.me} finds no acceptable swap")
        println(s"AgentBehaviour>${updatedMind.me} my insight = ${updatedMind.insight(updatedMind.getTimestamp)}")
        println(s"AgentBehaviour>${updatedMind.me} insight of $donor = $insight")
        println(s"AgentBehaviour>${updatedMind.me} beliefBase= ${updatedMind.beliefBase}")
      }
      if (trace) writer.println(s"$node -> $donor : Reject($delegation)")
      if (trace && debugInform) writer.println(s"Negotiator $node rejects since ${updatedMind.beliefBase} and${updatedMind.insight(updatedMind.getTimestamp)}")
      sender() ! Reject(delegation, updatedMind.insight(mind.getTimestamp), replyId = delegation.hashCode())
      if (isUpdated){ // Optimization by reactivating the proactive behaviour only if the belief base is updated
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
      }
      stay() using updatedMind

    // A depreciated acceptance is withdrawn and triggers the proactive agent
    case Event(Accept(delegation, insight, msgId, _), mind) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Accept from $recipient: $insight")
      val (isUpdated,updatedMind) = mind.isUpdateInsight(recipient, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Accept\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(updatedMind.getTimestamp), replyId = msgId)
      if (isUpdated) {
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
      }
      stay() using updatedMind

    // A depreciated counter-proposal is withdrawn and triggers the proactive agent
    case Event(CounterPropose(swap, insight, _), mind) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since CounterPropose from $recipient: $insight")
      val (isUpdated,updatedMind) = mind.isUpdateInsight(recipient, insight)
      val delegation = swap.initialDelegation()
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), replyId = swap.hashCode())
      if (isUpdated) {
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
      }
      stay() using updatedMind

    // A depreciated rejection is ignored and triggers the proactive agent
    case Event(Reject(_, insight, _), mind) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Reject from $recipient: $insight")
      val (isUpdated,updatedMind) = mind.isUpdateInsight(recipient, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Reject\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      if (isUpdated) {
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
      }
      stay() using updatedMind

    // An information leads the agent to update its belief base and eventually
    // trigger the proactive agent
    case Event(Inform(insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Inform from $peer: $insight")
      if (trace && debugInform) writer.println(s"$node updates insight since Inform from $peer: $insight : Inform in RESPONDER\ninsight timestamp : ${insight.timestamp}") //DEBUG
      val (isUpdated,updatedMind) = mind.isUpdateInsight(peer, insight)
      if (debugInform) println(s"Negotiator $node updates insight for $peer: $insight ")
      if (trace && debugInform) writer.println(s"new belief base : ${updatedMind.beliefBase}")
      if (isUpdated) {
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
      }
      stay() using updatedMind
  }

  /**
    * Or the agent is a contractor which waits for
    * a confirmation or a withdrawal to become Responder
    */
  when(Contractor) {

    // A confirmation of a delegation leads to send an Add request to the manager
    // and the agent becomes a Responder
    case Event(Confirm(delegation, insight, _), mind)  =>
      val donor: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Confirm from $donor: $insight")
      var updatedMind = mind.updateInsight(donor, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Confirm\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      
      //manager ! Add(delegation.endowment)
      // /!\ must wait for Manager answer to update the view on the bundle
      updatedMind = askManager(Add(delegation.endowment), updatedMind)

      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER") //DEBUG
      goto(Responder) using updatedMind

    //A confirmation of a swap which is still valid leads to send a Replace request 
    //to the manager and to became a Responder
    case Event(ConfirmSwap(swap, insight, _), mind) if swap.counterparts.toSet.subsetOf(mind.tasks) =>
      val donor: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Confirm from $donor: $insight")
      var updatedMind = mind.updateInsight(donor, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Confirm\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      swap match {
        case singleSwap : SingleSwap =>
            metrics = metrics.addDelegatedTasks(1)
            //manager ? Replace(List(singleSwap.counterpart), List(singleSwap.task))
            updatedMind = askManager(Replace(List(singleSwap.counterpart), List(singleSwap.task)), updatedMind)
        case _ =>
            metrics = metrics.addDelegatedTasks(swap.counterparts.size)
            //manager ? Replace(swap.counterparts, swap.endowment)
            updatedMind = askManager(Replace(swap.counterparts, swap.endowment), updatedMind)
      }
      swap match {
        case singleSwap: SingleSwap =>
          if (monitor.isDefined) monitor.get ! Give(List(singleSwap.counterpart))
        case _ =>
      }
      if (trace) writer.println(s"$node -> $donor : Confirm($swap)")
      sender() ! ConfirmSwap(swap, updatedMind.insight(mind.getTimestamp), swap.hashCode())
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER") //DEBUG
      goto(Responder) using updatedMind


    //A confirmation of a swap which is no longer valid leads to reply with a withdrawal
    //and to became a Responder
    case Event(ConfirmSwap(swap, insight, msgId), mind) if ! swap.counterparts.toSet.subsetOf(mind.tasks) =>
      val donor: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Confirm from $donor: $insight")
      val updatedMind = mind.updateInsight(donor, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Confirm\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      sender() ! WithdrawSwap(swap, updatedMind.insight(mind.getTimestamp), msgId)
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER")
      goto(Responder) using updatedMind      

    // A withdrawal leads to the Responder state
    case Event(Withdraw(_, insight, _), mind) =>
      val donor: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Withdraw from $donor: $insight")
      val updatedMind = mind.updateInsight(donor, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Withdraw\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      unstashAll()
      self ! Next
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER")
      goto(Responder) using updatedMind

    // A proposal is stashed
    case Event(Propose(_, insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Propose from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      if (trace && debugInform) writer.println(s"STASHED $node updates insight since Propose\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      randomStash()
      stay() using updatedMind

    // A deprecated acceptance is withdrawn
    case Event(Accept(delegation, insight, msgId, _), mind) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Accept from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Accept\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), msgId)
      stay() using updatedMind

    // A deprecated counter-proposal is withdrawn
    case Event(CounterPropose(swap, insight, msgId), mind) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since CounterPropose from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      val delegation = swap.initialDelegation()
      if (trace) writer.println(s"$node -> $recipient : Withdraw($swap)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), msgId)
      stay() using updatedMind

    // A deprecated rejection is ignored
    case Event(Reject(_, insight, _), mind) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Reject from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Reject\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      stay() using updatedMind

    // An information leads the agent to update its belief base
    case Event(Inform(insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Inform from $peer: $insight")
      if (trace && debugInform) writer.println(s"$node updates insight since Inform from $peer: $insight : Inform in CONTRACTOR\ninsight timestamp : ${insight.timestamp}")
      val updatedMind = mind.updateInsight(peer, insight)
      if (trace && debugInform) writer.println(s"new belief base : ${updatedMind.beliefBase}")
      stay() using updatedMind

    // A trigger is ignored
    case Event(Next, mind) =>
      stay() using mind
  }

  /**
    * Or the agent is a proposer which waits for an acceptance,
    * a rejection, a timeout, or a counter-proposal in order to becomes Responder
    */
  when(Proposer){

    // An acceptance of the last offer which is still valid
    // is confirmed and  the agent becomes Responder
    case Event(Accept(delegation, insight, msgId, replyId), mind)
      if replyId == mind.lastOffer.get
        && delegation.endowment.toSet.subsetOf(mind.tasks) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Accept from $recipient: $insight")
      var updatedMind = mind.updateInsight(recipient, insight)
      updatedMind = updatedMind.resetLastOffer
      if (trace && debugInform) writer.println(s"$node updates insight since Accept\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")

      if (debug) println(s"node 's bundle before Confirm ${updatedMind.tasks}")
      if (trace) writer.println(s"$node -> $recipient : Confirm($delegation)")
      sender() ! Confirm(delegation, updatedMind.insight(mind.getTimestamp), replyId = msgId)
      metrics = metrics.incrementDelegations()
      if (delegation.endowment.isEmpty)
        throw new RuntimeException(s"ERROR $node cannot confirm an empty delegation")
      metrics = metrics.addDelegatedTasks(delegation.endowment.size)
      unstashAll()
      //multicast(updatedMind.peers  - recipient, Inform(updatedMind.insight))// Add to ensure soundness of global flowtime
      if (monitor.isDefined) monitor.get ! Give(delegation.endowment)

      //manager ? Remove(delegation.endowment)
      updatedMind = askManager(Remove(delegation.endowment), updatedMind)

      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      cancelTimer()
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER")
      goto(Responder) using updatedMind

    // An acceptance of the last offer which is no more valid
    // is withdrawn and the agent becomes Responder
    case Event(Accept(delegation, insight, msgId, replyId), mind)
      if replyId == mind.lastOffer.get
        && ! delegation.endowment.toSet.subsetOf(mind.tasks) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Reject from $recipient: $insight")
      var updatedMind = mind.updateInsight(recipient, insight)
      updatedMind = updatedMind.resetLastOffer
      if (trace && debugInform) writer.println(s"$node updates insight since Accept\ninsight timestamp : ${insight.timestamp}") //DEBUG
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}") //DEBUG
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), msgId)
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      cancelTimer()
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER") //DEBUG
      goto(Responder) using updatedMind

    // An acceptance of a different offer is withdrawn
    case Event(Accept(delegation, insight, msgId, replyId), mind)
      if replyId != mind.lastOffer.get =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Accept from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Accept\ninsight timestamp : ${insight.timestamp}") //DEBUG
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}") //DEBUG
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), msgId)
      stay() using updatedMind

    // A counter-proposal which is acceptable and still valid
    // Then the agent confirms, asks the manager to remove the endowment from the bundle,
    //  and goes to Swapper state to wait the double confirmation
    case Event(CounterPropose(swap, insight, replyId), mind)
      if replyId == mind.lastOffer.get //
        && swap.endowment.toSet.subsetOf(mind.tasks)
        && counterProposalStrategy.acceptabilityRule(swap, mind.updateInsight(directory.nodeOf(sender()), insight), rule) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Accept from $recipient: $insight")
      var updatedMind = mind.updateInsight(recipient, insight)
      if (debugCounterPropose) println(s"node 's bundle before Confirm ${updatedMind.tasks}")
      if (trace) writer.println(s"$node -> $recipient : Confirm($swap)")
      sender() ! ConfirmSwap(swap, updatedMind.insight(mind.getTimestamp), swap.hashCode())

      // The negotiator must wait for the manager answer to update the view on the bundle
      updatedMind = askManager(Remove(swap.endowment), updatedMind)

      if (monitor.isDefined) monitor.get ! Give(swap.counterparts)
      cancelTimer()
      if (trace && debugPhase) writer.println(s"$node GOTO SWAPPER") //DEBUG
      goto(Swapper) using updatedMind

    // A counter-offer which is not acceptable or no longer valid 
    // The offer is withdrawn, the agent sends itself a trigger and
    // goes to Responder state
    case Event(CounterPropose(swap, insight, replyId), mind)
      if (replyId == mind.lastOffer.get
        && (!swap.endowment.toSet.subsetOf(mind.tasks)
        || !counterProposalStrategy.acceptabilityRule(swap, mind.updateInsight(directory.nodeOf(sender()), insight), rule))) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since CounterPropose from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      val delegation = swap.initialDelegation()
      if (debugCounterPropose)
        println(s"$node withdraws since some tasks in ${swap.endowment} are no more in the bundle ${mind.tasks} " +
          s"and acceptability ${counterProposalStrategy.acceptabilityRule(swap, mind.updateInsight(directory.nodeOf(sender()), insight), rule)}")
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), swap.hashCode())
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      cancelTimer()
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER")
      goto(Responder) using updatedMind

    // A counter-offer for different offer is withdrawn
    case Event(CounterPropose(swap, insight, replyId), mind) if replyId != mind.lastOffer.get =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since CounterPropose from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      val delegation = swap.initialDelegation()
      if (debugCounterPropose)
        println(s"$node withdraws since last offer ${replyId != mind.lastOffer.get} ")
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), swap.hashCode())
      stay() using updatedMind

    // A rejection of the the last offer leads the agent
    // to become responder
    case Event(Reject(_, insight, replyId), mind)
      if replyId == mind.lastOffer.get =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Reject from $recipient: $insight")
      var updatedMind = mind.updateInsight(recipient, insight)
      updatedMind = updatedMind.resetLastOffer
      if (trace && debugInform) writer.println(s"$node updates insight since Reject")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      cancelTimer()
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER")
      goto(Responder) using updatedMind

    // A rejection of a different offer is ignored
    case Event(Reject(_, insight, replyId), mind)
      if replyId != mind.lastOffer.get =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Reject from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Reject\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      stay() using updatedMind

    // An information leads the agent to update its belief base 
    case Event(Inform(insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Inform from $peer: $insight")
      if (trace && debugInform) writer.println(s"$node updates insight since Inform from $peer: $insight : Inform in PROPOSER\ninsight timestamp : ${insight.timestamp}")
      val updatedMind = mind.updateInsight(peer, insight)
      if (trace && debugInform) writer.println(s"new belief base : ${updatedMind.beliefBase}")
      stay() using updatedMind

    // A timeout leads the agent
    // to become responder
    case Event(Timer.Timeout, mind) => // StateTimeout
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Timeout")
      val updatedMind = mind.resetLastOffer
      metrics = metrics.incrementTimeout()
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      cancelTimer()
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER")
      goto(Responder) using updatedMind

    // A trigger is ignored
    case Event(Next, mind) =>
      stay() using mind

    // A proposal is stashed
    case Event(Propose(_, insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Propose from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      randomStash()
      stay() using updatedMind
  }

  /**
    * Or the agent is engaged in a swap and is waiting for the double confirmation
    * from the contractor
    */
  when(Swapper){
    // The swap is confirmed and the counterpart is added to the bundle
    case Event(ConfirmSwap(swap, insight, _), mind) =>
      if (debugPhase) println("swap confirmed")
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Confirm from $recipient: $insight")
      var updatedMind = mind.updateInsight(recipient, insight)
      if (debug) println(s"Negotiator $node asks the manager to add ${swap.counterparts} in the bundle")
      //manager ? Add(swap.counterparts)
      updatedMind = askManager(Add(swap.counterparts), updatedMind)
      metrics = metrics.incrementSwaps()
      metrics = metrics.addDelegatedTasks(swap.endowment.size)
      updatedMind = updatedMind.resetLastOffer
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      if (trace && debugPhase) writer.println(s"$node GOTO RESPONDER")
      goto(Responder) using updatedMind

  // The swap is withdrawn and the endowment is added once again tot the bundle
  case Event(WithdrawSwap(swap, insight, _), mind) =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Withdraw from $recipient: $insight")
      var updatedMind = mind.updateInsight(recipient, insight)
      if (debug) println(s"Negotiator $node asks the manager to add back ${swap.endowment} in the bundle")
      //manager ? Add(swap.endowment)
      updatedMind = askManager(Add(swap.endowment), updatedMind)
      updatedMind = updatedMind.resetLastOffer
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      if (trace && debugPhase) writer.println(s"$node GOTO Responder")
      goto(Responder) using updatedMind

  // An information leads the agent to update its belief base
    case Event(Inform(insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Inform from $peer: $insight")
      if (trace && debugInform) writer.println(s"$node updates insight since Inform from $peer: $insight : Inform in SWAPPER") //DEBUG
      val updatedMind = mind.updateInsight(peer, insight)
      stay() using updatedMind

  // A trigger is ignored
    case Event(Next, mind) =>
      stay() using mind

  // A proposal is stashed
    case Event(Propose(_, insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Propose from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      randomStash()
      stay() using updatedMind

  // A rejection of a different offer is ignored
    case Event(Reject(_, insight, replyId), mind)
      if replyId != mind.lastOffer.get =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Reject from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      stay() using updatedMind

    // An acceptance of a different offer is withdrawn
    case Event(Accept(delegation, insight, msgId, replyId), mind)
      if replyId != mind.lastOffer.get =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Accept from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), msgId)
      stay() using updatedMind

  // A counter-offer for different offer is withdrawn
    case Event(CounterPropose(swap, insight, replyId), mind) if replyId != mind.lastOffer.get =>
      val recipient = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since CounterPropose from $recipient: $insight")
      val updatedMind = mind.updateInsight(recipient, insight)
      val delegation = swap.initialDelegation()
      if (debugCounterPropose)
        println(s"$node withdraws since last offer ${replyId != mind.lastOffer.get} ")
      if (trace) writer.println(s"$node -> $recipient : Withdraw($delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), swap.hashCode())
      stay() using updatedMind
  }

  /**
    * Or the agent which is in pause is no more proactive
    */
  when(Pause) {

    // A proposal reactivates the agent in order to be evaluated latter
    case Event(Propose(delegation, insight), mind) =>
      if (debug) println(s"AgentBehaviour>$node with ${mind.tasks} receives a proposal of $delegation")
      val donor: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Propose from $donor: $insight")
      val updatedMind = mind.updateInsight(donor, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Propose\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Propose($delegation)")
      self forward Propose(delegation, insight)
      if (debug) println(s"AgentBehaviour>$node with ${updatedMind.tasks} forward a proposal of $delegation")
      if (trace && debugPhase) writer.println(s"$node GOTO Responder")
      goto(Responder) using updatedMind.resetAttitude

    // A deprecated acceptance is withdrawn and
    // triggers the proactivity of the agent
    case Event(Accept(delegation, insight, msgId, _), mind) =>
      val recipient: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Accept from $recipient: $insight")
      var (isUpdated,updatedMind) = mind.isUpdateInsight(recipient, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Accept\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      if (trace) writer.println(s"$node -> $recipient : Withdraw(delegation)")
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), msgId)
      if (isUpdated && ! updatedMind.isProactive) {
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
        updatedMind = updatedMind.reactivate
      }
      stay() using updatedMind

    // A rejection is ignored and
    // triggers the proactivity of the agent
    case Event(Reject(_, insight, _), mind) =>
      val recipient: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Reject from $recipient: $insight")
      var (isUpdated,updatedMind)= mind.isUpdateInsight(recipient, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Reject\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      if (isUpdated && ! updatedMind.isProactive) {
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
        updatedMind = updatedMind.reactivate
      }
      stay() using updatedMind

    // A counter-proposal is ignored and
    // triggers the proactivity of the agent
    case Event(CounterPropose(swap, insight, _), mind) =>
      val recipient: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since CounterPropose from $recipient: $insight")
      var (isUpdated,updatedMind)= mind.isUpdateInsight(recipient, insight)
      if (trace) writer.println(s"$node -> $recipient : Withdraw(delegation)")
      val delegation = swap.initialDelegation()
      sender() ! Withdraw(delegation, updatedMind.insight(mind.getTimestamp), swap.hashCode())
      if (isUpdated && !updatedMind.isProactive) {
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
        updatedMind = updatedMind.reactivate
      }
      stay() using updatedMind

    // A trigger may reactivate the agent if a new offer can be proposed
    case Event(Next, mind)
      if proposalStrategy.selectOffer(mind, rule).isDefined =>
      val delegation: Delegation = proposalStrategy.previousOffer.get
      if (debug) println(s"AgentBehaviour>${mind.me} my insight= ${mind.insight(mind.getTimestamp)}")
      if (debug) println(s"AgentBehaviour>${mind.me} beliefBase= ${mind.beliefBase}")
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Wakeup($delegation)")
      self ! Wakeup(delegation)
      if (trace && debugPhase) writer.println(s"$node GOTO Responder")
      goto(Responder) using mind.resetAttitude

    // A trigger may fail to reactivate the agent since a new offer cannot be proposed
    case Event(Next, mind)
      if proposalStrategy.selectOffer(mind, rule).isEmpty =>
      if (debug) println(s"AgentBehaviour>$node does not consider any delegation with ${mind.tasks} in state Pause")
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Close")
      if (!mind.isDesperate) self ! Close
      val updatedMind = mind.desperate
      stay() using updatedMind.deactivate

    // An information
    // may trigger the proactivity of the agent
    case Event(Inform(insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Inform from $peer: $insight")
      if (trace && debugInform) writer.println(s"$node updates insight since Inform from $peer: $insight : Inform in PAUSE\ninsight timestamp : ${insight.timestamp}")
      var (isUpdated,updatedMind) = mind.isUpdateInsight(peer, insight)
      if (trace && debugInform) writer.println(s"new belief base : ${updatedMind.beliefBase}")
      if (isUpdated && !updatedMind.isProactive) {
        if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
        self ! Next
        updatedMind = updatedMind.reactivate
      }
      stay() using updatedMind

    // A self message, which informs the agent that the mailbox is empty, is ignored expected in Pause
    case Event(Close, mind) =>
      if (internalTrace) writer.println(s"$node -> Manager : FinishedStage($metrics)")
      manager ! FinishedStage(metrics)
      if (trace && debugPhase) writer.println(s"$node GOTO EndStage")
      goto(EndStage) using mind.resetAttitude
  }

  /**
    * Or the agent is at the end of the stage and
    * it only replies to the supervisor
    */
  when(EndStage) {

    // Message which leads to the transition state
    case Event(ReadyForNextStage, mind) =>
      if (trace && debugPhase) writer.println(s"$node GOTO TransitionState")
      goto(TransitionState) using mind

    // A self message informs the agent that the mailbox is empty is ignored expected in Pause
    case Event(Close,mind) =>
      stay() using mind

    // Messages which does not need to be forward
    case Event(_ @ Next, mind) =>
      stay() using mind

    //  Reject messages are ignored
    case Event(Reject(_, insight, _), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Reject from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      if (trace && debugInform) writer.println(s"$node updates insight since Reject")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      stay() using updatedMind

    //  Inform message are forwarded
    case Event(msg @ Inform(insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Inform from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      if (trace && debugInform) writer.println(s"(FORWARD) $node updates insight since Inform from $peer: $insight : Inform in ENDSTAGE\ninsight timestamp : ${insight.timestamp}") //DEBUG
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      self forward msg
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Inform($insight)")
      if (internalTrace) writer.println(s"$node -> Manager : Restart since $msg")
      if (trace && debugInform) writer.println(s"Negotiator $node restarts since $msg")
      manager ! Restart
      if (trace && debugPhase) writer.println(s"$node GOTO Pause")
      goto(Pause) using updatedMind.resetAttitude

    // Accept message are forwarded
    case Event(msg @ Accept(delegation, insight, _, _), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Accept from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      if (trace && debugInform) writer.println(s"(FORWARD) $node updates insight since Accept\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      self forward msg
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Accept($delegation)")
      if (internalTrace) writer.println(s"$node -> Manager : Restart")
      if (trace && debugInform) writer.println(s"Negotiator $node restarts since $msg")
      manager ! Restart
      if (trace && debugPhase) writer.println(s"$node GOTO Pause")
      goto(Pause) using updatedMind.resetAttitude

    // Proposals are forwarded
    case Event(msg @ Propose(delegation, insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since Propose from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      if (trace && debugInform) writer.println(s"(FORWARD $node updates insight since Propose\ninsight timestamp : ${insight.timestamp}")
      if (trace && debugInform) writer.println(s"$node new belief base : ${updatedMind.beliefBase}")
      self forward msg
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Propose($delegation)")
      if (internalTrace) writer.println(s"$node -> Manager : Restart")
      if (trace && debugInform) writer.println(s"Negotiator $node restarts since $msg")
      manager ! Restart
      if (trace && debugPhase) writer.println(s"$node GOTO Pause")
      goto(Pause) using updatedMind.resetAttitude

   // Counter-proposals are forward
    case Event(msg @ CounterPropose(swap, insight, _), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"Negotiator $node updates insight since CounterPropose from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      self forward msg
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : CounterPropose($swap)")
      if (trace) writer.println(s"$node -> Supervisor : Restart")
      if (trace && debugInform) writer.println(s"Negotiator $node restarts since $msg")
      manager ! Restart
      if (trace && debugPhase) writer.println(s"$node GOTO Pause")
      goto(Pause) using updatedMind.resetAttitude
  }

  /**
    * Or the agent is waiting for the next stage
    */
  when(TransitionState){

    // A start stage message from the manager announcing the beginning of the new stage
    case Event(StartStage, mind) =>
      isFirstStage = !isFirstStage
      if (isFirstStage) proposalStrategy = firstProposalStrategy
      else proposalStrategy = secondProposalStrategy.get
      metrics = metrics.reset()
      unstashAll()
      if (trace && debugSelfMessage) writer.println(s"$node -> $node : Next")
      self ! Next
      if (trace && debugPhase) writer.println(s"$node GOTO Responder")
      goto(Responder) using mind.resetAttitude.resetLastOffer

    // A proposal is randomly stashed
    case Event(Propose(_, insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Propose from $peer: $insight")
      val updatedMind = mind.updateInsight(peer, insight)
      randomStash()
      stay() using updatedMind

    // An inform
    case Event(Inform(insight), mind) =>
      val peer: ComputingNode = directory.nodeOf(sender())
      if (debugInform) println(s"$node updates insight since Inform from $peer: $insight")
      if (trace && debugInform) writer.println(s"$node updates insight since Inform from $peer: $insight : Inform in TransitionState")
      val updatedMind = mind.updateInsight(peer, insight)
      stay() using updatedMind
  }

  /**
    * Whatever the state is
    **/
  whenUnhandled {

    // An enlightenment of the agents about its acquaintances
    case Event(Light(directory), mind)  =>
      supervisor = sender()
      this.directory = directory
      // sender() ! Ready done by the manager
      stay() using mind

    // A trace of the messages which are sent is required
    case Event(Trace(writer), mind)  =>
      this.trace = true
      this.writer = writer
      stay() using mind

    // A self message informs the agent that the mailbox is empty  is ignored expected in Pause
    case Event(Close,mind) =>
      stay() using mind

    // A query requests some metrics is ignored expected in Pause
    // case Event(Query(_), mind) =>
    //   stay() using mind

    // A wakeup is ignored excepted in Responder
    case Event(Wakeup(_), mind) =>
      stay() using mind

    case Event(Timer.Timeout, mind) => // deprecated Timeout
      stay() using mind

    // Other message are not expected
    case Event(msg @ _, _) =>
      throw new RuntimeException(s"ERROR $node was not expected message $msg in state $stateName")
  }

  /**
    * Associates actions with a transition instead of with a state and even, e.g. debugging
    */
  onTransition {
    case s1 -> s2 =>
      if (debugState) println(s"$node moves from the state $s1 to the state $s2")
  }

  // Finally Triggering it up using initialize, which performs
  // the transition into the initial state and sets up timers (if required).
  initialize()
}

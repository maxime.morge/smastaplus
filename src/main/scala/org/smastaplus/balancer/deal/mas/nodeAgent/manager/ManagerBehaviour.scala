// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2022
package org.smastaplus.balancer.deal.mas.nodeAgent.manager

import org.smastaplus.core._
import org.smastaplus.provisioning._
import org.smastaplus.utils.Timer
import org.smastaplus.balancer.deal.mas._

import org.joda.time.LocalDateTime
import akka.actor.{ActorRef, FSM}

/**
  * Manager agent with a concrete behaviour
  * @param stap instance
 *  @param node which is managed
  * @param negotiator is the corresponding actor
  * @param monitor if any
 */
class ManagerBehaviour(stap: STAP,
                       node: ComputingNode,
                       negotiator : ActorRef,
                       monitor: Option[ActorRef])
  extends Manager(stap, node, negotiator, monitor)
    with FSM[ManagerState, ManagerMind]{

  debug = false
  trace = true

  /**
    * Initially the manager
    * - is active
    * - assumes all the computing nodes are active
    * - has no task in the bundle,
    * - and in the first stage
    */
  startWith(
    Initial,
    new ManagerMind(node,
      peers = stap.ds.computingNodes.toSet - node,
      usage =  Map[ComputingNode, ProvisioningUsage]() ++ stap.ds.computingNodes.view.map(i => i -> Active),
      stap,
      taskSet = Set.empty[Task],
      isFirstStage = true)
  )

  /**
    * Either the manager is in the initial state where
    * it is waiting for its initial bundle to become Running
    */
  when(Initial) {
    // An enlightenment of the agents about its acquaintances
    case Event(Light(directory), mind)  =>
      supervisor = sender()
      this.directory = directory
      sender() ! Ready
      stay() using mind

    // A gift of a bundle to the agent which takes/sorts it,
    // broadcasts insight and becomes running
    case Event(Give(bundle), mind)  =>
      supervisor = sender()
      if (debug) println(s"Manager $node updates its bundle")
      val updatedMind = mind.initBundle(bundle)
     //println(s"$node bundle : ${updatedMind.getBundle}") //DEBUG
      if (debug) println(s"Manager $node commission negotiator")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle)
      if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
      broadcast(updatedMind.insight(LocalDateTime.now()))
      if (monitor.isDefined) updatedMind.bundleWriter("monitoring").write()

      if (trace && debug) writer.println(s"MANAGER $node GOTO RUNNING")
      goto(Running) using updatedMind
  }

  /**
    * Or the manager is running
    */
  when(Running) {
    // A message from the negotiator informing it has no longer reallocation to propose in the current stage
    case Event(FinishedStage(newMetrics), mind)  =>
      metrics = newMetrics
      if (debug) println(s"Manager $node sends and End message to the supervisor the insight ${mind.insight()}")
      supervisor ! End(mind.sortedTasks)
      if (trace && debug) writer.println(s"Manager$node goto EndStage")
      goto(EndStage) using mind

    // A message from the negotiator asking the manager to add a task in the bundle
    case Event(Add(tasks), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      if (debug) println(s"Manager $node adds the tasks $tasks in the bundle")
      val updatedMind = mind.addTasks(tasks)
      sender() ! GiveUpdatedBundle(updatedMind.getBundle, newTimestamp)
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
      multicast(updatedMind.peers,  Inform(newInsight)) //broadcast(updatedMind.insight)
      if (monitor.isDefined) updatedMind.bundleWriter("monitoring").write()
      stay() using updatedMind

    // A message from the negotiator asking the manager to remove tasks from the bundle
    case Event(Remove(tasks), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      if (debug) println(s"Manager $node removes the tasks $tasks in the bundle")
      val updatedMind = mind.removeTasks(tasks)
      sender() ! GiveUpdatedBundle(updatedMind.getBundle, newTimestamp)
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
      multicast(updatedMind.peers,  Inform(newInsight)) //broadcast(updatedMind.insight)
      if (monitor.isDefined) updatedMind.bundleWriter("monitoring").write()
      stay() using updatedMind

    // A message from the negotiator asking the manager to replace a task in the bundle
    case Event(Replace(endowment, counterpart), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      if (debug) println(s"Manager $node replaces the tasks $endowment by the tasks $counterpart in the bundle")
      val updatedMind = mind.replaceTasks(endowment, counterpart)
      sender() ! GiveUpdatedBundle(updatedMind.getBundle, newTimestamp)
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
       multicast(updatedMind.peers,  Inform(newInsight)) //broadcast(updatedMind.insight)
      if (monitor.isDefined) updatedMind.bundleWriter("monitoring").write()
      stay() using updatedMind
      
    // An obsolete query is ignored
    case Event(Query(_), mind) =>
      stay() using mind

    // Other message are not expected
    case Event(msg @ _, _) =>
      throw new RuntimeException(s"ERROR $node was not expected message $msg in state $stateName")
  }

  /**
   * Or the manager is at the end of a stage
   */
  when(EndStage) {
    // A restart from the negotiator
    case Event(Restart, mind) =>
      if (debug) println(s"Manager $node sends Restart to the supervisor")
      supervisor ! Restart
      if (trace && debug) writer.println(s"MANAGER $node GOTO RUNNING")
      goto(Running) using mind

    // The supervisor request the metrics
    case Event(Query(msgId), mind) => 
      if (debug) println(s"Manager $node sends the metrics to the supervisor")
      sender() ! Answer(metrics, msgId)
      stay() using mind

    // The supervisor kills the agent
    case Event(Kill, mind) =>
      if (debug) println(s"Manager $node sends Finished to the supervisor")
      supervisor ! Finished
      stay() using mind

    // A pre-trigger to initiate the next stage
    case Event(PreTrigger(_), mind) =>
      if (debug) println(s"Manager $node sends ReadyForNextStage to the negotiator and the supervisor")
      supervisor ! ReadyForNextStage
      negotiator ! ReadyForNextStage
      if (trace && debug) writer.println(s"Manager $node goto TransitionState")
      goto(TransitionState) using mind

    // Other message are not expected
    case Event(msg @ _, _) =>
      throw new RuntimeException(s"ERROR $node was not expected message $msg in state $stateName")
  }

  /**
   * Or the manager is in transition
   */
  when(TransitionState) {

    //a Trigger from the supervisor starting the next stage
    case Event(Trigger(_), mind) =>
      unstashAll()
      if (debug) println(s"Manager $node sends StartStage to the negotiator")
      negotiator ! StartStage
      if (trace && debug) writer.println(s"MANAGER $node GOTO RUNNING")
      goto(Running) using mind

    // Other message are not expected
    case Event(msg @ _, _) =>
      throw new RuntimeException(s"ERROR $node was not expected message $msg in state $stateName")
  }


    /**
      * Whatever the state is
      **/
    whenUnhandled {
      // A trace of the messages which are sent is required
      case Event(Trace(writer), mind)  =>
        this.trace = true
        this.writer = writer
        stay() using mind

      // A deprecated timeout
      case Event(Timer.Timeout, mind) =>
        stay() using mind
      
      // A query requests some metrics is ignored expected in Pause
      case Event(Query(_), mind) =>
        stay() using mind

      // Other message are not expected
      case Event(msg @ _, _) =>
        throw new RuntimeException(s"ERROR $node was not expected message $msg in state $stateName")
    }

    /**
      * Associates actions with a transition instead of with a state and even, e.g. debugging
      */
    onTransition {
      case s1 -> s2 =>
        if (debugState) println(s"manager $node moves from the state $s1 to the state $s2")
    }

    // Finally Triggering it up using initialize, which performs
    // the transition into the initial state and sets up timers (if required).
    initialize()
  }

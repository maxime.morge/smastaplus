// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.balancer.deal.mas.nodeAgent

import org.smastaplus.core._
import org.smastaplus.balancer.deal.mas._
import org.smastaplus.balancer.deal.mas.nodeAgent.negotiator.NegotiationBehaviour
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.ProposalStrategy
import org.smastaplus.balancer.deal.mas.nodeAgent.manager.ManagerBehaviour

import akka.actor.{Actor, ActorRef, Props, Stash}

/**
  * Node agent which encompass a negotiator, a manager and eventually a worker
  * @param stap instance
  * @param node represented by the agent
  * @param rule to adopt for reducing either the localFlowtime or the globalFlowtime or the makespan
  * @param firstProposalStrategy for making proposals in the first stage
  * @param secondProposalStrategy for eventually making proposals in the second stage
  * @param counterProposalStrategy for making counter-proposals in the second stage
  * @param monitor if any
  */
class NodeAgent(stap: STAP,
                node: ComputingNode,
                rule: SocialRule,
                firstProposalStrategy: ProposalStrategy,
                secondProposalStrategy: Option[ProposalStrategy],
                counterProposalStrategy: CounterProposalStrategy,
                monitor: Option[ActorRef])
  extends Actor with Stash {
  var trace: Boolean = false
  var debug: Boolean = false

  var supervisor: ActorRef = context.parent
  var directory: Directory = new Directory()

  //The name of the dispatcher, i.e. "akka.actor.default-dispatcher" since the node agent is composite
  val negotiator: ActorRef = context.actorOf(// build the actor
    Props(classOf[NegotiationBehaviour], stap, node, rule, firstProposalStrategy, secondProposalStrategy, counterProposalStrategy, monitor)
      .withDispatcher("akka.actor.default-dispatcher"), "negotiator@"+node.name) // .replace('ν','n')

  val manager: ActorRef = context.actorOf(// build the actor
    Props(classOf[ManagerBehaviour], stap, node, negotiator, monitor)
      .withDispatcher("akka.actor.default-dispatcher"), "manager@"+node.name) // .replace('ν','n')


  /**
   * Message handling
   */
  override def receive: Receive = {

    // Messages from the supervisor
    case msg@Light(directory)  => // Enlightenment the negotiator and the manager about the acquaintances
    supervisor = sender()
      this.directory = directory
      negotiator forward  msg
      manager forward msg

    case msg@Trace(_) => // Trace the negotiator and manager
      negotiator forward  msg
      manager forward msg

    case msg@Start() => // Start the negotiator
      negotiator forward  msg

    case msg@Propose(_,_) => // Initial proposals are forwarded to the negotiator
      negotiator forward  msg

    case msg@Restart() => // Restart the negotiator
      negotiator forward  msg

    case msg@Inform(_) => // Inform the negotiator about insight
      negotiator forward  msg

    case msg@Query(_) => // Query the manager about statistics
      manager forward  msg

    case msg@Kill => //  Kill the manager
      manager forward  msg

    case msg@Give(_) => // Give a bundle to the manager
      manager forward  msg

    case msg@PreTrigger(_) => // Pre-trigger a stage for the manager
      manager forward  msg

    case msg@Trigger(_) => // Trigger a stage for the manager
      manager forward  msg

    case msg@_ => // Unexpected message
      throw new RuntimeException(s"Error: NodeAgent ${node.name} receives a message which was not expected: " + msg)
  }
}

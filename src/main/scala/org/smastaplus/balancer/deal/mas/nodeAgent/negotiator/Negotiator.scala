// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022
package org.smastaplus.balancer.deal.mas.nodeAgent.negotiator

import org.smastaplus.core._
import org.smastaplus.balancer.deal.mas._
import org.smastaplus.balancer.deal.mas.nodeAgent.InternalAgent
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.ProposalStrategy

import akka.actor.ActorRef

/**
  * Abstract negotiator
  * @param stap instance
  * @param node represented by the agent
  * @param proposalStrategy for proposing/accepting/refusing proposals
  * @param counterProposalStrategy for proposing/accepting/refusing counterProposals
  * @param monitor if any
 */
abstract class Negotiator(stap: STAP, node: ComputingNode,
                          proposalStrategy: ProposalStrategy,
                          counterProposalStrategy: CounterProposalStrategy,
                          monitor: Option[ActorRef])
  extends InternalAgent(stap,node, monitor) {

  var manager : ActorRef = _

  var metrics = new Metrics()  // Metrics of the negotiation process

}
// Copyright (C) Maxime MORGE 2023
package org.smastaplus.consumer

import org.smastaplus.core._

/**
  * Decentralized and synchronous execution of the tasks
 *
  * @param stap instance
  * @param name of the consumer
  * @param monitor is true if the completion times are monitored during the solving
  * @param simulatedCost with a perfect information of the running environment by default
  */
abstract class SynchronousConsumer(stap: STAP,
                                   name: String = "Consumption",
                                   monitor: Boolean = false,
                                   simulatedCost: SimulatedCost = new SimulatedCostH())
  extends Consumer(stap, name, monitor, simulatedCost){
  
  /**
    * Run the consumption process and returns the empty allocation
    */
  def consume(allocation: ExecutedAllocation) : ExecutedAllocation
}

// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2023
package org.smastaplus.consumer.nodeal

import org.smastaplus.core._
import org.smastaplus.consumer.deal.mas.nodeAgent.manager.ManagerMind

/**
  * Immutable status of the distributor
  * @param allocation i.e the current allocation
  * @param minds are the ManagerMind for each node
  */
class DistributorStatus(val allocation: ExecutedAllocation, val minds: Map[ComputingNode, ManagerMind]) {
  val debug = false

  /**
   * Returns the status where the allocation is update
   */
  def updateAllocation(allocation: ExecutedAllocation) : DistributorStatus =
    new DistributorStatus(allocation, minds)

  def updateMind(node : ComputingNode, mind: ManagerMind) =
    new DistributorStatus(this.allocation, this.minds + (node -> mind))

}


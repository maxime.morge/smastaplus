// Copyright (C) Maxime MORGE 2021, 2022, 2023
package org.smastaplus.consumer.nodeal

import org.smastaplus.consumer.deal.mas._
import org.smastaplus.core._
import org.smastaplus.process.SingleDelegation
import org.smastaplus.utils.RandomUtils

import scala.collection.SortedSet
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.{Actor, ActorRef}

/**
 * Scheduler
 *
 * @param stap instance
 * @param rule is the social rule to be applied
 */
class Scheduler(stap: STAP, rule: SocialRule) extends Actor {

  import scala.concurrent.ExecutionContext.Implicits.global

  val debug = false
  private val TIMEOUT_VALUE: FiniteDuration = 6000 minutes // Default timeout of a run
  private val noFuture = false // true if the bids are sequential
  private var distributor: ActorRef = context.parent // The parent

  var potentialDelegatedTasks: List[Task] = List.empty[Task]
  var currentAllocation: ExecutedAllocation = _

  /**
   * Process the messages
   */
  override def receive: Receive = {

    case Reschedule(nodes, allocation: Allocation, potentialDelegatedTasks) =>
      distributor = sender()
      currentAllocation = allocation
      this.potentialDelegatedTasks = potentialDelegatedTasks
      val potentialAuctioneers = potentialNodes(nodes)
      if (potentialAuctioneers.nonEmpty) reallocate(potentialAuctioneers)

    case Stop =>
      if (debug) println(s"Scheduler> stops")
      context.stop(self)
  }


  /**
   * Reallocate the allocations with the nodes as auctioneers
   *
   */
  def reallocate(auctioneers: SortedSet[ComputingNode]): Unit = {
    val auctioneer = RandomUtils.random[ComputingNode](auctioneers)
    val tasks: List[Task] = currentAllocation.bundle(auctioneer).intersect(potentialDelegatedTasks)
    if (debug) println(s"Scheduler considers $auctioneer with  the potential tasks $tasks")
    val bidders: List[ComputingNode] = stap.ds.computingNodes.diff(Set(auctioneer)).toList
    val proposals: List[(ComputingNode, Task)] = bidders.flatMap(bidder => tasks.map(task => (bidder, task)))
    val initialBid = rule match { // Any bid must improve the current allocation
      case GlobalFlowtime =>
        (tasks.head, auctioneer, currentAllocation.ongoingGlobalFlowtime)
      case _ =>
        throw new RuntimeException(s"Scheduler cannot apply the social rule $rule")
    }
    val (bestTask, bestBidder, bestFlowtime): (Task, ComputingNode, Double) = if (noFuture) { // sequential computation of the best bid
      proposals.foldLeft(initialBid) {
        (best: (Task, ComputingNode, Double), proposal: (ComputingNode, Task)) =>
          val (bidder, task) = proposal
          val bid = rule match {
            case GlobalFlowtime =>
              accept(currentAllocation, auctioneer, bidder, task).ongoingGlobalFlowtime
            case _ =>
              throw new RuntimeException(s"Scheduler cannot apply the social rule $rule")
          }
          if (bid < best._3) (task, bidder, bid)
          else best
      }
    } else { // concurrents computation of the best bid
      val futures: List[Future[(Task, ComputingNode, Double)]] = proposals.map {
        case (bidder, task) =>
          val bid = rule match {
            case GlobalFlowtime =>
              accept(currentAllocation, auctioneer, bidder, task).ongoingGlobalFlowtime
            case _ =>
              throw new RuntimeException(s"Scheduler cannot apply the social rule $rule")
          }
          Future((task, bidder, bid))
      }
      val future: Future[(Task, ComputingNode, Double)] = Future.foldLeft(futures)(initialBid) {
        case (best: (Task, ComputingNode, Double), bid: (Task, ComputingNode, Double)) =>
          if (bid._3 < best._3) bid
          else best
      }
      Await.result(future, TIMEOUT_VALUE)
    }
    if (auctioneer != bestBidder) {
      if (debug) println(s"Scheduler reallocates $bestTask from $auctioneer to $bestBidder for flowtime $bestFlowtime ")
      val delegation = new SingleDelegation(stap, auctioneer, bestBidder, bestTask)
      distributor ! Suggest(delegation)
    } else {
      if (debug) println(s"Scheduler does not reallocate any task from $auctioneer")
    }
    potentialDelegatedTasks = potentialDelegatedTasks.filter(_ != bestTask)
  }


  /**
   * Returns the potential auctioneers for PSI
   * */
  private def potentialNodes(nodes: SortedSet[ComputingNode]): SortedSet[ComputingNode] =
    nodes.filter(node => currentAllocation.bundle(node).intersect(potentialDelegatedTasks).nonEmpty)


  /**
   * Returns the allocation by delegating a task from the auctioneer to the bidder in an allocation
   */
  private def accept(allocation: ExecutedAllocation, auctioneer: ComputingNode, bidder: ComputingNode, task: Task): ExecutedAllocation = {
    val delegation = new SingleDelegation(stap, auctioneer, bidder, task)
    delegation.execute(allocation)
  }
}
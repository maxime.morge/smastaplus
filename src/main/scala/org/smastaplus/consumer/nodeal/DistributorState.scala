// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2023
package org.smastaplus.consumer.nodeal

import org.smastaplus.consumer.deal.mas.State

/**
  * Trait representing as state in a FSM
  * for the distributor behaviour
  */
trait DistributorState extends State
case object Initial extends DistributorState
case object Running extends DistributorState
case object Terminal extends DistributorState
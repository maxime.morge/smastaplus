// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2023
package org.smastaplus.consumer.nodeal

import org.smastaplus.core._
import org.smastaplus.consumer.AsynchronousConsumer
import org.smastaplus.consumer.deal.mas.{Init, Outcome, Update}

import akka.actor.{ActorRef, ActorSystem, Props}

/**
  * Decentralized and asynchronous execution of the tasks
  * @param stap instance
  * @param system of actors
  * @param name of the consumer
  * @param dispatcherId is the name of the dispatcher "akka.actor.default-dispatcher" or "single-thread-dispatcher"
  * @param monitor is true if the completion times are monitored during the solving
  * @param simulated cost with  with a perfect information of the running environment by default
  * @param ssi true if sequential single-item auction is applie
  * @param pssi true if parallel single-item auction is applied
  */
class AsynchronousDecentralizedConsumer(stap: STAP,
                            val system: ActorSystem,
                            name: String,
                            val dispatcherId: String,
                            monitor: Boolean,
                            simulated: SimulatedCost,
                            val ssi : Boolean = false,
                                        val psi : Boolean = false)
  extends AsynchronousConsumer(stap, name, monitor, simulated){

  // Launch a new distributor
  private val distributor : ActorRef = system.actorOf(Props(classOf[Distributor], stap, simulated, dispatcherId, monitor, ssi, psi)
    withDispatcher dispatcherId, name = "Distributor" + name)

  override def receive: Receive = {
    case Init(allocation) =>
      parent = sender()
      allocation.simulatedCost = simulated
      stap.resetReleaseTime()
      init(allocation)
      distributor ! Init(allocation)

    case Update(additionalAllocation) =>
      distributor ! Update(additionalAllocation)

    case Outcome(result, metrics, nbFirstStages, nbSecondStages) =>
      parent ! Outcome(result, metrics, nbFirstStages, nbSecondStages)
      nbDeals = metrics.sumOfDelegationConfirmations + metrics.sumOfSwapConfirmations
      nbDelegations = metrics.sumOfDelegationConfirmations
      nbSwaps = metrics.sumOfSwapConfirmations
      nbDelegatedTasks = metrics.sumOfDelegatedTasks
      nbTimeouts = metrics.sumOfTimeouts
      avgEndowmentSize = metrics.sumOfDelegatedTasks.toDouble / (metrics.sumOfDelegationConfirmations + metrics.sumOfSwapConfirmations).toDouble
      if (avgEndowmentSize.isNaN) avgEndowmentSize = 0.0
      localRatio = result.localAvailabilityRatio
      // Do not close the system !!!
  }
}

/**
 * Companion object for testing DecentralizedConsumer
 */
object AsynchronousDecentralizedConsumer extends App {
  var id: Int = 1
}

// Copyright (C) Maxime MORGE 2023
package org.smastaplus.consumer

import org.smastaplus.core._
import org.smastaplus.utils.serialization.core.AllocationWriter

import com.typesafe.config.{Config, ConfigFactory}


/**
  * Class for performing the tasks
  * @param stap instance
  * @param name of the consumer
  * @param monitor is true if the completion times are monitored during the solving
  * @param simulatedCost with a perfect information of the running environment by default
  */
abstract class Consumer(stap: STAP,
                        val name: String = "Consumer",
                        val monitor: Boolean = false,
                        val simulatedCost: SimulatedCost = new SimulatedCostE()) {

  val debug = false

  // Metrics
  var localRatio: Double = 0.0
  var initialLocalityRate = 0.0
  var nbDeals: Int = 0
  var nbDelegations: Int = 0
  var nbSwaps: Int = 0
  var nbTimeouts: Int = 0
  var nbDelegatedTasks: Int = 0
  var avgEndowmentSize: Double = 0
  var nbFirstStages: Int = 0
  var nbSecondStages: Int = 0

  /**
   * Initiate the distributor with an allocation, eventually a random one
   */
  def init(allocation: ExecutedAllocation = ExecutedAllocation.randomAllocation(stap)): ExecutedAllocation = {
    if (debug) println(s"$name: initial (eventually random) allocation\n$allocation")
    if (monitor) {
      val config: Config = ConfigFactory.load()
      val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
        config.getString("path.monitoringDirectory")
      val writer = new AllocationWriter(monitoringDirectoryName + "/allocation.json", allocation, "monitoring")
      writer.write()
    }
    allocation.setSimulatedCost(simulatedCost)
    allocation
  }

}

// Copyright (C) Maxime MORGE 2021, 2022, 2023
package org.smastaplus.consumer.monitor

import org.smastaplus.core._
import org.smastaplus.strategy.consumption.BeliefBase
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.consumer.deal.mas._
import org.smastaplus.strategy.deal.proposal.ProposalStrategy
import org.smastaplus.consumer.deal.mas.nodeAgent.manager.ManagerMind
import org.smastaplus.provisioning.{Active, ProvisioningUsage}

import java.io._
import org.joda.time.LocalDateTime
import scala.math.log10
import scala.concurrent.duration._
import akka.actor.{Actor, ActorRef}
import scala.annotation.unused

/**
  * Monitor the insight during negotiation
  * @param stap instance
  * @param name of the file nameLog.csv which contains the metrics
  * @param proposalStrategy is eventually the proposal strategy of the negotiating agents
  * @param counterProposalStrategy is eventually the counter-proposal strategy of the negotiating agents
  */
class Monitor(stap: STAP,
              name: String,
              proposalStrategy: Option[ProposalStrategy],
              counterProposalStrategy: Option[CounterProposalStrategy]) extends Actor {

  var debug: Boolean = false

  var supervisor: ActorRef = context.parent
  var directory: Directory = new Directory()
  var startingTime: Long = System.nanoTime()

  private var allocation = new ExecutedAllocation(stap) // The current allocation
  var informers: Set[ComputingNode] = Set.empty // Who give its insight
  private var waitingAllInforms = true // True if some insights are waited
  var beliefBase: BeliefBase = new BeliefBase(timestamp = Map[ComputingNode, LocalDateTime]()) // The belief base
  var gift : Map[ComputingNode, Int] = stap.ds.computingNodes.map(n => n->0).toMap // The gifts

  // Manage the log file
  def folder : String = "./"
  def fileName : String = folder + s"${name}Log.csv"
  val writer = new PrintWriter(new File(fileName))
  val head: String = s"timestamp,logTimeStamp," +
    "nbDelegatedTasks,nbConsumedTasks,ongoingMeanGlobalFlowtime\n"
  writer.write(head)

  /**
    * Returns the belief about the completion time for the job by a node
    */
  @throws(classOf[RuntimeException])
  def beliefCompletionTime(job: Job, node: ComputingNode): Double =
    beliefBase.beliefCompletionTime(job, node)

  /**
    * Returns the belief about the completion time for the job in the allocation
    */
  def beliefCompletionTime(job: Job): Double =
    stap.ds.computingNodes.map(node => beliefCompletionTime(job, node)).max

  /**
    * Returns the belief about the global flowtime for the job in the allocation
    */
  def beliefGlobalFlowtime: Double =
    stap.jobs.foldLeft(0.0)((sum, job) => sum + beliefCompletionTime(job))

  /**
    * Returns the belief about the global flowtime for the job in the allocation
    */
  @unused
  private def beliefMeanGlobalFlowtime(): Double = beliefGlobalFlowtime / stap.m.toDouble

  /**
    * Returns what the monitor believes about the workload for the node
    */
  def beliefWorkload(node: ComputingNode): Double =  beliefBase.belief(node).values.sum

  /**
    * Returns what the monitor believes about the makespan
    */
  @unused
  private def beliefMakespan(): Double = stap.ds.computingNodes.map(node => beliefWorkload(node)).max

  /**
    * Process the messages
    */
  override def receive: Receive = {
    // When the directory is received
    case Light(directory)  =>
      startingTime = System.nanoTime()
      supervisor = sender()
      this.directory = directory
      if (debug) println(s"Monitor> receives the directory $directory")

    // When an informance is received
    case Inform(insight) =>
      val node: ComputingNode = directory.nodeOf(sender())
      if (debug && node.name == "n1") println(s"Monitor> receives an informance from n1")
      beliefBase= beliefBase.update(node, insight)

    // When a new allocation is received
    case Init(initialAllocation: Allocation) =>
      allocation = ExecutedAllocation(initialAllocation)
      stap.ds.computingNodes.foreach { node: ComputingNode => // For each node
        // The task list
        val taskList: List[Task] = allocation.bundle(node)
        // Build the mind
        var mind = new ManagerMind(node,
          peers = stap.ds.computingNodes.toSet - node,
          usage = Map[ComputingNode, ProvisioningUsage]() ++ stap.ds.computingNodes.view.map(i => i -> Active),
          stap,
          taskSet = taskList.toSet,
          isFirstStage = true)
        // The next task to compute
        val nextTask: Option[Task] = if (!mind.getBundle.isEmpty)
          Some(mind.getBundle.nextTaskToExecute)
        else None
        mind = mind.setTaskInProgress(nextTask)
        // Update the belief base
        beliefBase = beliefBase.update(node, mind.insight())
      }
      // Log the allocation, the beliefs and gifts
      if (allocation.isPartition && allocation.isReady)  updateLog()

    // When the monitor is notified of a modification of the allocation (consumption or reallocation)
    case Notify(node,bundle,insight,silent) =>
      if (debug && node.name == "n1") println(s"Monitor> receives a notification for n1")
      // Update the allocation
      allocation = allocation.update(node, bundle)
      allocation = allocation.setDelay()
      // Update the belief base
      beliefBase= beliefBase.update(node, insight)
      informers += node
      // Eventually log the allocation, the beliefs and gifts
      if ((waitingAllInforms && informers.size == stap.ds.m) || !waitingAllInforms) { // first log when ready
        if (!silent && allocation.isPartition && allocation.isReady) {
          if (debug) println("Log Notify")
          updateLog()
        }
        waitingAllInforms = false
      }

    // When the monitor is notified of a consumption
    case NotifyDone(node,completedTask,completedTime) =>
      // Update the allocation
      allocation = allocation.updateCompletedTask(node,completedTask,completedTime)
      allocation = allocation.updateTaskInProgress(node,None)
      allocation = allocation.setDelay()
      // Eventually log the allocation, the beliefs and gifts
      if (allocation.isPartition && allocation.isReady) {
        if (debug) println("Log Done")
        updateLog()
      }

    // When the monitor is notified of a reallocation
    case Give(endowment)  =>
      val node: ComputingNode = directory.nodeOf(sender())
      // Update the gift
      gift = stap.ds.computingNodes.map { n: ComputingNode =>
        if (n != node) n -> gift(n)
        else n -> (gift(n) + endowment.size)
      }.toMap

    // When the monitor is notified of a delegation
    case Suggest(delegation) =>
      val node: ComputingNode = delegation.donor
      // Update the gift
      gift = stap.ds.computingNodes.map { n: ComputingNode =>
        if (n != node) n -> gift(n)
        else n -> (gift(n) + delegation.endowment.size)
      }.toMap


    case TriggerSecondStage =>
      val timestamp = System.nanoTime() - startingTime
      if (debug) println(s"End first stage $proposalStrategy $counterProposalStrategy: ${timestamp.nanos.toMillis}")

    case EndNegotiation =>
      val timestamp = System.nanoTime() - startingTime
      if (debug) println(s"End negotiation $proposalStrategy $counterProposalStrategy: ${timestamp.nanos.toMillis}")

    case Stop =>
      if (debug) println(s"Monitor> stops")
      writer.close()
      context.stop(self)
  }

  /**
    * log the allocation, the beliefs and gifts
    */
    private def updateLog() : Unit =  {
      val timestamp = allocation.delay.getMillis
      val logTimestamp =  (log10(allocation.delay.getMillis.toDouble + 1) * 10).floor / 10
      val nbConsumedTask = allocation.consumedBundles.values.map(_.keys.size).sum
      val outcome = s"$timestamp,$logTimestamp," +
        gift.values.sum + "," +
        nbConsumedTask + "," +
        allocation.ongoingMeanGlobalFlowtime / 1000 / 60 + "\n"
      writer.write(outcome)
      writer.flush()
    }
}
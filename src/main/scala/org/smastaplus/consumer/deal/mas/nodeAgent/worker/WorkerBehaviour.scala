// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2022, 2023
package org.smastaplus.consumer.deal.mas.nodeAgent.worker

import org.smastaplus.core._
import org.smastaplus.consumer.deal.mas._
import org.smastaplus.utils.Timer

import akka.actor.{ActorRef, FSM, Props, Stash}
import org.joda.time.{Duration, LocalDateTime}
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Worker agent with a concrete behaviour
  * @param stap instance
 *  @param node which is managed
  * @param simulated cost
  * @param monitor if any
  * @param ticksActivated is true if the tick of the timer is activated for monitoring the progression of the current task
 */
class WorkerBehaviour(stap: STAP,
                       node: ComputingNode,
                       simulated: SimulatedCost,
                       monitor: Option[ActorRef],
                       ticksActivated: Boolean = true)
  extends Worker(stap, node, simulated, monitor)
    with FSM[WorkerState, WorkerMind] with Stash {

  implicit val timeout: akka.util.Timeout = Timeout(6000 minutes)

  debug = false
  trace = false

  private var currentTask : Task = _
  private var startTaskDate : LocalDateTime = _
  private var taskProgression : Int = _
  private var workTimer : ActorRef = _

  var simulatedCost : Double = _

  /**
   * Initially the worker
   * - is free
   * - waits to receive a task to consume
   */
  startWith(
    Free,
    new WorkerMind(node, stap)
  )

  /**
    * Synchronous message to request the progression of the current task
    */
  private def askProgression(workerTimer: ActorRef) : Int = {
      val future = workerTimer ? QueryRemainingWork
      val remaining : Option[Int] = try {
        Some(Await.result(future, timeout.duration).asInstanceOf[TaskProgression].progression)
      } catch {
        case _: Exception => None
      }
      if (remaining.isDefined){
        return remaining.get
      }
      0
  }

  /**
    * Either the worker has no task in progress and is able 
    * to receive a new one
    */
  when(Free) {
    // An enlightenment of the agents about its acquaintances
    case Event(Perform(task), mind)  =>
      manager = sender()
      taskProgression = 0
      currentTask = task
      simulatedCost = simulated.simulatedCost(stap.cost(task, node), node)
      if (!ticksActivated) tickInterval = simulatedCost.toInt
      workTimer = context.actorOf(Props(classOf[WorkerTimer], simulatedCost.toInt, tickInterval))
      workTimer ! Timer.Start
      startTaskDate = LocalDateTime.now()
      if (debug) println(s"worker $node starts to perform $task, estimated cost : ${stap.cost(task, node)}, simulated cost : $simulatedCost")
      goto(Busy) using mind

    //when the previous task is already done but the manager has still not processed the information
    case Event(QueryRemainingWork(), mind)  =>
      manager = sender()
      manager ! RemainingCost(0)
      stay() using mind
  }

  /**
    * Or the worker is busy and has a task in progress
    */
  when(Busy) {
    // A message from the negotiator informing it has no longer reallocation to propose in the current stage
    case Event(QueryRemainingWork(), mind)  =>
      val remainingTaskCost = simulatedCost - askProgression(workTimer)
      if (debug) println(s"worker $node sending remaining cost of $currentTask : $remainingTaskCost")
      sender() ! RemainingCost(remainingTaskCost)
      stay() using mind

    // A message from the timer to update the progression of the current task
    case Event(TaskProgression(progression), mind) =>
      taskProgression = progression
      stay() using mind

    // A timeout from the timer indicates that the task is done
    case Event(Timer.Timeout, mind) => 
      val completionDate = LocalDateTime.now()
      val duration = new Duration(startTaskDate.toDateTime(), completionDate.toDateTime()).getMillis
      if (debug) println(s"worker $node finished to perform $currentTask at date $completionDate, task duration : $duration")
      manager ! Done(completionDate)
      goto(Free) using mind
  }

    /**
      * Whatever the state is
      **/
    whenUnhandled {
      // A trace of the messages which are sent is required
      case Event(Trace(writer), mind)  =>
        this.trace = true
        this.writer = writer
        stay() using mind

      // A deprecated timeout
      case Event(Timer.Timeout, mind) =>
        stay() using mind
        
      // Other message are not expected
      case Event(msg @ _, _) =>
        throw new RuntimeException(s"ERROR Worker $node was not expected message $msg in state $stateName")
    }

    /**
      * Associates actions with a transition instead of with a state and even, e.g. debugging
      */
    onTransition {
      case s1 -> s2 =>
        if (debugState) println(s"manager $node moves from the state $s1 to the state $s2")
    }

    // Finally Triggering it up using initialize, which performs
    // the transition into the initial state and sets up timers (if required).
    initialize()
  }


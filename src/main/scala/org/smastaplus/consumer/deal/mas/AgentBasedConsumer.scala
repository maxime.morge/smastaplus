// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, Anne-Cécile CARON, 2020, 2021, 2022, 2023
package org.smastaplus.consumer.deal.mas

import org.smastaplus.core._
import org.smastaplus.strategy.deal.counterproposal._
import org.smastaplus.strategy.deal.proposal._
import org.smastaplus.consumer.SynchronousConsumer
import org.smastaplus.consumer.deal.mas.supervisor.Supervisor
import org.smastaplus.utils.serialization.core.AllocationWriter

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.actor.SupervisorStrategy.Stop
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}

/**
  * Agent-based consumer
  * @param stap instance
  * @param rule to adopt for reducing either the localFlowtime or the makespan
  * @param firstProposalStrategy for making proposals in the first stage
  * @param secondProposalStrategy for eventually making proposals in the second stage
  * @param counterProposalStrategy for making counter-proposals in the second stage
  * @param system of actors
  * @param name of the balancer
  * @param dispatcherId is the name of the dispatcher "akka.actor.default-dispatcher" or "single-thread-dispatcher"
  * @param monitor is true if the completion times are monitored during the solving
  * @param simulated cost with  with a perfect information of the running environment by default
  */
class AgentBasedConsumer(stap: STAP,
                         rule: SocialRule,
                         firstProposalStrategy: ProposalStrategy,
                         secondProposalStrategy: Option[ProposalStrategy],
                         counterProposalStrategy: CounterProposalStrategy,
                         val system: ActorSystem,
                         name: String = "AgentBasedConsumer",
                         val dispatcherId: String,
                         monitor: Boolean = false,
                         simulated: SimulatedCost = new SimulatedCostE()
                        )
  extends SynchronousConsumer(stap, name, monitor, simulated){

  var trace = false
  val TIMEOUT_VALUE : FiniteDuration = 6000 minutes // Default timeout of a run
  implicit val timeout : Timeout = Timeout(TIMEOUT_VALUE)

  // Launch a new supervisor
  AgentBasedConsumer.id += 1
  private val supervisorName: String = "supervisor"+AgentBasedConsumer.id
  val supervisor : ActorRef = system.actorOf(Props(classOf[Supervisor], stap, rule,
    firstProposalStrategy, secondProposalStrategy, counterProposalStrategy, simulated, dispatcherId, monitor)
    withDispatcher dispatcherId,
      name = supervisorName)

  /**
    * Initiate the balancer with an allocation, eventually a random one
    */
  override def init(allocation: ExecutedAllocation = ExecutedAllocation.randomAllocation(stap)): ExecutedAllocation = {
    if (debug) println(s"AgentBasedConsumer: initial (eventually random) allocation\n$allocation")
    allocation.setSimulatedCost(simulated)
    if (monitor) {
      val config: Config = ConfigFactory.load()
      val monitoringDirectoryName : String = config.getString("path.smastaplus") + "/" +
        config.getString("path.monitoringDirectory")
      val writer = new AllocationWriter(monitoringDirectoryName + "/allocation.json", allocation, "monitoring")
      writer.write()
    }
    allocation
  }

  /**
    * Returns an empty allocation with the bundle of tasks consumed by each node
    */
  def consume(allocation: ExecutedAllocation) : ExecutedAllocation = {
    allocation.setSimulatedCost(simulated)
    stap.resetReleaseTime()
    initialLocalityRate = allocation.localAvailabilityRatio
    if (trace) supervisor ! Trace
    init(allocation)
    if (debug) println("AgentBasedConsumer: reallocate")
    val future = supervisor ? Init(allocation)
    val result = Await.result(future, timeout.duration).asInstanceOf[Outcome]
    supervisor ! Stop
    if (debug) println(s"AgentBasedConsumer: reallocate ends")
    if (debug) println(s"AgentBasedConsumer: metrics ${result.metrics}")
    nbDeals= result.metrics.sumOfDelegationConfirmations + result.metrics.sumOfSwapConfirmations
    nbDelegations = result.metrics.sumOfDelegationConfirmations
    nbSwaps= result.metrics.sumOfSwapConfirmations
    nbFirstStages = result.nbFirstStages
    nbSecondStages = result.nbSecondStages
    nbDelegatedTasks = result.metrics.sumOfDelegatedTasks
    if (debug) println(s"Negotiation successful rate = $nbDeals")
    nbTimeouts =  result.metrics.sumOfTimeouts
    if (debug) println(s"Nb timeout = ${result.metrics.sumOfTimeouts}")
    if (debug) println(s"Nb proposals = ${result.metrics.sumOfProposals}")
    if (debug) println(s"Nb delegation confirmations = ${result.metrics.sumOfDelegationConfirmations}")
    if (debug) println(s"Nb swap confirmations = ${result.metrics.sumOfSwapConfirmations}")
    if (debug) println(s"Nb delegated tasks = ${result.metrics.sumOfDelegatedTasks}")
    if (debug) println(s"Nb first stages = ${result.nbFirstStages}")
    if (debug) println(s"Nb second stages = ${result.nbSecondStages}")
    avgEndowmentSize =  result.metrics.sumOfDelegatedTasks.toDouble / (result.metrics.sumOfDelegationConfirmations + result.metrics.sumOfSwapConfirmations).toDouble
    if (avgEndowmentSize.isNaN) avgEndowmentSize = 0.0
    if (debug) println(s"Endowment size = $avgEndowmentSize")
    localRatio = result.allocation.localAvailabilityRatio
    result.allocation
  }

  /**
    * Closes the consumer
    */
  def close() : Unit = {
      system.terminate()
  }
}

/**
 * Companion object for testing AgentBasedConsumer
 */
object AgentBasedConsumer extends App {
  var id: Int = 1
}

// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.consumer.deal.mas.nodeAgent.manager

import org.smastaplus.core._
import org.smastaplus.consumer.deal.mas.Metrics
import org.smastaplus.consumer.deal.mas.nodeAgent.InternalAgent
import akka.actor.ActorRef

/**
 * Abstract negotiator
 *
 * @param stap instance
 * @param node represented by the agent
 * @param negotiator for reallocating tasks
 * @param monitor which record the bundle if any
 */

abstract class Manager(stap: STAP, node: ComputingNode, negotiator: ActorRef, monitor: Option[ActorRef])
  extends InternalAgent(stap,node, monitor){
    var metrics = new Metrics()  // Metrics from the negotiator/manager
}
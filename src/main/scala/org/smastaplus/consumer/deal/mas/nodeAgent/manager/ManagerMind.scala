// Copyright (C) Maxime MORGE, Elle BEAUPREZ, Anne Cécile CARON, 2022, 2022, 2023
package org.smastaplus.consumer.deal.mas.nodeAgent.manager

import org.smastaplus.core.{ComputingNode, STAP, Task}
import org.smastaplus.provisioning.ProvisioningUsage
import org.smastaplus.utils.serialization.core.BundleWriter
import org.smastaplus.strategy.consumption._

import org.joda.time.LocalDateTime
import com.typesafe.config.{Config, ConfigFactory}
import scala.annotation.unused

/**
 *  Manager's state of mind
 * @param me is the node I manage
 * @param peers are nodes managed by the other agents
 * @param usage are the the provisioning usages of the computing nodes
 * @param stap instance
 * @param isFirstStage is true if the agent is in the first stage, false otherwise
 */
final class ManagerMind(me: ComputingNode,
                        peers: Set[ComputingNode],
                        usage: Map[ComputingNode, ProvisioningUsage],
                        stap: STAP, taskSet: Set[Task],
                        val isFirstStage: Boolean)
  extends Mind(me, peers, stap, taskSet) {

  debug = false
  val debugUpdate = false

  //information about the task in progress
  var taskInProgress : Option[Task] = None
  var remainingWork : Option[Double] = None

  //map to memorize the consumed tasks and their realisation date
  var completedTasks : Map[Task, LocalDateTime] = Map()

  bundle = new Bundle(stap, me).setBundle(taskSet)

  // in case of monitoring
  val config: Config = ConfigFactory.load()
  var monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
    config.getString("path.monitoringDirectory")
  var pathName: String = monitoringDirectoryName + "/trace/" + me.name + ".json"

  /**
   * Auxiliary  constructor
   */
  def this(me: ComputingNode,
           peers: Set[ComputingNode],
           usage: Map[ComputingNode, ProvisioningUsage],
           stap: STAP,
           bundle: Bundle,
           isFirstStage: Boolean) = {
    this(me, peers, usage, stap, Set.empty[Task], isFirstStage)
    this.bundle = bundle
  }

  /**
   * Auxiliary  constructor
   */
  def this(me: ComputingNode,
           peers: Set[ComputingNode],
           usage: Map[ComputingNode, ProvisioningUsage],
           stap: STAP,
           bundle: Bundle,
           isFirstStage: Boolean,
           taskInProgress: Option[Task],
           remainingWork: Option[Double],
           completedTasks: Map[Task, LocalDateTime]) = {
    this(me, peers, usage, stap, Set.empty[Task], isFirstStage)
    this.bundle = bundle
    this.taskInProgress = taskInProgress
    this.remainingWork = remainingWork
    this.completedTasks = completedTasks
  }

  override def toString: String = super.toString + s" and the usage is $usage and  "

  def bundleWriter(@unused mode : String) : BundleWriter = new BundleWriter(pathName, bundle, "monitoring")


  /**
   * Returns a copy
   */
  override def copy(): ManagerMind = {
    val copyOfBundle = this.bundle.copy()
    new ManagerMind(me, peers, usage, stap, copyOfBundle, isFirstStage, taskInProgress, remainingWork, completedTasks)
  }

  /**
   * Get the bundle of the mind
   */
  def getBundle : Bundle = this.bundle

  /**
   * Get the bundle of the mind
   */
  def getCurrentTasks: List[Task] = (taskInProgress ++ this.bundle.sortedTasks ++ this.completedTasks.keys).toList

  /**
   * Sets and sorts the bundle of the mind
   */
  override def setTaskSet(updatedBundle: Set[Task]) : ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.setBundle(updatedBundle), isFirstStage)

  /**
   * Sets  the bundle of the mind
   */
  override def setBundle(updatedBundle: Bundle) : ManagerMind =
    new ManagerMind(me, peers, usage, stap, updatedBundle, isFirstStage)

  /**
   * Adds a task to the bundle and sorts it
   */
  override def addTask(task: Task): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.addTask(task), isFirstStage, taskInProgress, remainingWork, completedTasks)

  /**
   * Adds an endowment to the bundle and sorts it
   */
  override def addTasks(endowment: List[Task]): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.addTasks(endowment), isFirstStage, taskInProgress, remainingWork, completedTasks)

  /**
   * Removes a task to the bundle and sorts it
   */
  override def removeTask(task: Task): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.removeTask(task), isFirstStage, taskInProgress, remainingWork, completedTasks)

  /**
   * Remove an endowment to the bundle and sorts it
   */
  override def removeTasks(endowment: List[Task]): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.removeTasks(endowment), isFirstStage, taskInProgress, remainingWork, completedTasks)

  /**
   * Replaces a task by a counterpart in the bundle and sorts it
   */
  override def replaceTask(task: Task, counterpart: Task):ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.replaceTask(task, counterpart), isFirstStage, taskInProgress, remainingWork, completedTasks)

  /**
   * Replaces an endowment by a counterpart in the bundle and sorts it
   */
  override def replaceTasks(endowment: List[Task], counterpart: List[Task]): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.replaceTasks(endowment, counterpart), isFirstStage, taskInProgress, remainingWork, completedTasks)

  /**
   * Initializes the negotiation mind with a new bundle
   */
  def initBundle(initialBundle: List[Task]): ManagerMind =
    new ManagerMind(me, peers, usage, stap, initialBundle.toSet, isFirstStage)

  /**
   * Go to the first stage
   */
  @unused
  def firstStage : ManagerMind =
    new ManagerMind(me, peers, usage, stap, bundle, true)

  /**
   * Go to the second stage
   */
  @unused
  def secondStage : ManagerMind =
    new ManagerMind(me, peers, usage, stap, bundle, false)

  /**
   * Set the task which has been given to the worker for execution
   */
  def setTaskInProgress(task: Option[Task]) : ManagerMind = {
    if (task.isDefined)
      new ManagerMind(me, peers, usage, stap, bundle.updateInProgressTask(task), isFirstStage, task, Some(stap.cost(task.get, me)), completedTasks)
    else
      new ManagerMind(me, peers, usage, stap, bundle.updateInProgressTask(task), isFirstStage, None, None, completedTasks)
  }

  def getTaskInProgress : Option[Task] = {
    taskInProgress
  }

  def getRemainingWork : Option[Double] = {
    remainingWork
  }

  def updateCompletedTasks(task: Task, completionDate: LocalDateTime) : ManagerMind = {
    val updatedTasks = completedTasks + (task -> completionDate)
    new ManagerMind(me, peers, usage, stap, bundle, isFirstStage, taskInProgress, remainingWork, updatedTasks)
  }

  def resetTaskInProgress : ManagerMind =
    new ManagerMind(me, peers, usage, stap, bundle.resetTaskInProgress, isFirstStage, None, None, completedTasks)

  /**
   * Update the remaining cost of the task which in being consumed by the worker
   */
  def updateRemainingWork(remaining: Double) : ManagerMind =
    new ManagerMind(me, peers, usage, stap, bundle.updateRemainingWork(remaining), isFirstStage, taskInProgress, Some(remaining), completedTasks)
}
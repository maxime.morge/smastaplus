// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022, 2023
package org.smastaplus.consumer.deal.mas.nodeAgent

import org.smastaplus.core._
import org.smastaplus.strategy.consumption._
import org.smastaplus.consumer.deal.mas._

import akka.actor.{Actor, ActorRef}
import java.io.{OutputStream, OutputStreamWriter, PrintWriter}

/**
  * Abstract internal agent
  * @param stap instance
  * @param node represented by the agent
  * @param monitor if any
  */
abstract class InternalAgent(stap: STAP,
                             node: ComputingNode,
                             monitor: Option[ActorRef]) extends Actor{
  var trace: Boolean = true
  var internalTrace = false
  var debug: Boolean = false
  var debugState: Boolean = false

  var outputStream: OutputStream = OutputStream.nullOutputStream
  var outputStreamWriter = new OutputStreamWriter(outputStream)
  var writer = new PrintWriter(outputStreamWriter, true) // new PrintWriter(System.out, true)

  var supervisor: ActorRef = _
  var directory: Directory = new Directory()

  /**
    * Multicast a message to some peers
    */
  def multicast(peers : Iterable[ComputingNode], message : Message): Unit = {
    if (monitor.isDefined) monitor.get ! message
    var isFirst = true
    var log = ""
    peers.foreach { peer =>
      if (trace){
        if (isFirst) log += s"$node-> $peer : $message\n"
        else log += s"& $node -> $peer :\n"
      }
      isFirst = false
      directory.addressOf(peer) ! message
    }
    if (trace) writer.print(log)
  }

  /**
    * Broadcast a message to neighbours
    */
  def broadcast(message : Message): Unit = {
    multicast(directory.peers(node), message)
  }

  /**
    * Broadcast insight
    */
  def broadcast(insight: Insight): Unit = broadcast(Inform(insight))
}
// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022, 2023
package org.smastaplus.consumer.deal.mas.nodeAgent.negotiator

import org.smastaplus.core._
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.ProposalStrategy
import org.smastaplus.consumer.deal.mas.Metrics
import org.smastaplus.consumer.deal.mas.nodeAgent.InternalAgent
import akka.actor.{ActorRef, Props, Stash}
import org.smastaplus.utils.Timer

import java.util.concurrent.ThreadLocalRandom

/**
  * Abstract negotiator
 *
 * @param stap instance
  * @param node represented by the agent
  * @param proposalStrategy for proposing/accepting/refusing proposals
  * @param counterProposalStrategy for proposing/accepting/refusing counterProposals
  * @param monitor if any
 */
abstract class Negotiator(stap: STAP, node: ComputingNode,
                          proposalStrategy: ProposalStrategy,
                          counterProposalStrategy: CounterProposalStrategy,
                          monitor: Option[ActorRef])
  extends InternalAgent(stap,node, monitor) with Stash{

  private val debugDeadline: Boolean = false
  var manager : ActorRef = _
  var metrics = new Metrics()  // Metrics of the negotiation process
  private val rnd: ThreadLocalRandom = ThreadLocalRandom.current()
  private var nbTimers = 0

  /**
   * Returns the deadline for  the next proposal
   */
  private def deadline(): Long = {
    val time = rnd.nextLong(300) + 600//rnd.nextInt(300) + 600
    if (debugDeadline) println(s"$node generates deadline with $time nanoseconds")
    time
  }

  /**
   * Policy for stashing received proposals
   */
  def randomStash(): Unit = {
    if (rnd.nextBoolean()) stash()
  }

  var timer: ActorRef = _

  /**
   * Start timer
   */
  def startTimer(): Unit = {
    nbTimers += 1
    val name = s"Timer${nbTimers}4$node".replace('ν', 'n')
    timer = context.actorOf( // build the actor
      Props(classOf[Timer], deadline()).withDispatcher("akka.actor.default-dispatcher"), name)
    timer ! Timer.Start
  }

  /**
   * Cancel timer
   */
  def cancelTimer(): Unit = timer ! Timer.Cancel

}
// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2020, 2021, 2022, 2023
package org.smastaplus.consumer.deal.mas

import org.smastaplus.core._
import org.smastaplus.process.{Delegation, Swap}
import org.smastaplus.strategy.consumption.{Bundle, Insight}
import org.joda.time.LocalDateTime

import java.io.PrintWriter
import scala.collection.SortedSet

/**
  * Class representing a message of the agent communication language
  * @param msgId of the message
  * @param replyId is eventually the id of the message to which it replies
  *
  */
class Message(msgId: Int = Message.ID, replyId: Option[Int] = None) {
  Message.ID +=1 // Increment the static variable
}

/**
  * Companion object for adding a static member
  */
object Message{
  var ID = 1
}

// Messages for negotiation
case class Propose(delegation: Delegation, insight: Insight) extends Message(delegation.hashCode(), None){// Make a proposal
  override def toString: String = s"Propose($delegation,$insight)"
}
case class Reject(delegation: Delegation, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Reject a proposal
  override def toString: String = s"Reject($delegation,$insight)"
}
case class Accept(delegation: Delegation, insight: Insight, msgId :Int = Message.ID, replyId : Int) extends Message(msgId, Some(replyId)) { // Accept a proposal
  override def toString: String = s"Accept($delegation,$insight)"
}
case class CounterPropose(swap: Swap, insight: Insight, replyId: Int) extends Message(swap.hashCode(), Some(replyId)) { // Make a counter-proposal
  override def toString: String = s"CounterPropose($swap,$insight)"
}
case class Confirm(deal: Delegation, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Confirm a bilateral deal (delegation)
  override def toString: String = s"Confirm($deal,$insight)"
}
case class ConfirmSwap(deal: Swap, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Confirm a bilateral deal (swap)
  override def toString: String = s"ConfirmSwap($deal,$insight)"
}
case class Withdraw(delegation: Delegation, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Withdraw a deal (delegation)
  override def toString: String = s"Withdraw($delegation,$insight)"
}
case class WithdrawSwap(swap: Swap, insight: Insight, replyId : Int) extends Message(Message.ID, Some(replyId)) { // Withdraw a deal (swap)
  override def toString: String = s"Withdraw($swap,$insight)"
}
case class Inform(insight: Insight) extends Message(Message.ID, None) { // Inform the peer about the insight
  override def toString: String = s"Inform($insight)"
}

// Internal messages for the negotiator
case class Next() extends Message(Message.ID, None) { // Trigger the offer strategy of an agent
  override def toString: String = s"Next"
}
case class WakeUp(delegation: Delegation) extends Message(Message.ID, None) { // Trigger the proposal of the delegation
  override def toString: String = s"WakeUp($delegation)"
}
case class Timeout() extends Message(Message.ID, None) { // Trigger the offer strategy of an agent
  override def toString: String = s"Timeout"
}
case class Close() extends Message(Message.ID, None) { // An agent is restarted
  override def toString: String = s"Close"
}
case class Cancel() extends Message(Message.ID, None) { // A swap has been withdrawn, the negotiator must return to Responder
  override def toString: String = s"Cancel"
}

// Messages from/to the supervisor
case class Trace(writer: PrintWriter) extends Message(Message.ID, None){// Ask the agents to trace the protocol
  override def toString: String = s"Trace"
}
case class Start() extends Message(Message.ID, None){// Start the supervisor/agents
  override def toString: String = s"Start"
}
case class Light(directory : Directory) extends Message(Message.ID, None){ // Light the  agents with their acquaintances
  override def toString: String = s"Trigger($directory)"
}
case class Outcome(allocation: ExecutedAllocation, metrics: Metrics, nbFirstStages: Int, nbSecondStages: Int) extends Message(Message.ID, None){ // The outcome of the distributed consumer
  override def toString: String = s"Outcome($allocation,$metrics, $nbFirstStages, $nbSecondStages)"
}
case class Init(allocation: ExecutedAllocation) extends Message(Message.ID, None) { // Init the supervisor with the allocation
  override def toString: String = s"Init($allocation)"
}
case class Update(additionalAllocation: ExecutedAllocation) extends Message(Message.ID, None) { //Upate the Distributor with the allocation
  override def toString: String = s"Update($additionalAllocation)"
}

case class Give(bundle: List[Task]) extends Message(Message.ID, None) { // Give a bundle to an agent
  override def toString: String = s"Give($bundle)"
}
case class Ready() extends Message(Message.ID, None) {// The agent is ready to start
override def toString: String = s"Ready"
}
case class End(bundle: List[Task], consumedTasks: Map[Task, LocalDateTime], taskInProgress: Option[Task]) extends Message(Message.ID, None) { // An agent is ended with a bundle
  override def toString: String = s"End($bundle, $consumedTasks, $taskInProgress)"
}
case class Restart() extends Message(Message.ID, None) { // An agent is restarted
  override def toString: String = s"Restart"
}
case class Query(msgId: Int = Message.ID) extends Message(msgId, None) { // Query the insight nbProposals to an agent
  override def toString: String = s"Query"
}
case class Answer(consumedTasks: Map[Task, LocalDateTime], metrics: Metrics, isEmpty: Boolean, replyId: Int) extends Message(Message.ID, Some(replyId)) { // Reply with some metrics
  override def toString: String = s"Answer($consumedTasks,$metrics, $isEmpty)"
}
case class PreTrigger(stage: Int) extends Message(Message.ID, None) { // PreTrigger a stage
  override def toString: String = s"PreTrigger($stage)"
}
case class ReadyForNextStage() extends Message(Message.ID, None) { // The agent is ready for the next stage
  override def toString: String = s"ReadyForNextStage"
}
case class Trigger(stage: Int) extends Message(Message.ID, None) { // Trigger a stage
  override def toString: String = s"TriggerStage($stage)"
}
case class Kill() extends Message(Message.ID, None) { // Kill the agent
  override def toString: String = s"Kill"
}
case class Finished(consumedTasks: Map[Task, LocalDateTime]) extends Message(Message.ID, None) { // Response to a kill message
  override def toString: String = s"Finished"
}
case class WorkDone(consumedTasks: Map[Task, LocalDateTime]) extends Message(Message.ID, None) { // Inform the supervisor about consumptions
  override def toString: String = s"WorkDone($consumedTasks)"
}
case class HasConsumedTasksDuringTransition() extends Message(Message.ID, None) { // Inform the supervisor about consumptions during transition
  override def toString: String = s"HasConsumedTasksDuringTransition"
}

// Message between the supervisor and the monitor
case class Stop() extends Message(Message.ID, None) { // Stop the monitor
  override def toString: String = s"Stop"
}
case class TriggerSecondStage() extends Message(Message.ID, None) { // Time markup
  override def toString: String = s"TriggerSecondStage"
}
case class TriggerFirstStage() extends Message(Message.ID, None) { // Time markup
  override def toString: String = s"TriggerFirstStage"
}
case class EndNegotiation() extends Message(Message.ID, None) { // Time markup
  override def toString: String = s"EndNegotiation"
}

//Message between the manager and the negotiator
//Message from the manager to the negotiator
case class GiveUpdatedBundle(bundle: Bundle, task: Option[Task], remainingCost: Option[Double], timestamp: LocalDateTime = LocalDateTime.now()) extends Message(Message.ID, None) { // Commission a negotiator to delegate a bundle
  override def toString: String = s"GiveUpdatedBundle($bundle, $task, $remainingCost, $timestamp)"
}
case class UpdateBundleAfterTransaction(success: Boolean, bundle: Bundle, task: Option[Task], remainingCost: Option[Double], timestamp: LocalDateTime = LocalDateTime.now()) extends Message(Message.ID, None) { // Commission a negotiator to delegate a bundle
  override def toString: String = s"UpdateBundleAfterTransaction($success, $bundle, $task, $remainingCost, $timestamp)"
}
case class StartStage() extends Message(Message.ID, None) { // Tell the negotiator to start the next stage
  override def toString: String = s"StartStage"
}
//Messages from the negotiator to the manager
case class FinishedStage(metrics: Metrics) extends Message(Message.ID, None) { // Inform the manager about the end of the current stage
  override def toString: String = s"FinishedStage($metrics)"
}
case class Add(endowment: List[Task]) extends Message(Message.ID, None) { // Ask the manager to add a endowment in the bundle
  override def toString: String = s"Add($endowment)"
}
case class Remove(endowment: List[Task]) extends Message(Message.ID, None) { // Ask the manager to remove a task from the bundle
  override def toString: String = s"Remove($endowment)"
}
case class Replace(endowment: List[Task], counterpart: List[Task]) extends Message(Message.ID, None) { // Ask the manager to replace a task by an other in the bundle
  override def toString: String = s"Replace($endowment, $counterpart)"
}

//Message between the manager and the worker
//Message from the manager to the worker
case class Perform(task: Task) extends Message(Message.ID, None) { //Ask the worker to perform the given task
  override def toString: String = s"Perform($task)"
}
case class QueryRemainingWork() extends Message(Message.ID, None) { //Ask the worker the remaining cost of the task in progress
  override def toString: String = s"QueryRemainingWork"
}
//Message from the worker to the manager
case class RemainingCost(cost: Double) extends Message(Message.ID, None) { //Informs the manager about the remaining cost 
  override def toString: String = s"RemainingCost($cost)"
}
case class Done(completionDate: LocalDateTime) extends Message(Message.ID, None) { //Informs the manager the finished task
  override def toString: String = s"Done($completionDate)"
}

//Message from WorkerTimer to Worker
case class TaskProgression(progression: Int) extends Message(Message.ID, None) { // Inform the worker about the progression of the current
  override def toString: String = s"TaskProgression($progression)"
}

// Message for the monitor
case class Notify(node: ComputingNode, bundle: List[Task], insight: Insight, silent : Boolean = true) extends Message(Message.ID, None) {
  // Notify the monitor about the bundle and insight, silently by default
  override def toString: String = s"Notify($node,$bundle,$insight,$silent)"
}
case class NotifyDone(node: ComputingNode, task: Task, completionTime:  LocalDateTime) extends Message(Message.ID, None) {
  // Notify the monitor about the consumption of the task at completionTime
  override def toString: String = s"Notify($node,$task,$completionTime)"
}

case class Reschedule(nodes: SortedSet[ComputingNode], allocation: ExecutedAllocation, potentialDelegatedTasks : List[Task]) extends Message(Message.ID, None) {
  // Ask the scheduler to apply Parallel Single-Issue Auction
  override def toString: String = s"Reschedule($nodes,$allocation,$potentialDelegatedTasks)"
}

// Messages for negotiation
case class Suggest(delegation: Delegation) extends Message(delegation.hashCode(), None){// The scheduler suggest a delegation
  override def toString: String = s"Suggest($delegation)"
}

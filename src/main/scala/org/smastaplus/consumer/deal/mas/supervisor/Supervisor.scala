// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022
package org.smastaplus.consumer.deal.mas.supervisor

import org.smastaplus.core._
import org.smastaplus.strategy.deal.counterproposal.CounterProposalStrategy
import org.smastaplus.strategy.deal.proposal.ProposalStrategy
import org.smastaplus.consumer.deal.mas._
import org.smastaplus.consumer.deal.mas.nodeAgent.NodeAgent

import akka.actor.{Actor, ActorRef, FSM, Props, Stash}
import org.smastaplus.consumer.monitor.Monitor

import java.io._

/**
  * Supervisor which starts/stops the reallocation
  * @param stap instance
  * @param rule to adopt for reducing either the localFlowtime or the makespan
  * @param firstProposalStrategy for proposing in the first stage
  * @param secondProposalStrategy for proposing in the second stage eventually
  * @param counterProposalStrategy for counter-proposing in the second stage
  * @param dispatcherId is the name of the dispatcher "akka.actor.default-dispatcher" or "single-thread-dispatcher"
  * @param isMonitored is true if the completion times are monitored during the solving
  **/
class Supervisor(stap: STAP, rule: SocialRule,
                 firstProposalStrategy: ProposalStrategy,
                 secondProposalStrategy: Option[ProposalStrategy],
                 counterProposalStrategy: CounterProposalStrategy,
                 simulated: SimulatedCost,
                 val dispatcherId: String,
                 isMonitored: Boolean)
  extends Actor with Stash
    with FSM[SupervisorState, SupervisorStatus] {

  var debug = false
  private val debugStage = false
  var debugState = false
  var trace = false
  private val debugMetrics = false

  def folder : String = "./"
  def fileName : String = folder + "log.txt"

  var outputStream: OutputStream = OutputStream.nullOutputStream
  var outputStreamWriter = new OutputStreamWriter(outputStream)
  var writer = new PrintWriter(outputStreamWriter, true) // rather than System.out

  var balancer: ActorRef = context.parent // Reference to the distributed balancer
  var monitor : Option[ActorRef] = None
  var directory = new Directory() // White page for the agents

  private var hasConsumptionsDuringTransition = false
  private var waitingForNextStage = false
  private var numberOfFirstStages = 1
  private var numberOfSecondStages = 0

  /**
    * Initially the allocation is empty
    */
  startWith(
    InitialState,
    new SupervisorStatus(allocation = new ExecutedAllocation(stap), isTwoStageProcess = secondProposalStrategy.isDefined, lastQueryId = None)
  )

  /**
    * Method invoked after starting the actor
    */
  override def preStart(): Unit = {
    if (debug) println(s"Supervisor> is monitored $isMonitored")
    monitor = if (isMonitored) {
      Some(context.actorOf(Props(classOf[Monitor], stap, "negotiation", Some(firstProposalStrategy), Some(counterProposalStrategy))
        .withDispatcher(dispatcherId),"Monitor"))
    } else None
    stap.ds.computingNodes.foreach{ node: ComputingNode => // For each node
      val actor = context.actorOf(// build the actor
        Props(classOf[NodeAgent], stap, node, rule, firstProposalStrategy, secondProposalStrategy, counterProposalStrategy, simulated, monitor)
          .withDispatcher(dispatcherId), node.name) //.replace('ν','n')
      directory.add(node, actor)// add it to the directory
    }
    if (debug) println(s"Supervisor> directory $directory")
  }

  /**
    * Either the supervisor is in the initial state and it is waiting for
    * the agent to be ready
    */
  when(InitialState) {
    // The initialization of the allocation by the balancer leads the supervisor to
    // enlighten the agents
    case Event(Init(allocation), status) =>
      balancer = sender()
      if (isMonitored) {
        monitor.get ! Light(directory)
        monitor.get ! Init(allocation)
      }
      directory.allActors().foreach { actor: ActorRef => // For each node
        val node = directory.nodeOf(actor)
        if (debug) println(s"Supervisor> Supervisor triggers $node with directory")
        actor ! Light(directory) // Wait the Ready messages to give them their initial bundle
      }
      // Set up the initial allocation
      stay() using new SupervisorStatus(allocation = allocation, isTwoStageProcess = status.isTwoStageProcess, lastQueryId = None)

    // When an agent informs the supervisor that it is ready this
    // information is updated in the status
    case Event(Ready, status) if status.sumReady < stap.ds.m-1 =>
      stay() using status.incrementNbReady

    // When all the agents are ready ready,
    // the supervisor triggers the first stage,
    // it gives to the agents their initial bundle, and
    // it goes to the RunningFirstStage  state
    case Event(Ready, status) if status.sumReady == stap.ds.m-1 =>
      if (debug) println(s"Supervisor: nbReady = ${status.sumReady}")
      //Gives a bundle to each of them
      directory.allActors().foreach { actor: ActorRef =>
        val agent = directory.nodeOf(actor)
        val bundle = status.allocation.bundle(agent)
        if (trace) writer.println(s"Supervisor -> $agent : Give($bundle)")
        actor ! Give(bundle)
      }
      unstashAll()
      goto(RunningFirstStage) using status.incrementNbReady

    // When an agent ends, the supervisor stashes the message
    // in order to process it in the RunningSupervisorState
    case Event(End(_,_,_), status) =>
      stash()
      stay() using status

    // Since supervisor are rerun several time, agent dead are ignored
    case Event(Finished, status) =>
      stay() using status
    // Since supervisor are rerun several time, agent dead are ignored
    case Event(Restart, status) =>
      stay() using status
    case Event(Close, status) =>
      stay() using status
    // Ignore redundant answer
    case Event(Answer(_,_,_,_), status) =>
      stay() using status
  }

  /**
    * Or the supervisor is in the running state for the first stage and it is
    * waiting for the agent ends
    */
  when(RunningFirstStage) {
    // When an agent ends, the supervisor updates its status
    case Event(End(bundle, consumedTasks, taskInProgress), status)
      if status.desperateNodes.size < stap.m -1 || status.desperateNodes.contains(directory.nodeOf(sender())) =>
      val node = directory.nodeOf(sender())
      if (debug) println(s"Supervisor receives End from $node (first case), desperate nodes : ${status.desperateNodes}")
      stay() using status.end(node, bundle, consumedTasks, taskInProgress)

    // When all the agents end, the supervisor closes the stage and
    // it goes to the EndFirstStage state
    case Event(End(bundle, consumedTasks, taskInProgress), status)
      if status.desperateNodes.size == stap.m -1
        && ! status.desperateNodes.contains(directory.nodeOf(sender())) =>
      val node = directory.nodeOf(sender())
      if (debug) println(s"Supervisor receives End from $node (second case)")
      self ! Close
      goto(EndFirstStage) using status.end(node, bundle, consumedTasks, taskInProgress)

    // When an agent restarts
    case Event(Restart, status) =>
      val node = directory.nodeOf(sender())
      stay() using status.restart(node)

    // The closure of the stage is deprecated and so ignored
    case Event(Close, status) =>
      stay() using status

    // The answers are deprecated and so dropped
    case Event(Answer(_,_,_,_), status) =>
      stay() using status

    //An obsolete WorkDone is ignored
    case Event(WorkDone(_), status) =>
      stay() using status

    //An obsolete message is ignored
    case Event(HasConsumedTasksDuringTransition, status) =>
      stay() using status
  }

  /**
    * Or the supervisor is in the end state for the first stage
    * and it is waiting for the metrics of the agents to initiate
    * the next stage or to end the process
    */
  when(EndFirstStage) {
    // The restart of an agent leads the supervisor to remove it from
    // the desperate node and return back to the running state
    case Event(Restart, status) =>
      val node = directory.nodeOf(sender())
      val updatedStatus = status.resetLastQueryId
      goto(RunningFirstStage) using updatedStatus.restart(node)

    // The end of the stage leads the supervisor to ask for some metrics
    case Event(Close, status) =>
      if (debug) println(s"Supervisor: all the agent ends the first stage")
      val query = Query()
      directory.allActors().foreach(a => a ! query)
      val updatedStatus = status.setLastQueryId(query.msgId)
      stay() using updatedStatus

    // The answer with some metrics of an agent leads the supervisor to change status
    case Event(Answer(completedTasks, metrics, isBundleEmpty, replyId), status)  if status.lastQueryId.isDefined && replyId == status.lastQueryId.get &&
      (status.answerNodes.size < stap.m -1 || status.answerNodes.contains(directory.nodeOf(sender()))) =>
      if (debugMetrics) println(s"Supervisor update metrics: $metrics")
      val updatedStatus = status.updateCompletedTasks(completedTasks, directory.nodeOf(sender()), isBundleEmpty)
      stay() using updatedStatus.updateMetrics(metrics, directory.nodeOf(sender()))

    // The last metrics of an agent leads the supervisor to change status,
    // and it goes to to the second stage if it is defined and there is still tasks to consume
    case Event(Answer(completedTasks, metrics, isBundleEmpty, replyId), status)  if status.lastQueryId.isDefined && replyId == status.lastQueryId.get &&
      (status.answerNodes.size ==  stap.m -1 && ! status.answerNodes.contains(directory.nodeOf(sender()))
      && status.isTwoStageProcess && !status.allocation.areBundlesEmpty) =>
      var updatedStatus = status.updateMetrics(metrics, directory.nodeOf(sender()) )
      if (debugMetrics) println(s"Supervisor updates metrics: $metrics")
      updatedStatus = updatedStatus.updateCompletedTasks(completedTasks, directory.nodeOf(sender()), isBundleEmpty)
      if (debug) println("End of the first stage, supervisor initiates the second stage")
      directory.allActors().foreach(a => a ! PreTrigger(2))
      updatedStatus = updatedStatus.resetLastQueryId
      if (isMonitored) monitor.get ! TriggerSecondStage
      if (debug) println(s"TriggerSecondStage: ${updatedStatus.metrics}")
      goto(FirstToSecond) using updatedStatus.resetStage


    // If there is no defined second stage, the last metrics of an agent leads the supervisor to change status,
    // and to wait for the last tasks to be consumed
    case Event(Answer(completedTasks, metrics, isBundleEmpty, replyId), status)  if status.lastQueryId.isDefined && replyId == status.lastQueryId.get &&
      (status.answerNodes.size ==  stap.m -1 && ! status.answerNodes.contains(directory.nodeOf(sender()))
      && !status.isTwoStageProcess
      && ((status.emptyBundles.size < stap.m -1 || !isBundleEmpty) || (status.emptyBundles.size==stap.m-1 && status.emptyBundles.contains(directory.nodeOf(sender()))))) =>
      var updatedStatus = status.updateMetrics(metrics, directory.nodeOf(sender()))
      updatedStatus = updatedStatus.updateCompletedTasks(completedTasks, directory.nodeOf(sender()), isBundleEmpty)
      if (debugMetrics) println(s"Supervisor update metrics: $metrics")
      if (debug) println("End of the first stage, supervisor waits for the last tasks before killing the agents, completed tasks :\n" +
        updatedStatus.allocation.consumedTasksToString)
      updatedStatus = updatedStatus.resetLastQueryId
      stay() using updatedStatus

    // The last metrics of an agent leads the supervisor to change status,
    // and kill the agents if the bundles are empty
    case Event(Answer(completedTasks, metrics, isBundleEmpty, replyId), status)  if status.lastQueryId.isDefined && replyId == status.lastQueryId.get &&
      (isBundleEmpty && status.answerNodes.size ==  stap.m -1 && ! status.answerNodes.contains(directory.nodeOf(sender()))
      && isBundleEmpty
        && ((status.emptyBundles.size == stap.m -1 && !status.emptyBundles.contains(directory.nodeOf(sender())))||status.emptyBundles.size==stap.m)) =>
      var updatedStatus = status.updateMetrics(metrics, directory.nodeOf(sender()))
      if (debugMetrics) println(s"Supervisor update metrics: $metrics")
      updatedStatus = updatedStatus.updateCompletedTasks(completedTasks, directory.nodeOf(sender()), isBundleEmpty)
      if (debug) println("End of the first stage, supervisor kills the agents")
      directory.allActors().foreach{a =>
        if (trace) writer.println(s"Supervisor -> ${directory.nodeOf(a)}: Kill")
        a ! Kill
      }
      updatedStatus = updatedStatus.resetLastQueryId
      if (isMonitored) monitor.get ! EndNegotiation
      if (debug) println(s"End negotiation: ${updatedStatus.metrics}")
      goto(TransitionalEnd) using updatedStatus

    /**
      * The supervisor is notified that the tasks of the last working agent have been consumed, 
      * so it updates its status and kills the agent
      */
    case Event(WorkDone(completedTasks), status) if !status.isTwoStageProcess &&
    (status.emptyBundles.size == stap.m-1 && !status.emptyBundles.contains(directory.nodeOf(sender()))) =>
      val updatedStatus = status.updateWorkFreeNodes(completedTasks, directory.nodeOf(sender()))
      if (debug) println("End of the first stage, supervisor kills the agents")
      directory.allActors().foreach(a => a ! Kill)
      goto(TransitionalEnd) using updatedStatus

    /**
      * The supervisor is notified that the last tasks of the sender have been consumed but there are still non-empty bundles, 
      * so it updates its status
      */
    case Event(WorkDone(completedTasks), status) if !status.isTwoStageProcess &&
    (status.emptyBundles.size < stap.m-1 && !status.emptyBundles.contains(directory.nodeOf(sender()))) =>
      val updatedStatus = status.updateCompletedTasks(completedTasks, directory.nodeOf(sender()), isBundleEmpty = true)
      stay() using updatedStatus

    /**
      * In other cases, the WorkDone is ignored
      */
    case Event(WorkDone(_), status) =>
      stay() using status

     // The deprecated answers are ignored
    case Event(Answer(_,_,_,replyId), status)  if status.lastQueryId.isEmpty || replyId != status.lastQueryId.get =>
      stay() using status

    // The deprecated answers are ignored
    case Event(Answer(_,_,isBundleEmpty,_), status) =>
      println(s"/!\\ Supervisor received an unexpected answer from ${directory.nodeOf(sender())} in EndFirstStage\n" +
        s"answerNodes : ${status.answerNodes} ; isBundleEmpty : $isBundleEmpty ; list of empty bundles : ${status.emptyBundles}"
      )
      stay() using status
  }

  /**
    * Or the supervisor has initiated a second stage and
    * it is waiting for the agents being ready for the next stage
    */
  when(FirstToSecond) {
    case Event(ReadyForNextStage, status) if status.sumReadyForNext < stap.m -1 =>
      stay() using status.incrementNbReadyForNextStage

    case Event(ReadyForNextStage, status) if status.sumReadyForNext == stap.m -1 =>
      directory.allActors().foreach(a => a ! Trigger(2))
      if (debug || debugStage) println("START NEW SECOND STAGE")
      numberOfSecondStages += 1
      goto(RunningSecondStage) using status.resetForNextStage

    // Ignore redundant answer
    case Event(Answer(_,_,_,_), status) =>
      stay() using status

    //Ignore obsolete Restart
    case Event(Restart, status) =>
      stay() using status

    // Ignore redundant close
    case Event(Close, status) =>
      stay() using status

    // Ignore obsolete End
    case Event(End(_, _, _), status) =>
      stay() using status

    // Ignore late WorkDone
    case Event(WorkDone(_), status) =>
      stay() using status

    // An obsolete message is ignored
    case Event(HasConsumedTasksDuringTransition, status) =>
      stay() using status
  }

  /**
    * Or the supervisor is in the running second stage state and
    * it is waiting for the agent ends
    */
  when(RunningSecondStage) {
    // When an agent ends, the supervisor updates its status
    case Event(End(bundle, consumedTasks, taskInProgress), status)
      if status.desperateNodes.size < stap.m -1
        || status.desperateNodes.contains(directory.nodeOf(sender())) =>
      val node = directory.nodeOf(sender())
      stay() using status.end(node, bundle, consumedTasks, taskInProgress)

    // When all the agents end, close the stage of all the agents and
    // move to EndSecondStage
    case Event(End(bundle, consumedTasks, taskInProgress), status)
      if status.desperateNodes.size == stap.m -1
        && ! status.desperateNodes.contains(directory.nodeOf(sender())) =>
      val node = directory.nodeOf(sender())
      self ! Close
      goto(EndSecondStage) using status.end(node, bundle, consumedTasks, taskInProgress)

    // When an agent restarts
    case Event(Restart, status) =>
      val node = directory.nodeOf(sender())
      stay() using status.restart(node)

    // The end of the stage in the running state is ignored
    case Event(Close, status) =>
      stay() using status

    // The answers are dropped
    case Event(Answer(_,_,_,_), status) =>
      stay() using status

    case Event(WorkDone(_), status) =>
      stay() using status

    //An obsolete message is ignored
    case Event(HasConsumedTasksDuringTransition, status) =>
      stay() using status

  }

  /**
    * Or the supervisor is in the end state for the second stage
    * and it is waiting for the metrics of the agents to
    * either initiate the next first stage
    * or end the process
    */
  when(EndSecondStage) {
    // The restart of an agent leads the supervisor to remove it from
    // the desperate node and return back to the running state
    case Event(Restart, status) =>
      val node = directory.nodeOf(sender())
      val updatedStatus = status.resetLastQueryId
      goto(RunningSecondStage) using updatedStatus.restart(node)

    // The closure of the stage leads the supervisor to ask for some metrics
    case Event(Close, status) =>
      if (debug) println(s"Supervisor: all the agents end")
      val query = Query()
      directory.allActors().foreach(a => a ! query)
      val updatedStatus = status.setLastQueryId(query.msgId)
      stay() using updatedStatus

    // The answer with some metrics of an agent leads the supervisor to change status
    case Event(Answer(_, metrics, _, replyId), status)  if status.lastQueryId.isDefined && replyId == status.lastQueryId.get &&
      (status.answerNodes.size < stap.m -1  || status.answerNodes.contains(directory.nodeOf(sender()))) =>
      if (debugMetrics) println(s"Supervisor update metrics: $metrics")
      stay() using status.updateMetrics(metrics, directory.nodeOf(sender()))

    // The last answer with some metrics of an agent leads the supervisor to change status,
    // and to go to first stage
    case Event(Answer(completedTasks, metrics,isBundleEmpty, replyId), status)  if status.lastQueryId.isDefined && replyId == status.lastQueryId.get &&
      (status.answerNodes.size ==  stap.m -1 && ! status.answerNodes.contains(directory.nodeOf(sender()))
      &&
      ((status.emptyBundles.size < stap.m -1 || !isBundleEmpty) || (status.emptyBundles.size==stap.m-1 && status.emptyBundles.contains(directory.nodeOf(sender()))))) =>
      var updatedStatus = status.updateMetrics(metrics, directory.nodeOf(sender()))
      updatedStatus = updatedStatus.updateCompletedTasks(completedTasks, directory.nodeOf(sender()), isBundleEmpty)
      if (debugMetrics) println(s"Supervisor update metrics: $metrics")
      if (debug) println("End of the second stage, supervisor initiates the first stage")
      directory.allActors().foreach(a => a ! PreTrigger(1))
      if (isMonitored) monitor.get ! TriggerFirstStage
      if (debug) println(s"TriggerFirstStage: ${updatedStatus.metrics}")
      goto(SecondToFirst) using updatedStatus.resetStage

    // The last answer with some metrics of an agent leads the supervisor to change status,
    // and kill the agents if the bundles are empty
    case Event(Answer(completedTasks, metrics, isBundleEmpty, replyId), status)  if status.lastQueryId.isDefined && replyId == status.lastQueryId.get &&
      (status.answerNodes.size ==  stap.m -1 && ! status.answerNodes.contains(directory.nodeOf(sender()))
      &&
      isBundleEmpty && ((status.emptyBundles.size == stap.m -1 && !status.emptyBundles.contains(directory.nodeOf(sender())))||status.emptyBundles.size==stap.m)) =>
      var updatedStatus = status.updateMetrics(metrics, directory.nodeOf(sender()))
      updatedStatus = updatedStatus.updateCompletedTasks(completedTasks, directory.nodeOf(sender()), isBundleEmpty)
      if (debugMetrics) println(s"Supervisor update metrics: $metrics")
      if (debug) println("End of the second stage, supervisor kills the agents")
      directory.allActors().foreach(a => a ! Kill)
      updatedStatus = updatedStatus.resetLastQueryId
      goto(TransitionalEnd) using updatedStatus

    // The deprecated answers are ignored
    case Event(Answer(_,_,_,replyId), status)  if status.lastQueryId.isEmpty || replyId != status.lastQueryId.get =>
      stay() using status

    // The deprecated answers are ignored
    case Event(Answer(_,_,isBundleEmpty,_), status) =>
      println(s"/!\\ Supervisor received an unexpected answer from ${directory.nodeOf(sender())} in EndSecondStage\n" +
        s"answerNodes : ${status.answerNodes} ; isBundleEmpty : $isBundleEmpty ; list of empty bundles : ${status.emptyBundles}"
      )
      stay() using status

    /**
      * In other cases, the WorkDone is ignored
      */
    case Event(WorkDone(completedTasks), status) =>
      if (debug) println(s"Supervisor received WorkDone from ${directory.nodeOf(sender())} in EndSecondStage")
      val updatedStatus = status.updateWorkFreeNodes(completedTasks, directory.nodeOf(sender()))
      stay() using updatedStatus

  }

  /**
    * Or the supervisor has initiated the next first stage and
    * it is waiting for the agents being ready
    */
  when(SecondToFirst) {
    case Event(ReadyForNextStage, status) if status.sumReadyForNext < stap.m -1 =>
      stay() using status.incrementNbReadyForNextStage

    case Event(ReadyForNextStage, status) if status.sumReadyForNext == stap.m -1 
      && (status.metrics.nbReallocationsCurrentStage > 0 || hasConsumptionsDuringTransition )=>
      directory.allActors().foreach(a => a ! Trigger(1))
      if (debug || debugStage) println("START NEW FIRST STAGE")
      hasConsumptionsDuringTransition = false //reset before next stage
      waitingForNextStage = false
      numberOfFirstStages += 1
      goto(RunningFirstStage) using status.resetForNextStage

    case Event(ReadyForNextStage, status) if status.sumReadyForNext == stap.m -1 
      && status.metrics.nbReallocationsCurrentStage == 0 && !hasConsumptionsDuringTransition =>
      //waiting for a consumption to occur
      if (debug) println("Supervisor is waiting for a consumption to occur before starting a new phase")
      waitingForNextStage = true
      stay() using status.incrementNbReadyForNextStage

    case Event(WorkDone(completedTasks), status) if waitingForNextStage =>
      if (debug) println(s"Supervisor received WorkDone from ${directory.nodeOf(sender())} in SecondToFirst state")
      val updatedStatus = status.updateWorkFreeNodes(completedTasks, directory.nodeOf(sender()))
      waitingForNextStage = false //reset for new phase
      directory.allActors().foreach(a => a ! Trigger(1))
      if (debug || debugStage) println("START NEW FIRST STAGE")
      numberOfFirstStages += 1
      goto(RunningFirstStage) using updatedStatus.resetForNextStage

    case Event(WorkDone(completedTasks), status) if !waitingForNextStage =>
      if (debug) println(s"Supervisor received WorkDone from ${directory.nodeOf(sender())} in SecondToFirst state")
      val updatedStatus = status.updateWorkFreeNodes(completedTasks, directory.nodeOf(sender()))
      stay() using updatedStatus

    case Event(HasConsumedTasksDuringTransition, status) if waitingForNextStage =>
      if (debug) println(s"Supervisor is informed that ${directory.nodeOf(sender())} consumed a task during transition")
      waitingForNextStage = false //reset for new phase
      directory.allActors().foreach(a => a ! Trigger(1))
      if (debug || debugStage) println("START NEW FIRST STAGE")
      numberOfFirstStages += 1
      goto(RunningFirstStage) using status.resetForNextStage

    case Event(HasConsumedTasksDuringTransition, status) if !waitingForNextStage =>
      if (debug) println(s"Supervisor is informed that ${directory.nodeOf(sender())} consumed a task during transition")
      hasConsumptionsDuringTransition = true
      stay() using status

    // Ignore redundant answer
    case Event(Answer(_,_,_,_), status) =>
      stay() using status

    //Ignore obsolete Restart
    case Event(Restart, status) =>
      stay() using status

    // Ignore redundant close
    case Event(Close, status) =>
      stay() using status

    //Ignore obsolete End
    case Event(End(_, _, _), status) =>
      stay() using status

  }

  /**
    * Or the supervisor wants to end the process and it is
    * waiting for the agents' response to the kill message
    */
  when(TransitionalEnd) {
    // The reply of an agent leads the supervisor to change status
    case Event(Finished(completedTasks), status)  if status.sumKilled < stap.m -1 =>
      val updatedStatus = status.updateWorkFreeNodes(completedTasks, directory.nodeOf(sender()))
      if (debug) println(s"Supervisor collects completed tasks from ${directory.nodeOf(sender())} :${updatedStatus.consumedBundles}")
      stay() using updatedStatus.incrementNbKilled

    // The last reply of an agent leads the supervisor to change status
    // and send to the balancer the outcome
    case Event(Finished(completedTasks), status)  if status.sumKilled ==  stap.m -1 =>
      val updatedStatus = status.updateWorkFreeNodes(completedTasks, directory.nodeOf(sender()))
      if (trace) writer.println("@enduml\n")
      if (debug) println("Supervisor sends outcome")
      balancer ! Outcome(allocation = updatedStatus.allocation, metrics = updatedStatus.metrics, nbFirstStages = numberOfFirstStages, nbSecondStages = numberOfSecondStages)
      if (isMonitored) monitor.get ! Stop
      writer.close()
      context.stop(self) // stops the Supervisor
      goto(Terminal) using updatedStatus

    // Ignore redundant answer
    case Event(Answer(_,_,_,_), status) =>
      stay() using status

    case Event(WorkDone(_), status) =>
      stay() using status

    // Ignore redundant Close
    case Event(Close, status) =>
      stay() using status

    // Ignore obsolete restart
    case Event(Restart, status) =>
      stay() using status
  }

  when(Terminal) {
    case Event(_, status) =>
      if (debug) println("Supervisor ends")
      stay() using status
  }

  /**
    * Whatever the state is
    **/
  whenUnhandled {
    // The information that the interactions must be traced
    case Event(Trace, status) =>
      trace = true
      outputStream = new FileOutputStream(fileName)//OutputStream.nullOutputStream
      outputStreamWriter = new OutputStreamWriter(outputStream)
      writer = new PrintWriter(outputStreamWriter, true) // new PrintWriter(System.out, true)
      writer.println("'1- Download plantuml at https://plantuml.com")
      writer.println("'2 - Run the following command line")
      writer.println(s"'java -jar plantuml.jar $fileName")
      writer.println("@startuml")
      writer.println("!pragma teoz true")
      writer.println("skinparam monochrome true")
      //alternatively writer.println("skinparam handwritten true")
      writer.println("hide footbox")
      writer.println("participant Supervisor")
      writer.flush()
      for (node <- stap.ds.computingNodes)
        writer.println(s"participant $node")
        directory.allActors().foreach(_ ! Trace(writer))
      stay() using status

    // 4JobRelease: when the allocation is updated
    case Event(Update(additionalAllocation), status) =>
      println(s"Supervisor has a new allocation $additionalAllocation")
      directory.allActors().foreach { actor: ActorRef => // For each worker
        val node = directory.nodeOf(actor)
        val taskList: List[Task] = additionalAllocation.bundle(node)
        if (taskList.nonEmpty) {
          if (trace) writer.println(s"Supervisor -> $node : Give($taskList)")
          actor ! Give(taskList)
        }
      }
      stay() using status.updateAllocation(additionalAllocation)

    // The other messages are not expected
    case Event(msg @ _, status) =>
      throw new RuntimeException(s"Supervisor>ERROR: Supervisor in state $stateName receives the message $msg from ${directory.nodeOf(sender())} which was not expected")
      stay() using status
  }

  /**
    * Associates actions with a transition instead of with a state and even, e.g. debugging
    */
  onTransition {
    case s1 -> s2 =>
      if (debugState) println(s"Supervisor>Supervisor moves from the state $s1 to the state $s2")
  }

  // Finally triggering it up using initialize,
  // which performs the transition into the initial state and sets up timers (if required).
  initialize()
}

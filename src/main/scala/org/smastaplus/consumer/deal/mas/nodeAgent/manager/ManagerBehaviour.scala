// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, Anne-Cécile CARON, 2022, 2023
package org.smastaplus.consumer.deal.mas.nodeAgent.manager

import org.smastaplus.core._
import org.smastaplus.provisioning._
import org.smastaplus.consumer.deal.mas._
import org.smastaplus.utils.Timer

import org.joda.time.LocalDateTime
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.{ActorRef, FSM}
import akka.pattern.ask
import akka.util.Timeout

/**
 * Manager agent with a concrete behaviour
 * @param stap instance
 *  @param node which is managed
 * @param negotiator is the corresponding actor
 *  @param worker is the corresponding actor
 * @param monitor if any
 */
class ManagerBehaviour(stap: STAP,
                       node: ComputingNode,
                       negotiator : ActorRef,
                       worker : ActorRef,
                       monitor: Option[ActorRef])
  extends Manager(stap, node, negotiator, monitor)
    with FSM[ManagerState, ManagerMind]{

  debug = false
  implicit val timeout: akka.util.Timeout = Timeout(6000 minutes)
  private var waitingToBeKilled = false

  /**
   * Asks the remaining work of the task in progress to the worker and waits for the answer
   * and update the mind
   * Synchronous message
   * @param request: the message to send to the manager
   * @param mind: the current mind of the manager
   * @return the mind updated with the new bundle
   */
  private def askWorker(request: Message, mind: ManagerMind) : ManagerMind = {
    val future = worker ? request
    val remaining : Option[Double] = try {
      Some(Await.result(future, timeout.duration).asInstanceOf[RemainingCost].cost)
    } catch {
      case _: Exception => None
    }
    if (remaining.isDefined){
      val updatedMind = mind.updateRemainingWork(remaining.get) //updating the remaining work
      return updatedMind
    }else{
      // do nothing (should not happen)
    }
    mind
  }

  /**
   * Initially the manager
   * - is active
   * - assumes all the computing nodes are active
   * - has no task in the bundle,
   * - and in the first stage
   */
  startWith(
    Initial,
    new ManagerMind(node,
      peers = stap.ds.computingNodes.toSet - node,
      usage =  Map[ComputingNode, ProvisioningUsage]() ++ stap.ds.computingNodes.view.map(i => i -> Active),
      stap,
      taskSet = Set.empty[Task],
      isFirstStage = true)
  )

  /**
   * Either the manager is in the initial state where
   * it is waiting for its initial bundle to become Running
   */
  when(Initial) {
    // An enlightenment of the agents about its acquaintances
    case Event(Light(directory), mind)  =>
      supervisor = sender()
      this.directory = directory
      sender() ! Ready
      stay() using mind

    // A gift of a bundle to the agent which takes/sorts it,
    // broadcasts insight and becomes running
    //if the bundle is not empty, the prior task is given to the worker
    case Event(Give(bundle), mind)  if bundle.nonEmpty =>
      supervisor = sender()
      if (debug) println(s"Manager $node updates its bundle")
      var updatedMind = mind.initBundle(bundle)
      //give prior task to the worker
      val nextTask = updatedMind.getBundle.nextTaskToExecute
      updatedMind = updatedMind.setTaskInProgress(Some(nextTask))
      if (debug) println(s"Manager $node gives task $nextTask to the worker")
      worker ! Perform(nextTask)
      if (debug) println(s"Manager $node commission negotiator")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle, Some(nextTask), Some(stap.cost(nextTask, node)))
      if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
      broadcast(updatedMind.insight(LocalDateTime.now()))
      if (monitor.isDefined) {
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight(),silent = false)
      }
      goto(Running) using updatedMind

    //if the bundle in empty, the manager becomes inactive
    case Event(Give(bundle), mind) if bundle.isEmpty =>
      supervisor = sender()
      if (debug) println(s"Manager $node updates its bundle")
      val updatedMind = mind.initBundle(bundle)
      if (debug) println(s"Manager $node commission negotiator")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle, None, None)
      if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
      broadcast(updatedMind.insight(LocalDateTime.now()))
      if (monitor.isDefined) {
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight(),silent = false)
      }
      if (debug) println(s"Manager $node moving to WorkFree from Initial")
      goto(WorkFree) using updatedMind
  }

  /**
   * Or the manager is running
   */
  when(Running) {
    // A message from the negotiator informing it has no longer reallocation to propose in the current stage
    case Event(FinishedStage(newMetrics), mind)  =>
      metrics = newMetrics
      if (debug) println(s"Manager $node sends an End message to the supervisor the insight ${mind.insight()}")
      supervisor ! End(mind.sortedTasks, mind.completedTasks, mind.getTaskInProgress)
      if (trace && debug) writer.println(s"Manager$node goto EndStage")
      goto(EndStage) using mind

    // A done message from the worker and the consumed task was not the last one
    case Event(Done(completionDate), mind) if !mind.getBundle.isEmpty =>
      val completedTask = mind.getTaskInProgress
      if (debug) println(s"Manager $node in Running is notified that $completedTask has been consumed")
      var updatedMind = mind.updateCompletedTasks(mind.taskInProgress.get, completionDate)
      if (debug) println(s"Tasks consumed by $node : ${updatedMind.completedTasks}")
      val nextTask = updatedMind.getBundle.nextTaskToExecute
      updatedMind = updatedMind.setTaskInProgress(Some(nextTask))
      if (debug) println(s"Manager $node gives task $nextTask to the worker")
      worker ! Perform(nextTask)
      if (debug) println(s"Manager $node commission negotiator")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle, Some(nextTask), Some(stap.cost(nextTask, node)))
      if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
      broadcast(updatedMind.insight(LocalDateTime.now()))
      if (monitor.isDefined) {
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight())
        monitor.get ! NotifyDone(node, completedTask.get, completionDate)
      }
      stay() using updatedMind

    // A done message from the worker and the consumed task was not the last one
    case Event(Done(completionDate), mind) if mind.getBundle.isEmpty =>
      val completedTask = mind.getTaskInProgress
      if (debug) println(s"Manager $node in Running is notified that ${mind.getTaskInProgress} has been consumed (the bundle is empty)")
      var updatedMind = mind.updateCompletedTasks(mind.taskInProgress.get, completionDate)
      updatedMind = updatedMind.resetTaskInProgress
      if (debug) println(s"Tasks consumed by $node : ${updatedMind.completedTasks}")
      if (debug) println(s"Manager $node commission negotiator")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle, None, None)
      if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
      broadcast(updatedMind.insight(LocalDateTime.now()))
      if (debug) println(s"Manager $node moving to WorkFree from Running")
      if (monitor.isDefined) {
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight())
        monitor.get ! NotifyDone(node, completedTask.get, completionDate)
        updatedMind.bundleWriter("monitoring").write()
      }
      goto(WorkFree) using updatedMind

    // A message from the negotiator asking the manager to add a task in the bundle
    case Event(Add(tasks), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      if (debug) println(s"Manager $node adds the tasks $tasks in the bundle")
      var updatedMind = mind.addTasks(tasks)
      //ask the remaining work to the worker
      updatedMind = askWorker(QueryRemainingWork(), updatedMind)
      sender() ! GiveUpdatedBundle(updatedMind.getBundle, updatedMind.getTaskInProgress, updatedMind.getRemainingWork, newTimestamp)
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
      if (debug) println(s"Manager $node new bundle : ${updatedMind.getBundle}")
      multicast(updatedMind.peers,  Inform(newInsight)) //broadcast(updatedMind.insight)
      if (monitor.isDefined){
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight(),silent = false)
      }
      stay() using updatedMind

    // 4JobRelease: A message from the supervisor asking the manager to add tasks in the bundle
    case Event(Give(tasks), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      if (debug) println(s"Manager $node adds the tasks $tasks in the bundle")
      var updatedMind = mind.addTasks(tasks)
      //ask the remaining work to the worker
      updatedMind = askWorker(QueryRemainingWork(), updatedMind)
      if (debug) println(s"Manager $node update bundle ${updatedMind.getBundle}")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle, updatedMind.getTaskInProgress, updatedMind.getRemainingWork, newTimestamp)
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
      if (debug) println(s"Manager $node new bundle : ${updatedMind.getBundle}")
      multicast(updatedMind.peers, Inform(newInsight))
      if (monitor.isDefined) {
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight(), silent = false)
      }
      stay() using updatedMind

    // A message from the negotiator asking the manager to remove tasks from the bundle
    case Event(Remove(tasks), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      var success = true
      var updatedMind = try {
        if (debug) println(s"Manager $node removes the tasks $tasks in the bundle")
        mind.removeTasks(tasks)
      } catch {
        case _: RuntimeException =>
          if (debug) println(s"Manager $node can't remove the tasks $tasks in the bundle")
          success = false
          mind
      }
      //ask the remaining work to the worker
      updatedMind = askWorker(QueryRemainingWork(), updatedMind)
      sender() ! UpdateBundleAfterTransaction(success, updatedMind.getBundle, updatedMind.getTaskInProgress, updatedMind.getRemainingWork, newTimestamp)
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
      if (debug) println(s"Manager $node new bundle : ${updatedMind.getBundle}")
      multicast(updatedMind.peers,  Inform(newInsight))
      if (monitor.isDefined) {
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight(),silent = false)
      }
      stay() using updatedMind

    // A message from the negotiator asking the manager to replace a task in the bundle
    case Event(Replace(endowment, counterpart), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      var success = true
      var updatedMind = try {
        if (debug) println(s"Manager $node replaces the tasks $endowment by the tasks $counterpart in the bundle")
        mind.replaceTasks(endowment, counterpart)
      } catch {
        case _: RuntimeException =>
          if (debug) println(s"Manager $node can't remove the tasks $counterpart in the bundle")
          success = false
          mind
      }
      // Ask the remaining work to the worker
      updatedMind = askWorker(QueryRemainingWork(), updatedMind)
      sender() ! UpdateBundleAfterTransaction(success, updatedMind.getBundle, updatedMind.getTaskInProgress, updatedMind.getRemainingWork, newTimestamp)
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
      multicast(updatedMind.peers,  Inform(newInsight)) //broadcast(updatedMind.insight)
      if (monitor.isDefined){
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight(),silent = false)
      }
      stay() using updatedMind

    // An obsolete query is ignored
    case Event(Query(_), mind) =>
      stay() using mind

    // A Restart is ignored if already running
    case Event(Restart, mind) =>
      stay() using mind
  }

  /**
   * Or the manager is inactive when there is not task to consume
   */
  when(WorkFree) {
    // A message from the negotiator informing it has no longer reallocation to propose in the current stage
    case Event(FinishedStage(newMetrics), mind)  =>
      metrics = newMetrics
      if (debug) println(s"Manager $node sends an End message to the supervisor with the insight ${mind.insight()}")
      supervisor ! End(mind.sortedTasks, mind.completedTasks, mind.getTaskInProgress)
      if (trace && debug) writer.println(s"Manager$node goto EndStage")
      goto(EndStage) using mind

    // A message from the negotiator asking the manager to add new tasks in the bundle
    case Event(Add(tasks), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      if (debug) println(s"Manager $node adds the tasks $tasks in the bundle")
      var updatedMind = mind.addTasks(tasks)
      //give prior task to the worker
      val nextTask = updatedMind.getBundle.nextTaskToExecute
      updatedMind = updatedMind.setTaskInProgress(Some(nextTask))
      if (debug) println(s"Manager $node gives task $nextTask to the worker")
      worker ! Perform(nextTask)
      if (debug) println(s"Manager $node commission negotiator")
      sender() ! GiveUpdatedBundle(updatedMind.getBundle, Some(nextTask), Some(stap.cost(nextTask, node)))
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
      multicast(updatedMind.peers,  Inform(newInsight))
      if (monitor.isDefined){
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight(), silent = false)
      }
      goto(Running) using updatedMind

    // 4JobRelease: A message from the supervisor asking the manager to add new tasks in the bundle
    case Event(Give(tasks), mind) =>
      val newTimestamp: LocalDateTime = LocalDateTime.now()
      if (debug) println(s"Manager $node adds the tasks $tasks in the bundle")
      var updatedMind = mind.addTasks(tasks)
      //Give prior task to the worker
      val nextTask = updatedMind.getBundle.nextTaskToExecute
      updatedMind = updatedMind.setTaskInProgress(Some(nextTask))
      if (debug) println(s"Manager $node gives task $nextTask to the worker")
      worker ! Perform(nextTask)
      if (debug) println(s"Manager $node commission negotiator")
      if (debug) println(s"Manager $node update bundle ${updatedMind.getBundle}")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle, Some(nextTask), Some(stap.cost(nextTask, node)))
      val newInsight = updatedMind.insight(newTimestamp)
      if (debug) println(s"Manager $node informs the peers with the insight $newInsight")
      multicast(updatedMind.peers, Inform(newInsight))
      if (monitor.isDefined) {
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight(), silent = false)
      }
      goto(Running) using updatedMind

    // A Restart is ignored if already running
    case Event(Restart, mind) =>
      stay() using mind
  }

  /**
   * Or the manager is at the end of a stage
   */
  when(EndStage) {
    //A Restart is ignored if the manager is  waiting to be killed
    case Event(Restart, mind) if waitingToBeKilled=>
      if (debug) println(s"Manager $node ignores Restart from negotiator")
      stay() using mind

    // A Restart from the negotiator when there are tasks to consume
    case Event(Restart, mind) if mind.getBundle.nonEmpty || mind.getTaskInProgress.isDefined =>
      if (debug) println(s"Manager $node sends Restart to the supervisor")
      supervisor ! Restart
      goto(Running) using mind

    // A restart from the negotiator when there is no longer tasks to consume
    case Event(Restart, mind) if mind.getBundle.isEmpty && mind.getTaskInProgress.isEmpty =>
      if (debug) println(s"Manager $node sends Restart to the supervisor")
      supervisor ! Restart
      if (debug) println(s"Manager $node moving to WorkFree from EndStage")
      goto(WorkFree) using mind

    // A done message from the worker and the consumed task was not the last one
    case Event(Done(completionDate), mind) if !mind.getBundle.isEmpty =>
      val completedTask = mind.getTaskInProgress
      if (debug) println(s"Manager $node in EndStage is notified that $completedTask has been consumed")
      var updatedMind = mind.updateCompletedTasks(mind.taskInProgress.get, completionDate)
      val nextTask = updatedMind.getBundle.nextTaskToExecute
      updatedMind = updatedMind.setTaskInProgress(Some(nextTask))
      if (debug) println(s"Manager $node gives task $nextTask to the worker")
      worker ! Perform(nextTask)
      if (!waitingToBeKilled){
        if (debug) println(s"Manager $node commission negotiator")
        negotiator ! GiveUpdatedBundle(updatedMind.getBundle, Some(nextTask), Some(stap.cost(nextTask, node)))
        if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
        broadcast(updatedMind.insight(LocalDateTime.now()))
        if (monitor.isDefined) {
          updatedMind.bundleWriter("monitoring").write()
          monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight())
          monitor.get ! NotifyDone(node, completedTask.get, completionDate)
        }
      }
      stay() using updatedMind

    // A done message from the worker and the consumed task was the last one
    case Event(Done(completionDate), mind) if mind.getBundle.isEmpty =>
      val completedTask = mind.getTaskInProgress
      if (debug) println(s"Manager $node is notified that $completedTask has been consumed (the bundle is empty)")
      var updatedMind = mind.updateCompletedTasks(mind.taskInProgress.get, completionDate)
      updatedMind = updatedMind.resetTaskInProgress
      if (waitingToBeKilled){
        if (debug) println(s"Manager $node sends Finished to the supervisor with completed tasks ${updatedMind.completedTasks}")
        supervisor ! Finished(updatedMind.completedTasks)
      }else{
        if (debug) println(s"Manager $node sends WorkDone to the supervisor with completed tasks ${updatedMind.completedTasks}")
        supervisor ! WorkDone(updatedMind.completedTasks)
        if (debug) println(s"Manager $node commission negotiator")
        negotiator ! GiveUpdatedBundle(updatedMind.getBundle, None, None)
        if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
        broadcast(updatedMind.insight(LocalDateTime.now()))
        if (monitor.isDefined) {
          updatedMind.bundleWriter("monitoring").write()
          monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight())
          monitor.get ! NotifyDone(node, completedTask.get, completionDate)
        }
      }
      stay() using updatedMind

    // The supervisor request the metrics
    case Event(Query(msgId), mind) =>
      if (debug) println(s"Manager $node sends the metrics to the supervisor")
      sender() ! Answer(mind.completedTasks, metrics, mind.getBundle.isEmpty, msgId)
      stay() using mind

    // The supervisor kills the agent if there is no task being processed
    case Event(Kill, mind) if mind.getTaskInProgress.isEmpty =>
      if (debug) println(s"Manager $node sends Finished to the supervisor with completed tasks ${mind.completedTasks}")
      supervisor ! Finished(mind.completedTasks)
      stay() using mind

    // The supervisor kills the agent, the task being processed must end first
    case Event(Kill, mind) if mind.getTaskInProgress.isDefined =>
      waitingToBeKilled = true
      if (debug) println(s"Manager $node waits for ${mind.getTaskInProgress.get} to end before being killed")
      stay() using mind

    // A pre-trigger to initiate the next stage
    case Event(PreTrigger(_), mind) =>
      if (debug) println(s"Manager $node sends ReadyForNextStage to the negotiator and the supervisor")
      supervisor ! ReadyForNextStage
      negotiator ! ReadyForNextStage
      if (trace && debug) writer.println(s"Manager $node goto TransitionState")
      goto(TransitionState) using mind

    //a FinishedStage message is ignored
    case Event(FinishedStage(_), mind) =>
      stay() using mind
  }

  /**
   * Or the manager is in transition
   */
  when(TransitionState) {

    case Event(Trigger(_), mind) if !mind.getBundle.isEmpty || mind.getTaskInProgress.isDefined =>
      if (debug) println(s"Manager $node sends StartStage to the negotiator with the bundle ${mind.getBundle}")
      negotiator ! StartStage
      GiveUpdatedBundle(mind.getBundle, mind.getTaskInProgress, mind.getRemainingWork)
      broadcast(mind.insight(LocalDateTime.now()))
      if (monitor.isDefined) {
        mind.bundleWriter("monitoring").write()
        monitor.get ! Notify(mind.me, mind.getCurrentTasks, mind.insight())
      }
      goto(Running) using mind

    case Event(Trigger(_), mind) if mind.getBundle.isEmpty && mind.getTaskInProgress.isEmpty =>
      if (debug) println(s"Manager $node sends StartStage to the negotiator with the bundle ${mind.getBundle}")
      negotiator ! StartStage
      GiveUpdatedBundle(mind.getBundle, mind.getTaskInProgress, mind.getRemainingWork)
      broadcast(mind.insight(LocalDateTime.now()))
      if (monitor.isDefined) {
        mind.bundleWriter("monitoring").write()
        monitor.get ! Notify(mind.me, mind.getCurrentTasks, mind.insight())
      }
      goto(WorkFree) using mind

    case Event(Done(completionDate), mind) if !mind.getBundle.isEmpty =>
      val completedTask = mind.getTaskInProgress
      if (trace) writer.println(s"$node -> $node : Done(${completedTask.get})")
      if (debug) println(s"Manager $node in EndStage is notified that $completedTask has been consumed")
      supervisor ! HasConsumedTasksDuringTransition
      var updatedMind = mind.updateCompletedTasks(mind.taskInProgress.get, completionDate)
      val nextTask = updatedMind.getBundle.nextTaskToExecute
      updatedMind = updatedMind.setTaskInProgress(Some(nextTask))
      if (debug) println(s"Manager $node gives task $nextTask to the worker")
      worker ! Perform(nextTask)
      if (debug) println(s"Manager $node commission negotiator")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle, Some(nextTask), Some(stap.cost(nextTask, node)))
      if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
      broadcast(updatedMind.insight(LocalDateTime.now()))
      if (monitor.isDefined) {
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight())
        monitor.get ! NotifyDone(node, completedTask.get, completionDate)
      }
      stay() using updatedMind

    case Event(Done(completionDate), mind) if mind.getBundle.isEmpty =>
        val completedTask = mind.getTaskInProgress
      if (trace) writer.println(s"$node -> $node : Done(${completedTask.get})")
      if (debug) println(s"Manager $node is notified that ${mind.getTaskInProgress} has been consumed (the bundle is empty)")
      var updatedMind = mind.updateCompletedTasks(mind.taskInProgress.get, completionDate)
      updatedMind = updatedMind.resetTaskInProgress
      if (debug) println(s"Manager $node sends WorkDone to the supervisor with completed tasks ${updatedMind.completedTasks}")
      supervisor ! WorkDone(updatedMind.completedTasks)
      if (debug) println(s"Manager $node commission negotiator")
      negotiator ! GiveUpdatedBundle(updatedMind.getBundle, None, None)
      if (debug) println(s"Manager $node informs the peers with the insight ${updatedMind.insight()}")
      broadcast(updatedMind.insight(LocalDateTime.now()))
      if (monitor.isDefined){
        updatedMind.bundleWriter("monitoring").write()
        monitor.get ! Notify(updatedMind.me, updatedMind.getCurrentTasks, updatedMind.insight())
        monitor.get ! NotifyDone(node, completedTask.get, completionDate)
      }
      stay() using updatedMind

    // A FinishedStage message is ignored
    case Event(FinishedStage(_), mind) =>
      stay() using mind

    // A Restart is ignored
    case Event(Restart, mind) =>
      stay() using mind
  }

  /**
   * Whatever the state is
   **/
  whenUnhandled {
    // A trace of the messages which are sent is required
    case Event(Trace(writer), mind)  =>
      this.trace = true
      this.writer = writer
      stay() using mind

    case Event(PreTrigger(_), mind) =>
      if (debug) println(s"manager $node receives a PreTrigger in state $stateName after restarted, going to TransitionState")
      supervisor ! ReadyForNextStage
      negotiator ! ReadyForNextStage
      goto(TransitionState) using mind

    // 4JobRelease: Gift are postponed during pause
    case Event(Give(taskList), mind) =>
      self forward Give(taskList)
      stay() using(mind)

    // A deprecated timeout
    case Event(Timer.Timeout, mind) =>
      stay() using mind

    // A query requests some metrics is ignored expected in Pause
    case Event(Query(_), mind) =>
      stay() using mind

    // Other message are not expected
    case Event(msg @ _, _) =>
      throw new RuntimeException(s"ERROR Manager $node was not expected message $msg in state $stateName")
  }

  /**
   * Associates actions with a transition instead of with a state and even, e.g. debugging
   */
  onTransition {
    case s1 -> s2 =>
      if (debugState) println(s"manager $node moves from the state $s1 to the state $s2")
  }

  // Finally Triggering it up using initialize, which performs
  // the transition into the initial state and sets up timers (if required).
  initialize()
}

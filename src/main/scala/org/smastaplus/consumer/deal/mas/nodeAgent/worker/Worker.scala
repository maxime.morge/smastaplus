// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022, 2023
package org.smastaplus.consumer.deal.mas.nodeAgent.worker

import org.smastaplus.core._
import org.smastaplus.consumer.deal.mas.nodeAgent.InternalAgent

import akka.actor.ActorRef

/**
  * Abstract worker agent
  * @param stap instance
  * @param node represented by the agent
  * @param simulated cost
  * @param monitor if any
 */
abstract class Worker(stap: STAP,
                       node: ComputingNode,
                       simulated: SimulatedCost,
                       monitor: Option[ActorRef])
  extends InternalAgent(stap,node, monitor) {

    var manager : ActorRef = _
    var tickInterval = 1000//100

    def simulatedCost(task: Task) : Double = {
      val cost = stap.cost(task, node)
      simulated.simulatedCost(cost, node)
    }
  }

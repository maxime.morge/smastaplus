// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, Anne-Cécile CARON, 2020, 2021, 2022, 2023
package org.smastaplus.consumer.deal.mas

import org.smastaplus.core._
import org.smastaplus.strategy.deal.counterproposal._
import org.smastaplus.strategy.deal.proposal._
import org.smastaplus.consumer.AsynchronousConsumer
import org.smastaplus.consumer.deal.mas.supervisor.Supervisor
import org.smastaplus.utils.serialization.core.AllocationWriter

import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.SupervisorStrategy.Stop
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}

/**
 * Agent-based consumer
 * @param stap instance
 * @param system of actors
 * @param name of the balancer
 * @param rule to adopt for reducing either the localFlowtime or the makespan
  * @param firstProposalStrategy for making proposals in the first stage
  * @param secondProposalStrategy for eventually making proposals in the second stage
  * @param counterProposalStrategy for making counter-proposals in the second stage
  * @param dispatcherId is the name of the dispatcher "akka.actor.default-dispatcher" or "single-thread-dispatcher"
  * @param monitor is true if the completion times are monitored during the solving
  * @param simulated cost with  with a perfect information of the running environment by default
  */
class AsynchronousAgentBasedConsumer(stap: STAP,
                                     val system: ActorSystem,
                                     name : String,
                                     rule: SocialRule,
                                     firstProposalStrategy: ProposalStrategy,
                                     secondProposalStrategy: Option[ProposalStrategy],
                                     counterProposalStrategy: CounterProposalStrategy,
                                     val dispatcherId: String,
                                     monitor: Boolean = false,
                                     simulated: SimulatedCost = new SimulatedCostE()
                        )
  extends AsynchronousConsumer(stap, name, monitor, simulated) {

  var trace = true
  val TIMEOUT_VALUE: FiniteDuration = 6000 minutes // Default timeout of a run
  implicit val timeout: Timeout = Timeout(TIMEOUT_VALUE)

  // Launch a new supervisor
  val supervisor: ActorRef = system.actorOf(Props(classOf[Supervisor], stap, rule,
    firstProposalStrategy, secondProposalStrategy, counterProposalStrategy, simulated, dispatcherId, monitor)
    withDispatcher dispatcherId,
    name = "supervisor")

  /**
   * Initiate the balancer with an allocation, eventually a random one
   */
  override def init(allocation: ExecutedAllocation = ExecutedAllocation.randomAllocation(stap)): ExecutedAllocation = {
    if (debug) println(s"AsynchronousAgentBasedConsumer: initial (eventually random) allocation\n$allocation")
    allocation.setSimulatedCost(simulated)
    if (monitor) {
      val config: Config = ConfigFactory.load()
      val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
        config.getString("path.monitoringDirectory")
      val writer = new AllocationWriter(monitoringDirectoryName + "/allocation.json", allocation, "monitoring")
      writer.write()
    }
    allocation
  }


  override def receive: Receive = {
    case Trace =>
      trace = true

    case Init(allocation) =>
      parent = sender()
      allocation.setSimulatedCost(simulated)
      stap.resetReleaseTime()
      initialLocalityRate = allocation.localAvailabilityRatio
      if (trace) supervisor ! Trace
      init(allocation)
      if (debug) println("AsynchronousAgentBasedConsumer: reallocate")
      supervisor ! Init(allocation)

    case Update(additionalAllocation) =>
      supervisor ! Update(additionalAllocation)

    case Outcome(result, metrics, _, _) =>
      supervisor ! Stop
      if (debug) println(s"AsynchronousAgentBasedConsumer: reallocate ends")
      if (debug) println(s"AsynchronousAgentBasedConsumer: metrics $metrics")
      nbDeals = metrics.sumOfDelegationConfirmations + metrics.sumOfSwapConfirmations
      nbDelegations = metrics.sumOfDelegationConfirmations
      nbSwaps = metrics.sumOfSwapConfirmations
      nbDelegatedTasks = metrics.sumOfDelegatedTasks
      if (debug) println(s"Negotiation successful rate = $nbDeals")
      nbTimeouts = metrics.sumOfTimeouts
      if (debug) println(s"Nb timeout = ${metrics.sumOfTimeouts}")
      if (debug) println(s"Nb proposals = ${metrics.sumOfProposals}")
      if (debug) println(s"Nb delegation confirmations = ${metrics.sumOfDelegationConfirmations}")
      if (debug) println(s"Nb swap confirmations = ${metrics.sumOfSwapConfirmations}")
      if (debug) println(s"Nb delegated tasks = ${metrics.sumOfDelegatedTasks}")
      if (debug) println(s"Nb first stages = $nbFirstStages")
      if (debug) println(s"Nb second stages = $nbSecondStages")
      avgEndowmentSize = metrics.sumOfDelegatedTasks.toDouble / (metrics.sumOfDelegationConfirmations + metrics.sumOfSwapConfirmations).toDouble
      if (avgEndowmentSize.isNaN) avgEndowmentSize = 0.0
      if (debug) println(s"Endowment size = $avgEndowmentSize")
      localRatio = result.localAvailabilityRatio
      parent ! Outcome(result, metrics, nbFirstStages, nbSecondStages)
      // Do not close the system !!!

    // Other message are not expected
    case msg =>
      throw new RuntimeException(s"ERROR AsynchronousAgentBasedConsumer was not expected message $msg ")
  }
}

/**
 * Companion object for testing AgentBasedConsumer
 */
private object AsynchronousAgentBasedConsumer extends App {
  var id: Int = 1
}

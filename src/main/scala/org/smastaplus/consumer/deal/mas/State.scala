package org.smastaplus.consumer.deal.mas

/**
 * Trait representing a state in a FSM
 * for a behaviour
 */
trait State

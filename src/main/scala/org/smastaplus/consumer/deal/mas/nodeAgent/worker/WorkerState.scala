// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022
package org.smastaplus.consumer.deal.mas.nodeAgent.worker

import org.smastaplus.consumer.deal.mas.State

/**
  * Trait representing as state in a FSM
  * for the worker behaviour
  */
trait WorkerState extends State
case object Free extends WorkerState
case object Busy extends WorkerState

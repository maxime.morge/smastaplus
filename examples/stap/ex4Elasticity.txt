m: 6
m2: 6
k: 9
computingNodes: cn1, cn2, cn3, cn4, cn5, cn6
resourceNodes: rn1, rn2, rn3, rn4, rn5, rn6
resources: r1 (5.0), r2 (3.0), r3 (1.0), r4 (4.0), r5 (2.0), r6 (5.0), r7 (2.0), r8 (2.0), r9 (4.0)
r1: rn1, rn4
r2: rn1, rn6
r3: rn3, rn5
r4: rn2, rn4
r5: rn1, rn6
r6: rn3, rn6
r7: rn2, rn5
r8: rn2, rn5
r9: rn3, rn6
acquaintance: (cn3,rn3); (cn1,rn1); (cn2,rn2); (cn4,rn4); (cn6,rn6); (cn5,rn5)
n: 9
l: 3
tasks: t1, t2, t3, t4, t5, t6, t7, t8, t9
jobs: J1, J2, J3
t1: r1
t2: r2
t3: r3
t4: r4
t5: r5
t6: r6
t7: r7
t8: r8
t9: r9
J1: t1, t2, t3
releaseTime: J1 0
J2: t4, t5, t6
releaseTime: J2 0
J3: t7, t8, t9
releaseTime: J3 0